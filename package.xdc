/******************************************************************************
 * FILE PURPOSE: Package specification file 
 ******************************************************************************
 * FILE NAME: package.xdc
 *
 * DESCRIPTION: 
 *  This file contains the package specification for the TraceFramework Driver
 *
 * Copyright (C) 2011-2014, Texas Instruments, Inc.
 *****************************************************************************/
package ti.instrumentation.traceframework[1, 1, 1, 9] {
    module Settings;
}

