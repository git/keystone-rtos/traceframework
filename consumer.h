/**
 *   @file  consumer.h
 *
 *   @brief   
 *      This is a very slim consumer framework library implementation.
 *
 *  ============================================================================
 *  @n   (C) Copyright 2012, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
 
#ifndef __CONSUMER_H__
#define __CONSUMER_H__

#ifdef __cplusplus
extern "C" {
#endif

/* CPPI include files */
#include <ti/instrumentation/traceframework/trace_contract.h>

/** @addtogroup TRACEFRAMEWORK_ENUM
@{
*/

/**
 * @brief Trace framework Producer-Consumer NOTIFY types 
 */
typedef enum {
  TF_CONSUMER_NOTIFY_VIA_REGMASK    = 0x0,
  /**< Notify the consumer task in the application with IPC kind of mechanism */
  TF_CONSUMER_NOTIFY_VIA_LOCCBK     = 0x1,
  /**< notify the task using a local call back function */
  TF_CONSUMER_NOTIFY_NONE           = 0x2,
  /**< no notify method, consumers can have their own method to check for new buffers */
  TF_CONSUMER_NOTIFY_VIA_LOCCBK2    = 0x3
  /**< notify the task using a local call back2 function */  
} tf_notifyTypes_e;

/**
 * @brief  Enumerations showing the consumer status
 */
typedef enum consStatus 
{
  TF_CONSUMER_NO_AVAILABLE_CONTRACT = 0,
  /**< No avilable contract for the consumer */
  TF_CONSUMER_INVALID_ARGUMENTS,
  /**< Invalid arguments are passed for the consumer */
  TF_CONSUMER_SUCCESS,
  /**< Success in consumer operations */
  TF_CONSUMER_FAIL,
  /**< failure in consumer operations */
  TF_CONSUMER_WAIT_ACK,
  /**< Consumer is busy in the operation, waiting for the Ack */
  TF_CONSUMER_ACK_RECIVED,
  /**< Consumer got the ACK from the System */
  TF_CONSUMER_NOTHING_TO_CONSUME,  
  /**< nothing to consume in consumer operations */
  TF_CONSUMER_INVALID_HANDLE,
  /**< Invalid consumer handle is provided */
  TF_CONSUMER_INVALID_CONTRACT_HANDLE
  /**< Invalid contract handle is noticed in consumer instance */  
} tf_consStatus_e;

/**
 * @brief  Enumerations showing the application send information to 
 *         the traceframework library
 */
typedef enum consAppSendRespStatus
{
    TF_CONSUMER_APP_SEND_OK_WAIT_ACK,
  /**< Application sent the passed buffer succesfully and frees up the buffer for the library during ACK */        
    TF_CONSUMER_APP_SEND_OK_NO_WAIT_ACK,
  /**< Application sent the passed buffer succesfully and frees up the buffer for the library immediately */            
    TF_CONSUMER_APP_SEND_NOK_RETRY
  /**< Application did not sent the passed buffer succesfully and would like to retry sometime later in time */            
}tf_consAppSendRespStatus_e;

/**
@}
*/

/** @addtogroup TRACEFRAMEWORK_DATASTRUCT
@{
*/

/**
 * @brief   Consumer Handle
 */
typedef void*  tf_cons_HANDLE;

/* re-define for consistancy */
#define  tf_consumer_HANDLE tf_cons_HANDLE

/**
 * @brief Consumer Notify glue structure 
 */
typedef struct consRegNotify
{
  uint32_t                  reg_addr;
  /**< register address e.g, IPCGR regisers for IPC communication*/
  uint32_t                  reg_mask;
  /**< register value to be written */
} tf_consRegNotify_t;

/**
 * @brief Consumer Notify Structure (IPC kind or Local Call back 
 */
typedef struct consNotify
{
  uint32_t              notify_method;
  /**< Notify method for the consumer */
  union 
  {
    tf_consRegNotify_t     notify_reg; 
	/** < notifications are expected to be done via Register masks, like IPCGRx */
    int32_t                 (*notify_callbk)(uint32_t ringbuf_index);
	/** < notifications are expected to be done via local call back function */
    int32_t                 (*notify_callbk2)(tf_consumer_HANDLE loc_handle);	
	/** < notifications are expected to be done via local call back function, added new parameter of local_handle */
  } notifyScheme;
} tf_consNotify_t;

/**
 * @brief Structure with consumer NetCp config 
 */
typedef struct consConfig
{
  tf_contract_HANDLE       contract_handle;
  /**< contract handle to be used by the consumer */
  tf_consNotify_t      consumer_notify;
  /**< consumer notify information structure */
  tf_consAppSendRespStatus_e   (*sendFxn)(uint32_t param, uint8_t* buf, uint32_t len);
  /**< transport send function, sends the buffer using the transport
    * Note: If sendFxn() function implementation decides to call tf_consAck() within, 
    * it shall do this just before the return to avoid the recursion complications */
  uint32_t            param;
  /**< parameter value used by the application */       
} tf_consConfig_t;

/**
@}
*/

/** @addtogroup TRACEFRAMEWORK_FUNCTIONS
@{
*/

/**
 * ============================================================================
 *  @n@b tf_isThereToConsume
 *
 *  @b  brief
 *  @n  Checks the consumer if there is anything to consume
 *
 *  @param[in]  cHandle
 *      consumer handle
 *
 *  @return
 *      Number of buffers to be consumed
 *
 * =============================================================================
 */
extern uint32_t tf_isThereToConsume(tf_consumer_HANDLE cHandle);
extern uint32_t tf_isthere_to_consume (tf_consumer_HANDLE cHandle);

/**
 * ============================================================================
 *  @n@b tf_consProcess
 *
 *  @b  brief
 *  @n  this triggers consumers to look into contract and see if there are any
 *      new buffers available to ship to the transport
 *
 *  @param[in]  cHandle
 *      Pointer to the consumer handle
 *
 *  @return
 *      Status of the operation
 *
 * =============================================================================
 */

extern tf_consStatus_e tf_consProcess(tf_consumer_HANDLE cHandle);

/**
 * ============================================================================
 *  @n@b tf_consAck
 *
 *  @b  brief
 *  @n  This acknowledges the consumer buffer send and triggers the index advance
 *
 *  @param[in]  cHandle
 *      Pointer to the consumer handle
 *
 *  @return
 *      Status of the operation
 *
 * =============================================================================
 */

extern tf_consStatus_e tf_consAck(tf_consumer_HANDLE cHandle);

/**
 * ============================================================================
 *  @n@b tf_consDestroy
 *
 *  @b  brief
 *  @n  this clears the consumer attached to the contract
 *
 *  @param[in]  cHandle
 *      Pointer to the consumer handle
 *
 *  @return
 *      Status of the operation
 *
 * =============================================================================
 */

extern tf_consStatus_e tf_consDestroy(tf_consumer_HANDLE cHandle);

/**
 * ============================================================================
 *  @n@b tf_consCreate
 *
 *  @b  brief
 *  @n  Creates the consumer instance on the trace framework
 *
 *  @param[in]  config
 *      consumer creation configuration
 *
 *  @return
 *      Status of the operation
 *
 * =============================================================================
 */
extern tf_consumer_HANDLE tf_consCreate(tf_consConfig_t   *config);

/**
 * ============================================================================
 *  @n@b tf_consumerForceFreezeProducer
 *
 *  @b  brief
 *  @n  API to force freeze the produer to frozen state
 *      Keeping the in the frozen state for producer triggers the same ring buffer
 *      to be returned for the buffer exchange
 *
 *  @param[in]  cHandle
 *      consumer handle
 *
 *  @return
 *      Status of the operation
 *
 * =============================================================================
 */
extern tf_consStatus_e tf_consumerForceFreezeProducer(tf_cons_HANDLE cHandle);

/**
 * ============================================================================
 *  @n@b tf_consumerGetContractHandle
 *
 *  @b  brief
 *  @n  API to get the contract handle that is associated with the consumer
 *
 *  @param[in]  cHandle
 *      consumer handle
 *
 *  @param[in]  contract_handle
 *      Address of the contract handle to store
 *  @return
 *      Status of the operation
 *
 * =============================================================================
 */
extern tf_consStatus_e tf_consumerGetContractHandle(tf_cons_HANDLE cHandle, tf_contract_HANDLE *contract_handle);


/**
 * ============================================================================
 *  @n@b tf_alignConsumer2FrozenProd
 *
 *  @b  brief
 *  @n  API to align the internal data information on consumers when producer is responded
 *      back as frozen. This API needs to be called first after we detect producer is frozen
 *      before calling tf_consProcess API
 *
 *  @param[in]  cHandle
 *      consumer handle
 *
 *  @return
 *      Status of the operation
 *
 * =============================================================================
 */
extern tf_consStatus_e tf_alignConsumer2FrozenProd(tf_consumer_HANDLE cHandle);
/**
@}
*/

/* Below defines are added for backwards compatibility */
#define	CONS_APP_SEND_OK_WAIT_ACK      TF_CONSUMER_APP_SEND_OK_WAIT_ACK
#define	CONS_APP_SEND_OK_NO_WAIT_ACK   TF_CONSUMER_APP_SEND_OK_NO_WAIT_ACK
#define	CONS_APP_SEND_NOK_RETRY        TF_CONSUMER_APP_SEND_NOK_RETRY
#define	CONSUMER_NO_AVAILABLE_CONTRACT TF_CONSUMER_NO_AVAILABLE_CONTRACT
#define	CONSUMER_INVALID_ARGUMENTS     TF_CONSUMER_INVALID_ARGUMENTS
#define	CONSUMER_SUCCESS               TF_CONSUMER_SUCCESS
#define CONSUMER_FAIL                  TF_CONSUMER_FAIL
#define	CONSUMER_WAIT_ACK              TF_CONSUMER_WAIT_ACK
#define	CONSUMER_ACK_RECIVED           TF_CONSUMER_ACK_RECIVED
#define	CONSUMER_NOTHING_TO_CONSUME    TF_CONSUMER_NOTHING_TO_CONSUME 
#define	NOTIFY_VIA_REGMASK             TF_CONSUMER_NOTIFY_VIA_REGMASK	
#define	NOTIFY_VIA_LOCCBK              TF_CONSUMER_NOTIFY_VIA_LOCCBK
#define	NOTIFY_NONE                    TF_CONSUMER_NOTIFY_NONE

#ifdef __cplusplus
}
#endif

#endif /* __CONSUMER_H__ */


