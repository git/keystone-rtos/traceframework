/**
 *   @file  trace_contract.h
 *
 *   @brief   
 *      This has defintions and implementations for the contract between producers and consumers.
 *
 *  ============================================================================
 *  @n   (C) Copyright 2012, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef CONSPROD_CONTRACT_H_
#define CONSPROD_CONTRACT_H_

#ifdef __cplusplus
extern "C" {
#endif


#include <string.h>
/* OSAL Includes */
#include <ti/instrumentation/traceframework/contract_osal.h>

/** @addtogroup TRACEFRAMEWORK_SYMBOL
@{
*/

/**
 * @brief   contract cache line size
 */
#define  TF_CONTRACT_CACHE_LINEZ             128

/**
 * @brief   contract memory size requirements
 *          contract has 12 cache lines
 */
#define  TF_CONTRACT_SIZE_BYTES                (TF_CONTRACT_CACHE_LINEZ * 12)

/**
 * @brief   Alignment for the tf_contract memory
 */
#define  TF_CONTRACT_BUFS_ALIGN                 128

/**
 * @brief   Ring Buffer Alignment
 */
#define TF_PRODUCER_LOGBUF_ALGIN                128

/**
 * @brief   Maximum number of Ring buffers supported in the ring
 */
#define  TF_MAX_RING_BUFS_SUPPORT               1024


/**
 * @brief   maximum consumers allowed per contract
 */
#define  TF_MAX_CONSUMERS_PERCONTRACT             4

/**
 * @brief   number of bytes allocated for the Out of Band information
 *          Out of band information is really for the application to keep some information in the traceframework contract
 *          and retrive it from either producer/consumers as it is. Traceframework does not alter any information
 *          stored in 
 */

#define  TF_CONTRACT_OOB_SIZE_BYTES             TF_CONTRACT_CACHE_LINEZ

/**
 * @brief   number of bytes allocated for the name of the contract (applicable only for contract version 1)
 *          since contract version 2 does not handle name within traceframework
 */

#define  TF_CONTRACT_NAME_SIZE_BYTES            TF_CONTRACT_CACHE_LINEZ


/**
 *  @defgroup tfCtrlBitMap Trace Framework Control Bit map Definitions
 *  @ingroup TRACEFRAMEWORK_API
 *  @{
 *
 *  @name TraceFramework Control Bit Map Definitions
 *
 *  Bitmap definition of the ctrlBitMap in @ref tf_StartCfg_t. 
 */ 
/*@{*/
/**
 *  @def  tf_CONTRACT_BLK_ADDR_TYPE
 *        Control Info -- 0: The contract block (version 1) base is physical address 
 *                        1: The contract block (version 1) base is virtual  address 
 */
#define tf_CONTRACT_BLK_ADDR_TYPE    0x0001 


/**
@}
*/

/** @addtogroup TRACEFRAMEWORK_ENUM
@{
*/

 /**
  * @brief Contract State added here
  * When contracts are newly created, they are marked Available and
  * as and when the contracts are used between producers and consumer
  * instances, they would be marked as not available.
  */
 typedef enum {
	TF_CONTRACT_AVAILABLE = 0x55555555,
   /**< tf_contract slot is available */	
	TF_CONTRACT_NOT_AVAILABLE = 0x0
   /**< tf_contract slot is not available */	
  }tf_contractState_e;


 /**
  * @brief Contract Versions added here
  * Contract Version number signifies the set of APIs to be used
  * There are few APIs marked in the traceframework functions, which are 
  * deprecated for contract version 1.
  */
 typedef enum {

    TF_CONTRACT_VERSION_2 = 0x66666666,
   /**< trace framework contract version 2 (no named contracts and they can be created as and when needed */
    TF_CONTRACT_VERSION_1 = 0x0,
   /**< trace framework contract version 1 (Legacy mode, all system contracts are named, and they are initialized 
        and created at one time and declared as AVAILABLE) */
    TF_CONTRACT_VERSION_UNKNOWN = 0xf05d
    /**< Unknown Contract Version which Trace Framework can not understand */
  }tf_contractVersion_e;

/**
 * @brief Contract Types added here 
 * Trace framework can support multiple contract types such as
 * 1. Contracts having streaming producer always ( can have 4 consumers maximum)
 * 2. Contracts having stream/freeze producer (supports only one consumer per contract)
 */
typedef enum {
   TF_CONTRACT_DSP_ARM = 0,
   /**< tf_contract between DSP and ARM, where ARM has consumer for DSP producer */
   TF_CONTRACT_STREAM_FREEZE_PRODUCER = TF_CONTRACT_DSP_ARM, 
   /**< tf_contract has stream/freeze producer, this is same as DSP ARM contract, kept for backwards compatibility */	   
   TF_CONTRACT_ARM_ARM = 1
   /**< tf_contract between ARM and ARM, where ARM has both producer and consumers */  
 }tf_contractType_e;

/**
 * @brief Trace Framework Ring Buffer Address type
 */
typedef enum {

   TF_CONTRACT_RING_BUF_PHYSICAL_ADDR = 0,
  /**< trace framework contract ring buffer address configured is physical memory */
   TF_CONTRACT_RING_BUF_NOT_PHYSICAL_ADDR = 1
  /**< trace framework contract ring buffer address configured is not physical memory */   
 }tf_ringBufAddrType_e; 

/**
 * @brief Trace Framework Contract Buffer Address type
 */
typedef enum {

   TF_CONTRACT_BUF_PHYSICAL_ADDR = 0,
  /**< trace framework contract buffer address configured is physical memory */
   TF_CONTRACT_BUF_NOT_PHYSICAL_ADDR = 1
  /**< trace framework contract buffer address configured is not physical memory */    
 }tf_contractBufAddrType_e; 


/**
@}
*/

/** @addtogroup TRACEFRAMEWORK_DATASTRUCT
@{
*/

/**
 * @brief   Contract handle 
 */
typedef void *  tf_contract_HANDLE;

/**
 * @brief Contract Create Configuration Structure
 */
typedef struct tf_contractConfig
{
    void* tf_contract_start_addr;
    /**< Contract start address */
    uint32_t num_contracts;
    /**< Number of Contracts in the system */ 
    void* ringbuf_start_addr;
    /**< ringbuffer start address */
    uint32_t num_ringbufs;
    /**< Number of ring buffers in the tf_contract */ 
    uint32_t ringbuf_size;
    /**< size of each ring buffer */         
	tf_ringBufAddrType_e  ring_base_type; 
	/**< is the ring buffer base address configured physical ? */	
	tf_contractBufAddrType_e contract_base_type; 
	/**< is the contract base virtual or physical */
	tf_contractType_e     contractType;
	/**< type of the contract to be created */	
	int32_t               producer_state;
	/**< Initial state of the producer to begin with */
} tf_contractConfig_t;   


/**
 * @brief trace framework start configuration structure (to support multi-instance and legacy features)
 */
typedef struct {
  void*     instPoolBaseAddr;        /**< Base address of the global shared memory pool from which global 
	                                        LLD instance & channel instance memory is allocated */
  uint32_t  instPoolSize;            /**< Pool size of the global shared memory */												
  uint32_t  ctrlBitMap;              /**< Control bit map @ref tfCtrlBitMap */  												
} tf_StartCfg_t;


/**
 * @brief get trace framework contract information interface structure
 */
typedef struct {
  tf_contractType_e  contractType;  /**< contract type */
  uint32_t  num_consumers; /**< total number of current consumers registered in the contract */
  uint32_t  overrun_count; /**< producer's over run count on the ring buffers */
  uint32_t  rb_wrap_flag;  /**< Contract ring buffer wrap around or not */
  uint32_t  contractState;   /**< tf_contract state */
  uint32_t  contractVersion;  /**< tf_contract version */
  uint32_t  consumerAppRetryCount[TF_MAX_CONSUMERS_PERCONTRACT]; /**< number of times consumer application send function could not consume buffers */
  uint8_t*  ringbuffer_base;    /**< ring buffer base for the tf_contract */
  uint32_t  num_ringbuf;    /**< number of ring buffers */
  uint32_t  ringbuffer_size;     /**< ring buffer size */
  tf_ringBufAddrType_e  ring_base_type; /**< is the ring buffer base address configured virtual Or physical @ref tf_ringBufAddrType_e */  
  uint32_t  producer_index;  /** < producer current index in the ring buffer */
  uint32_t  producer_current_state; /**< producer state */
} tf_contractGetInfo_t;


/**
@}
*/

/** @addtogroup TRACEFRAMEWORK_FUNCTIONS
@{
*/

/**
 * ============================================================================
 *  @n@b tf_contractInit
 *
 *  @b  brief
 *  @n  initializes the tf_contract as avilable for producer/consumer 
 *      @n@b WARNING:  @n - deprecated with contract version TRACE_CONTRACT_VERSION_2
 *      @n This is the very first API that needs to be called by an one time entity
 *      to initialze all the contract memory that system can use and declare them
 *      as available
 *
 *  @param[in]  tf_contract_base
 *      Pointer to the base address of the tf_contract array
 *
 *  @param[in]  num
 *      number of tf_contract buffers
 *
 *  @return
 *      none
 *
 * =============================================================================
 */ 
void tf_contractInit(void* tf_contract_base, uint32_t num);
    
/**
 * ============================================================================
 *  @n@b tf_contractCreate
 *
 *  @b  brief
 *  @n  sets up initial values for producer/consumer
 *      @n@b WARNING:  @n- deprecated with contract version TRACE_CONTRACT_VERSION_2
 *      @n Since the contracts are required to be created sequentially in memory,
 *      there needs to be a protection in the API call to make sure no two 
 *      contracts are created in the same slot. This is accomplished with an
 *      OSAL call out TF_CONTRACT_osalEnter() function, which blocks this function
 *      if being used to create a contract from multi core environment. 
 *
 *  @param[in]  tf_contractConfig
 *      tf_contract configuration structure - needs knowledge about the contract 
 *      base address, number of contracts in the system, ring buffer size, ring 
 *      buffer start address and number of ring buffers.
 *
 *  @param[in]  name
 *      Pointer to the name of the tf_contract
 *
 *  @return
 *      Trace Framework Contract Handle
 *
 * =============================================================================
 */
tf_contract_HANDLE tf_contractCreate(tf_contractConfig_t *tf_contractConfig, char* name);

/**
 * ============================================================================
 *  @n@b tf_newContract
 *
 *  @b  brief
 *  @n  API added to create the contract between producer/consumer instances
 *      This API is applicable for the contract version TRACE_CONTRACT_VERSION_2
 *      Application needs to maintain any names associated with this contract handle 
 *      outside traceframework
 *
 *  @param[in]  tf_contractConfig
 *      tf_contract configuration structure
 *
 *  @return
 *      Contract Handle
 *
 * =============================================================================
 */
tf_contract_HANDLE tf_newContract(tf_contractConfig_t *tf_contractConfig);

    

/**
 * ============================================================================
 *  @n@b tf_contractFind
 *
 *  @b  brief
 *  @n  finds the tf_contract based on the name
 *      @n@b WARNING:  @n - deprecated with contract version TRACE_CONTRACT_VERSION_2
 *
 *  @param[in]  tf_contract_start_addr
 *      tf_contract start address
 *
 *  @param[in]  num_contracts
 *      number of contracts in the system
 *
 *  @param[in]  name
 *      Pointer to the name of the tf_contract
 *
 *  @return
 *      Contract Handle
 *
 * =============================================================================
 */
tf_contract_HANDLE tf_contractFind (void* tf_contract_start_addr, uint32_t num_contracts, char* name); 


/**
 * ============================================================================
 *  @n@b tf_getOverRunCount
 *
 *  @b  brief
 *  @n  Gets the cumulative count of how many times the ring is overrun resulting in same 
 *       last filled buffer
 *
 *      Application Requriments:
 *      Application need to save the current overrun count in some context memory for that 
 *        contract handle
 *      Application needs to compare the latest cumulative overrun count retuned by the 
 *        above API with earlier sampled value to decide the congestion.
 * 
 *  @param[in]  contract_handle
 *      contract handle
 *
 *  @return
 *      Returns the cumulative count of how many times the ring is overrun resulting in same last filled buffer
 *
 * =============================================================================
 */
uint32_t  tf_getOverRunCount(tf_contract_HANDLE contract_handle);

/**
 * ============================================================================
 *  @n@b tf_isLogInfoNextBuf
 *
 *  @b  brief
 *  @n  Gets the flag to indicate if the logging information starts from first buffer
 *      or next buffer
 *
 *      Application Requirements:
 *      Application would need to create valid contract handle before calling this function
 *  @param[in]  contract_handle
 *      contract handle
 *
 *  @return
 *      Returns the flag to indicate if the wrap around happened in the ring buffer
 *      during the production. If this flag is set, indicates the wrap around hence 
 *      the logger information needs to be read from next buffer, otherwise it needs
 *      to be read from first ring buffer.
 *
 * =============================================================================
 */
uint32_t  tf_isLogInfoNextBuf (tf_contract_HANDLE contract_handle);

/**
 * ============================================================================
 *  @n@b tf_contractGetInfo
 *
 *  @b brief
 *  @n gets contract information details 
 *
 *      Application Requirements:
 *      Application would need to create valid contract handle before calling this function
 *  @param[in]  handle
 *      trace framework contract handle
 *
 *  @param[in]  info
 *      contract information returned
 *
 *  @return
 *     none
 *
 * =============================================================================
 */
void tf_contractGetInfo(tf_contract_HANDLE handle, tf_contractGetInfo_t* info);

/**
 * ============================================================================
 *  @n@b tf_systemCfg
 *
 *  @b brief
 *  @n starts the system with start configurations 
 *
 *      Application Requirements:
 *      Application would need to call this API once per process/core with proper startCfg arguments
 *      before Contract Create, Produer Create and Consumer Create functions
 *
 *  @param[in]  startCfg
 *      start configuration structure
 *
 *  @return
 *     none
 *
 * =============================================================================
 */
void tf_systemCfg (tf_StartCfg_t *startCfg);

/**
@}
*/

/* Define below function alias names for backward compatibility */
tf_contract_HANDLE TF_CONTRACT_CREATE(tf_contractConfig_t *tf_contractConfig, char* name);
void TF_CONTRACT_INIT(void* tf_contract_base, uint32_t num);
tf_contract_HANDLE TF_NEW_CONTRACT(tf_contractConfig_t *tf_contractConfig);
tf_contract_HANDLE TF_CONTRACT_FIND (void* tf_contract_start_addr, uint32_t num_contracts, char* name); 
uint32_t  tf_get_overrun_count(tf_contract_HANDLE contract_handle);
uint32_t  tf_is_logInfo_next_buf(tf_contract_HANDLE handle);

 /* below is kept for backwards compatibility */
#define    TRACE_CONTRACT_VERSION_2     TF_CONTRACT_VERSION_2
#define    TRACE_CONTRACT_VERSION_1     TF_CONTRACT_VERSION_1
#define    TRACE_CONTRACT_AVAILABLE     TF_CONTRACT_AVAILABLE
#define    TRACE_CONTRACT_NOT_AVAILABLE TF_CONTRACT_NOT_AVAILABLE

/* Local Errno for Trace Framework */
#define TF_ERRNO_BASE                           -128
#define TF_ERRNO_NO_CONTRACT_FOUND              (TF_ERRNO_BASE                           + 1)
#define TF_ERRNO_ALL_CONTRACTS_FULL             (TF_ERRNO_NO_CONTRACT_FOUND              + 1)
#define TF_ERRNO_CONSUMER_ALLOC_FAIL            (TF_ERRNO_ALL_CONTRACTS_FULL             + 1)
#define TF_ERRNO_CONSUMER_INVALID_CONFIG_PARAMS (TF_ERRNO_CONSUMER_ALLOC_FAIL            + 1)
#define TF_ERRNO_NULL_RING_BUFFER_FOUND         (TF_ERRNO_CONSUMER_INVALID_CONFIG_PARAMS + 1)
#define TF_ERRNO_MAX_RING_BUFFER_REACHED        (TF_ERRNO_NULL_RING_BUFFER_FOUND         + 1)
#define TF_ERRNO_INVALID_CONSUMER_HANDLE        (TF_ERRNO_MAX_RING_BUFFER_REACHED        + 1)
#define TF_ERRNO_INVALID_PRODUCER_HANDLE        (TF_ERRNO_INVALID_CONSUMER_HANDLE        + 1)
#define TF_ERRNO_MAX_NUM_CONSUMERS_REACHED      (TF_ERRNO_INVALID_PRODUCER_HANDLE        + 1)
#define TF_ERRNO_UNEXPECTED_NUM_CONSUMERS       (TF_ERRNO_MAX_NUM_CONSUMERS_REACHED      + 1)
#define TF_ERRNO_PRODUCER_ALLOC_FAIL            (TF_ERRNO_UNEXPECTED_NUM_CONSUMERS       + 1)
#define TF_ERRNO_INVALID_CONTRACT_HANDLE        (TF_ERRNO_PRODUCER_ALLOC_FAIL            + 1)
#define TF_ERRNO_INVALID_CONTRACT_ELEMENTS      (TF_ERRNO_INVALID_CONTRACT_HANDLE        + 1)
#define TF_ERRNO_SYNC_LOCK_ERROR_HANDLE         (TF_ERRNO_INVALID_CONTRACT_ELEMENTS      + 1)
#define TF_ERRNO_SYNC_LOCK_ERROR_USER_ID        (TF_ERRNO_SYNC_LOCK_ERROR_HANDLE         + 1)
#define TF_ERRNO_SYNC_LOCK_ERROR_DBL_LOCK       (TF_ERRNO_SYNC_LOCK_ERROR_USER_ID        + 1)
#define TF_ERRNO_SYNC_LOCK_ERROR_DBL_UNLOCK     (TF_ERRNO_SYNC_LOCK_ERROR_DBL_LOCK       + 1)


#ifdef __cplusplus
}
#endif

#endif /* CONSPROD_CONTRACT_H_ */

/* Nothing past this point */    

