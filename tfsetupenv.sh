#!/bin/sh
#******************************************************************************
#* FILE PURPOSE: Environment Setup for building SA LLD
#******************************************************************************
#* FILE NAME: setupenv.sh
#*
#* DESCRIPTION: 
#*  Configures and sets up the Build Environment for SA LLD
#*  Customers are expected to modify this file as per their build environment.
#*
#* Copyright (C) 2012, Texas Instruments, Inc.
#*****************************************************************************

# Update the install directory location from here
if [ ! -n "$XDC_INSTALL_PATH" ]; then
  export XDC_INSTALL_PATH=/home/a0756924/ti/xdctools_3_25_00_48
  echo setting XDC_INSTALL_PATH to user set path: $XDC_INSTALL_PATH
fi

# PATH for the XDC Plugin gen path
if [ ! -n "$XDCPLUGIN_INSTALL_DIR" ]; then
  export XDCPLUGIN_INSTALL_DIR=../DEP_TOOLS/xdc_eclipse_plugin_gen/20091203  
  echo setting XDCPLUGIN_INSTALL_DIR to user set path: $XDCPLUGIN_INSTALL_DIR
fi

# c66x Tool chain path
if [ ! -n "$C6X_GEN_INSTALL_PATH" ]; then
  export C6X_GEN_INSTALL_PATH=$HOME/ti/TI_CGT_C6000_7.3.16
  echo setting C6X_GEN_INSTALL_PATH to user set path: $C6X_GEN_INSTALL_PATH  
fi
export C66CODEGENTOOL=$C6X_GEN_INSTALL_PATH
export XDCCGROOT=$C6X_GEN_INSTALL_PATH
#REM ---------------------------------
#REM Enabling debug flags
#REM ---------------------------------
export EXTDBGFLAGS="-mn -g --optimize_with_debug"

# export cgxml install path
if [ ! -n "$CGXML_INSTALL_PATH" ]; then
  export CGXML_INSTALL_PATH=$HOME/ti/cg_xml
  echo setting CGXML_INSTALL_PATH to user set path: $CGXML_INSTALL_PATH  
fi


# export cgxml install path
if [ ! -n "$UIA_INSTALL_PATH" ]; then
  export UIA_INSTALL_PATH=$HOME/ti/uia_1_01_04_27
  echo setting UIA_INSTALL_PATH to user set path: $UIA_INSTALL_PATH  
fi

# ARM Cortex-A8 Tool chain path
if [ ! -n "$CROSS_TOOL_INSTALL_PATH" ]; then
  export CROSS_TOOL_INSTALL_PATH=/opt/linaro-2013.03/bin
  export CROSS_TOOL_PRFX=arm-linux-gnueabihf-
  echo setting CROSS_TOOL_INSTALL_PATH to user set path: $CROSS_TOOL_INSTALL_PATH  
fi
export GCARMV7ACGTOOL=$CROSS_TOOL_INSTALL_PATH
export GCARMV7ALONGNAME=$CROSS_TOOL_PRFX

# Set path to root folder of CSL (i.e packages directory of the PDK)
if [ ! -n "$PDK_INSTALL_PATH" ]; then
  export PDK_INSTALL_PATH=$HOME/ti/pdk_tci6614_1_01_00_01/packages
  echo setting PDK_INSTALL_PATH to user set path: $PDK_INSTALL_PATH  
fi

# Verify setup paths (Please do not change anything from here 
export DIR_NOT_EXIST=
if ! [ -d "$XDC_INSTALL_PATH" ]; then
export DIR_NOT_EXIST=FALSE
echo XDC_INSTALL_PATH:$XDC_INSTALL_PATH does not exist!!!
fi

if ! [ -d "$C6X_GEN_INSTALL_PATH" ]; then
export DIR_NOT_EXIST=FALSE
echo C6X_GEN_INSTALL_PATH:$C6X_GEN_INSTALL_PATH does not exist!!!
fi

if ! [ -d "$UIA_INSTALL_PATH" ]; then
export DIR_NOT_EXIST=FALSE
echo UIA_INSTALL_PATH:$UIA_INSTALL_PATH does not exist!!!
fi


if ! [ -d "$CROSS_TOOL_INSTALL_PATH" ]; then
export DIR_NOT_EXIST=FALSE
echo CROSS_TOOL_INSTALL_PATH:$CROSS_TOOL_INSTALL_PATH does not exist!!!
fi

if ! [ -d "$PDK_INSTALL_PATH" ]; then
export DIR_NOT_EXIST=FALSE
echo PDK_INSTALL_PATH:$exitINSTALL_PATH does not exist !!!
fi

if ! [ -d "$CGXML_INSTALL_PATH" ]; then
export DIR_NOT_EXIST=FALSE
echo CGXML_INSTALL_PATH:$CGXML_INSTALL_PATH does not exist !!!
fi

if [ "$DIR_NOT_EXIST" == "" ];then
	echo TRACEFRAMEWORK BUILD ENVIRONMENT SUCCESFULLY CONFIGURED
  # Any other components you could add as
  # XDCPATH=<your component>/packages;...
  export XDCPATH="$C6X_GEN_INSTALL_PATH/include;$XDC_INSTALL_PATH/packages;$UIA_INSTALL_PATH/packages"
  export PATH=$PATH:$XDC_INSTALL_PATH:$CROSS_TOOL_INSTALL_PATH:$CGXML_INSTALL_PATH/bin:$C6X_GEN_INSTALL_PATH/bin	
else
  echo TRACEFRAMEWORK LLD BUILD ENVIRONMENT NOT CONFIGURED 
fi
