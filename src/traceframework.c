/**
 *   traceframework.c
 *
 *   Description:
 *      Implements the local utiligy functions for trace framework functions.
 *
 *  ============================================================================
 *  (C) Copyright 2012-2013, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#if defined(_BIG_ENDIAN)
  #define SWIZ(x)  (sizeof((x)) == 1 ? (x) : (sizeof((x)) == 2 ? swiz16((x)) : (sizeof((x)) == 4 ? swiz32((x)) : 0)))
#else
  #define SWIZ(x)  (x)
#endif

#include "src/tf_utils.h"
#include "src/tf_loc.h"

#include "traceframework.h"

#ifdef TF_LINUX_USERSPACE
#include <stdio.h>
#include "src/cuia_prodif.h"
#else
#include "src/uia_prodif.h"
#endif

#include "tfver.h"
const char	 tfVersionStr[] = TF_DRV_VERSION_STR ":" __DATE__	":" __TIME__;

/* TF Local Object */
Tf_LocalObj tfLObj = {
                       {
					   	NULL, /* instPoolBaseAddr */
						0,    /* Inst pool size */
						0     /* control bit map */
					   },
					   0      /* tf_errno */
                     };

#if defined(_BIG_ENDIAN)

/*********************************************************************
 * FUNCTION PURPOSE: Swizzling
 *********************************************************************
 * DESCRIPTION: The TF contract requires all multi-byte fields in
 *              little endian format.
 *********************************************************************/
static inline uint16_t swiz16(uint16_t x)
{
  return ((x >> 8) | (x << 8));
}

static inline uint32_t swiz32 (uint32_t x)
{
  return (((x) >> 24) | (((x) >> 8) & 0xff00L) | (((x) << 8) & 0xff0000L) | ((x) << 24));
}
#endif

static int32_t setupName(char *dst, char *src)
{
  int     len;
  int32_t ret = 0;
  
  len = strlen(src);
  
  if (len < 127)
  	strncpy(dst, src, 126);
  else
  	ret = -1;

  return (ret);
}

static inline int32_t validate_offset (uint32_t offset) 
{
  int32_t retval = 0;
  
  if (tfLObj.cfg.instPoolBaseAddr != 0) {
  	/* do a size check on the offset, offset should never be greater than pool size */
	if (offset > tfLObj.cfg.instPoolSize)
		retval = -1;
  }
  
  return (retval);
}

static inline int32_t validate_contract_handle(tf_contract_HANDLE handle) 
{
	tf_contract_t	*cPtr = (tf_contract_t *) tf_get_addr(handle);

    /* Two stage verification is done here */

	/* 1. Check if the contract handle is correct when using the instPoolBaseAddress */
	if (validate_offset((uint32_t)handle)) {
	  return (-1);
	}

	/* Cache Invalidate Contract fileds to read in this function is not required since contract
	 * contractInfo field woudl have been already cached during the contract create
	 */

	/* 2. Check if the contract has the magic id value */
	if (cPtr->tf_contractInfo.magic_word == SWIZ(0xbabeface)) {
	  return (0);	
	}
	else {
	  return (-1);
	}
	
}

static int32_t  getSlotNum(tf_contract_t* ptr, uint32_t unique_id)
{
  int             i;
  uint32_t        magicId;
  tf_contract_t   *cPtr = ptr;

  for (i = 0; i < TF_MAX_CONSUMERS_PERCONTRACT; i++)
  {
    TF_CONTRACT_osalBeginMemAccess(&cPtr->consumer_shared_info[i], 128);
    tf_contract_reg32_r (&magicId, &cPtr->consumer_shared_info[i].consumerInfo.magic_id);
	if (magicId == 0) /* if slot is available */
	{
      tf_contract_reg32_w (&unique_id, &cPtr->consumer_shared_info[i].consumerInfo.magic_id);
      TF_CONTRACT_osalEndMemAccess(&cPtr->consumer_shared_info[i], 128);	
      TF_CONTRACT_osalBeginMemAccess(&cPtr->consumer_shared_info[i], 128);
      tf_contract_reg32_r (&magicId, &cPtr->consumer_shared_info[i].consumerInfo.magic_id);	 
	  if (magicId == unique_id)
	  	break;
	}
  }
  if (i < TF_MAX_CONSUMERS_PERCONTRACT)
  	return (i); /* slot found */
  else 
  	return (-1); /* no slot available */
}

static inline int32_t tf_updateRegMask(uint32_t reg_addr, uint32_t reg_mask)
{
    uint32_t ddr3_space, msmc_space;
   
    DEVICE_REG32_W(reg_addr, reg_mask);

    ddr3_space  = reg_addr & 0x80000000;
    msmc_space  = reg_addr & 0x0c000000;      

    /* Check if the register address space is MSMC or DDR3 for doing cache invalidates */
    if ( (ddr3_space == 0x80000000) ||
         (msmc_space == 0x0c000000))
    {
      TF_CONTRACT_osalEndMemAccess ((void*) reg_addr, 128);
    }
    return (GEN_PROD_SUCCESS);
}

/*
 * ============================================================================
 *  tf_contract_reg32_r
 *
 *  Description: reads a 32 bit value from the tf_contract to the local variable ptr
 *
 *  param[in]  localVarPtr
 *      Pointer to the local variable
 *
 *  param[in]  globalVarPtr
 *      Pointer to the global variable
 *
 *  return
 *      none
 *
 * =============================================================================
 */
static inline void tf_contract_reg32_r(uint32_t* localVarPtr, uint32_t* globalVarPtr)    
{      
  uint32_t temp;
  temp = *globalVarPtr;
  temp = SWIZ(temp);
  memcpy(localVarPtr, &temp, 4);
} 

/*
 * ============================================================================
 *  tf_contract_reg32_w
 *
 *  Description: write a 32 bit value from the local variable to the global memory
 *
 *  param[in]  localVarPtr
 *      Pointer to the local variable
 *
 *  param[in]  globalVarPtr
 *      Pointer to the global variable
 *
 *  return
 *      none
 *
 * =============================================================================
 */
static inline void tf_contract_reg32_w(uint32_t* localVarPtr, uint32_t* globalVarPtr)
{
  uint32_t temp;
  temp = *localVarPtr;
  temp = SWIZ(temp);
  memcpy(globalVarPtr,&temp, 4);
} 

/*
 * ============================================================================
 *  tf_contract_update_bytes
 *
 *  Description:
 *     write a size bytes to global memory
 *
 *  param[in]  sharedMemAddr
 *      Pointer to the local variable
 *
 *  param[in]  localMemAddr
 *      Pointer to the global variable
 *
 *  param[in]  size
 *      Pointer to the global variable
 *
 *  return
 *      none
 *
 * =============================================================================
 */
static inline void tf_contract_update_bytes(void* sharedMemAddr, void* localMemAddr, size_t size)    
{
  memcpy(sharedMemAddr, localMemAddr, size);
  /* write back the contents of the cache. */
  TF_CONTRACT_osalEndMemAccess(sharedMemAddr, size);
}

/*
 * ============================================================================
 *  tf_contract_update_smem
 *
 *  Description:
 *   write a size bytes to global memory
 *
 *  param[in]  sharedMemAddr
 *      Pointer to the local variable
 *
 *  param[in]  localMemAddr
 *      Pointer to the global variable
 *
 *  param[in]  size
 *      Pointer to the global variable
 *
 *  return
 *      none
 *
 * =============================================================================
 */
static inline void tf_contract_update_smem(void* sharedMemAddr, void* localMemAddr, size_t size)    
{
#if defined(_BIG_ENDIAN)
  uint32_t temp, i;
  uint32_t* t1;
  uint32_t* t2;

  t1 = (uint32_t *) localMemAddr;
  t2 = (uint32_t *) sharedMemAddr;
  /* Size is multiple of 4 */
  for (i=0; i<(size/4); i++)
  {
    temp = *(t1+i);
	temp = SWIZ(temp);
    memcpy(t2+i, &temp, 4);
  }
#else
  memcpy(sharedMemAddr, localMemAddr, size);
#endif
  TF_CONTRACT_osalEndMemAccess(sharedMemAddr, size);
}

/*
 * ============================================================================
 *  tf_consumer_increment_var
 *
 *  brief: Increments a variable by max_count amount in a circular way
 *
 *  param[in]  var
 *      variable to increment
 *
 *  param[in]  inc_val
 *      increment value
 *
 *  param[in]  max_count
 *      max value for the circular buffer
 *
 *  return
 *      Status of the operation
 *
 * =============================================================================
 */

static inline void tf_consumer_increment_var (uint32_t* var, uint32_t inc_val, uint32_t max_count)
{
  uint32_t init_val;

  init_val = *var;

  init_val += inc_val;

  if (init_val >= max_count)
    init_val -= max_count;

  *var  = init_val;
}

/*
 * ============================================================================
 *  tf_get_distance
 *
 *  brief:Get the distance between two circular indexes
 *      Assumptions: index1 is always ahead of index2
 *      The circular index progresses in time in clock wise direction.
 *
 *  param[in]  index1
 *      circular buffer index1
 *
 *  param[in]  index2
 *      circular buffer index2
 *
 *  param[in]  max_count
 *      max value for the circular buffer
 *
 *  return
 *      Status of the operation
 *
 * =============================================================================
 */

static inline int32_t tf_get_distance (int32_t index1, int32_t index2, int32_t max_count)
{
  int32_t temp;

  /* assuming index1 is always ahead of index2 in clockwise direction */
  
  index1 -= index2;

  if (index1 < 0)
    temp = max_count + index1;
  else
    temp = index1;

  return (temp);
}

/***************** global variables used only in core0 ***************************/    
static void tf_cons_update_global_lfb(tf_consumer_HANDLE cHandle)
{
  tf_consInst_t*       cInst_ptr = (tf_consInst_t *)cHandle;
  tf_contract_t*       contract_ptr;  
  uint32_t          global_lfb_index, lfb_index, i, max_distance;
  int32_t           distance, temp;
  uint32_t          consumer_id, ignoreFlag;
  consumerInfo_t*   cInfo_ptr;

  contract_ptr = cInst_ptr->contract_ptr;
  	
  /* Cache Invalidate Contract fileds to read in this function */
  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->cArea, 128);  
  tf_contract_reg32_r(&global_lfb_index, &contract_ptr->cArea.global_last_free_buf_index); 
  
  /* find the least inc distance from all the consumers */
  max_distance = cInst_ptr->buf_info.num_buf;
  distance = max_distance; /* keep it for the max */

  for (i=0; i< TF_MAX_CONSUMERS_PERCONTRACT; i++)
  {
	  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->consumer_shared_info[i], sizeof (conSharedInfo_t));
      cInfo_ptr = (consumerInfo_t *)&contract_ptr->consumer_shared_info[i];
      tf_contract_reg32_r(&consumer_id, &cInfo_ptr->magic_id);  
	  tf_contract_reg32_r(&ignoreFlag, &cInfo_ptr->ignoreFlag);

	  if ( (consumer_id == (uint32_t)NULL) || 
	  	   (ignoreFlag == 1))
             continue;
	  
      tf_contract_reg32_r(&lfb_index, &cInfo_ptr->last_free_buf_index);
      temp = tf_get_distance(lfb_index, global_lfb_index, max_distance);

      if (temp < distance)
        distance = temp;
  }

  /* Update the contract only if you are the slowest consumer. */
  if (distance)
  {
    tf_consumer_increment_var(&global_lfb_index, distance, max_distance);
    tf_contract_reg32_w(&global_lfb_index, &contract_ptr->cArea.global_last_free_buf_index); 
	/* Cache Wb to reflect in the contract area */
    TF_CONTRACT_osalEndMemAccess(&contract_ptr->cArea, 128);
  }
}

static inline uint32_t  tf_get_over_run_count(tf_contract_HANDLE contract_handle)
{
  tf_contract_t* contract_ptr = (tf_contract_t *) tf_get_addr(contract_handle);
  uint32_t       over_run_cnt;

  /* Check if contract handle is valid */
  if (validate_contract_handle(contract_handle)) {
  	return (0xFFFFFFFF);
  }

  /* Cache Invalidate Contract fileds to read in this function */
  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->overrun_count, 128); 
  
  tf_contract_reg32_r(&over_run_cnt, &contract_ptr->overrun_count);

  return (over_run_cnt);
}

static inline uint32_t  tf_check_log_info_next_buf(tf_contract_HANDLE contract_handle)
{
  tf_contract_t* contract_ptr;
  uint32_t       wb_flag;

  /* Check if contract handle is valid */
  if (validate_contract_handle(contract_handle)) {
	  return (0xFFFFFFFF);
  }  

  contract_ptr = (tf_contract_t*) tf_get_addr(contract_handle);
  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->tf_contractInfo, sizeof (tf_contractInfo_t) );

  if (contract_ptr->tf_contractInfo.magic_word ==0xbabeface)
  	tf_contract_reg32_r(&wb_flag, &contract_ptr->tf_contractInfo.rb_wrap_flag);
  else
  	wb_flag = 0xFFFFFFFF;

  return (wb_flag);
}

static inline uint32_t tf_check_to_consume(tf_consumer_HANDLE cHandle)
{
   tf_consInst_t *cInst_ptr;
   tf_contract_t* contract_ptr;
   uint32_t bIndex, pIndex, maxVal, retVal;

   if (cHandle == NULL) {
	   return (TF_CONSUMER_INVALID_ARGUMENTS);
   }   

   cInst_ptr = (tf_consInst_t*) cHandle;
   contract_ptr = cInst_ptr->contract_ptr;
   
   bIndex = cInst_ptr->buf_info.buf_filled_notsent_index;
   maxVal = cInst_ptr->buf_info.num_buf;

   TF_CONTRACT_osalBeginMemAccess(&contract_ptr->producer_current_index, 128); 
   tf_contract_reg32_r(&pIndex, &contract_ptr->producer_current_index);   

#ifdef TF_STREAM_FREEZE_PRODUCER_SUPPORT
   if (cInst_ptr->streamFlag == 0)
   {
	 int32_t producer_state;

	 TF_CONTRACT_osalBeginMemAccess(&contract_ptr->producer_state, 128);
	 tf_contract_reg32_r ((uint32_t *)&producer_state, (uint32_t *)&contract_ptr->producer_state);

     /* Check producer state */
     if ( (producer_state == TF_PRODUCER_STATE_FREE_RUNNING) || 
          (producer_state == TF_PRODUCER_STATE_PROCESS_FREEZE_REQ) )
       return 0;
   }
#endif

   retVal = 0;
   while (bIndex != pIndex)   
   {
     retVal++; 
     tf_consumer_increment_var(&bIndex, 1, maxVal);     
   }

   return (retVal);   
   
}
/*
 * ============================================================================
 *  tf_contract_init
 *
 *  brief
 *  initializes the tf_contract as avilable for producer/consumer 
 *
 *  param[in]  tf_contract_base
 *      Pointer to the base address of the tf_contract array
 *      Assumes all the contracts are in contiguous memory
 *
 *  param[in]  num
 *      number of tf_contract buffers
 *
 *  return
 *      none
 *
 * =============================================================================
 */
static inline void tf_contract_init (void* tf_contract_base, uint32_t num)
{
   tf_contract_t *cPtr, *cPtr_base;
   uint32_t    i, temp;
   
#ifdef TF_LINUX_USERSPACE
    uint32_t sz;
	/* Get the memory map size */	
	sz = sizeof (tf_contract_t) * num;

    if ((tfLObj.cfg.ctrlBitMap & tf_CONTRACT_BLK_ADDR_TYPE) == TF_CONTRACT_BUF_PHYSICAL_ADDR) 
	{
		cPtr_base = (tf_contract_t *) TF_CONTRACT_osalMmap (tf_contract_base, sz);
    }
	else
#endif
    {
        cPtr_base = (tf_contract_t *) tf_contract_base;
    }

    cPtr = cPtr_base;
   /* Set the tf_contract as available & clear any stale data in the tf_contract */
   for (i=0; i<num; i++)
   {
     memset(cPtr, 0, sizeof(tf_contract_t));

	 temp = 0xbabeface;
	 tf_contract_reg32_w(&temp, &cPtr->tf_contractInfo.magic_word);

	 temp = TRACE_CONTRACT_AVAILABLE;
     tf_contract_reg32_w(&temp, &cPtr->tf_contractInfo.contractState);
	 TF_CONTRACT_osalEndMemAccess(cPtr, sizeof (tf_contract_t));
	 
     cPtr++;
   }
}    

/*
 * ============================================================================
 *  tf_contract_find
 *
 *   brief
 *    finds the tf_contract based on the name
 *
 *  param[in]  tf_contract_start_addr
 *      tf_contract start address
 *
 *  param[in]  num_contracts
 *      number of contracts in the system
 *
 *  param[in]  name
 *      Pointer to the name of the tf_contract
 *
 *  return
 *      Contract Handle
 *
 * =============================================================================
 */
static inline tf_contract_HANDLE tf_contract_find(void* tf_contract_start_addr, uint32_t num_contracts, char* name)
{
   tf_contract_t *cPtr;
   uint32_t    num, flag = 0;
   int32_t     val; 
   uint32_t    magic_word, i, sz;
   
   num = num_contracts;  

   val = strlen(name);
   if (val == 0) 
      return ((tf_contract_HANDLE)(-1));

   sz = sizeof (tf_contract_t) * num;

#ifdef TF_LINUX_USERSPACE   
   if ((tfLObj.cfg.ctrlBitMap & tf_CONTRACT_BLK_ADDR_TYPE) == TF_CONTRACT_BUF_PHYSICAL_ADDR) 
   {
	  cPtr = (tf_contract_t *) TF_CONTRACT_osalMmap (tf_contract_start_addr, sz);
   }
   else
#endif
   {
	  cPtr = (tf_contract_t *) tf_contract_start_addr;
   }

    if (cPtr == 0)
		return ((tf_contract_HANDLE)(-1));

    TF_CONTRACT_osalBeginMemAccess(cPtr, sz);  
    tf_contract_reg32_r(&magic_word, &cPtr->tf_contractInfo.magic_word);
		
	/* Not a valid contract */
	if (magic_word != 0xbabeface)
		return ((tf_contract_HANDLE)(-1));
    
   for (i =0 ; i<num; i++ )
   {
     val = strcmp(cPtr->name, name);
     
     if (val == 0 )
     {
        flag = 1;
        break;
     }
     cPtr++;
   }

   if (flag == 1) 
   {
	  return ((tf_contract_HANDLE) tf_get_offset(cPtr));
   }
   else {
      tfLObj.tf_errno = TF_ERRNO_NO_CONTRACT_FOUND;
      return ((tf_contract_HANDLE)(-1));
   }
}  

static inline tf_contract_HANDLE tf_contract_create(tf_contractConfig_t *tf_contractConfig, char* name)
{
   tf_contract_t *cPtr;
   uint32_t    flag = 0;
   uint32_t    i, num, tmp;
#ifdef TF_LINUX_USERSPACE
   uint32_t    sz;
#endif
   int                           contractType;

   contractType = tf_contractConfig->contractType;
   if ( (contractType < TF_CONTRACT_DSP_ARM) ||
   	    (contractType > TF_CONTRACT_ARM_ARM) )
   	    return (NULL);

   if (tf_contractConfig->tf_contract_start_addr == NULL)
   	  return (NULL);

   TF_CONTRACT_osalEnter();

   num = tf_contractConfig->num_contracts;

#ifdef TF_LINUX_USERSPACE 

   sz = sizeof (tf_contract_t) * num;

   if ((tfLObj.cfg.ctrlBitMap & tf_CONTRACT_BLK_ADDR_TYPE) == TF_CONTRACT_BUF_PHYSICAL_ADDR) 
   {
	   cPtr = (tf_contract_t *) TF_CONTRACT_osalMmap (tf_contractConfig->tf_contract_start_addr, sz);
   }
   else
#endif
   {
	   cPtr = (tf_contract_t *) tf_contractConfig->tf_contract_start_addr;
   }	

   for (i =0 ; i<num; i++ )
   {
     TF_CONTRACT_osalBeginMemAccess(&cPtr->tf_contractInfo, 128);
     tf_contract_reg32_r(&tmp, &cPtr->tf_contractInfo.contractState);
     if (tmp == TRACE_CONTRACT_AVAILABLE) 
     {
        /* set up the name */
        if (setupName(cPtr->name, name) != 0) {
          break;    
        }
		
        flag = 1;
    	tmp = TRACE_CONTRACT_NOT_AVAILABLE;
		tf_contract_reg32_w(&tmp, &cPtr->tf_contractInfo.contractState);
        break;
     }
     cPtr++;
   }

   if (flag == 1)
   {
	 
	 TF_CONTRACT_osalEndMemAccess(cPtr->name, 128);
     
     /* Ring Buffer Setup */  
	 tmp = (uint32_t )tf_contractConfig->ringbuf_start_addr;
     tmp = tf_get_offset(tmp); /* store the offset of the ring buffer */
     tf_contract_reg32_w(&tmp, (uint32_t*) &cPtr->tf_contractInfo.buffer_base);
     tf_contract_reg32_w(&tf_contractConfig->ringbuf_size, &cPtr->tf_contractInfo.buffer_size);
     tf_contract_reg32_w(&tf_contractConfig->num_ringbufs, &cPtr->tf_contractInfo.num_buf);
	 if (tfLObj.cfg.instPoolBaseAddr != 0)
       tmp = TF_CONTRACT_RING_BUF_NOT_PHYSICAL_ADDR;
	 else
       tmp = (uint32_t ) tf_contractConfig->ring_base_type;
	 
	 tf_contract_reg32_w(&tmp, (uint32_t *)&cPtr->tf_contractInfo.ring_base_type);	 	

	 tmp = TRACE_CONTRACT_VERSION_1;
     tf_contract_reg32_w(&tmp, &cPtr->tf_contractInfo.contractVersion);	

	 tmp = (uint32_t) contractType; /* contract type, during start up */
	 tf_contract_reg32_w(&tmp, &cPtr->tf_contractInfo.contractType);	 

	 TF_CONTRACT_osalEndMemAccess(&cPtr->tf_contractInfo, 128);
	 
	 tmp = tf_contractConfig->num_ringbufs - 1;
     tf_contract_reg32_w(&tmp, &cPtr->cArea.global_last_free_buf_index);
	 TF_CONTRACT_osalEndMemAccess(&cPtr->cArea, 128);

	 
#ifdef TF_STREAM_FREEZE_PRODUCER_SUPPORT
	 if ( (tf_contractConfig->producer_state < TF_PRODUCER_STATE_STREAMING) ||
		  (tf_contractConfig->producer_state > TF_PRODUCER_STATE_FROZEN) )
		  tmp = TF_PRODUCER_STATE_STREAMING; /* Error condition, start with streaming mode */
	 else
		  tmp = tf_contractConfig->producer_state; /* consider whatever config has requested */
	 tf_contract_reg32_w(&tmp, (uint32_t *)&cPtr->producer_state);
	 TF_CONTRACT_osalEndMemAccess(&cPtr->producer_state, 128);	 
#endif

	 tmp = 0;	 
     tf_contract_reg32_w(&tmp, &cPtr->producer_current_index);
	 TF_CONTRACT_osalEndMemAccess(&cPtr->producer_current_index, 128);
   }

   TF_CONTRACT_osalExit();

   if (flag == 1) {
    return ((tf_contract_HANDLE) tf_get_offset(cPtr));
   }
   else {
    tfLObj.tf_errno = TF_ERRNO_ALL_CONTRACTS_FULL;   	
    return ((tf_contract_HANDLE) (-1));
   }
}    

/*
 * ============================================================================
 *  tf_new_contract
 *
 *  b  brief
 *  Creates the contract for trace framework producer and consumers for Ver 2
 *
 *  param[in]  tf_contractConfig
 *      tf_contract configuration structure 
 *
 *  return
 *      Contract Handle
 *
 * =============================================================================
 */
static inline tf_contract_HANDLE tf_new_contract(tf_contractConfig_t *tf_contractConfig)
{

   tf_contract_t                 *cPtr;
   uint32_t                      i, sz, tmp;
   tf_contract_HANDLE            handle;
   int                           contractType;

   contractType = tf_contractConfig->contractType;

   if ( (contractType < TF_CONTRACT_DSP_ARM) ||
   	    (contractType > TF_CONTRACT_ARM_ARM) )
   	    return (NULL);

   sz = sizeof (tf_contract_t) ;

#ifdef TF_LINUX_USERSPACE   
   if ((tf_contractConfig->contract_base_type == TF_CONTRACT_BUF_PHYSICAL_ADDR))
   {
	   cPtr = (tf_contract_t *) TF_CONTRACT_osalMmap (tf_contractConfig->tf_contract_start_addr, sz);
   }
   else
#endif
   {
       cPtr = (tf_contract_t *) tf_contractConfig->tf_contract_start_addr;
   }   

   TF_CONTRACT_osalBeginMemAccess(cPtr, sz); 

   memset(cPtr, 0, sizeof(tf_contract_t));

   tmp = TRACE_CONTRACT_NOT_AVAILABLE;
   tf_contract_reg32_w(&tmp, &cPtr->tf_contractInfo.contractState);

   /* Store 128 bytes of information */
   memcpy(cPtr->name, tfVersionStr, sizeof (tfVersionStr));

   tmp = (uint32_t )tf_contractConfig->ringbuf_start_addr;
   tmp = tf_get_offset(tmp); /* store the offset of the ring buffer */
   tf_contract_reg32_w(&tmp, (uint32_t*) &cPtr->tf_contractInfo.buffer_base);
   tf_contract_reg32_w(&tf_contractConfig->ringbuf_size, &cPtr->tf_contractInfo.buffer_size);
   tf_contract_reg32_w(&tf_contractConfig->num_ringbufs, &cPtr->tf_contractInfo.num_buf);

   if (tfLObj.cfg.instPoolBaseAddr != 0)
     tmp = TF_CONTRACT_RING_BUF_NOT_PHYSICAL_ADDR;
   else
     tmp = (uint32_t ) tf_contractConfig->ring_base_type;
	 
   tf_contract_reg32_w(&tmp, (uint32_t *)&cPtr->tf_contractInfo.ring_base_type);	   
   tmp = tf_contractConfig->contractType;
   tf_contract_reg32_w(&tmp, &cPtr->tf_contractInfo.contractType);   

   tmp = tf_contractConfig->num_ringbufs - 1;
   tf_contract_reg32_w(&tmp, &cPtr->cArea.global_last_free_buf_index);
   
#ifdef TF_STREAM_FREEZE_PRODUCER_SUPPORT
   if ( (tf_contractConfig->producer_state < TF_PRODUCER_STATE_STREAMING) ||
   	    (tf_contractConfig->producer_state > TF_PRODUCER_STATE_FROZEN) )
   	    tmp = TF_PRODUCER_STATE_STREAMING; /* Error condition, start with streaming mode */
   else
   	    tmp = tf_contractConfig->producer_state; /* consider whatever config has requested */
   tf_contract_reg32_w(&tmp, (uint32_t *) &cPtr->producer_state);   
#endif

   tmp = 0;	
   tf_contract_reg32_w(&tmp, &cPtr->producer_current_index);
   
   tmp = TRACE_CONTRACT_VERSION_2;
   tf_contract_reg32_w(&tmp, &cPtr->tf_contractInfo.contractVersion); 
   
   tmp = (uint32_t) contractType; /* contract type */
   tf_contract_reg32_w(&tmp, &cPtr->tf_contractInfo.contractType);

   tmp = 0xbabeface;
   tf_contract_reg32_w(&tmp, &cPtr->tf_contractInfo.magic_word);

   for (i=0;i<TF_MAX_CONSUMERS_PERCONTRACT;i++)
   {
       /* Clear the shared information from all consumers */
	   memset(&cPtr->consumer_shared_info[i], 0, sizeof(conSharedInfo_t));
   }

   /* Wb the information in the shared location */
   TF_CONTRACT_osalEndMemAccess(cPtr, sz);   

   handle = (tf_contract_HANDLE)tf_get_offset(cPtr);
   return (handle);

}

uint32_t tf_get_overrun_count(tf_contract_HANDLE contract_handle)
{
  return (tf_get_over_run_count(contract_handle));
}

uint32_t  tf_is_logInfo_next_buf(tf_contract_HANDLE contract_handle)
{
  return (tf_check_log_info_next_buf(contract_handle));
}

uint32_t tf_isthere_to_consume(tf_consumer_HANDLE cHandle)
{
	return (tf_check_to_consume(cHandle)); 
}

uint32_t  tf_getOverRunCount(tf_contract_HANDLE contract_handle)
{
  return (tf_get_over_run_count(contract_handle));
}

uint32_t  tf_isLogInfoNextBuf(tf_contract_HANDLE contract_handle)
{
  return (tf_check_log_info_next_buf(contract_handle));
}

uint32_t tf_isThereToConsume(tf_consumer_HANDLE cHandle)
{
   return (tf_check_to_consume(cHandle));      
}

#ifdef TF_STREAM_FREEZE_PRODUCER_SUPPORT
tf_consStatus_e tf_consumerSetProducerState(tf_cons_HANDLE cHandle, tf_producerState_e state)
{
  tf_consInst_t*   cInst_ptr = (tf_consInst_t *)cHandle;
  tf_contract_t*   contract_ptr;
  int32_t          prodState = state;
  consumerInfo_t*  cInfo_ptr;  

  if (cHandle == NULL) {
	  return (TF_CONSUMER_INVALID_ARGUMENTS);
  }
  
  /* Validate state value */
  if (state > TF_REQUEST_PRODUCER_STATE_FREEZING || state < TF_PRODUCER_STATE_STREAMING)
      return TF_CONSUMER_INVALID_ARGUMENTS;

  /* Check if the contract type is Stream/Freeze type */
  contract_ptr = cInst_ptr->contract_ptr;

  /* Update Producer state in contract */
  tf_contract_reg32_w((uint32_t *) &prodState, (uint32_t *) &contract_ptr->producer_state);
  TF_CONTRACT_osalEndMemAccess(&contract_ptr->producer_state, 128);

  /* Record the current producer index during the state change */
  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->producer_current_index, 128); 
  cInst_ptr->buf_info.myview_prod_current_index = contract_ptr->producer_current_index;

  /* Indicate that this consumer requested producer streaming state changes */
  if (state == TF_PRODUCER_STATE_STREAMING)
  	cInst_ptr->streamFlag = 1;
  else
  	cInst_ptr->streamFlag = 0;

  if (state == TF_REQUEST_PRODUCER_STATE_FREEZING)
  	cInst_ptr->trigFlag = 0;

  /* Update the information in the shared memory */
  cInfo_ptr = (consumerInfo_t *)&contract_ptr->consumer_shared_info[cInst_ptr->myslot];  
  if ( (state == TF_REQUEST_PRODUCER_STATE_FREEZING) ||
  	   (state == TF_PRODUCER_STATE_FREE_RUNNING) )
     cInfo_ptr->ignoreFlag = 1;
  else
  	 cInfo_ptr->ignoreFlag = 0;
  
  TF_CONTRACT_osalEndMemAccess(&contract_ptr->consumer_shared_info[cInst_ptr->myslot], sizeof (conSharedInfo_t));

  return CONSUMER_SUCCESS;

}

/*
 This function will get current producer state from contract. 
 */
tf_producerState_e tf_consumerGetProducerState(tf_cons_HANDLE cHandle)
{
  tf_consInst_t*   cInst_ptr = (tf_consInst_t *)cHandle;
  tf_contract_t*   contract_ptr;
  int32_t          prodState;

  if (cHandle == NULL) {
	  return (TF_PRODUCER_STATE_INVALID);
  }  

  contract_ptr = cInst_ptr->contract_ptr;

  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->producer_state, 128);
  tf_contract_reg32_r((uint32_t *) &prodState, (uint32_t *) &contract_ptr->producer_state);

  return ((tf_producerState_e)prodState);
}

/* In case that producer is not responding to the freeze request
 * OR DSP crashed for some reason, here is the best effort to help
 * read the ring buffers and set the producer state to be frozen
 */
tf_consStatus_e tf_consumerForceFreezeProducer(tf_cons_HANDLE cHandle)
{
  tf_consInst_t*   cInst_ptr =  (tf_consInst_t*)cHandle;
  tf_contract_t*   contract_ptr;
  int32_t          prodState;
  consumerInfo_t*  cInfo_ptr; 

  contract_ptr = cInst_ptr->contract_ptr;

  /* Force producer state to be FROZEN, so that producer stops filling up next buffers */
  prodState = TF_PRODUCER_STATE_FROZEN;
  tf_contract_reg32_w((uint32_t *) &prodState, (uint32_t *) &contract_ptr->producer_state);
  TF_CONTRACT_osalEndMemAccess(&contract_ptr->producer_state, 128);

  cInst_ptr->streamFlag = 0; /* forcing the producer state not to stream */

  /* Update the information in the shared memory */
  cInfo_ptr = (consumerInfo_t *)&contract_ptr->consumer_shared_info[cInst_ptr->myslot];  
  cInfo_ptr->ignoreFlag = 0; /* Dont ignore the glast computations from this consumer */
  TF_CONTRACT_osalEndMemAccess(&contract_ptr->consumer_shared_info[cInst_ptr->myslot], sizeof (conSharedInfo_t));  

  return TF_CONSUMER_SUCCESS;
}
#endif

tf_consStatus_e tf_consumerGetContractHandle(tf_cons_HANDLE cHandle, tf_contract_HANDLE *contract_handle)
{
   tf_consInst_t*	 inst = (tf_consInst_t *) cHandle;

   if (cHandle == NULL)
     return (TF_CONSUMER_INVALID_HANDLE);
  
   *contract_handle = (tf_contract_HANDLE) tf_get_offset(inst->contract_ptr);

   /* Check if contract handle is valid */
   if (validate_contract_handle(contract_handle)) {
  	 return (TF_CONSUMER_INVALID_CONTRACT_HANDLE);
   }   
   
   return (TF_CONSUMER_SUCCESS);
}

tf_consStatus_e tf_alignConsumer2FrozenProd(tf_consumer_HANDLE cHandle)
{
	consumerInfo_t*   cInfo1; 
    tf_consInst_t*   cInst_ptr = (tf_consInst_t *)cHandle; 
    tf_contract_t*   contract_ptr;	
	int32_t          wb_flag;

	if (cHandle == NULL)
	  return (TF_CONSUMER_INVALID_HANDLE);

	if (cInst_ptr->trigFlag == 0) 
		cInst_ptr->trigFlag = 1;
	else
		return (TF_CONSUMER_SUCCESS); /* No action */

    contract_ptr = cInst_ptr->contract_ptr;		
	cInfo1 = (consumerInfo_t *) &contract_ptr->consumer_shared_info[cInst_ptr->myslot];	
	
	/* Dont ignore updates during producer frozen state */
	cInfo1->ignoreFlag = 0; 	
	TF_CONTRACT_osalEndMemAccess(&contract_ptr->consumer_shared_info[cInst_ptr->myslot], sizeof(conSharedInfo_t)); 

    /* Get the wrap around flag information from the contract */
	wb_flag  = tf_check_log_info_next_buf((tf_contract_HANDLE)tf_get_offset(cInst_ptr->contract_ptr));	

	if (wb_flag)		
	{
		/* Check if we need to consume anything on this call */
		cInst_ptr->buf_info.last_free_buf_index 	 =	contract_ptr->producer_current_index;
   	    tf_consumer_increment_var(&cInst_ptr->buf_info.last_free_buf_index, 1, cInst_ptr->buf_info.num_buf); 
	
	    cInst_ptr->buf_info.buf_filled_notsent_index = cInst_ptr->buf_info.last_free_buf_index;
	    tf_consumer_increment_var(&cInst_ptr->buf_info.buf_filled_notsent_index, 1, cInst_ptr->buf_info.num_buf); 
	}
	else /* The ring buffer is not wrapped around, no need to consume full ring  */
	{
		/* Get the current producer index */
		TF_CONTRACT_osalBeginMemAccess(&contract_ptr->producer_current_index, 128); 
		
		if (contract_ptr->producer_current_index == cInst_ptr->buf_info.myview_prod_current_index)
			return (TF_CONSUMER_SUCCESS);

	}

	return (TF_CONSUMER_SUCCESS);	
}

tf_consStatus_e tf_consProcess(tf_consumer_HANDLE cHandle)
{
  tf_consInst_t*   cInst_ptr = (tf_consInst_t *)cHandle; 
  tf_contract_t*   contract_ptr;
  uint8_t          *logBuf;
  uint32_t         temp1; 
  tf_consAppSendRespStatus_e app_send_response;
  tf_consStatus_e  retVal = TF_CONSUMER_SUCCESS;
  int32_t          producer_state;

  if (cHandle == NULL) {
  	return (TF_CONSUMER_INVALID_HANDLE);
  }

  contract_ptr = cInst_ptr->contract_ptr;
  
#ifdef TF_STREAM_FREEZE_PRODUCER_SUPPORT
   /* Check producer state */
  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->producer_state, 128);     
  tf_contract_reg32_r ((uint32_t *) &producer_state, (uint32_t *) &contract_ptr->producer_state);  
  if (cInst_ptr->streamFlag == 0) /* consumer who initiated the change */
  {
    if ( (producer_state == TF_PRODUCER_STATE_FREE_RUNNING) || 
    	   (producer_state == TF_PRODUCER_STATE_PROCESS_FREEZE_REQ) )
    {
                return (TF_CONSUMER_NOTHING_TO_CONSUME);
    }  
    else if (producer_state == TF_PRODUCER_STATE_FROZEN)
    {
		tf_alignConsumer2FrozenProd(cHandle);
    } 
  }
#endif 

  /* Check if we need to consume anything on this call */
  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->producer_current_index, 128);  
  tf_contract_reg32_r(&cInst_ptr->buf_info.myview_prod_current_index, &contract_ptr->producer_current_index); 

  /* Producer did not produce anything, return by doing nothing */
  if (cInst_ptr->buf_info.buf_filled_notsent_index == cInst_ptr->buf_info.myview_prod_current_index )
    return (CONSUMER_NOTHING_TO_CONSUME);

  /* Find log buffer that will be sent */
  logBuf     = cInst_ptr->buf_info.buf_ptr[cInst_ptr->buf_info.buf_filled_notsent_index];

  /* Invalidate the cache since the logBuf can be updated outside this core */
  TF_CONSUMER_osalBeginMemAccess(logBuf, cInst_ptr->buf_info.buf_size);

  /* Send the buffer information to appliation */  
  if (cInst_ptr->buf_info.buf_size != 0)
  {
     if (cInst_ptr->sendFxn)
	 	app_send_response = (*cInst_ptr->sendFxn)(cInst_ptr->param, logBuf, cInst_ptr->buf_info.buf_size);
	 else
	 	app_send_response = CONS_APP_SEND_OK_NO_WAIT_ACK;

     switch (app_send_response)
     {
          case CONS_APP_SEND_NOK_RETRY:
           { 
               consumerInfo_t*   cInfo; 
               /* Increment the retry count */
               cInst_ptr->consumer_retry_count++;
               cInfo = (consumerInfo_t *) &contract_ptr->consumer_shared_info[cInst_ptr->myslot];			   
			   tf_contract_reg32_w(&cInst_ptr->consumer_retry_count, &cInfo->consumer_app_retry_count);
               TF_CONTRACT_osalEndMemAccess(&contract_ptr->consumer_shared_info[cInst_ptr->myslot], sizeof(conSharedInfo_t)); 			   
           }
           break;        
          case CONS_APP_SEND_OK_WAIT_ACK:
           /* If no Ack received for the previous buffer, don't send the buffer to the system */
           if (cInst_ptr->consumer_state == TF_CONSUMER_ACK_RECIVED)	  	
           {
               consumerInfo_t*   cInfo; 
               cInfo = (consumerInfo_t *) &contract_ptr->consumer_shared_info[cInst_ptr->myslot];

               /* Set the consumer state to Waiting for ACK */
               cInst_ptr->consumer_state = CONSUMER_WAIT_ACK;
             
               /* Increment the buffer sent index */
               tf_contract_increment_var(temp1, cInst_ptr->buf_info.buf_filled_notsent_index,cInst_ptr->buf_info.num_buf);

               /* Reset the previous retry count */
               cInst_ptr->consumer_retry_count       = 0;               
			   tf_contract_reg32_w(&cInst_ptr->consumer_retry_count, &cInfo->consumer_app_retry_count);			   
               TF_CONTRACT_osalEndMemAccess(&contract_ptr->consumer_shared_info[cInst_ptr->myslot], sizeof(conSharedInfo_t)); 			   
           }
		   retVal = TF_CONSUMER_WAIT_ACK;
       	   break;
          case CONS_APP_SEND_OK_NO_WAIT_ACK:
          default:		  	
           { 
               consumerInfo_t*   cInfo; 
               
               /* Increment the local last free buf index */
               //tf_contract_increment_var(temp1, cInst_ptr->buf_info.last_free_buf_index, cInst_ptr->buf_info.num_buf);
               cInst_ptr->buf_info.last_free_buf_index = cInst_ptr->buf_info.buf_filled_notsent_index;

               /* Increment the buffer sent index */
               tf_contract_increment_var(temp1, cInst_ptr->buf_info.buf_filled_notsent_index,cInst_ptr->buf_info.num_buf); 			   
             
               /* Update in the shared memory area of the contract */
               cInfo = (consumerInfo_t *) &contract_ptr->consumer_shared_info[cInst_ptr->myslot];  
               tf_contract_reg32_w(&cInst_ptr->buf_info.last_free_buf_index, &cInfo->last_free_buf_index);               
               TF_CONTRACT_osalEndMemAccess(&contract_ptr->consumer_shared_info[cInst_ptr->myslot], sizeof(conSharedInfo_t)); 
			   
               /* Update the global last free buf index for the producer */
               tf_cons_update_global_lfb(cHandle);              

               /* Reset the previous retry count */
               cInst_ptr->consumer_retry_count       = 0;  

			   /* force that ack is received */
			   cInst_ptr->consumer_state = TF_CONSUMER_ACK_RECIVED;
           }	  
           break;		   
     }

  }

  return (retVal);
}

tf_consStatus_e tf_consAck(tf_consumer_HANDLE cHandle)
{
  tf_consInst_t*    cInst_ptr = (tf_consInst_t *)cHandle; 
  tf_contract_t*    contract_ptr;
  consumerInfo_t*   cInfo; 

  if (cHandle == NULL) {
  	return (TF_CONSUMER_INVALID_HANDLE);
  }
  
  contract_ptr = cInst_ptr->contract_ptr;
  
  /* Check if we need to consume anything on this call */
  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->producer_current_index, 128);
  tf_contract_reg32_r(&cInst_ptr->buf_info.myview_prod_current_index, &contract_ptr->producer_current_index);

  /* Set the consumer state to ACK received */
  cInst_ptr->consumer_state = TF_CONSUMER_ACK_RECIVED;  
  
  /* Increment the local last free buf index */
  //tf_contract_increment_var(temp1, cInst_ptr->buf_info.last_free_buf_index, cInst_ptr->buf_info.num_buf);
  cInst_ptr->buf_info.last_free_buf_index = cInst_ptr->buf_info.buf_filled_notsent_index;

  /* Update in the shared memory area of the contract */
  cInfo = (consumerInfo_t *) &contract_ptr->consumer_shared_info[cInst_ptr->myslot];  
  tf_contract_reg32_w(&cInst_ptr->buf_info.last_free_buf_index, &cInfo->last_free_buf_index);

  /* Write back the shared information in the contract */
  TF_CONTRACT_osalEndMemAccess(&contract_ptr->consumer_shared_info[cInst_ptr->myslot], sizeof (conSharedInfo_t));


  /* Update the global last free buf index for the producer */
  tf_cons_update_global_lfb(cHandle);  
  
  return (TF_CONSUMER_SUCCESS);
}


tf_consStatus_e tf_consDestroy(tf_consumer_HANDLE cHandle)
{
  tf_contract_t*                contract_ptr;
  tf_consInst_t*                cInst_ptr = (tf_consInst_t *)cHandle; 
  conSharedInfo_t               consumer_info;
  void*                         sharedMemAddr;
#ifdef TF_LINUX_USERSPACE  
  void*                         vaddr;
  uint32_t                      size;
#endif  
  uint32_t                      num_consumers;
  uint32_t                      magic_id;

  if (cHandle == NULL) {
  	return (TF_CONSUMER_INVALID_HANDLE);
  }

  TF_CONTRACT_osalEnter();  

  contract_ptr  = cInst_ptr->contract_ptr;
  sharedMemAddr = (void*) &contract_ptr->consumer_shared_info[cInst_ptr->myslot];

  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->consumer_shared_info[cInst_ptr->myslot], sizeof (conSharedInfo_t));
  tf_contract_reg32_r(&magic_id, &contract_ptr->consumer_shared_info[cInst_ptr->myslot].consumerInfo.magic_id);

  if (magic_id != (uint32_t)cHandle) {
  	  tfLObj.tf_errno = TF_ERRNO_INVALID_CONSUMER_HANDLE;	
      TF_CONTRACT_osalExit();  	  
	  return (TF_CONSUMER_INVALID_HANDLE);
  }

  /* Read the information from the contract memory */
  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->num_consumers, 128);  
  tf_contract_reg32_r(&num_consumers, &contract_ptr->num_consumers);

  num_consumers --;

  tf_contract_reg32_w (&num_consumers, &contract_ptr->num_consumers);
  TF_CONTRACT_osalEndMemAccess(&contract_ptr->num_consumers, 128);  

  /* Clear the consumer information in the contract */
  memset ((void *)&consumer_info, 0, sizeof (conSharedInfo_t));
  tf_contract_update_bytes(sharedMemAddr,&consumer_info,sizeof (conSharedInfo_t));  

#ifdef TF_LINUX_USERSPACE
  /* remove the ring buffer mapping done during create */
  vaddr     = (void*)  cInst_ptr->buf_info.buf_ptr[0];
  size      = cInst_ptr->buf_info.buf_size * cInst_ptr->buf_info.num_buf;
  TF_CONTRACT_osalUnmap(vaddr, size);
#endif

  TF_CONSUMER_osalMemFree((void*) cInst_ptr, TF_CONSUMER_INST_SIZE);

  TF_CONTRACT_osalExit();  

  return (TF_CONSUMER_SUCCESS);
}

tf_consumer_HANDLE tf_consCreate(tf_consConfig_t   *config)
{
  tf_consInst_t*                cInst_ptr; 
  tf_contract_t*                contract_ptr;
  tf_consLogBufInfo_t*          bufInfo_ptr;
  uint8_t*                      base_ptr;
  uint32_t                      i,glast_free_buf_index,num_consumers;
  conSharedInfo_t               consumer_info;
  tf_consumer_HANDLE            tf_cons_handle;
  uint32_t                      sz, temp, ring_base_type;
  int32_t                       slot_id;
  TF_CONTRACT_osalEnter();

  /* Check if contract handle is valid, otherwise return NULL */
  if (validate_contract_handle(config->contract_handle)) {
	return (NULL);
  }   
  
  if ( (config->sendFxn          == NULL)                 ||
       (config->contract_handle  == NULL) ) 
  {
    /* Un supported slot id, can not fit */
	tfLObj.tf_errno = TF_ERRNO_CONSUMER_INVALID_CONFIG_PARAMS;
    TF_CONTRACT_osalExit();  			
  	return NULL;
  }
  
  contract_ptr = (tf_contract_t*) tf_get_addr(config->contract_handle);

  memset((void*)&consumer_info, 0, sizeof (conSharedInfo_t));
  tf_cons_handle = TF_CONSUMER_osalMemAlloc(TF_CONSUMER_INST_SIZE, TF_CONSUMER_INST_ALIGN);

  if (tf_cons_handle == NULL) {
    TF_CONTRACT_osalExit();  
    tfLObj.tf_errno = TF_ERRNO_CONSUMER_ALLOC_FAIL;
    return (NULL);
  }

  cInst_ptr = (tf_consInst_t*)tf_cons_handle;

  /* Clear all the elements of consumer instance */
  memset (cInst_ptr, 0, sizeof (tf_consInst_t));

  bufInfo_ptr = &cInst_ptr->buf_info;  
    
  cInst_ptr->magicId                    = (uint32_t) tf_cons_handle; /* Use the consumer Handle for the MAGIC ID representation */
  cInst_ptr->param                      = config->param;
  cInst_ptr->sendFxn                    = config->sendFxn;
  cInst_ptr->contract_ptr               = contract_ptr;
  cInst_ptr->consumer_state             = CONSUMER_ACK_RECIVED;
  cInst_ptr->consumer_retry_count       = 0;
  cInst_ptr->streamFlag                 = 1; /* Always create the consumer for streaming producer */

  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->producer_current_index, 128);
  tf_contract_reg32_r(&bufInfo_ptr->myview_prod_current_index, &contract_ptr->producer_current_index);

  /* Get the contract Info */
  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->tf_contractInfo, 128);  
  tf_contract_reg32_r(&bufInfo_ptr->num_buf,  &contract_ptr->tf_contractInfo.num_buf);  
  tf_contract_reg32_r(&bufInfo_ptr->buf_size, &contract_ptr->tf_contractInfo.buffer_size);         
  tf_contract_reg32_r(&ring_base_type, &contract_ptr->tf_contractInfo.ring_base_type);

  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->cArea, 128);
  tf_contract_reg32_r(&glast_free_buf_index, &contract_ptr->cArea.global_last_free_buf_index);

 /* Get the pending buffer to be sent that is filled up by producer */ 
  bufInfo_ptr->buf_filled_notsent_index = glast_free_buf_index;
  tf_consumer_increment_var(&bufInfo_ptr->buf_filled_notsent_index, 1 ,bufInfo_ptr->num_buf);

  /* Get the last free Local index in that consumer */
  bufInfo_ptr->last_free_buf_index = glast_free_buf_index;

  /* Update the buffer pointers locally for fast accesses */
  sz = (bufInfo_ptr->buf_size * bufInfo_ptr->num_buf);
  
  tf_contract_reg32_r(&temp,(uint32_t *)&contract_ptr->tf_contractInfo.buffer_base);       
  base_ptr  = (uint8_t *)tf_get_addr(temp);

#ifdef TF_LINUX_USERSPACE  
  if (ring_base_type == TF_CONTRACT_RING_BUF_PHYSICAL_ADDR)
  {
  	 base_ptr = (uint8_t*) TF_CONTRACT_osalMmap(base_ptr, sz);
  }
#endif

  if (base_ptr == 0)
  {
#ifdef TF_LINUX_USERSPACE 
     printf ("tf_consCreate: mapping the ring buffer base (%p), with buffer size (%d) bytes, num_buf =%d failed \n", 
                                              base_ptr, 
                                              bufInfo_ptr->buf_size, 
                                              bufInfo_ptr->num_buf);
#endif
	 TF_CONTRACT_osalExit();   
	 tfLObj.tf_errno = TF_ERRNO_NULL_RING_BUFFER_FOUND;
     return (void*)0;
  }

  sz = bufInfo_ptr->buf_size;   

  if (bufInfo_ptr->num_buf > TF_CONSUMER_MAX_NUM_BUF)
  {
    TF_CONSUMER_osalMemFree((void*) cInst_ptr, TF_CONSUMER_INST_SIZE);
	tfLObj.tf_errno = TF_ERRNO_MAX_RING_BUFFER_REACHED;
    TF_CONTRACT_osalExit();  		
    return (NULL);
  }
  
  for (i=0; i<bufInfo_ptr->num_buf; i++)
  {
    bufInfo_ptr->buf_ptr[i]  = &base_ptr[i*sz];
  }

  /* Update the shared information to be filled into Contract */
  consumer_info.consumerInfo.last_free_buf_index = cInst_ptr->buf_info.last_free_buf_index;
  consumer_info.consumerInfo.magic_id            = cInst_ptr->magicId;
  switch (config->consumer_notify.notify_method)
  {
       case TF_CONSUMER_NOTIFY_VIA_REGMASK: 
        {
          consumer_info.consumerInfo.notify_method       = TF_CONSUMER_NOTIFY_VIA_REGMASK;
          consumer_info.consumerInfo.notify_reg_addr     = (void*) config->consumer_notify.notifyScheme.notify_reg.reg_addr;
          consumer_info.consumerInfo.notify_reg_mask     = config->consumer_notify.notifyScheme.notify_reg.reg_mask;
          consumer_info.consumerInfo.notify_callbk       = NULL;
        }
        break;
       case TF_CONSUMER_NOTIFY_VIA_LOCCBK:
        { 
          consumer_info.consumerInfo.notify_callbk       = config->consumer_notify.notifyScheme.notify_callbk;
          consumer_info.consumerInfo.notify_method       = TF_CONSUMER_NOTIFY_VIA_LOCCBK;
          consumer_info.consumerInfo.notify_reg_addr     = NULL;
          consumer_info.consumerInfo.notify_reg_mask     = (uint32_t)NULL;
        }
        break;
       case TF_CONSUMER_NOTIFY_VIA_LOCCBK2:
        { 
          consumer_info.consumerInfo.notify_callbk2      = config->consumer_notify.notifyScheme.notify_callbk2;
          consumer_info.consumerInfo.notify_method       = TF_CONSUMER_NOTIFY_VIA_LOCCBK2;
          consumer_info.consumerInfo.notify_reg_addr     = NULL;
          consumer_info.consumerInfo.notify_reg_mask     = (uint32_t)NULL;
        }
        break;		
       case TF_CONSUMER_NOTIFY_NONE:
        { 
          consumer_info.consumerInfo.notify_method       = TF_CONSUMER_NOTIFY_NONE;
        }
        break;        
       default:
    	   break;
  }

#ifdef TF_LINUX_USERSPACE
  consumer_info.consumerInfo.nameId[0] = 'A';
  consumer_info.consumerInfo.nameId[1] = 'R';
  consumer_info.consumerInfo.nameId[2] = 'M';
  consumer_info.consumerInfo.nameId[4] = '\0';
#else
  consumer_info.consumerInfo.nameId[0] = 'D';
  consumer_info.consumerInfo.nameId[1] = 'S';
  consumer_info.consumerInfo.nameId[2] = 'P';
  consumer_info.consumerInfo.nameId[4] = '\0';
#endif
  consumer_info.consumerInfo.nameId[3] = '0' + TF_PRODUCER_osalGetProcId();

  slot_id = getSlotNum(contract_ptr, (uint32_t) tf_cons_handle);

  if (slot_id < 0) {
     tfLObj.tf_errno = TF_ERRNO_MAX_NUM_CONSUMERS_REACHED;
 	 TF_CONSUMER_osalMemFree((void*) cInst_ptr, TF_CONSUMER_INST_SIZE);	
     TF_CONTRACT_osalExit();  		
   	 return (NULL);
  }		

  /* Put the information in the available slot of the contract */
  cInst_ptr->myslot = slot_id;
  tf_contract_update_smem(&contract_ptr->consumer_shared_info[slot_id],&consumer_info,sizeof (conSharedInfo_t));

  /* Indicate the count for this new consumer */
  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->num_consumers, 128);	
  tf_contract_reg32_r (&num_consumers, &contract_ptr->num_consumers);
  num_consumers ++; 
  tf_contract_reg32_w (&num_consumers, &contract_ptr->num_consumers);
  TF_CONTRACT_osalEndMemAccess(&contract_ptr->num_consumers, 128);
  
  TF_CONTRACT_osalExit();  
  return (tf_cons_handle);
    
}

/*
 * ============================================================================
 *  tf_contractGetInfo
 *
 *   brief
 *   gets number of consumers registered in the contract 
 *
 *  param[in]  hanle
 *      tf_contract handle
 *
 *  return
 *      number of consumers registered
 *
 * =============================================================================
 */
void tf_contractGetInfo(tf_contract_HANDLE handle, tf_contractGetInfo_t* info)
{
  tf_contract_t     *cPtr = (tf_contract_t*) tf_get_addr(handle);
  consumerInfo_t    *cInfo;  
  int               i;
  uint32_t          rBase = 0;
 
  if (info == NULL) {
    return;
  }

  /* Check if contract handle is valid */
  if (validate_contract_handle(handle)) {
    return;
  }	 

  TF_CONTRACT_osalBeginMemAccess(&cPtr->tf_contractInfo, 128);
  tf_contract_reg32_r(&info->contractState, &cPtr->tf_contractInfo.contractState); 
  tf_contract_reg32_r(&info->contractVersion,&cPtr->tf_contractInfo.contractVersion);
  tf_contract_reg32_r((uint32_t*) &info->contractType,(uint32_t *) &cPtr->tf_contractInfo.contractType);  
  tf_contract_reg32_r(&rBase,(uint32_t *)&cPtr->tf_contractInfo.buffer_base);  

  info->ringbuffer_base = (uint8_t *)tf_get_addr(rBase);
  
  tf_contract_reg32_r(&info->ringbuffer_size,&cPtr->tf_contractInfo.buffer_size);
  tf_contract_reg32_r((uint32_t *)&info->ring_base_type, (uint32_t *)&cPtr->tf_contractInfo.ring_base_type);  
  tf_contract_reg32_r(&info->num_ringbuf, &cPtr->tf_contractInfo.num_buf);
  tf_contract_reg32_r(&info->rb_wrap_flag, &cPtr->tf_contractInfo.rb_wrap_flag);    

  TF_CONTRACT_osalBeginMemAccess(&cPtr->num_consumers, 128); 
  tf_contract_reg32_r(&info->num_consumers,&cPtr->num_consumers);

  TF_CONTRACT_osalBeginMemAccess(&cPtr->producer_state, 128); 
  tf_contract_reg32_r(&info->producer_current_state,(uint32_t *) &cPtr->producer_state);  

  TF_CONTRACT_osalBeginMemAccess(&cPtr->overrun_count, 128);   
  tf_contract_reg32_r(&info->overrun_count,&cPtr->overrun_count);

  TF_CONTRACT_osalBeginMemAccess(&cPtr->producer_current_index, 128);   
  tf_contract_reg32_r(&info->producer_index,&cPtr->producer_current_index);  

  for ( i = 0; i < TF_MAX_CONSUMERS_PERCONTRACT; i++) {
  	cInfo = (consumerInfo_t *) &cPtr->consumer_shared_info[i];
    TF_CONTRACT_osalBeginMemAccess(cInfo, sizeof(conSharedInfo_t));
  	tf_contract_reg32_r(&info->consumerAppRetryCount[i],&cInfo->consumer_app_retry_count);
  }

  if (info->contractVersion == TF_CONTRACT_VERSION_1)
	  info->contractVersion = 1;
  else if (info->contractVersion == TF_CONTRACT_VERSION_2)
	  info->contractVersion = 2;
  else
	  info->contractVersion = TF_CONTRACT_VERSION_UNKNOWN;   

  return;  
}

/*
 * ============================================================================
 *  tf_contractCreate
 *
 *   brief
 *   sets up initial values for producer/consumer 
 *
 *  param[in]  tf_contractConfig
 *      tf_contract configuration structure
 *
 *  param[in]  name
 *      Pointer to the name of the tf_contract
 *
 *  return
 *      Contract Handle
 *
 * =============================================================================
 */
tf_contract_HANDLE tf_contractCreate(tf_contractConfig_t *tf_contractConfig, char* name) 
{
   return (tf_contract_create(tf_contractConfig, name));
}

tf_contract_HANDLE tf_contractFind (void* tf_contract_start_addr, uint32_t num_contracts, char* name) 
{
  return (tf_contract_find(tf_contract_start_addr, num_contracts, name));
}

/* back wards compatibility */
tf_contract_HANDLE TF_CONTRACT_CREATE(tf_contractConfig_t *tf_contractConfig, char* name) 
{
   return (tf_contract_create(tf_contractConfig, name));
}

void TF_CONTRACT_INIT(void* tf_contract_base, uint32_t num)
{
  tf_contract_init(tf_contract_base, num);
}

tf_contract_HANDLE tf_newContract(tf_contractConfig_t *tf_contractConfig) 
{
  return(tf_new_contract(tf_contractConfig));
}

tf_contract_HANDLE TF_NEW_CONTRACT(tf_contractConfig_t *tf_contractConfig) 
{
  return(tf_new_contract(tf_contractConfig));
}

tf_contract_HANDLE TF_CONTRACT_FIND (void* tf_contract_start_addr, uint32_t num_contracts, char* name) 
{
  return (tf_contract_find(tf_contract_start_addr, num_contracts, name));
}

tf_prodStatus_e tf_prodGetContractHandle(tf_producer_HANDLE pHandle, tf_contract_HANDLE *contract_handle)
{
   tf_prodInst_t*	  prodInst = (tf_prodInst_t*) pHandle;
   
   /* Producer is not yet created, so return the same buffer */
   if (pHandle == NULL) {
	  tfLObj.tf_errno = TF_ERRNO_INVALID_PRODUCER_HANDLE;	
	     return (TF_PRODUCER_INVALID_HANDLE);
   }
   
   *contract_handle = (tf_contract_HANDLE) tf_get_offset(prodInst->contract_ptr);

   /* Check if contract handle is valid */
   if (validate_contract_handle(contract_handle)) {
  	 return (TF_PRODUCER_INVALID_CONTRACT_HANDLE);
   } 
   
   return (TF_PRODUCER_PROD_SUCCESS);
}


/* Notify all the consumers in the contract */
void tf_prodNotifyConsumers(tf_producer_HANDLE pHandle)
{
  tf_prodInst_t*    prodInst;
  tf_contract_t*    contract_ptr;
  uint32_t          i, reg_addr, reg_mask, buf_index_start, buf_index_end, consumer_id ;
  int32_t           pStatus;
  void*             logBuf;
  uint32_t          numNotify;
  consumerInfo_t*   cInfo_ptr, *pCInfo_ptr;
  uint32_t          temp;  

  if (pHandle == NULL)
  	 return;
 
  prodInst = (tf_prodInst_t*) pHandle;

  contract_ptr = prodInst->contract_ptr;

  /* Update num of consumers information from the contract */
  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->num_consumers, 128);
  tf_contract_reg32_r(&prodInst->num_consumers, &contract_ptr->num_consumers);

  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->cArea, 128);
  tf_contract_reg32_r (&prodInst->glast_free_index, &contract_ptr->cArea.global_last_free_buf_index);

  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->producer_state, 128);  
  tf_contract_reg32_r (&temp, (uint32_t *) &contract_ptr->producer_state);
  prodInst->prodState = (tf_producerState_e)temp;  /* update the local state */

  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->producer_current_index, 128);
  tf_contract_reg32_r (&buf_index_start, &contract_ptr->producer_current_index);

  buf_index_end = prodInst->buf_info.current_index;

  /* Update the wrap around flag in the shared memory of the contract if needed (only once during wrap around */
  if (prodInst->rb_wrap_flag == 1) 
  {
     tf_contract_reg32_w(&prodInst->rb_wrap_flag,&contract_ptr->tf_contractInfo.rb_wrap_flag);
     TF_CONTRACT_osalEndMemAccess(&contract_ptr->tf_contractInfo, sizeof (tf_contractInfo_t) );
  }

 numNotify = tf_get_distance(buf_index_end, buf_index_start, prodInst->buf_info.num_buf);

 #ifdef TF_STREAM_FREEZE_PRODUCER_SUPPORT
  {  
	  if(prodInst->prodState == (tf_producerState_e) TF_PRODUCER_STATE_PROCESS_FREEZE_REQ)	
	  {
		/* Update last free index and prodState in contract */
		prodInst->prodState = TF_PRODUCER_STATE_FROZEN; /* set the producer state */
		temp = TF_PRODUCER_RESPONSE_STATE_ACK_FREEZE_REQ; /* Ack the consumer that request is honoured */
    	tf_contract_reg32_w(&temp, (uint32_t *) &contract_ptr->producer_state);		
		TF_CONTRACT_osalEndMemAccess(&contract_ptr->producer_state, 128);
	  } 
  }
#endif

  if (numNotify <= 0 )
	   return;	/* There is nothing to notify */

  /* Update the hit index in the shared memory of the contract */
  tf_contract_reg32_w (&prodInst->hit_by_slow_consumer_count, &contract_ptr->overrun_count);  
  TF_CONTRACT_osalEndMemAccess(&contract_ptr->overrun_count, 128);

  /* Check if any notifications need to be done for the consumers registered */
  while (numNotify > 0) {
   /* Service the posted notification */
   numNotify--;
   /* Find log buffer that needs to be cache invalidated */
   logBuf = prodInst->buf_info.buf_ptr[buf_index_start];
   TF_PRODUCER_osalEndMemAccess(logBuf, prodInst->buf_info.buf_size);

   buf_index_start++;
   buf_index_start %= prodInst->buf_info.num_buf; 
  }

  /* Update the current index in the shared memory of the contract */
  tf_contract_reg32_w (&buf_index_end, &contract_ptr->producer_current_index);
  TF_CONTRACT_osalEndMemAccess(&contract_ptr->producer_current_index, 128);

   /* post notify to registered  consumers, first update the local context */
   for (i = 0; i < TF_MAX_CONSUMERS_PERCONTRACT; i++) {
		 cInfo_ptr = (consumerInfo_t *)&contract_ptr->consumer_shared_info[i];
		 consumer_id = prodInst->consumer_notify_Info[i].magic_id;
     pCInfo_ptr  = &prodInst->consumer_notify_Info[i];
		 /* Update the producer's local cached information only if needed */
		 if (consumer_id == (uint32_t)NULL) {
       /* Read into local context to avoid future conversions */
       TF_CONTRACT_osalBeginMemAccess(cInfo_ptr, 128);       
       tf_contract_reg32_r(             &pCInfo_ptr->magic_id,                           &cInfo_ptr->magic_id                 );
       tf_contract_reg32_r(             &pCInfo_ptr->last_free_buf_index,                &cInfo_ptr->last_free_buf_index      );
       tf_contract_reg32_r(             &pCInfo_ptr->notify_method,                      &cInfo_ptr->notify_method            );       
       tf_contract_reg32_r(             &pCInfo_ptr->notify_reg_mask,                    &cInfo_ptr->notify_reg_mask          );       
       tf_contract_reg32_r(             &pCInfo_ptr->consumer_app_retry_count,           &cInfo_ptr->consumer_app_retry_count );
       tf_contract_reg32_r(             &pCInfo_ptr->ignoreFlag,                         &cInfo_ptr->ignoreFlag               );
       
       tf_contract_reg32_r((uint32_t *) &pCInfo_ptr->notify_reg_addr,       (uint32_t *) &cInfo_ptr->notify_reg_addr          );
       tf_contract_reg32_r((uint32_t *) &pCInfo_ptr->notify_callbk,         (uint32_t *) &cInfo_ptr->notify_callbk            );
       tf_contract_reg32_r((uint32_t *) &pCInfo_ptr->notify_callbk2,        (uint32_t *) &cInfo_ptr->notify_callbk2           );
		 }   	
   }

   /* Before processing local consumers, notify consumers with reg mask registered */
   for (i = 0; i < TF_MAX_CONSUMERS_PERCONTRACT; i++) 
   {
      consumerInfo_t*  cInfo;
      /* cInfo is now local instance */		 
      cInfo    = &prodInst->consumer_notify_Info[i];
    
      /* No consumers registered yet */
      if (cInfo->magic_id == (uint32_t)NULL)
        continue;
         		 
      if (cInfo->notify_method == NOTIFY_VIA_REGMASK) {
        reg_addr = (uint32_t) cInfo->notify_reg_addr;
        reg_mask = cInfo->notify_reg_mask;
        pStatus = tf_updateRegMask(reg_addr, reg_mask);
        if (pStatus != TF_PRODUCER_PROD_SUCCESS)
        {
          /* Raise any exception for notify failures */
          TF_PRODUCER_osalException(prodInst->prodType, TF_PRODUCER_EXCEPTION_NOTIFY_FAIL);
        }
      }
   	}

   /* Now process other methods */
   for (i = 0; i < TF_MAX_CONSUMERS_PERCONTRACT; i++) 
   {
      consumerInfo_t*  cInfo;
      /* cInfo is now local instance */		 
      cInfo    = &prodInst->consumer_notify_Info[i];   

      /* No consumers registered yet or it is already notified */
      if ((cInfo->magic_id == (uint32_t)NULL) || (cInfo->notify_method == NOTIFY_VIA_REGMASK) )
        continue;	  
 
 	 if (cInfo->notify_method == NOTIFY_VIA_LOCCBK)
 	 { 
        cInfo->notify_callbk(prodInst->buf_info.current_index);
      }
      else if (cInfo->notify_method == TF_CONSUMER_NOTIFY_VIA_LOCCBK2)
      { 
        cInfo->notify_callbk2((tf_consumer_HANDLE)cInfo->magic_id);
      }
 	 else if (cInfo->notify_method == NOTIFY_NONE)
 	 {
 	    /* Do nothing */
 	 }       
     else 
	 {
        /* Trap for no option case */
            TF_PRODUCER_osalException(prodInst->prodType, TF_PRODUCER_EXCEPTION_INVALID_NOTIFY);
     }     
   }   
   prodInst->notify_pend --; /* Update the statistics */
}



/************************* Update the consumers in local prod instance **********************/
int32_t tf_prodUpdateConsumers (tf_producer_HANDLE  pHandle)
{
  tf_prodInst_t*    prodInst = (tf_prodInst_t *) pHandle;
  tf_contract_t*    contract_ptr;
  uint32_t          i, num_consumers;
  consumerInfo_t*   cInfo_ptr, *pCInfo_ptr;  

  if (pHandle == NULL)
  	 return (-1);
  
  contract_ptr = prodInst->contract_ptr;

  /* Update num of consumers information from the contract */
  num_consumers = prodInst->num_consumers;
  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->num_consumers, 128); 
  tf_contract_reg32_r(&prodInst->num_consumers, &contract_ptr->num_consumers);

  /* Update the local producer instance to have all consumers registered from the contract, only when there is a change in consumers in the contract */
  for (i=0; (i < TF_MAX_CONSUMERS_PERCONTRACT) && (num_consumers != prodInst->num_consumers);i++)
  {
       cInfo_ptr = (consumerInfo_t *)&contract_ptr->consumer_shared_info[i];
       pCInfo_ptr = &prodInst->consumer_notify_Info[i];

       /* Read into local context to avoid future conversions */
       TF_CONTRACT_osalBeginMemAccess(cInfo_ptr, 128);       
       tf_contract_reg32_r(             &pCInfo_ptr->magic_id,                           &cInfo_ptr->magic_id                 );
       tf_contract_reg32_r(             &pCInfo_ptr->last_free_buf_index,                &cInfo_ptr->last_free_buf_index      );
       tf_contract_reg32_r(             &pCInfo_ptr->notify_method,                      &cInfo_ptr->notify_method            );       
       tf_contract_reg32_r(             &pCInfo_ptr->notify_reg_mask,                    &cInfo_ptr->notify_reg_mask          );       
       tf_contract_reg32_r(             &pCInfo_ptr->consumer_app_retry_count,           &cInfo_ptr->consumer_app_retry_count );
       tf_contract_reg32_r(             &pCInfo_ptr->ignoreFlag,                         &cInfo_ptr->ignoreFlag               );
       
       tf_contract_reg32_r((uint32_t *) &pCInfo_ptr->notify_reg_addr,       (uint32_t *) &cInfo_ptr->notify_reg_addr          );
       tf_contract_reg32_r((uint32_t *) &pCInfo_ptr->notify_callbk,         (uint32_t *) &cInfo_ptr->notify_callbk            );
       tf_contract_reg32_r((uint32_t *) &pCInfo_ptr->notify_callbk2,        (uint32_t *) &cInfo_ptr->notify_callbk2           );       
  }

  /* Check if the producer has any slots for the consumer */
  if (prodInst->num_consumers > TF_MAX_CONSUMERS_PERCONTRACT) {
  	tfLObj.tf_errno = TF_ERRNO_UNEXPECTED_NUM_CONSUMERS;
    return (-1);
  }

  return (prodInst->num_consumers);

}

/* Producer Destroy API */
tf_prodStatus_e tf_prodDestroy(tf_producer_HANDLE pHandle)
{
  tf_contract_t*        contract_ptr;
  tf_prodInst_t*        pInst = (tf_prodInst_t *) pHandle;
  uint32_t              loc_var;

  if (pHandle == NULL)
  	 return (TF_PRODUCER_INVALID_HANDLE);

  contract_ptr = pInst->contract_ptr;

  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->tf_contractInfo, sizeof (tf_contractInfo_t));

  tf_contract_reg32_r(&loc_var, &contract_ptr->tf_contractInfo.contractState);
  if (loc_var == TRACE_CONTRACT_AVAILABLE)
  {
    /* Contract is already available, possible mis use */
    return (TF_PRODUCER_PROD_FAIL);
  }

  loc_var = TRACE_CONTRACT_AVAILABLE;
  tf_contract_reg32_w  (&loc_var, &contract_ptr->tf_contractInfo.contractState);
  TF_CONTRACT_osalEndMemAccess(&contract_ptr->tf_contractInfo, sizeof (tf_contractInfo_t));
  
  loc_var =  pInst->buf_info.num_buf-1;
  tf_contract_reg32_w  (&loc_var, &contract_ptr->cArea.global_last_free_buf_index); 
  TF_CONTRACT_osalEndMemAccess(&contract_ptr->cArea, 128);
  
  loc_var = 0;
  tf_contract_reg32_w  (&loc_var,&contract_ptr->producer_current_index);
  TF_CONTRACT_osalEndMemAccess(&contract_ptr->producer_current_index, 128);  

  TF_PRODUCER_osalMemFree((void*) pInst, TF_PRODUCER_INST_SIZE);
  
  /* Success */
  return (TF_PRODUCER_PROD_SUCCESS);  
}


/* Set the OOB data in the contract */
void tf_prodSetOOBData(tf_contract_HANDLE cHandle, uint8_t* data)
{
  tf_contract_t* contract_ptr = (tf_contract_t*) tf_get_addr(cHandle);  
  tf_contract_update_bytes((void*)contract_ptr->oob_info, (void*)data , TF_CONTRACT_OOB_SIZE_BYTES);
  return;
}

/* Get the current ring buffer index in the producer */
void* tf_prodGetCurBuf(tf_producer_HANDLE pHandle)
{
  tf_prodInst_t*        pInst   = (tf_prodInst_t*)pHandle;

  if (pHandle == NULL)
  	 return (NULL);
  
  uint32_t cur_index            = pInst->buf_info.current_index;
  return (pInst->buf_info.buf_ptr[cur_index]);

}

/* Set the OOB data in the contract */
void tf_prodGetOOBData(tf_contract_HANDLE cHandle, uint8_t* data)
{
  tf_contract_t* contract_ptr = (tf_contract_t*) tf_get_addr(cHandle);  
  if (validate_contract_handle(cHandle)) {
  	return;
  }
  /* Read into local context to avoid future conversions */
  TF_CONTRACT_osalBeginMemAccess(&contract_ptr->oob_info[0], 128);         
  memcpy((void*)contract_ptr->oob_info, (void*)data , TF_CONTRACT_OOB_SIZE_BYTES);
  return;
}

/* Gen Producer Create API */
tf_producer_HANDLE tf_prodCreate
(
  tf_prodConfig_t   *config  /* Producer Configuration from Application */
)
{
  uint32_t            coreNum;
  //tf_contract_t       loc_contract;
  uint32_t            producer_state,  global_last_free_buf_index;
  uint32_t            buffer_size, buffer_base, ring_base_type; 
  
  tf_contract_t       *cPtr;  
  void*               newBuf;
  tf_prodInst_t*      pInst;
  tf_producer_HANDLE  tf_producer_handle;
  uint32_t            sz;
  uint8_t*            base_ptr;  
  int                 i, num_buf;

  if (validate_contract_handle(config->contract_handle)) {
  	return (NULL);
  }
  
  tf_producer_handle = TF_PRODUCER_osalMemAlloc(TF_PRODUCER_INST_SIZE, TF_PRODUCER_INST_ALIGN);

  if (tf_producer_handle == NULL) {
  	tfLObj.tf_errno = TF_ERRNO_PRODUCER_ALLOC_FAIL;
    return (NULL);
  }
  /* clear the buffer */
  memset(tf_producer_handle, 0, TF_PRODUCER_INST_SIZE);

  pInst = (tf_prodInst_t*)tf_producer_handle;

  cPtr  = (tf_contract_t *) tf_get_addr(config->contract_handle);

  if (cPtr == NULL) 
  {
    TF_PRODUCER_osalMemFree((void*) pInst, TF_PRODUCER_INST_SIZE);
	tfLObj.tf_errno = TF_ERRNO_INVALID_CONTRACT_HANDLE;
    /* Return failure since there is no contract available */
    return (NULL);
  } 

  coreNum = TF_PRODUCER_osalGetProcId();

  /* Snapshot the contract updates */
  // tf_contract_snapshot_smem((void*) cPtr, (void*) &loc_contract, sizeof (tf_contract_t));
  TF_CONTRACT_osalBeginMemAccess(cPtr,  sizeof (tf_contract_t));

  tf_contract_reg32_r(&producer_state, (uint32_t *) &cPtr->producer_state);
  tf_contract_reg32_r(&buffer_size, &cPtr->tf_contractInfo.buffer_size);
  tf_contract_reg32_r((uint32_t *) &num_buf, (uint32_t *) &cPtr->tf_contractInfo.num_buf);
  tf_contract_reg32_r(&buffer_base, (uint32_t *) &cPtr->tf_contractInfo.buffer_base);
  tf_contract_reg32_r(&ring_base_type, &cPtr->tf_contractInfo.ring_base_type);
  tf_contract_reg32_r(&global_last_free_buf_index, &cPtr->cArea.global_last_free_buf_index);
  
  pInst->prodState = (tf_producerState_e) producer_state;

  /* Get the memory maps */   
  sz = (buffer_size * num_buf);
#ifdef TF_LINUX_USERSPACE  
  if ((tf_ringBufAddrType_e)ring_base_type == TF_CONTRACT_RING_BUF_PHYSICAL_ADDR)
  {
  	base_ptr = (uint8_t*) TF_CONTRACT_osalMmap((void*)buffer_base, sz);
  }
  else
#endif  	
  {
  	base_ptr = (uint8_t *)tf_get_addr(buffer_base); /* Assuming Native Address Type for the Ring buffer */
  }
  
  if (base_ptr == 0)
  {
#ifdef TF_LINUX_USERSPACE
       printf ("tf_prodCreate: mapping the ring buffer base (%p), with buffer size (%d), num_buf =%d failed \n", (void *)buffer_base,buffer_size, num_buf);
#endif
	   TF_PRODUCER_osalMemFree((void*) pInst, TF_PRODUCER_INST_SIZE);
       return (void*)0;
  }

  if ( (num_buf <= 0)                        ||
       (num_buf > TF_PROD_MAX_NUM_BUF)       ||
       (buffer_size == 0)                    ||
       (base_ptr == 0))
  {
    TF_PRODUCER_osalMemFree((void*) pInst, TF_PRODUCER_INST_SIZE);
	tfLObj.tf_errno = TF_ERRNO_INVALID_CONTRACT_ELEMENTS;
    /* Return NULL since there is no valid ring buffer attributes passed in */
    return (NULL);
  }
  
  /* Copy the OOB data information from the System to the contract area */
  tf_prodSetOOBData((tf_contract_HANDLE)config->contract_handle, config->oob_info);

  /* Set the producer Instance */
  pInst->num_consumers          = 0;
  pInst->contract_ptr           = (tf_contract_t *) tf_get_addr(config->contract_handle); /* latch to the mapped one */
  pInst->magicId                = (config->prodType | coreNum << 29); 
  pInst->buf_info.num_buf       = num_buf;
  pInst->buf_info.buf_size      = buffer_size;
  pInst->prodType               = config->prodType;
  pInst->glast_free_index       = global_last_free_buf_index;
  pInst->buf_info.current_index = pInst->glast_free_index;
  pInst->buf_info.current_index ++;
  if (pInst->buf_info.current_index >= pInst->buf_info.num_buf)
  	pInst->buf_info.current_index = 0;  

#ifdef TF_LINUX_USERSPACE
  pInst->xchg_cb_fxn            =&tf_prodNotifyConsumers;
#else
  /* set to the exchange call back function from the app */
  pInst->xchg_cb_fxn            = config->xchg_cb_fxn;
#endif

#ifdef TF_SUP_LOGGERSTREAMER2
  if (config->prodType == UIA_MINST_PROD_MAGIC_ID)
  {
	/* set the producer handle as the context for the logger streamer2 */
	LoggerStreamer2_setContext((LoggerStreamer2_Handle) config->obj_handle, (UArg)pInst);
  }
#endif

  /* Update rest of the producer Instance values */
  /* Initialize all log buffers in this core with GEN headers */
  sz = buffer_size;
  for (i = 0; i < num_buf; i++) 
  {
    /* get each individual buffer in the array */
    pInst->buf_info.buf_ptr[i] = base_ptr + (i*sz);
#ifndef TF_LINUX_USERSPACE	
    if (config->prodType == UIA_PROD_MAGIC_ID)
    {
      uiaLogStrmBufferInit(pInst->buf_info.buf_ptr[i], coreNum);
    } 
#ifdef TF_SUP_LOGGERSTREAMER2
	else if (config->prodType == UIA_MINST_PROD_MAGIC_ID)
	{
      uiaLogStrm2BufferInit((LoggerStreamer2_Handle) config->obj_handle, pInst->buf_info.buf_ptr[i], coreNum);
	}
#endif	 /* TF_SUP_LOGGERSTREAMER2 */
#endif /* TF_LINUX_USERSPACE */

#ifdef TF_LINUX_USERSPACE	
    if (config->prodType == CUIA_PROD_MAGIC_ID)
    {
      cuiaBufferInit(pInst->buf_info.buf_ptr[i], coreNum,config->crcApp16);
    }    
#endif
	
  }  

#ifndef TF_LINUX_USERSPACE
  if (config->prodType == UIA_PROD_MAGIC_ID)
  {
    /* Store the instance handle created for UIA exchange context -- REMOVE Later*/ 
    uiaProdHandlePush(tf_producer_handle);
	
    /* Prime the buffer */
    newBuf = (void *) (pInst->buf_info.buf_ptr[pInst->buf_info.current_index]);
    tf_uiaProdRuntimePrime(newBuf);
  }
#ifdef TF_SUP_LOGGERSTREAMER2
  else if (config->prodType == UIA_MINST_PROD_MAGIC_ID)
  {
	/* Prime the buffer */
	newBuf = (void *) (pInst->buf_info.buf_ptr[pInst->buf_info.current_index]);
	tf_uiaProdRuntimePrime2((LoggerStreamer2_Handle) config->obj_handle, newBuf);
  }
#endif /* TF_SUP_LOGGERSTREAMER2 */
#endif /* TF_LINUX_USERSPACE */

#ifdef TF_LINUX_USERSPACE  
  if (config->prodType == CUIA_PROD_MAGIC_ID)
  {
	/* Store the instance handle created for UIA exchange context -- REMOVE Later*/ 
	cuiaProdHandlePush(tf_producer_handle);

    /* Prime the buffer */
    newBuf = (void *) (pInst->buf_info.buf_ptr[pInst->buf_info.current_index]);
    tf_cuiaProdRuntimePrime(newBuf, pInst->buf_info.buf_size);
  }    
#endif  

  /* Write back all the initialized ring buffers in the shared memory */
  TF_PRODUCER_osalEndMemAccess(pInst->buf_info.buf_ptr[0], (pInst->buf_info.buf_size * pInst->buf_info.num_buf));

  /* Success */
  return (tf_producer_handle);
  
}

Ptr tf_prodBufExchange (tf_producer_HANDLE pHandle, uint8_t *full)
{
  tf_prodInst_t*        prodInst;
  uint32_t              glast_free_index;  
  void*                 newBuf;
  uint32_t              current_index;
  uint32_t              coreNum, temp;
  uint32_t              pMagicId;  

  coreNum = TF_PRODUCER_osalGetProcId();
  prodInst = (tf_prodInst_t*) pHandle;

  /* Producer is not yet created, so return the same buffer */
  if (pHandle == NULL) {
  	tfLObj.tf_errno = TF_ERRNO_INVALID_PRODUCER_HANDLE;  	
  	return ((Ptr) full);
  }

  pMagicId =   ( GEN_PROD_MAGIC_ID | CUIA_PROD_MAGIC_ID | UIA_PROD_MAGIC_ID | UIA_MINST_PROD_MAGIC_ID | (coreNum << 29) );

  temp     = pMagicId & prodInst->magicId;

  /* Producer is not yet educated about the ring buffer OR no consumers yet*/
  if ( (temp == 0) || 
  	   (prodInst->num_consumers == 0) )
  {
    /* return the same buffer created during prime for next exchange */
     return ((Ptr) full);
  }
   
  current_index = prodInst->buf_info.current_index;
  
  glast_free_index = prodInst->glast_free_index;

  if (current_index == prodInst->buf_info.num_buf-1)
   	 prodInst->rb_wrap_flag += 1;
  
#ifdef TF_STREAM_FREEZE_PRODUCER_SUPPORT
  if ( (prodInst->prodState == (tf_producerState_e) TF_PRODUCER_STATE_PROCESS_FREEZE_REQ)  	 ||
  	   (prodInst->prodState == TF_PRODUCER_STATE_FROZEN) )
  {
    /* Consumer is demanding producer to freeze, always return the current buffer */
	newBuf = (void *) full;
  }
  else
#endif  	
  if (( prodInst->prodState == TF_PRODUCER_STATE_STREAMING ) &&
  	       ( current_index == glast_free_index) )
  {
     /* Increment the nullBufCount */
     prodInst->hit_by_slow_consumer_count++;
     newBuf = (void*) full; 
  }
  /* In case of prodState == UIA_PROD_STATE_FREE_RUNNING, always returns a new buffer */  
  else
  {

    /* increment current buffer in local producer Instance */
    tf_contract_increment_var(temp, current_index, prodInst->buf_info.num_buf);
    
    /* provide the next available buffer for the UIA producer */
    newBuf = (void *) (prodInst->buf_info.buf_ptr[current_index]);

	prodInst->buf_info.current_index = current_index;

	prodInst->notify_pend ++;
    
   }

  if (prodInst->xchg_cb_fxn!= NULL)
  	prodInst->xchg_cb_fxn((tf_producer_HANDLE) prodInst);

  /* Return the new buffer to the UIA Producer */
  return ((void*) newBuf);
}

/***********************************************************************************
 * FUNCTION PURPOSE: Adds TraceFramework configuration
 ***********************************************************************************
 * DESCRIPTION: This function needs to be called as very first Trace Framework function 
 *           from all cores to initialize TraceFramework with 
 *           per core configurations
 ***********************************************************************************/
void tf_systemCfg (tf_StartCfg_t *startCfg)
{
    if (startCfg == NULL)
		return;
	
    tfLObj.cfg = *startCfg;
	tfLObj.tf_errno = 0;
}

/* Nothing past this point */
