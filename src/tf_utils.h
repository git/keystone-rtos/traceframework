/**
 *   @file  tf_utils.h
 *
 *   @brief   
 *      This has defintions internal utility functions for traceframework.
 *
 *  ============================================================================
 *  @n   (C) Copyright 2012-2013, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef __TF_UTILS_H_
#define __TF_UTILS_H_

#ifdef __cplusplus
extern "C" {
#endif


#include <string.h>
#include "contract_osal.h"
#include "consumer.h"

/*
 * ============================================================================
 *  tf_contract_reg32_r
 *
 *  Description: reads a 32 bit value from the tf_contract to the local variable ptr
 *
 *  param[in]  localVarPtr
 *      Pointer to the local variable
 *
 *  param[in]  globalVarPtr
 *      Pointer to the global variable
 *
 *  return
 *      none
 *
 * =============================================================================
 */
static void tf_contract_reg32_r(uint32_t* localVarPtr, uint32_t* globalVarPtr);    

/*
 * ============================================================================
 *  tf_contract_reg32_w
 *
 *  Description: write a 32 bit value from the local variable to the global memory
 *
 *  param[in]  localVarPtr
 *      Pointer to the local variable
 *
 *  param[in]  globalVarPtr
 *      Pointer to the global variable
 *
 *  return
 *      none
 *
 * =============================================================================
 */
static void tf_contract_reg32_w(uint32_t* localVarPtr, uint32_t* globalVarPtr);
                                                        

/*
 * ============================================================================
 *  tf_contract_update_bytes
 *
 *  Description:
 *   write a size bytes to global memory
 *
 *  param[in]  sharedMemAddr
 *      Pointer to the local variable
 *
 *  param[in]  localMemAddr
 *     Pointer to the global variable
 *
 *  param[in]  size
 *      Pointer to the global variable
 *
 *  return
 *      none
 *
 * =============================================================================
 */
static void tf_contract_update_bytes(void* sharedMemAddr, void* localMemAddr, size_t size);    

/*
 * ============================================================================
 *  tf_contract_update_smem
 *
 *  Description:
 *    write a size bytes to global memory
 *
 *  param[in]  sharedMemAddr
 *      Pointer to the local variable
 *
 *  param[in]  localMemAddr
 *      Pointer to the global variable
 *
 *  param[in]  size
 *      Pointer to the global variable
 *
 *  return
 *      none
 *
 * =============================================================================
 */
static void tf_contract_update_smem(void* sharedMemAddr, void* localMemAddr, size_t size);    

/*
 * ============================================================================
 *  tf_cons_update_global_lfb
 *
 *  Description:
 *   increment a variable
 *
 *  param[in]  localVar
 *      local variable
 *
 *  param[in]  globalVar
 *     global variable to be incremented
 *
 *  param[in]  maxSize
 *     decrement value
 *
 *  return
 *      none
 *
 * =============================================================================
 */

static void   tf_cons_update_global_lfb(tf_consumer_HANDLE cHandle);

/*
 * ============================================================================
 *  tf_contract_increment_var
 *
 *  Description:
 *   increment a variable
 *
 *  param[in]  localVar
 *      local variable
 *
 *  param[in]  globalVar
 *     global variable to be incremented
 *
 *  param[in]  maxSize
 *     Increment value
 *
 *  return
 *      none
 *
 * =============================================================================
 */
#define tf_contract_increment_var(localVar, globalVar, maxSize) \
{                                                 \
  localVar = globalVar;                           \
  localVar++;                                     \
  if(localVar >= maxSize)                         \
    localVar = 0;                                 \
  globalVar = localVar;                           \
}

/*
 * ============================================================================
 *  tf_contract_decrement_var
 *
 *  Description:
 *   increment a variable
 *
 *  param[in]  localVar
 *      local variable
 *
 *  param[in]  globalVar
 *     global variable to be incremented
 *
 *  param[in]  maxSize
 *     decrement value
 *
 *  return
 *      none
 *
 * =============================================================================
 */
#define tf_contract_decrement_var(localVar, globalVar, maxSize) \
{                                                 \
  localVar = globalVar;                           \
  localVar--;                                     \
  if(localVar < 0)                                \
    localVar = maxSize;                           \
  globalVar = localVar;                           \
}


#ifdef __cplusplus
}
#endif

#endif /* __TF_UTILS_H_ */
/* Nothing past this point */    
