/**
 *   @file  cuia_exchange.c
 *
 *   @brief   
 *      CUIA buffer exchange implementation..
 *
 *  ============================================================================
 *  @n   (C) Copyright 2012-2013, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/* Standard Include Files. */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>

#include "ti/instrumentation/traceframework/producer.h"
#include "ti/instrumentation/traceframework/src/cuia_prodif.h"

static tf_producer_HANDLE pInstCore[MAX_NUM_CUIA_CONTEXT];

/*
 *  Description:  
 *      Utility function which initializes the buffer for CUIA.
 *
 *  param[in]  buffer
 *      buffer address
 *
 *  param[in]  id
 *      id to be put in the buffer
 *
 *  retval
 *      None
 */

void cuiaBufferInit(Ptr buffer,uint32_t id, uint32_t crcApp16)
{
  LoggerStreamer_initBuffer(buffer, id, crcApp16);
}

void cuiaProdHandlePush(tf_producer_HANDLE handle)
{
    pInstCore[0] = handle;
}


/*
 *  Description
 *     Utility function returns the UIA producer handle locally.
 *
 *  param[in]  
 *     None
 *
 *  retval
 *      tf_producer_HANDLE
 */

tf_producer_HANDLE cuiaProdHandlePop(void)
{
    return(pInstCore[0]);
}

/*
 *  Description
 *      Utility function which initializes the prime for CUIA.
 *
 *  param[in]  buffer
 *      buffer address
 *
 *  retval
 *      None
 */

void tf_cuiaProdRuntimePrime(Ptr buffer, uint32_t len)
{
  LoggerStreamer_init((UInt32*) buffer, len);   
}

/* Called when LoggerStream is full.
   Input:  ptr to buffer that is full.
   Return: ptr to new buffer
*/
Ptr bufferExchange(Char *full)
{
    Ptr newBuf;
    newBuf = (Ptr) tf_cuiaProducerBufExchange(cuiaProdHandlePop(), (uint8_t*) full);
    return (newBuf);
}

Ptr tf_cuiaProducerBufExchange (tf_producer_HANDLE pHandle, uint8_t *full)
{
  Ptr                   newBuf;
  
  newBuf = tf_prodBufExchange(pHandle, full);

  /* Return the new buffer to the UIA Producer */
  return ((Ptr) newBuf);
}
/* Nothing past this point */
