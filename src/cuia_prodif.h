/**
 *   @file  cuia_prodif.h
 *
 *   @brief   
 *      This is a very slim cuia ring producer framework library implementation.
 *
 *  ============================================================================
 *  @n   (C) Copyright 2012, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef __CCUIA_PRODUCER_H__
#define __CCUIA_PRODUCER_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <ti/uiaplus/loggers/stream/LoggerStreamer.h>
#include "ti/instrumentation/traceframework/trace_contract.h"

/*
 *  tempory definitions: would not be required when cuia has the instance during exchange
 */
#define  MAX_NUM_CUIA_CONTEXT            64

/** @addtogroup TRACEFRAMEWORK_CUIA_FUNCTIONS
@{
*/

/**
 * ============================================================================
 *  @n@b tf_cuiaProducerBufExchange
 *
 *  @b  brief
 *  @n  buffer exchange function for the cuia producer 
 *
 *  @param[in]  prod_handle
 *      producer handle 
 *
 *  @param[in]  full
 *      filled log buffer 
 *
 *  @return
 *      free log buffer 
 *
 * =============================================================================
 */
extern Ptr tf_cuiaProducerBufExchange (void* prod_handle, uint8_t* full);

/**
 * ============================================================================
 *  @n@b tf_cuiaProdPrime
 *
 *  @b  brief
 *  @n  buffer prime function for the cuia producer 
 *      This function initializes the CUIA module/buffers. 
 *      Called during XDC startup...well before main() 
 *
 *  @return
 *      Returns the ptr to the first log buffer to be used by CUIA for this core
 *
 * =============================================================================
 */
extern Ptr   tf_cuiaProdPrime(void);

/**
 * ============================================================================
 *  @n@b tf_cuiaProdRuntimePrime
 *
 *  @b  brief
 *  @n  buffer prime function for the cuia producer 
 *      This function initializes the CUIA module/buffers. 
 *      Called during the producer create after main() 
 *
 *  @param[in]  newBuf
 *      new log buffer 
 *
 *  @param[in]  len
 *      new log buffer length
 *
 *  @return
 *      Returns the ptr to the first log buffer to be used by CUIA for this core
 *
 * =============================================================================
 */
extern void             tf_cuiaProdRuntimePrime(Ptr  newBuf, uint32_t len);

/*
 * ============================================================================
 *  cuiaBufferInit
 *
 *  brief: buffer inits for the CUIA logger streamer before it actually starts logging 
 *
 *  param[in]  buffer
 *      new log buffer 
 *
 *  param[in]  id
 *      id to be put in the buffer
 *
 *  param[in]  crcApp16
 *      Unique Application CRC ID
 *  return
 *      None
 *
 * =============================================================================
 */
extern void             cuiaBufferInit(Ptr buffer, uint32_t id, uint32_t crcApp16);
extern void cuiaProdHandlePush(tf_producer_HANDLE handle);
extern tf_producer_HANDLE cuiaProdHandlePop( void );
extern Ptr bufferExchange(Char *full);

/**
@}
*/

#ifdef __cplusplus
}
#endif
#endif /* __CCUIA_PRODUCER_H__ */
/* Nothing past this point */

