/**
 *   @file  uia_exchange.c
 *
 *   @brief   
 *      UIA transport mechanism implementations
 *
 *  ============================================================================
 *  @n   (C) Copyright 2012, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/* Standard Include Files. */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>

#include "ti/instrumentation/traceframework/src/uia_prodif.h"

static tf_producer_HANDLE pInstCore[MAX_NUM_UIA_CONTEXT];

/* Get the LoggerStreamer Context for the application */
tf_producer_HANDLE tf_getLoggerStreamerContext(LoggerStreamer_Handle lstHandle)
{
    uint32_t  coreNum;
	tf_producer_HANDLE pHandle;
	
	coreNum = TF_PRODUCER_osalGetProcId();
	pHandle = (tf_producer_HANDLE) pInstCore[coreNum];

	return (pHandle);
}

/* Get the LoggerStreamer2 Context for the application */
tf_producer_HANDLE tf_getLoggerStreamer2Context(LoggerStreamer2_Handle lst2Handle)
{
	tf_producer_HANDLE pHandle;
	
	pHandle = (tf_producer_HANDLE)LoggerStreamer2_getContext(lst2Handle);

	return (pHandle);
}

/*
 *  Description
 *     Utility function which initializes the buffer for UIA.
 *
 *  param[in]  buffer
 *      buffer address
 *
 *  param[in]  id
 *      id to be put in the buffer
 *
 *  retval
 *      None
 */

void uiaLogStrmBufferInit(Ptr buffer,uint32_t id)
{
  LoggerStreamer_initBuffer(buffer, id);
}

/*
 *  Description
 *     Utility function stores the UIA producer handle locally.
 *
 *  param[in]  handle
 *      trace framework producer handle
 *
 *
 *  retval
 *      None
 */

void uiaProdHandlePush(tf_producer_HANDLE handle)
{
    uint32_t procId;
	procId = TF_PRODUCER_osalGetProcId();
    pInstCore[procId] = handle;
}


/*
 *  Description
 *     Utility function returns the UIA producer handle locally.
 *
 *  param[in]  
 *     None
 *
 *  retval
 *      tf_producer_HANDLE
 */

tf_producer_HANDLE uiaProdHandlePop(void)
{
    uint32_t procId;
	procId = TF_PRODUCER_osalGetProcId();
    return(pInstCore[procId]);
}

/*
 *  Description
 *     Utility function which initializes the prime for UIA.
 *
 *  param[in]  buffer
 *      buffer address
 *
 *  retval
 *      None
 */

void tf_uiaProdRuntimePrime(Ptr buffer)
{
  LoggerStreamer_prime(buffer);   
}

#ifdef TF_SUP_LOGGERSTREAMER2

void uiaLogStrm2BufferInit(LoggerStreamer2_Handle handle,Ptr buffer,uint32_t id)
{
  LoggerStreamer2_initBuffer(handle, buffer, id);
}

/*
 *  Description
 *     Utility function which initializes the prime for UIA multi instance support.
 *
 *  param[in]  handle
 *      Logger stramer2 handle
 *
 *  param[in]  buffer
 *      buffer address
 *
 *  retval
 *      None
 */

void tf_uiaProdRuntimePrime2(LoggerStreamer2_Handle handle, Ptr buffer)
{
  LoggerStreamer2_prime(handle, buffer);   
}
/* Called when LoggerStream2 is full.
   Input:  Logger Streamer2 handle and ptr to buffer that is full.
   Return: ptr to new buffer
*/
Ptr tf_uiaProducerBufExchange2(LoggerStreamer2_Handle handle, uint8_t *full)
{
	tf_producer_HANDLE	  pHandle;
	Ptr  				  newBuf;
	
	pHandle = (tf_producer_HANDLE*) tf_getLoggerStreamer2Context(handle);
	
	newBuf = tf_prodBufExchange(pHandle, full);
	
	/* Return the new buffer to the multi instance UIA Producer */
	return ((Ptr) newBuf);

}
#endif

/* Called when LoggerStream is full.
   Input:  ptr to buffer that is full.
   Return: ptr to new buffer
*/
Ptr tf_uiaProducerBufExchange (uint8_t *full)
{
  tf_producer_HANDLE    pHandle;
  Ptr                   newBuf;

  pHandle = uiaProdHandlePop();

  newBuf = tf_prodBufExchange(pHandle, full);

  /* Return the new buffer to the UIA Producer */
  return ((Ptr) newBuf);
}

/* Nothing past this point */

