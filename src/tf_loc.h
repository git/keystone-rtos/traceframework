/**
 *   @file  trace_contract.h
 *
 *   @brief   
 *      This has defintions and implementations for the contract between producers and consumers.
 *
 *  ============================================================================
 *  @n   (C) Copyright 2012, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef CONSPROD_CONTRACT_LOC_H_
#define CONSPROD_CONTRACT_LOC_H_

#ifdef __cplusplus
extern "C" {
#endif


#include <string.h>

/* OSAL Includes */
#include "contract_osal.h"
#include "producer.h"

/*
 * @brief   maximum number of buffers in the ring handled by the trace framework's producer instance
 */
#define TF_PROD_MAX_NUM_BUF             TF_MAX_RING_BUFS_SUPPORT

/*
 * @brief   Maximum number of buffers in the ring handled by the Trace Framework's consumer instance 
 */
#define TF_CONSUMER_MAX_NUM_BUF         TF_MAX_RING_BUFS_SUPPORT

#define DEVICE_REG32_W(x,y)   *(volatile unsigned int *)(x)=(y)
#define DEVICE_REG32_R(x)    (*(volatile unsigned int *)(x))

/* TF Local object */
typedef struct {
  tf_StartCfg_t cfg;
  int32_t       tf_errno;
} Tf_LocalObj;


extern Tf_LocalObj tfLObj;

#define tf_get_offset(addr) ((uint32_t) addr - (uint32_t) tfLObj.cfg.instPoolBaseAddr)
#define tf_get_addr(offset) ((uint32_t) tfLObj.cfg.instPoolBaseAddr + (uint32_t) offset)


/*
 * @brief Contract Element Structure 128 bit elements
 */
typedef struct ctractInfo {
    uint32_t              contractType;
    /**< tf_contract type */
    uint32_t              contractState;
    /**< tf_contract state */
    uint32_t              contractVersion;
    /**< tf_contract version */
    uint8_t*              buffer_base;    
    /**< ring buffer base for the tf_contract */
    uint32_t              num_buf;    
    /**< number of ring buffers */
    uint32_t              buffer_size;  
    /**< ring buffer size */
	uint32_t              rb_wrap_flag;
	/**<  indication if the ring buffer is wrapped around or not */
	uint32_t              ring_base_type; 
	/**< is the ring buffer base address configured virtual Or physical @ref tf_ringBufAddrType_e */	
	uint32_t              magic_word;
	/**< Magic word 0xbabeface checked to see if the contract is initialized */
} tf_contractInfo_t;

/*
 * @brief Consumer Information Structure
 */
typedef struct consInfo{
    uint32_t magic_id; 
    /**< Should not be zero, zero indicates no owner */

    uint32_t last_free_buf_index;
    /**< The consumer's last free buf index for the ring buffer */   

    void*  notify_reg_addr;
    /**< notify register address for the consumer if any */

    uint32_t notify_reg_mask;
    /**< Notify register address mask for the consumer if any */

    uint32_t notify_method;     
    /**< notify method showing if call back option is used or not */
    
    int  (*notify_callbk)(uint32_t ringbuf_index);   
    /**< local call back function for the consumer task - @n@b WARNING: @n No bios calls are allowed within the local call back function since
               this function is called from buffer write finish context of UIA, which needs interrupts disabled */
    int  (*notify_callbk2)(void* local_handle);	 
		/**< local call back2 function for the consumer task - @n@b WARNING: @n No bios calls are allowed within the local call back function since
				   this function is called from buffer write finish context of UIA, which needs interrupts disabled */ 
    uint32_t  consumer_app_retry_count;
		/** < number of times consumer send function could not consume the buffers */

    char     nameId[TF_MAX_CONSUMERS_PERCONTRACT + 1];
		/** < consumer core Id for this slot, helpful for debug purposes */
    uint32_t ignoreFlag; 
		/** < Flag to indicate ignore global last free contribution */
 } consumerInfo_t;

/*
 * @brief Consumer Shared information in the tf_contract
 */
typedef union e5
{
    consumerInfo_t     consumerInfo;
    /**< Consumer shared information in the tf_contract */
    uint8_t            pad[TF_CONTRACT_CACHE_LINEZ];
} conSharedInfo_t;   

/*
 * @brief Producer/Consumer Shared variables in the tf_commonArea_t
 */
typedef struct 
{
    uint32_t           global_last_free_buf_index; /**< index of the slowest consumer in the ring */ 
    uint8_t            pad3[TF_CONTRACT_CACHE_LINEZ - sizeof (uint32_t)];	
} tf_commonArea_t;

/*
 * @brief Each tf_contract element would be aligned to 128 bytes 
 *        Assumptions: 
 *        All the elements for the tf_contract needs to be uint32_t except the tf_contract name
 *        Do not change the order the elements in the contract area 
 */
 typedef struct tf_contract{
    char               name[TF_CONTRACT_NAME_SIZE_BYTES];   /**< name of the tf_contract - @n@b WARNING: @n deprecated with contract version TRACE_CONTRACT_VERSION_2 */
	
    tf_contractInfo_t  tf_contractInfo;     /**< tf_contract information describing the ring buffer and type */ 
	uint8_t            pad1[TF_CONTRACT_CACHE_LINEZ- sizeof (tf_contractInfo_t)];

   
    uint32_t           producer_current_index;     /**< producer's current index (head) for the ring buffer */      
    uint8_t            pad2[TF_CONTRACT_CACHE_LINEZ - sizeof (uint32_t)];
	
    tf_commonArea_t    cArea; /**< Producer and Consumer Common shared area */
	
    uint32_t           num_consumers;     /**< number of consumers registered in the tf_contract */
    uint8_t            pad4[TF_CONTRACT_CACHE_LINEZ - sizeof (uint32_t)];
	
    conSharedInfo_t    consumer_shared_info[TF_MAX_CONSUMERS_PERCONTRACT];     /**< List describing consumer's shared information */
	
    uint8_t            oob_info[TF_CONTRACT_OOB_SIZE_BYTES];  /**< 128 bytes of out of band data provided for application, 
                                                                    trace framework does not alter information in this area */
	
    uint32_t           overrun_count;     /**< producer's over run count on the ring buffers */
    uint8_t            pad5[TF_CONTRACT_CACHE_LINEZ - sizeof (uint32_t)];

    int32_t            producer_state; /**< Reflects prodcuer state Applicable for Stream/Freeze contract types */
	uint8_t            pad3[TF_CONTRACT_CACHE_LINEZ - sizeof (uint32_t)];
	
  } tf_contract_t;


#define DEVICE_REG32_W(x,y)   *(volatile unsigned int *)(x)=(y)
#define DEVICE_REG32_R(x)     (*(volatile unsigned int *)(x))

/*
 * @brief   Producer Instance Buffer Requirements
 */

#define TF_PRODUCER_INST_SIZE      sizeof (tf_prodInst_t)

/*
 * @brief   Producer Instance Buffer Alignment Requirements
 */
#define TF_PRODUCER_INST_ALIGN     128

/*
 * @brief Structure with log buffer information for each core 
 */
typedef struct  prodLogBufInfo
{
  uint32_t  current_index;
  /**< index for the current buffer that is being logged */  

  uint32_t  buf_size;
  /**< Payload Size */  

  uint32_t  num_buf;
  /**< Number of buffers */  

  uint8_t* buf_ptr[TF_PROD_MAX_NUM_BUF];
  /**< Array of pointers to log buffers for this instance */  
} tf_prodLogBufInfo_t;

/*
 * @brief Structure with Trace Framework Producer instance Structure
 */
typedef struct prodInst {
  uint32_t          magicId;
  /**< Producer Identification field */  
  
  tf_prodLogBufInfo_t   buf_info;
  /**< producer Log Buf information */  
  
  tf_contract_t*    contract_ptr;
  /* Contract memory pointer associated for this producer ring */
  
  uint32_t          num_consumers;
  /**< total number of consumers attached to the producer ring */
  
  consumerInfo_t    consumer_notify_Info[TF_MAX_CONSUMERS_PERCONTRACT];
  /**< Information of the consumers attached to the ring */
  
  uint32_t          hit_by_slow_consumer_count;  
  /**< total number of times producer did not get a new buffer */  

  uint32_t          rb_wrap_flag;  
  /**< flag that will be set when the ring buffer (buf_info.current_index) wraps for the first time */ 

  tf_producerTypes_e    prodType;  
  /**< producer type */  

  uint32_t           glast_free_index;
  /**< snap shot of the global last free index */

  int32_t            notify_pend;
  /**< number of pending notifications */

  void           (*xchg_cb_fxn)(tf_producer_HANDLE phandle);
  /**< buffer exchange call back function for application to trigger
             the notifications to the registered consumers by calling 
             tf_prodNotifyConsumers() function */

  tf_producerState_e    prodState;  
  /**< producer state */               

} tf_prodInst_t;


/*
 * @brief   Consumer Instance Buffer Requirements 
 */
#define TF_CONSUMER_INST_SIZE        sizeof (tf_consInst_t)

/*
 * @brief   Consumer Instance Buffer Alignment Requirements 
 */
#define TF_CONSUMER_INST_ALIGN       128

/*
 * @brief  Structure with log buffer information for each core 
 */
typedef struct  consLogBufInfo
{
  uint32_t  last_free_buf_index;
  /**< index for the last buffer that was freed */

  uint32_t  buf_filled_notsent_index;  
  /**< index for the buffer filled but not sent index */  

  uint32_t  myview_prod_current_index;
  /**< index for the current buffer that is being logged from consumers view */  
  
  uint32_t  num_buf;
 /**< Number of buffers */

   uint32_t  buf_size;
 /**< buffer size in the ring */
    
  uint8_t* buf_ptr[TF_CONSUMER_MAX_NUM_BUF];
 /**< individual ring buffer pointers */
 
} tf_consLogBufInfo_t;

/*
 * @brief Structure with uia transport consumer instance 
 */
typedef struct  consInst
{
  uint32_t                  magicId;
  /**< magic ID of the consumer */
  uint32_t                  myslot;
  /**< consumers slot in the contract memory layout */
  tf_consAppSendRespStatus_e   (*sendFxn)(uint32_t param, uint8_t* buf, uint32_t length);
  /**< send function, which sends the buffer to the application */
  tf_consLogBufInfo_t          buf_info;
  /**< consumer's buffer information structure */
  tf_contract_t*            contract_ptr;
  /**< contract handle for the consumer */
  uint32_t                  param;
  /**< parameter value used by the application */
  uint32_t                  consumer_state;
  /**< Each call to sendFxn() sets the flag to Busy, each tf_consAck() 
     sets it back to Free. tf_consProcess will just return without calling 
     notifyData if the consumer state is Busy */
  uint32_t                  consumer_retry_count;
  /**< consumer retry count */ 
  uint32_t                  streamFlag;
  /**< indicates what consumers have configured producer for (ring buffer streaming or not) */
  uint32_t                  trigFlag;
  /**< trigger consuming producer buffers when it is in frozen state */ 
  
} tf_consInst_t;


#ifdef __cplusplus
}
#endif

#endif /* CONSPROD_CONTRACT_LOC_H_ */
/* Nothing past this point */    
