/**
 *   @file  uia_prodif.h
 *
 *   @brief   
 *      This is a very slim uia ring producer framework library implementation.
 *
 *  ============================================================================
 *  @n   (C) Copyright 2012, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef __UIA_PRODUCER_H__
#define __UIA_PRODUCER_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifndef TF_LINUX_USERSPACE
/*
 * Uia producer functions
 *
 * Defines the Functions, Data Structures, Enumerations and Macros
 * within UIA Producer Infrastructure. Producers communicate to consumers
 * via the contract memory. Please refer to "contract_t" structure for
 * the contract memory layout
 *
 */

/* Include legacy single instance LoggerStreamer Interface */
#include "ti/uia/sysbios/LoggerStreamer.h"

/* Interface for the multi-instance Loggerstreamer support */
#include <ti/uia/sysbios/LoggerStreamer2.h> 


#include "ti/instrumentation/traceframework/trace_contract.h"
#include "ti/instrumentation/traceframework/producer.h"
#include "ti/instrumentation/traceframework/producer_osal.h"

/*
 *  tempory definitions: would not be required when uia has the instance during exchange
 */
#define  MAX_NUM_UIA_CONTEXT            64

/* External Buffers */
extern uint8_t  gUiaLogBufNull0[];

/** @addtogroup TRACEFRAMEWORK_UIA_FUNCTIONS
@{
*/

/**
 * ============================================================================
 *  @n@b tf_getLoggerStreamerContext
 *
 *  @b  brief
 *  @n  Get the producer instance for that core for the logger streamer instance created
 *      @n Please note that there can be only one instance per core that can be created
 *         for LoggerStreamer object
 *
 *  @param[in]  lstHandle
 *      logger streamer handle  (single instance)
 *
 *  @return
 *      producer handle associated with that LoggerStreamer on that core 
 *
 * =============================================================================
 */
extern void* tf_getLoggerStreamerContext(LoggerStreamer_Handle lstHandle);

/**
 * ============================================================================
 *  @n@b tf_getLoggerStreamer2Context
 *
 *  @b  brief
 *  @n  Get the producer instance for that core for the logger streamer2 instance created
 *      @n Please note that there can be multiple instance per core that can be created
 *         for LoggerStreamer2 object
 *
 *  @param[in]  lst2Handle
 *      LoggerStreamer2 handle  (multi instance)
 *
 *  @return
 *      producer handle associated with that LoggerStreamer2 on that core for the given  
 *      LoggerStreamer2 handle 
 *
 * =============================================================================
 */
extern void* tf_getLoggerStreamer2Context(LoggerStreamer2_Handle lst2Handle);

/**
 * ============================================================================
 *  @n@b tf_uiaProducerBufExchange2
 *
 *  @b  brief
 *  @n  buffer exchange function for the uia LoggerStreaner2 interface 
 *
 *  @param[in]  handle
 *      logger streamer2 handle 
 *
 *  @param[in]  full
 *      filled log buffer 
 *
 *  @return
 *      free log buffer 
 *
 * =============================================================================
 */
extern Ptr tf_uiaProducerBufExchange2(LoggerStreamer2_Handle handle, uint8_t* full);

/**
 * ============================================================================
 *  @n@b tf_uiaProdRuntimePrime2
 *
 *  @b  brief
 *  @n  buffer prime function for the uia producer 
 *      This function initializes the UIA module/buffers. 
 *      Called during the producer create in the trace framework internally
 *
 *  @param[in]  handle
 *      Handle to logger streamer 2 object 
 *
 *  @param[in]  newBuf
 *      new log buffer 
 *
 *  @return
 *      Returns the ptr to the first log buffer to be used by UIA for this core
 *
 * =============================================================================
 */
extern void             tf_uiaProdRuntimePrime2(LoggerStreamer2_Handle handle, Ptr  newBuf);

/* local functions for UIA producer */
extern void             uiaLogStrm2BufferInit(LoggerStreamer2_Handle handle, Ptr buffer, uint32_t id);
extern void uiaProdHandlePush(tf_producer_HANDLE handle);
extern tf_producer_HANDLE uiaProdHandlePop( void );

/**
 * ============================================================================
 *  @n@b tf_uiaProducerBufExchange
 *
 *  @b  brief
 *  @n  buffer exchange function for the uia loggerstreamer interface 
 *
 *  @param[in]  full
 *      filled log buffer 
 *
 *  @return
 *      free log buffer 
 *
 * =============================================================================
 */
extern Ptr tf_uiaProducerBufExchange(uint8_t* full);

/**
 * ============================================================================
 *  @n@b tf_uiaProdRuntimePrime
 *
 *  @b  brief
 *  @n  buffer prime function for the uia producer 
 *      This function initializes the UIA module/buffers. 
 *      Called during the producer create in the trace framework internally 
 *
 *  @param[in]  newBuf
 *      new log buffer 
 *
 *  @return
 *      Returns the ptr to the first log buffer to be used by UIA for this core
 *
 * =============================================================================
 */
extern void             tf_uiaProdRuntimePrime(Ptr  newBuf);

extern void             uiaLogStrmBufferInit(Ptr buffer, uint32_t id);

/**
@}
*/

#endif /* TF_LINUX_USERSPACE */

#ifdef __cplusplus
}
#endif

#endif /* __UIA_PRODUCER_H__ */
/* Nothing past this point */

