/*
 *
 * Copyright (C) 2010-2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/



/* Generate and verify the system test framework
 * 
 * The test framework consists of the pa driver instance, a cppi/cdma/qm configuration,
 * memory for packet transmission and reception, and semaphores that are used
 * for every test in the PA unit test.
 * 
 */

#include "uTestEthSend.h"
#include <xdc/runtime/System.h>
//#include <xdc/runtime/IHeap.h>
#include "c66x/bios/tfutestCache.h"

utfEth_HANDLE utf_GetEthSendHandle(utfEthConfig_t *utfEthConfig)
{
	bmetChConfig_t   bmetChConfig;
	transport_HANDLE handle;

    uint32_t         localUdpPortNum = 0, coreId;


    coreId = CSL_chipReadDNUM();

	/* clear the config */
	memset(&bmetChConfig, 0, sizeof (bmetChConfig_t));

	/* configure the bare metal ethernet channel */
	memcpy (bmetChConfig.IPDst, utfEthConfig->dstIp, utfEth_IPV4_ADDR_SIZE);
	memcpy (bmetChConfig.IPSrc, utfEthConfig->srcIp, utfEth_IPV4_ADDR_SIZE);
	memcpy (bmetChConfig.MacDst, utfEthConfig->dstMac, utfEth_MAC_ADDR_SIZE);
	memcpy (bmetChConfig.MacSrc, utfEthConfig->srcMac, utfEth_MAC_ADDR_SIZE);

	bmetChConfig.payload_size             = utfEthConfig->constPayLoadSize;
	bmetChConfig.sgmii_send_port_num      = UTF_ETH_SEND_PORT;
	bmetChConfig.num_trasport_desc	      = UTF_NUM_BMET_DESC;
	bmetChConfig.global_free_queue_hnd    = utfEthConfig->QfreeDesc;
	bmetChConfig.desc_size		            = utfEthConfig->desc_size;
	bmetChConfig.send_vlan_header         = BMET_NO_VLAN_HEADER; /* No Header, 1 for VLAN Header */
	bmetChConfig.eth_tx_queue_num         = BMET_ETH_TX_QUEUE_NUM_KEYSTONE1;
	bmetChConfig.local_udp_port			      = localUdpPortNum;
	bmetChConfig.remote_udp_port		      = utfEthConfig->remoteUdpPortNum;
	bmetChConfig.moduleId                 = coreId;

	/* Create the transport channel */
    handle          = bmet_create(bmetChConfig);

    if (handle == NULL)
    {
    	System_printf ("can not create the BMET transport channel for Core: \n", coreId);
    	return (NULL);
    }
	
    return ((utfEth_HANDLE)handle);
}

int32_t utf_EthDestroy(utfEth_HANDLE ethHandle)
{
	if ( bmet_destroy(ethHandle) != BMET_SUCCESS) {
       return (-1);
	}
	return (0);
}

int32_t utf_EthSend(utfEth_HANDLE ethhandle, uint8_t *logBuf, uint32_t size, int32_t param)
{
	 int status;
	 char stat_string[80];
	 int32_t retVal = 0;
	 transport_HANDLE handle = (transport_HANDLE) ethhandle;

	 /* send the log buffer over ethernet */
	 status = bmet_send(handle, logBuf, param);

	   if (status != BMET_SUCCESS)
	   {
	       retVal = -1;
		   switch (status)
		   {
		   case BMET_ATTCHED_BD_NULL: 
		        strcpy(stat_string, "BMET_ATTCHED_BD_NULL"); break;
			   /**<BMET attached BD found null */
		   case BMET_ATTCHED_BD_NOT_MATCHED:strcpy (stat_string, "BMET_ATTCHED_BD_NOT_MATCHED"); break;
			   /**<BMET attached BD descriptor does not match */
		   case BMET_DESC_FOUND_FREE_BEFORE_FREE:strcpy (stat_string, "BMET_DESC_FOUND_FREE_BEFORE_FREE"); break;
			   /**<BMET descriptor found free before freed by library */
		   case BMET_DESC_INDEX_EXCEED_FAIL:strcpy (stat_string, "BMET_DESC_INDEX_EXCEED_FAIL"); break;
			   /**< BMET descriptor index exceeded the configured limit */
		   case BMET_QUEUEOPEN_FAIL:strcpy (stat_string, "BMET_QUEUEOPEN_FAIL"); break;
			   /**< BMET QUEUE OPEN is not successful */
		   case BMET_DESC_ALOCFAIL:strcpy (stat_string, "BMET_DESC_ALOCFAIL"); break;
			   /**< BMET Descriptor allocation failed */
		   case BMET_FAIL:strcpy (stat_string, "BMET_FAIL"); break;
			   /**< BMET General operation failed */
		   case BMET_UNSUPPORTED:strcpy (stat_string, "BMET_UNSUPPORTED"); break;
			   /**< BMET Unsupported arguments */
		   case BMET_DESC_NOTAVAILABLE:strcpy (stat_string, "BMET_DESC_NOTAVAILABLE"); break;
		   default:
			   strcpy(stat_string, "Unknown BMET Status"); break;
		   }

		   System_printf ("ethernet send failed with status= %s \n", stat_string);
		   System_flush();
	   }

	   return (retVal);
}
/* Nothing past this point */
