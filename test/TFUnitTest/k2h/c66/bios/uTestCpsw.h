#ifndef _TFW_UTEST_C6614_CPSW
#define _TFW_UTEST_C6614_CPSW

#ifdef __cplusplus
extern "C" {
#endif

/* ============================================================= */
/**
 *   @file  tfw_utest_cpsw.h
 *
 *   @brief  CPSW Definitions
 *
 *  ============================================================================
 *  Copyright (c) Texas Instruments Incorporated 2009-2013
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
	/* CSL EMAC include */
#ifndef __LINUX_USER_SPACE
#include <ti/csl/csl_cpsw.h>
#include <ti/csl/csl_cpsgmii.h>
#include <ti/csl/csl_cpsgmiiAux.h>
#include <ti/csl/cslr_cpsgmii.h>
#include <ti/csl/csl_mdio.h>
#include <ti/csl/csl_mdioAux.h>
#include "ti/csl/csl_bootcfgAux.h"
#endif

/** Number of ports in the ethernet subsystem */
#define         UTF_NUM_PORTS                   5u

/* Define LoopBack modes */  
#define CPSW_LOOPBACK_NONE           0   /* No Loopback */
#define CPSW_LOOPBACK_INTERNAL       1   /* SGMII internal Loopback */
#define CPSW_LOOPBACK_EXTERNAL       2   /* Loopback outside SoC */
#define CPSW_LOOPBACK_SERDES         3   /* SGMII Serdes Loopback */

#include <ti/csl/csl_serdes_ethernet.h>

typedef uint32_t csl_serdes_refclk_t;
#define SERDES_REF_CLK_156250_KHZ       156250
#if defined(DEVICE_K2K) || defined(DEVICE_K2H) || defined(DEVICE_K2L) || defined(DEVICE_K2E)
#define UTF_EXAMPLE_REF_CLK_KHZ     SERDES_REF_CLK_156250_KHZ
#endif

/* Functions */
int initCpsw (void);

#ifdef __cplusplus
}
#endif
  

#endif  /* _TFW_UTEST_C6614_CPSW */
