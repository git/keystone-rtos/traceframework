/*
 *
 * Copyright (C) 2010-2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/



/* Generate and verify the system test framework
 * 
 * The test framework consists of the pa driver instance, a cppi/cdma/qm configuration,
 * memory for packet transmission and reception, and semaphores that are used
 * for every test in the PA unit test.
 * 
 */

#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>
#include "c66x/bios/tfutestCache.h"

/* CSL Chip Functional Layer */
#include <ti/csl/csl_chip.h>
#include <ti/csl/csl_cache.h>
#include <ti/csl/csl_mdio.h>
#include <ti/csl/cslr_emac.h>

/* EMAC Driver Header File. */
#include <ti/drv/emac/emac_hwcfg.h>
#include <ti/drv/emac/emac_drv.h>

/* OSAL Include Files. */
#include <ti/drv/emac/emac_osal.h>

#include "uTestEthSend.h"

static uint8_t  IPDst[4];
/**< Source IP address */

static uint8_t  MacDst[6];
/**< Destination MAC address */

static uint8_t  IPSrc[4];
/**< Source IP address */

static uint8_t  MacSrc[6];

static uint32_t remote_udp_port;

utf_EthSendStats_t ethStat = {0};

Ptr TestApp_eth_MemAlloc (UInt32 num_bytes, Uint32 alignment);
Void TestApp_eth_MemFree (Ptr ptr, UInt32 size);
void TestApp_eth_EndMemAccess(void* ptr, uint32_t size);

static void iPChecksum(IPHeader* ptr_iphdr)
{
    int32_t   tmp1;
    uint16_t  *pw;
    uint32_t  TSum = 0;

    /* Get header size in 4 byte chunks */
    tmp1 = ptr_iphdr->VerLen & 0xF;

    /* Checksum field is NULL in checksum calculations */
    ptr_iphdr->Checksum = 0;

    /* Checksum the header */
    pw = (uint16_t *)ptr_iphdr;
    do {
        TSum += (uint32_t)*pw++;
        TSum += (uint32_t)*pw++;
    } while( --tmp1 );
    TSum = (TSum&0xFFFF) + (TSum>>16);
    TSum = (TSum&0xFFFF) + (TSum>>16);
    TSum = ~TSum;

    /* Note checksum is Net/Host byte order independent */
    ptr_iphdr->Checksum = (uint16_t)TSum;
    return;
}

static void buildNetPktHeader(uint8_t* pkg_header_added, uint8_t * pkt, uint32_t * pktSize)
{
  EthHeader*        ptrEthHeader;
  IPHeader*         ptrIPHeader;
  UDPHeader*        ptrUDPHeader;
  uint32_t               packetLen;
  uint16_t               localUdpPort= 0;
  uint16_t               remoteUdpPort= remote_udp_port;

  /* Get the packet length of the Payload. For UIA transport, payload is always MTU size.*/
  packetLen = *pktSize;

  /* Get the pointer to the various headers. */
  ptrEthHeader = (EthHeader*)pkg_header_added;

  ptrIPHeader  = (IPHeader*)((uint8_t*)ptrEthHeader  + ETHHDR_SIZE);

  ptrUDPHeader = (UDPHeader*)((uint8_t*)ptrIPHeader  + IPHDR_SIZE);

  memcpy((uint8_t*)ptrUDPHeader+UDPHDR_SIZE, pkt, packetLen);

  /* Populate the UDP Header: The destination port should be the input parameter remoteUdpPort
   * and the source port is the input parameter localUdpPort. */
  ptrUDPHeader->DstPort   = htons(remoteUdpPort);
  ptrUDPHeader->SrcPort   = htons(localUdpPort);
  ptrUDPHeader->Length    = htons(UDPHDR_SIZE + packetLen);
  ptrUDPHeader->UDPChecksum = 0x0000;

  /* The packet length now includes the UDP Header */
  packetLen = packetLen + UDPHDR_SIZE;

  /* Populate the IP Header*/
  ptrIPHeader->VerLen   = 0x45;
  ptrIPHeader->Tos      = 0;
  ptrIPHeader->Id       = 0x2e54;
  ptrIPHeader->FlagOff  = 0x0;
  ptrIPHeader->Ttl      = 128;
  ptrIPHeader->Protocol   = IPPROTO_UDP;

  memcpy(ptrIPHeader->IPSrc, IPSrc, utfEth_IPV4_ADDR_SIZE);
  memcpy(ptrIPHeader->IPDst, IPDst, utfEth_IPV4_ADDR_SIZE);

  /* The packet length now includes the IP Header also. */
  packetLen = packetLen + IPHDR_SIZE;

  /* Setup the total length in the packet. */
  ptrIPHeader->TotalLen   = htons(packetLen);

  /* Setup the IP Header checksum. */
  iPChecksum (ptrIPHeader);

  /* Populate the MAC Header.
   * - Setup the destination and source MAC Address
   * - This is a VLAN Packet. */
  memcpy(ptrEthHeader->SrcMac, MacSrc, utfEth_MAC_ADDR_SIZE);
  memcpy(ptrEthHeader->DstMac, MacDst, utfEth_MAC_ADDR_SIZE);

  ptrEthHeader->Type = htons(ETH_IP);

  /* The packet length now includes the Eth Header also. */
  packetLen = packetLen + ETHHDR_SIZE;

  TestApp_eth_EndMemAccess(pkg_header_added, packetLen);

  *pktSize = packetLen;

  return;
}

/* Memory allocated for the packet buffer. This is 128 bytes aligned. */
Uint8   app_pkt_buffer[APP_TOTAL_PKTBUF_SIZE];
#pragma DATA_SECTION(app_pkt_buffer, ".far:PKTBUF_L2MEM")
#pragma DATA_ALIGN(app_pkt_buffer, CACHE_L2_LINESIZE)

/* Memory allocated for the application control block */
APP_EMAC_MCB_T   app_mcb;
#pragma DATA_SECTION(app_mcb, ".far:APP_EMAC")

/**********************************************************************
 ****************** Test Configuration Variables **********************
 **********************************************************************/
/* Packet buffer allocated in external memory flag */
Bool    pktbuf_extmem = FALSE;

#ifdef ENABLE_RX_PKTS
/**
 *  @b app_rx0_int
 *  @n
 *      Receive Packet ISR for port 0
 *
 *  @param[in]  void
 *
 *  @retval
 *      void
 */
static void
app_rx0_int
(
    void
)
{

    /* Call the EMAC driver RX interrupt service function for port 0 */
    emac_int_service(0, TRUE);

}
#endif

/**
 *  @b app_tx0_int
 *  @n
 *      Transmit Complete ISR.for port 0
 *
 *  @param[in]  void
 *
 *  @retval
 *      void
 */
static void
app_tx0_int
(
    void
)
{
    /* Call the EMAC driver TX interrupt service function for port 0 */
    emac_int_service(0, FALSE);
}

/**
 *  @b app_queue_pop
 *  @n
 *      Dequeues a packet descriptor from an app queue.
 *
 *  @param[in]  pq
 *      Packet queue of type APP_PKT_QUEUE_T .
 *
 *  @retval
 *      EMAC_Pkt popped from the queue.
 */
EMAC_PKT_DESC_T*
app_queue_pop
(
    Uint32              port_num,
    APP_PKT_QUEUE_T*    pq
)
{
    EMAC_PKT_DESC_T*    pPktHdr;

    if (!pq->Count)
    {
        return 0;
    }

    Emac_osalEnterSingleCoreCriticalSection(port_num);
    pPktHdr = pq->pHead;
    if( pPktHdr )
    {
        pq->pHead = pPktHdr->pNext;
        pq->Count--;
        pPktHdr->pPrev = pPktHdr->pNext = 0;
    }
    Emac_osalExitSingleCoreCriticalSection(port_num);

    return( pPktHdr );
}

/**
 *  @b app_queue_push
 *  @n
 *      Enqueues a packet in EMAC_Pkt queue.
 *
 *  @param[in]  pq
 *      Packet queue of type EMAC_PKT_QUEUE_T .
 *  @param[in]  pPktHdr
 *      EMAC_PKT_DESC_T type packet to push.
 *
 *  @retval
 *      void
 */
static void
app_queue_push
(
    Uint32              port_num,
    APP_PKT_QUEUE_T*    pq,
    EMAC_PKT_DESC_T*    pPktHdr
)
{
    Emac_osalEnterSingleCoreCriticalSection(port_num);
    pPktHdr->pNext = 0;

    if( !pq->pHead )
    {
        /* Queue is empty - Initialize it with this one packet */
        pq->pHead = pPktHdr;
        pq->pTail = pPktHdr;
    }
    else
    {
        /* Queue is not empty - Push onto end */
        pq->pTail->pNext = pPktHdr;
        pq->pTail        = pPktHdr;
    }
    pq->Count++;
    Emac_osalExitSingleCoreCriticalSection(port_num);
}

/**
 *  @b Description
 *  @n
 *      Call back function provided by application for EMAC driver
 *      to allocate a packet descriptor.
 *
 *  @retval
 *      pointer to the allocated packet descriptor.
 */
static EMAC_PKT_DESC_T*
app_alloc_pkt
(
    Uint32              port_num,
    Uint32              pkt_size
)
{
    EMAC_PKT_DESC_T*    p_pkt_desc = NULL;

    if (pkt_size < APP_EMAC_MAX_PKT_SIZE)
    {
        /* Get a packet descriptor from the free queue */
        p_pkt_desc              = app_queue_pop(port_num, &app_mcb.emac_pcb[port_num].freeQueue);
        p_pkt_desc->AppPrivate  = (Uint32)p_pkt_desc;
        p_pkt_desc->BufferLen   = pkt_size;
        p_pkt_desc->DataOffset  = 0;
    }
    else
    {
        System_printf ("app_alloc_pkt on port %d failed, packet size %d is too big\n", port_num, pkt_size);
        System_flush();
    }

    return p_pkt_desc;
}

/**
 *  @b Description
 *  @n
 *      Call back function provided by application for EMAC driver
 *      to free a packet descriptor.
 *
 *  @retval
 *      None.
 */
static void
app_free_pkt
(
    Uint32              port_num,
    EMAC_PKT_DESC_T*    p_pkt_desc
)
{
    /* Free a packet descriptor to the free queue */
    app_queue_push(port_num, &app_mcb.emac_pcb[port_num].freeQueue,
                   (EMAC_PKT_DESC_T *)p_pkt_desc->AppPrivate);
}

/**
 *  @b Description
 *  @n
 *      Call back function provided by application for EMAC driver
 *      to report a received packet descriptor.
 *
 *  @retval
 *      None.
 */
static void
app_rx_pkt
(
    Uint32              port_num,
    EMAC_PKT_DESC_T*    p_pkt_desc
)
{
    EMAC_PKT_DESC_T*    p_pkt_enq;

    p_pkt_enq = (EMAC_PKT_DESC_T *)p_pkt_desc->AppPrivate;
    memcpy(p_pkt_enq, p_pkt_desc, sizeof(EMAC_PKT_DESC_T));

    /* enqueue the received packet descriptor to the channel specific RX queue */
    app_queue_push(port_num, &app_mcb.emac_pcb[port_num].rxQueue[p_pkt_enq->PktChannel],
                  (EMAC_PKT_DESC_T *)p_pkt_enq);
}

/**
 *  @b app_interrupt_init
 *  @n
 *      Registering Interrupts and Enabling global interrupts.
 *
 *  @param[in]  void
 *
 *  @retval
 *      void
 */
static void
app_interrupt_init
(
    Uint32          port_num
)
{
    Hwi_Params      hwi_params;
    Error_Block     eb;
    static Bool     port0Init   = FALSE;
#ifdef TWO_PORT_DEV
    static Bool     port1Init   = FALSE;
#endif
    Bool            error_flag;

    if ((!port0Init) && (port_num==0))
    {
        error_flag = FALSE;

        /* Initialize the error block. */
        Error_init(&eb);
        Hwi_Params_init (&hwi_params);
		
#ifdef ENABLE_RX_PKTS
        /*
         * Setup RX Int using BIOS6 Hwi module
         */
        hwi_params.arg          = (UArg) 0;
        hwi_params.enableInt    = 1;
        hwi_params.eventId      = APP_EMAC0_RX_INT_EV;
        hwi_params.maskSetting  = Hwi_MaskingOption_SELF;

        if (Hwi_create (APP_EMAC0_RXINT_ID, (Hwi_FuncPtr ) &app_rx0_int, &hwi_params, &eb) == NULL)
        {
            System_printf("app_interrupt_init(): could not configure the RX interrupt for port %d", port_num);
            error_flag = TRUE;
        }
#endif
        /*
         * Setup TX Int using BIOS6 Hwi module
         */
        hwi_params.arg          = (UArg) 0;
        hwi_params.enableInt    = 1;
        hwi_params.eventId      = APP_EMAC0_TX_INT_EV;
        hwi_params.maskSetting  = Hwi_MaskingOption_SELF;

        if (Hwi_create (APP_EMAC0_TXINT_ID, (Hwi_FuncPtr) &app_tx0_int, &hwi_params, &eb) == NULL)
        {
            System_printf("app_interrupt_init(): could not configure the TX interrupt for port %d", port_num);
            error_flag = TRUE;
        }

        if(!error_flag)
        {
            port0Init = TRUE;
            Osal_emacEnterSingleCoreCriticalSection(0);
        }
    }

}



void
app_init
(
    void
)
{
    Uint32              i, j;
    EMAC_PKT_DESC_T*    p_pkt_desc;
    Uint8*              pktbuf_ptr;

    System_printf ("EMAC initialization\n");

    MDIOR->CONTROL = 0x400000A5;

    /* Reset application control block */
    memset(&app_mcb, 0, sizeof (APP_EMAC_MCB_T));

    app_mcb.core_num = CSL_chipReadReg (CSL_CHIP_DNUM);

    if (pktbuf_extmem)
    {
        /* If packet buffer stored in external memory */
        pktbuf_ptr = (Uint8 *)(APP_EXTMEM+app_mcb.core_num*APP_TOTAL_PKTBUF_SIZE);
    }
    else
    {
        pktbuf_ptr = (Uint8 *) ((Uint32) app_pkt_buffer | 0x10000000);
    }

    /* Initialize the free packet queue */
    for (i=0; i<MAX_NUM_EMAC_PORTS; i++)
    {
        if (i == 0)
        {
            app_mcb.emac_pcb[0].phy_addr   = APP_PORT0_PHY_ADDR;
        }
#ifdef TWO_PORT_DEV
        else
        {
            app_mcb.emac_pcb[1].phy_addr   = APP_PORT1_PHY_ADDR;
        }
#endif

        for (j=0; j<APP_MAX_PKTS; j++)
        {
            p_pkt_desc               = &app_mcb.emac_pcb[i].pkt_desc[j];
            p_pkt_desc->pDataBuffer  = pktbuf_ptr;
            p_pkt_desc->BufferLen    = APP_EMAC_MAX_PKT_SIZE;
            app_queue_push( i, &app_mcb.emac_pcb[i].freeQueue, p_pkt_desc );
            pktbuf_ptr += APP_EMAC_MAX_PKT_SIZE;
        }
        /* Initialize the EMAC interrupts */
        app_interrupt_init(i);
    }

}


/** ============================================================================
 *   @n@b emac_setup
 *
 *   @b Description
 *   @n Entry point for single core example application.
 *
 *   @param[in]  
 *   @n None
 * 
 *   @return
 *   @n None
 * =============================================================================
 */
int emac_setup (void)
{
    EMAC_OPEN_CONFIG_INFO_T     open_cfg;
    EMAC_CHAN_MAC_ADDR_T        chan_cfg[APP_EMAC_NUM_CHANS_PER_CORE];
    EMAC_MAC_ADDR_T             mac_addr[APP_EMAC_NUM_CHANS_PER_CORE*APP_EMAC_NUM_MACADDRS_PER_CHAN];
    EMAC_CONFIG_INFO_T          emac_cfg;
    Uint32                      port_num, chan_num, addr_num;
    Uint8                       def_mac_addr[EMAC_MAC_ADDR_LENTH] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05};

	app_init();

    /*
    *  Configures MAC addresses per channel/port/core
    *  Hardware gives support for 32 MAC addresses for 8 receive channels per EMAC port
    *  Here total 4 MAC addresses are assigned to 8 receive channels on one EMAC port per core
    *  4 MAC addresses per channel
    *
    *  MAC addresses and channels allocated are like mentioned below:
    *
    *  core #      port #      channel #       MAC address
    *  0           0           0               00.01.02.03.04.05
    *                                          00.01.02.03.04.15(address used for loopback test)
    *                                          00.01.02.03.04.25
    *                                          00.01.02.03.04.35
    *
    *  0           0           1               00.01.02.03.14.05
    *                                          00.01.02.03.14.15(address used for loopback test)
    *                                          00.01.02.03.14.25
    *                                          00.01.02.03.14.35
    *                          .
    *                          .
    *                          .
    *
    *  0           1           0               00.01.02.13.04.05
    *                                          00.01.02.13.04.15(address used for loopback test)
    *                                          00.01.02.13.04.25
    *                                          00.01.02.13.04.35
    *
    *  0           1           1               00.01.02.13.14.05
    *                                          00.01.02.13.14.15(address used for loopback test)
    *                                          00.01.02.13.14.25
    *                                          00.01.02.13.14.35
    *                          .
    *                          .
    *                          .
    *
    */

    /* Set the Core specific nibble in the default MAC address */
    def_mac_addr[2] += (Uint8)(app_mcb.core_num * 0x10);

    for (port_num=0; port_num<MAX_NUM_EMAC_PORTS; port_num++)
    {
        /* Set the emac_open configuration */
        if (app_mcb.core_num == APP_MASTER_CORE_NUM)
        {
            open_cfg.master_core_flag   = TRUE;
            open_cfg.mdio_flag          = TRUE;
        }
        else
        {
            open_cfg.master_core_flag   = FALSE;
            open_cfg.mdio_flag          = FALSE;
        }
        open_cfg.loop_back              = FALSE;
        open_cfg.num_of_tx_pkt_desc     = APP_EMAC_INIT_TX_PKTS;
        open_cfg.num_of_rx_pkt_desc     = APP_EMAC_INIT_RX_PKTS;
        open_cfg.max_pkt_size           = APP_EMAC_INIT_PKT_SIZE;
        open_cfg.num_of_chans           = APP_EMAC_NUM_CHANS_PER_CORE;
        open_cfg.p_chan_mac_addr        = chan_cfg;
        open_cfg.rx_pkt_cb              = app_rx_pkt;
        open_cfg.alloc_pkt_cb           = app_alloc_pkt;
        open_cfg.free_pkt_cb            = app_free_pkt;

        if (port_num == 0)
        {
            open_cfg.phy_addr           = APP_PORT0_PHY_ADDR;
        }
#ifdef TWO_PORT_DEV
        else
        {
            open_cfg.phy_addr           = APP_PORT1_PHY_ADDR;
        }
#endif

        for (chan_num=0; chan_num<APP_EMAC_NUM_CHANS_PER_CORE; chan_num++)
        {
            /* Set the channel configuration */
            chan_cfg[chan_num].chan_num            = app_mcb.core_num*APP_EMAC_NUM_CHANS_PER_CORE+chan_num;
            chan_cfg[chan_num].num_of_mac_addrs    = APP_EMAC_NUM_MACADDRS_PER_CHAN;
            chan_cfg[chan_num].p_mac_addr          = &mac_addr[chan_num*APP_EMAC_NUM_MACADDRS_PER_CHAN];

            for (addr_num=0; addr_num<APP_EMAC_NUM_MACADDRS_PER_CHAN; addr_num++)
            {
                memcpy(chan_cfg[chan_num].p_mac_addr[addr_num].addr, def_mac_addr, EMAC_MAC_ADDR_LENTH);

                /* Set the port specific nibble in the MAC address */
                chan_cfg[chan_num].p_mac_addr[addr_num].addr[3] += (Uint8)(port_num * 0x10);

                /* Set the channel specific nibble in the MAC address */
                chan_cfg[chan_num].p_mac_addr[addr_num].addr[4] += (Uint8)(chan_cfg[chan_num].chan_num * 0x10);

                /* Set the MAC address specific nibble in the MAC address */
                chan_cfg[chan_num].p_mac_addr[addr_num].addr[5] += (Uint8)(addr_num * 0x10);

            }
        }


        /* Save the MAC address configuration */
        memcpy(app_mcb.emac_pcb[port_num].mac_addr, mac_addr,
               sizeof(EMAC_MAC_ADDR_T)*APP_EMAC_NUM_CHANS_PER_CORE*APP_EMAC_NUM_MACADDRS_PER_CHAN);

        /* Call low-level open function */
        if (emac_open(port_num, &open_cfg) == EMAC_DRV_RESULT_OK)
        {
            /* Set the 'initial' Receive Filter */
            emac_cfg.rx_filter       = EMAC_PKTFLT_MULTICAST;
            emac_cfg.mcast_cnt       = 0;
            emac_cfg.p_mcast_list    = NULL;

            emac_config(port_num, &emac_cfg);

            app_mcb.emac_pcb[port_num].emac_state = APP_EMAC_PORT_STATE_OPEN;

            /* Enable RX/TX interrupt */
            Osal_emacExitSingleCoreCriticalSection(port_num);
        }
        else
        {
            System_printf ("Application open EMAC port %d error \n", port_num);
        }
    }


    app_mcb.emac_pcb[0].emac_state=APP_EMAC_PORT_STATE_LINKUP;

    return (0);
}


utfEth_HANDLE utf_GetEthSendHandle(utfEthConfig_t *utfEthConfig)
{
    memcpy(IPDst, utfEthConfig->dstIp,   utfEth_IPV4_ADDR_SIZE);
    memcpy(IPSrc, utfEthConfig->srcIp,   utfEth_IPV4_ADDR_SIZE);
    memcpy(MacDst, utfEthConfig->dstMac, utfEth_MAC_ADDR_SIZE);
    memcpy(MacSrc, utfEthConfig->srcMac, utfEth_MAC_ADDR_SIZE);
    remote_udp_port = utfEthConfig->remoteUdpPortNum;

    emac_setup();
	/* C6657 (EMAC LLD) does not use ethernet handle to send the packet */
    return ((utfEth_HANDLE)NULL);
}

int32_t utf_EthSend(utfEth_HANDLE ethhandle, uint8_t *logBuf, uint32_t logBufSize, int32_t param)
{
	uint8_t                     *logBufHeaderAdded;
	EMAC_PKT_DESC_T*			p_pkt_desc;
	Uint32						size, pkt_size, port_num=0;
	int32_t                     retVal = 0;

	pkt_size = logBufSize + ETHHDR_SIZE + IPHDR_SIZE + UDPHDR_SIZE;

    logBufHeaderAdded = (uint8_t *)TestApp_eth_MemAlloc (pkt_size, 128);
  
    if (logBufHeaderAdded == 0) {
  	  System_printf (" Could not allocate packet memory \n");
  	  System_flush();
  	  while (1); /* Trap here */
    }

    memset(logBufHeaderAdded, 0, pkt_size);	
	
	buildNetPktHeader(logBufHeaderAdded, logBuf, &logBufSize);

    /* First get a packet descriptor from the free queue */
    p_pkt_desc = app_queue_pop(port_num, &app_mcb.emac_pcb[0].freeQueue);

   if(p_pkt_desc)
   {
       /* Set the size to be min payload size + 2 bytes of Ether Type */
       size = logBufSize;
       if (size > APP_EMAC_MAX_LB_TX_PKT_SIZE)
       {
           size = APP_EMAC_MAX_LB_TX_PKT_SIZE;
       }

       p_pkt_desc->AppPrivate   = (Uint32)p_pkt_desc;
       p_pkt_desc->Flags        = EMAC_PKT_FLAG_SOP | EMAC_PKT_FLAG_EOP;
       p_pkt_desc->ValidLen     = size;
       p_pkt_desc->DataOffset   = 0;
       p_pkt_desc->PktChannel   = 0;
       p_pkt_desc->PktLength    = size;
       p_pkt_desc->PktFrags     = 1;

       /* Fill in the Ethernet packet  */
       memcpy(p_pkt_desc->pDataBuffer,
              logBufHeaderAdded,
              size);

       /* Write back cache to memory if data in external memory */
       Osal_emacEndMemAccess(p_pkt_desc->pDataBuffer, size+EMAC_MAC_ADDR_LENTH*2);

       emac_send(port_num, p_pkt_desc);
       ethStat.sentCnt ++;

   }	
   else {
	   retVal = -1;
	   ethStat.failCnt ++;
   }

   /* Free the allocation */
   TestApp_eth_MemFree(logBufHeaderAdded, pkt_size);
   
   return (retVal);
}
/* Nothing past this point */
