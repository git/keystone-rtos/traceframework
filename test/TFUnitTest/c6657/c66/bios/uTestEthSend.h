#ifndef _TFW_UTEST_C6657_ETHSEND
#define _TFW_UTEST_C6657_ETHSEND

#ifdef __cplusplus
extern "C" {
#endif

/* ============================================================= */
/**
 *   @file  uTestEthSend.h
 *
 *   @brief  Ethernet Send function definitions
 *
 *  ============================================================================
 *  Copyright (c) Texas Instruments Incorporated 2009-2013
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include <ti/sysbios/family/c64p/Hwi.h>
#include <ti/csl/csl_cacheAux.h>
#include <ti/csl/csl_xmcAux.h>

#include <ti/drv/qmss/qmss_drv.h>

/* EMAC Driver Header File. */
#include <ti/drv/emac/emac_hwcfg.h>
#include <ti/drv/emac/emac_drv.h>

/* OSAL Include Files. */
#include <ti/drv/emac/emac_osal.h>

#include "uTestCpsw.h"

/**********************************************************************
 ************************** LOCAL Definitions *************************
 **********************************************************************/

/**
 * @brief   Applicaton external memory start address
 */
#define     APP_EXTMEM                          0x80000000
#define		APP_MSMCMEM							0x0C000000


/**
 * @brief   Master core number
 */
#define     APP_MASTER_CORE_NUM                 0

/**
 * @brief   Number of channels configured by a core on one port
 */
#define     APP_EMAC_NUM_CHANS_PER_CORE         8

/**
 * @brief   Nummber of EMAC MAC addresses configured per channel
 */
#define     APP_EMAC_NUM_MACADDRS_PER_CHAN      4

/**
 * @brief   Max EMAC packet size in bytes initialized for the driver
 */
#define     APP_EMAC_INIT_PKT_SIZE              1514

/**
 * @brief   EMAC loopback Test TX packet size in bytes: (APP_EMAC_INIT_PKT_SIZE - 4 bytes CRC)
 */
#define     APP_EMAC_MAX_LB_TX_PKT_SIZE         (APP_EMAC_INIT_PKT_SIZE-4)

/**
 * @brief
 *  Minimum Ethernet packet payload size
 */
#define     APP_EMAC_MIN_PL_SIZE                46

/**
 * @brief
 *  Number of packets to be tested for each channel
 */
#define     APP_EMAC_ETH_TYPE_SIZE              2

/**
 * @brief
 *  Number of packets to be tested for each channel
 */
#define     APP_EMAC_HEADER_SIZE                (2*EMAC_MAC_ADDR_LENTH+APP_EMAC_ETH_TYPE_SIZE)

/**
 * @brief   Max packet size in bytes used in the application,
 *          align to 128 byte cache line size
 */
#define     APP_EMAC_MAX_PKT_SIZE               1536

/**
 * @brief   Max number of packets in the application free packet queue
 *
 */
#define     APP_MAX_PKTS                        128

/**
 * @brief   Max number of packet descriptors per port initialized
 *          for driver managed RX queue
 */
#define     APP_EMAC_INIT_RX_PKTS               (8*APP_EMAC_NUM_CHANS_PER_CORE)

/**
 * @brief   Max number of packet descriptors per port initialized
 *          for driver managed TX queue
 */
#define     APP_EMAC_INIT_TX_PKTS               (APP_MAX_PKTS-APP_EMAC_INIT_RX_PKTS)

/**
 * @brief   Total packet buffer size in bytes per core
 *
 */
#define     APP_TOTAL_PKTBUF_SIZE               (MAX_NUM_EMAC_PORTS*APP_MAX_PKTS*APP_EMAC_MAX_PKT_SIZE)

/**
 * @brief   EMAC port 0 phy address
 */
#ifdef PARTNO_C6657
#define     APP_PORT0_PHY_ADDR                  24
#endif

/**
 * @brief   EMAC port 1 phy address
 */
#define     APP_PORT1_PHY_ADDR                  25

/**
 * @brief   EMAC TX/RX Interrupt Definitions
 */
#define     APP_EMAC0_RXINT_ID                  7
#define     APP_EMAC0_TXINT_ID                  8
#define     APP_EMAC0_INT_FLAG                  (3<<APP_EMAC0_RXINT_ID)

#ifdef TWO_PORT_DEV
#define     APP_EMAC1_RXINT_ID                  9
#define     APP_EMAC1_TXINT_ID                  10
#define     APP_EMAC1_INT_FLAG                  (3<<APP_EMAC1_RXINT_ID)
#endif


#ifdef PARTNO_C6657
#define     APP_EMAC0_RX_INT_EV                 99
#define     APP_EMAC0_TX_INT_EV                 94
#endif

/**
 * @brief
 *  Number of packets to be tested for each channel
 */
#define     APP_NUM_TEST_PKTS                   10240

/**
 * @brief
 *  DDR2 definitions
 */
#define DDR2C   ((CSL_Ddr2Regs *) CSL_DDR2_0_REGS)

/**
 * @brief
 *  Phy definitions
 */
#define MDIOR   ((CSL_MdioRegs *) CSL_MDIO_0_REGS)

enum APP_ETHERNET_MODES_E
{
    PHY_LOOPBACK            = 0,
    COPPER_LOOPBACK,
    AMC_MODE,
    NORMAL_PHY_MODE
};

/**
 * @brief
 *  PLLC definitions
 */
#define PLLCTL_1	0x029A0100  // PLL1 control register
#define PLLM_1		0x029A0110	// PLL1 multiplier control register
#define PLLCMD_1	0x029A0138	// PLL1 controller command register
#define PLLSTAT_1	0x029A013C	// PLL1 controller status register
#define ALNCTL_1	0x029A0140	// PLL1 controller clock align control register
#define DCHANGE_1	0x029A0144	// PLL1 PLLDIV ratio change status register
#define SYSTAT_1	0x029A0150	// PLL1 SYSCLK status register
#define PLLDIV7_1	0x029A016C	// PLL1 controller divider 7 register
#define PLLDIV8_1	0x029A0170	// PLL1 controller divider 8 register
#define PLLDIV9_1	0x029A0174	// PLL1 controller divider 9 register
#define PLLDIV10_1	0x029A0178	// PLL1 controller divider 10 register

#define PLLCTL_2	0x029C0100	// PLL2 control register
#define PLLM_2		0x029C0110	// PLL2 multiplier control register
#define PLLDIV1_2	0x029C0118	// PLL2 controller divider 1 register
#define PLLDIV2_2	0x029C011C	// PLL2 controller divider 2 register
#define PLLDIV3_2	0x029C0120	// PLL2 controller divider 3 register
#define PLLCMD_2	0x029C0138	// PLL2 controller command register
#define PLLSTAT_2	0x029C013C	// PLL2 controller status register
#define ALNCTL_2	0x029C0140	// PLL2 controller clock align control register
#define DCHANGE_2	0x029C0144	// PLL2 PLLDIV ratio change status register
#define SYSTAT_2	0x029C0150	// PLL2 SYSCLK status register
#define PLLDIV4_2	0x029A0160	// PLL2 controller divider 4 register
#define PLLDIV5_2	0x029A0164	// PLL2 controller divider 5 register
#define PLLDIV6_2	0x029A0168	// PLL2 controller divider 6 register

#define PLLCTL_3	0x029C0500	// PLL3 control register
#define PLLM_3		0x029C0510	// PLL3 multiplier control register
#define PLLCMD_3	0x029C0538	// PLL3 controller command register
#define PLLSTAT_3	0x029C053C	// PLL3 controller status register

 /**
 * @brief
 *  Application Queue Data Structure
*/
typedef struct APP_PKT_QUEUE_tag
{
    Uint32            Count;
    /**< Number of packets in queue */
    EMAC_PKT_DESC_T*  pHead;
    /**< Pointer to the first packet */
    EMAC_PKT_DESC_T*  pTail;
    /**< Pointer to the last packet */
} APP_PKT_QUEUE_T;

/**
 * @brief
 *  EMAC port states
 */
#define APP_EMAC_PORT_STATE_CLOSE       0
#define APP_EMAC_PORT_STATE_OPEN        1
#define APP_EMAC_PORT_STATE_LINKUP      2
#define APP_EMAC_PORT_STATE_LINKDN      3

/**
 * @brief
 *  Core specific EMAC port control block
 *
 * @details
 *  Maintains the EMAC port control information of a core
 */
typedef struct APP_EMAC_PCB_tag
{
    Uint32                          emac_state;
    /**< EMAC Port state */
    Uint32                          phy_addr;
    /**< Physical layer transceiver address mapped to the EMAC port */
    EMAC_PKT_DESC_T                 pkt_desc[APP_MAX_PKTS];
    /**< Pre-allocated/initialized packet descriptiors for both free queue and RX queues */
    APP_PKT_QUEUE_T                 freeQueue;
    /**< Free packet descriptor queue, one queue per channel */
    APP_PKT_QUEUE_T                 rxQueue[APP_EMAC_NUM_CHANS_PER_CORE];
    /**< Received packet descriptor queue, one queue per channel */
    EMAC_MAC_ADDR_T                 mac_addr[APP_EMAC_NUM_CHANS_PER_CORE][APP_EMAC_NUM_MACADDRS_PER_CHAN];
    /**< MAC address for all the channels */

} APP_EMAC_PCB_T;


/**
 * @brief
 *  EMAC Master Control Block
 *
 * @details
 *  Maintains the EMAC control information and error statistics.
 */
typedef struct APP_EMAC_MCB_tag
{
    Uint32              core_num;
    /**< DSP core number */
    Uint32              timer_count;
    /**< 100 msec timer count */
    APP_EMAC_PCB_T      emac_pcb[MAX_NUM_EMAC_PORTS];
    /**< EMAC port control block */
} APP_EMAC_MCB_T;

extern APP_EMAC_MCB_T   app_mcb;

/* Parmeters for network header */
#define ETHHDR_SIZE     14
#define IPHDR_SIZE      20
#define UDPHDR_SIZE     8

//#define LOCAL_UDP_PORT          0
//#define REMOTE_UDP_PORT         1235


/**
 * @brief   This is the protocol identification field in the Ethernet
 * header which identifies the packet as an IPv4 packet.
 */
#define ETH_IP                  0x800

/**
 * @brief   This is the protocol identification field in the Ethernet
 * header which identifies the packet as a VLAN Packet.
 */
#define ETH_VLAN                0x8100

/**
 * @brief   This is the protocol identification field in the IPv4 header
 * which identifies the packet as a UDP packet.
 */
#define IPPROTO_UDP             17


#define  htons(a)    ( (((a)>>8)&0xff) + (((a)<<8)&0xff00) )
#define  htonl(a)    ( (((a)>>24)&0xff) + (((a)>>8)&0xff00) + \
                       (((a)<<8)&0xff0000) + (((a)<<24)&0xff000000) )
#define  ntohl(a)   htonl(a)
#define  ntohs(a)   htons(a)

/**
 * @brief   This is the EthHeader
 */
typedef struct EthHeader
{
    uint8_t   DstMac[6];
    uint8_t   SrcMac[6];
    uint16_t  Type;
}EthHeader;


/**
 * @brief   This is the IPHeader
 */
typedef struct IPHeader
{
    uint8_t    VerLen;
    uint8_t    Tos;
    uint16_t   TotalLen;
    uint16_t   Id;
    uint16_t   FlagOff;
    uint8_t    Ttl;
    uint8_t    Protocol;
    uint16_t   Checksum;
    uint8_t    IPSrc[4];
    uint8_t    IPDst[4];
    uint8_t    Options[1];
}IPHeader;

/**
 * @brief   This is the UDPHeader
 */
typedef struct UDPHeader
{
    uint16_t   SrcPort;
    uint16_t   DstPort;
    uint16_t   Length;
    uint16_t   UDPChecksum;
}UDPHeader;

/* Ethernet statistics */
typedef struct {
	int     sentCnt;
	int     failCnt;
} utf_EthSendStats_t;
extern utf_EthSendStats_t ethStat;

extern cregister volatile unsigned int  IER;

extern Int32 emac_setup (Void);

/* ethernet send handle */
typedef void* utfEth_HANDLE;

#define utfEth_MAC_ADDR_SIZE       6 
typedef unsigned char utfEthMacAddr_t[utfEth_MAC_ADDR_SIZE];

#define utfEth_IPV4_ADDR_SIZE      4
typedef unsigned char utfEthIpv4Addr_t[utfEth_IPV4_ADDR_SIZE];

typedef struct utfEthConfig_s
{
	utfEthMacAddr_t *dstMac;
	utfEthMacAddr_t *srcMac;
	utfEthIpv4Addr_t *dstIp;
	utfEthIpv4Addr_t *srcIp;
	uint32_t remoteUdpPortNum;
	uint32_t constPayLoadSize;
	Qmss_QueueHnd QfreeDesc;		      				/* System Descriptor */
	uint32_t      desc_size;                           /* system descriptor size */
} utfEthConfig_t;

/* Ethernet Send Initialization */
utfEth_HANDLE utf_GetEthSendHandle(utfEthConfig_t *utfEthConfig);
int32_t       utf_EthSend(utfEth_HANDLE handle, uint8_t *buffer, uint32_t size, int32_t param);

#ifdef __cplusplus
}
#endif
  

#endif  /* _TFW_UTEST_C6657_ETHSEND */
