/*
 *
 * Copyright (C) 2010-2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/* provide the shared memory infrastructure for unit test framework
 * 
 */

#include "fw_shm.h"

/* shared memory create at Master process */
int32_t fw_shmCreate(key_t key, int size, fwShm_HANDLE* handle)
{
  struct shmid_ds info;
  fwShm_HANDLE shm_handle = NULL;
  int          shm_id;

  shm_id = shmget(key, size, IPC_CREAT | 0666 );
  if (shm_id < 0) {
    perror("fw_shmCreate\n");
	*handle = shm_handle;
      return (-1);
  }
  else {
    shm_handle = shmat(shm_id, 0, 0);
    if (shm_handle == (fwShm_HANDLE) -1) {			
      perror ("fw_shmCreate\n");
	  *handle = shm_handle;			
      return (-1);
    }
    shmctl(shm_id, IPC_STAT, &info);
    memset(shm_handle, 0, size);
  }

  /* Success */
  *handle = shm_handle;	
  return (0);
}

/* Delete the shared memory */
int32_t fw_shmDelete(key_t key)
{
  int shm_id;
  shm_id = shmget(key, 0, 0666 );
  if (shm_id < 0){
    perror("fw_shmDelete\n");
    return(-1);
  }
  else {
    int err=shmctl(shm_id, IPC_RMID, 0);
    if(err<0) {
      perror("fw_shmDelete\n");
	  return (-1);
    }		
  }
  return (0);
}


/* Slave Process would open the previously created Shared memory */
int32_t fw_shmOpen(key_t key, fwShm_HANDLE* handle)
{
  int shm_id;
  struct shmid_ds info;
  fwShm_HANDLE  h = NULL;
  shm_id = shmget(key, 0, 0666 );

  if (shm_id < 0)	{
    perror ("fw_shmOpen");
    return(-1);
  }
  else {
    shmctl(shm_id, IPC_STAT, &info);
    h = (fwShm_HANDLE) shmat(shm_id, 0, 0);
  }

  *handle = h;
  return (0);
}

int32_t fw_shmClose(fwShm_HANDLE* handle)
{
    /* Remove the references */
	*handle = NULL;
	return (0);
}

/* Nothing past this point */
