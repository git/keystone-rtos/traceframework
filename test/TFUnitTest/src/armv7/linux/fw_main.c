/*  (C) Copyright 2012, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <time.h>
#include <string.h>
#include "fw_mem_allocator.h"
#include "tfutest.h"
#include "fw_test.h"
#include "fw_task.h"

void  *topLevelTest (void *args);

uint32_t  taskNum;
/* Error counter */
uint32_t                errorCount = 0;

#define UTF_NUM_TASKS  4 /* minimum 2 tasks are needed for ARM only contracts, one for producer and another for a consumer (max tasks can be 5) */
/** ============================================================================
 *   @n@b main
 *
 *   @b Description
 *   @n test application main
 * 
 *   @return    int
 * =============================================================================
 */
int main(void) 
{	
  task_handle test_th;
  int status;
  int child = 0;
  int parent = 0;  
  pid_t pid;  
  int retVal = 0;  
  int i;

  if (armOnlyContract == 0){
  	taskNum = 1; /* set for consumer task */
    printf ("Creating single ARM consumer task \n");
  }
  else {
    printf ("ARM only Contract tests:  Forking off %d tasks\n", UTF_NUM_TASKS);
    /* make Trace framework Number of tasks */
    for (taskNum = 1; taskNum < UTF_NUM_TASKS; taskNum++)  {
      pid = fork();
      if (! pid) { 
        child = 1;
        break;
      }
    }
    if (! child) {
      taskNum = 0;
      parent = 1;
    }
  }	

  printf ("I am task num: %d \n", taskNum);
  
  fw_osalInit();

  /* clear the test framework local object */
  memset(&unitTestFramework, 0, sizeof (unitTestFramework));

  if (fw_memAllocInit((uint8_t*)MSMC_SRAM_BASE_ADDR,
                                  MSMC_TEST_PERM_MEM_SZ) == fw_FALSE) {
    printf("ERROR: \"Top Level Test\" fw_memAllocInit failed\n");
        return (-1);
  }

 // printf ("task_create() before from task num: %d \n", taskNum);
  status = task_create(topLevelTest, NULL, &test_th);
  if (status) {
        printf("ERROR: \"Top Level Test\" task-create failed (%d)\n", status);
        return (-1);
  }
 // printf ("task_create() after from task num: %d \n", taskNum);

  task_wait(&test_th);
  fw_osalshutdown();

  /* wait for children to exit */
  if (parent) {
#if 0
    printf("Waiting for children to exit\n");
#endif

	for (i = 0; i < (UTF_NUM_TASKS - 1); i++) {
      pid = wait (&status);
      if (WIFEXITED(status))   {
        if (WEXITSTATUS(status)) {
          printf ("Child %d returned fail (%d)\n", pid, WEXITSTATUS(status));
          retVal = 1;
		}
      }
      else {
        printf ("Child %d failed to exit\n", pid);
        retVal = 1;
      }
    }
    if ((! retVal) && (! errorCount)){
      printf ("\n\n ------- Trace Framework Unit Test Complete ---------\n");	      
    }
  }

  if (errorCount) {
        retVal = 1;
  }

  if (retVal) {
    printf("****FAIL****\n");
  }   
  
  return 0;
}

/** ============================================================================
 *   @n@b topLevelTest
 *
 *   @b Description
 *   @n Routine running as a separate thread
 * 
 *   @return    int
 * =============================================================================
 */
void *topLevelTest (void *args)
{
    task_handle fw_test_th;
    int status;
    utfTestArgs_t testArgs;
	Int passCount;
	Int failCount;
	Int notRunCount;
    int i;

    testArgs.tf = &unitTestFramework;
    
    printf ("\n\n ------- Trace Framework Unit Test Starting for taskNum: %d, args (1: yes, 0: no): %d ---------\n", taskNum, (args == NULL));

    if ((armOnlyContract == 1) && 
		(taskNum == 0) ) { /* Master task for the initialization */
      /* setup the test bench */
      if (setupTestFramework ())  {
      	printf ("topLevelTest (%s:%d): setupTestFramework returned error, exiting\n", __FILE__, __LINE__);
        goto tf_test_exit;
      }
  
      /* Make sure the setup matches what is expected */
      if (verifyTestFramework())  {
      	printf ("topLevelTest (%s:%d): verifyTestFramework returned error after initial framework setup, exiting\n", __FILE__, __LINE__);
        goto tf_test_exit;
      }
    }
	else  { /* Slave tasks */
      /* setup the test bench */
      if (setupTestFramework_slave() )  {
      	printf ("topLevelTest (%s:%d): setupTestFramework returned error, exiting\n", __FILE__, __LINE__);
        goto tf_test_exit;
      }
  
      /* Make sure the setup matches what is expected */
      if (verifyTestFramework())  {
      	printf ("topLevelTest (%s:%d): verifyTestFramework returned error after initial framework setup, exiting\n", __FILE__, __LINE__);
        goto tf_test_exit;
      }		
	}

	/* Run the tests */
	  for (i = 0; tfTestList[i].testFunction != NULL; i++ )  {		
      testArgs.pat = &tfTestList[i];

  		status = task_create(tfTestList[i].testFunction, (void *)&testArgs, &fw_test_th);
      if (status) {
        printf("ERROR: \"fwTest\" task-create failed (%d)\n", status);
        goto tf_test_exit;
      }

  		/* The test task will terminate upon completion. */
      task_wait(&fw_test_th);

  		if (armOnlyContract) {
  			if (taskNum == 0) {
  				if (tfTestList[i].testStatus == UTF_TEST_PASSED)
  				  printf ("%s:  PASSED\n", tfTestList[i].name);
  				else {
  				  printf ("%s:  FAILED\n", tfTestList[i].name);			
            goto tf_test_exit;
          }
  			}
  		}
  		else {
  			if (tfTestList[i].testStatus == UTF_TEST_PASSED)
  			  printf ("%s:  PASSED\n", tfTestList[i].name);
  			else {
  			  printf ("%s:  FAILED\n", tfTestList[i].name);
          goto tf_test_exit;          
        }
  		}

  		/* Do a quick check of the test framework */
  		if (verifyTestFramework ())  {
  			printf ("topLevelTest (%s:%d): verifyTestFramework returned error after test %s. Exiting.\n", __FILE__, __LINE__, tfTestList[i].name);
        goto tf_test_exit;
      }
    }

	/* Summarize the test results */
	if (armOnlyContract) {
		if (taskNum == 0) {	
			for (i = passCount = failCount = notRunCount = 0; tfTestList[i].testFunction != NULL; i++)	{
				if (tfTestList[i].testStatus == UTF_TEST_PASSED)
					passCount += 1;
				else if (tfTestList[i].testStatus == UTF_TEST_FAILED)
					failCount += 1;
				else
					notRunCount += 1;
			}
    	printf ("\n\nTest summary:\n\tTests Passed: %d\n\tTests Failed: %d\n\tTests not run: %d\n\n",
    		    	  passCount, failCount, notRunCount);

      if(passCount == unitTestFramework.numTests)
    	    System_printf ("All tests have passed!");     
		}	  
	}
	else {
		for (i = passCount = failCount = notRunCount = 0; tfTestList[i].testFunction != NULL; i++)  {
			if (tfTestList[i].testStatus == UTF_TEST_PASSED)
				passCount += 1;
			else if (tfTestList[i].testStatus == UTF_TEST_FAILED)
				failCount += 1;
			else
				notRunCount += 1;
		} 
  	printf ("\n\nTest summary:\n\tTests Passed: %d\n\tTests Failed: %d\n\tTests not run: %d\n\n",
  		    	  passCount, failCount, notRunCount);

    if(passCount == unitTestFramework.numTests)
  	    System_printf ("All tests have passed!");  
      
    printf ("\n\n ------- Trace Framework Unit Test Complete ---------\n");	      
	}

  	/* Clean the test framework */
   	if (taskNum == 0) {     
   		if (clearTestFramework() ) {
   			System_printf ("\n\n ------- TF Unit Test Clean Failed ---------\n");
   		}
   		System_printf ("\n\n ------- TF Unit Test Complete ---------\n");
    		System_flush();
   	}
   	else {
   		if (clearTestFramework_slave() ) {
   			System_printf ("\n\n ------- TF Unit Test Clean Failed ---------\n");
   		}
   	}	

tf_test_exit:
    pthread_exit((void*) 0);

    if  ( ( armOnlyContract == 1 ) &&
          ( taskNum == 0) )
    {
      /* There should not be any consumers still active at this time */
     	tf_contractGetInfo_t info;
      tf_contract_HANDLE   handle;

      handle = testCommonGetTfContractHandle (unitTestTFArmCUiaContractV2Name, TF_CONTRACT_VERSION_2);

	    tf_contractGetInfo(handle, &info); 

      if (info.num_consumers != 0)
        System_printf ("There are still (%d) consumers associated with this contract, premature exit \n", info.num_consumers);
    }
    return ((void*) 0);
}

/* Nothing past this point */
