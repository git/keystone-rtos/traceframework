/******************************************************************************
 * FILE PURPOSE:  Functions to OSAL related routines for running Example
 ******************************************************************************
 * FILE NAME:   fw_osal.c
 *
 * DESCRIPTION: Functions to initialize framework resources for running Example
 *
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2010-2011
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "fw_test.h"
#include <stdlib.h>
#include <stdio.h>
#include "fw_mem_allocator.h"
#include "tfutest.h"

#ifdef __TF_ARM_ONLY_CONTRACT
/* Semaphore Includes */
#include <semaphore.h>

/* Semaphore instance */
extern sem_t                       mutex;
#endif

#define System_printf   printf
uint32_t    globalCritkey; 

/* Lock to be used for critical section */
pthread_mutex_t mutex_lock;

void fw_osalInit() 
{
    pthread_mutex_init(&mutex_lock, NULL);
    return;
}

void fw_osalshutdown() 
{
    pthread_mutex_destroy(&mutex_lock);
    return;
}

static inline void fw_osalEnterCS()
{
#if 0
    pthread_mutex_lock(&mutex_lock);
#endif 
    return;
}

static inline void fw_osalLeaveCS()
{

#if 0
    pthread_mutex_unlock(&mutex_lock);
#endif
    return;
}

/*****************************************************************************
 * FUNCTION PURPOSE: Cache Invalidation Routine
 ***************************************************************************** 
 * DESCRIPTION: Cache Invalidation Routine
 *****************************************************************************/
void Osal_invalidateCache (void *blockPtr, uint32_t size) 
{
    /* Stub Function. TBD: Would need to handle when cache is enabled for ARM */

    /* to eliminate strict compilation error */	
	blockPtr = blockPtr;
	size = size;
    return;
}

/*****************************************************************************
 * FUNCTION PURPOSE: Cache Writeback Routine
 ***************************************************************************** 
 * DESCRIPTION: Cache Invalidation Routine
 *****************************************************************************/
void Osal_writeBackCache (void *blockPtr, uint32_t size) 
{
    /* Stub Function. TBD: Would need to handle when cache is enabled for ARM */

    /* to eliminate strict compilation error */	
	blockPtr = blockPtr;
	size = size;	
    return;
}


void Osal_fwCsEnter (uint32_t *key)
{ 

    /* Stub Function. TBD: Would need to handle when for multi proc access 
     * To be handled using infrastructure available from Kernel
     */
     key = key;
    return;
}

void Osal_fwCsExit (uint32_t key)
{

    /* Stub Function. TBD: Would need to handle when for multi proc access 
     * To be handled using infrastructure available from Kernel
     */

     /* to eliminate strict compilation error */	
     key = key;
    return;
}

void System_flush(void)
{
    fflush(stdout);
	return;
}

void Task_exit(void)
{
    return;
}

void Task_yield(void)
{
    return;
}

/*
* ======== disablePreemption ========
* This function is called by LoggerStreamer
* disable the preemption.
*/
uint32_t disablePreemption(){
	return (0);
}
/*
 * ======== restorePreemption ========
 * This function is called by LoggerStreamer
 * Restores the preemption enable state to the
 * value indicated by the key that is passed in as a parameter.
 */
void restorePreemption(uint32_t key){
	/* to eliminate strict compilation error */
    key = key;
	return;
}

/*
 * ======== getTimestamp ========
 * This function is called by LoggerStreamer when an event is being logged.
 * It returns the 64b timestamp to use for the event in the two words
 * pointed to by the lsw and msw UInt32* pointers.
 * In order to allow correlation of events from multiple cores on Keystone 
 * architecture devices, the application should be compiled with a predefined
 * symbol (GLOBAL_TIMESTAMP_ADRS) that specifies the virtual address of the 
 * emulation performance counter in the PLLctrl module 
 */
void getTimestamp(uint32_t* lsw, uint32_t* msw){
#ifdef GLOBAL_TIMESTAMP_ADRS
    *lsw = *(Uint32 *)(GLOBAL_TIMESTAMP_ADRS);
    *msw = *(Uint32 *)(4+GLOBAL_TIMESTAMP_ADRS);

#else
    Long res = clock();

    *lsw = (Bits32)(res & 0xFFFFFFFF);  // for multicore correlation, use 
#if xdc_target__sizeof_Long > xdc_target__sizeof_Int32
    *msw = (Bits32)(res >> 32);  // for multicore correlation, use 
#else
    *msw = 0;
#endif
#endif

}

void* Osal_contract_mmap (void *ptr, uint32_t size)
{
	/* to eliminate strict compilation error */
    size = size; 
	
	return(fw_PhyToVirt(ptr));
}

void Osal_contract_unmap (void *ptr, uint32_t size)
{        
     /* since the memory map did not actually map the memory
      * there is nothing to implement for unmap */

     /* to eliminate strict compilation error */	 
	 ptr = ptr;
	 size = size;
	 return;
}

/**
 * @brief  This macro is used to alert the application that the Trace Framework Contract is
 *         going to access table memory. The application must ensure
 *         cache coherency and semaphores for multi-core applications
 *
 *
 * <b> Prototype: </b>
 *  The following is the C prototype for the expected OSAL API.
 *
 *  @verbatim
        void Osal_contract_BeginMemAccess (void* addr, uint32_t sizeWords)
    @endverbatim
 *
 *  <b> Parameters </b>
 *  @n  The address of the table to be accessed
 *  @n  The number of bytes in the table
 *
 *  @note TF contract will make nested calls to this function for memory access
 *        protection of different memory tables. The multicore semaphore
 *        should be allocated only for the first call of a nested group 
 *        of calls. 
 */
 

void Osal_contract_BeginMemAccess (void* addr, uint32_t size)
{
    /* Cached ARM access, nothing to be implemented */
	
     /* to eliminate strict compilation error */	 
	 addr = addr;
	 size = size;
	 return;
}

/**
 * @brief  This macro is used to alert the application that the TF Contract
 *         has completed access to table memory. This call will always
 *         be made following a call to Osal_paBeginMemAccess and have
 *         the same parameters
 *
 * <b> Prototype: </b>
 *  The following is the C prototype for the expected OSAL API.
 *
 *  @verbatim
        void Osal_contract_EndMemAccess (void* addr, uint32_t sizeWords)
    @endverbatim
 *
 *  <b> Parameters </b>
 *  @n The address of the table to be accessed
 *  @n The number of bytes in the table
 *
 *  @note TF Contract will make nested calls to this function for memory access
 *        protection of different memory tables. The multicore semaphore
 *        should be freed when all previous memory access has completed,
 *        in other words, when the nested call level reaches 0.
 */

void Osal_contract_EndMemAccess (void* addr, uint32_t size)
{
    /* Cached ARM access, nothing to be implemented */

     /* to eliminate strict compilation error */	 
	 addr = addr;
	 size = size;
	 return;	
}

void* Osal_producer_MemAlloc (uint32_t num_bytes, uint32_t alignment)
{
	void* mem_ptr;	
	mem_ptr = malloc (num_bytes);	 
	alignment = alignment;
	/* Allocate a buffer from the default HeapMemMp */    
	return (mem_ptr);

}
void Osal_producer_MemFree (void* ptr, uint32_t size)
{
    size = size;
    free (ptr);
}
void Osal_producer_Exception(uint32_t moduleId, int32_t exceptionID)
{
 /* TBD  , needs to be implemented */
 moduleId = moduleId;
 exceptionID = exceptionID;
}
void Osal_producer_EndMemAccess(void* ptr, uint32_t size)
{
  ptr = ptr;
  size = size;
}

void* Osal_consumer_MemAlloc (uint32_t num_bytes, uint32_t alignment)
{
	void* mem_ptr;	
	mem_ptr = malloc (num_bytes);	 
	alignment = alignment;
	/* Allocate a buffer from the default HeapMemMp */    
	return (mem_ptr);

}
void Osal_consumer_MemFree (void* ptr, uint32_t size)
{
   size = size;
    free (ptr);
}

void Osal_consumer_BeginMemAccess(void* pkt, uint32_t size)
{
   pkt = pkt;
   size = size;
}

void Osal_consumer_EndMemAccess(void* ptr, uint32_t size)
{
  ptr = ptr;
  size = size;
}
/**
 *  @b Description
 *  @n
 *      The function is used to get the process ID.
 *
 *  @param[in]  None
 *      None.
 *
 *  @retval
 *      Returns the process ID
 */
uint32_t Osal_producer_GetProcId (void)
{   
    return (taskNum);
}


/**
 *  @b Description
 *  @n
 *      The function is used to protect contract memory from being written from
 *      multiple process/cores
 *
 *  @param[in]  None
 *      None.
 *
 *  @retval
 *      Returns the process ID
 */
void Osal_contract_Enter(void)
{
#ifdef __TF_ARM_ONLY_CONTRACT
  /* wait for the semaphore */
  sem_wait(&mutex);
#endif
}

/**
 *  @b Description
 *  @n
 *      The function is used to exit protect contract memory from being written from
 *      multiple process/cores
 *
 *  @param[in]  None
 *      None.
 *
 *  @retval
 *      None
 */
void Osal_contract_Exit(void)
{
#ifdef __TF_ARM_ONLY_CONTRACT
  /* Release the semaphore. */
  sem_post(&mutex);
#endif
}


