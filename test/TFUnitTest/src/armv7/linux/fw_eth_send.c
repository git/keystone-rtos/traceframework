/*
 *
 * Copyright (C) 2010-2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/



/* Generate and verify the system test framework
 * 
 * The test framework consists of the pa driver instance, a cppi/cdma/qm configuration,
 * memory for packet transmission and reception, and semaphores that are used
 * for every test in the PA unit test.
 * 
 */

#include "fw_eth_send.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h> 
#include <string.h>

/* Ethernet information */
static int  port_num_to_send;
static int skt;
static struct sockaddr_in si_other;
static int count = 0;

utfEth_HANDLE utf_GetEthSendHandle(utfEthConfig_t *utfEthConfig)
{
    int      i;
    
    port_num_to_send = utfEthConfig->remoteUdpPortNum;

    for ( i = 0; ;i ++) {
        if (utfEthConfig->dstIp[i] == '\r') {
            utfEthConfig->dstIp[i] = 0;
            break;
        }
    }
    
/* ETHERNET SETUP */
    skt=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(port_num_to_send);
    if (inet_aton(utfEthConfig->dstIp, &si_other.sin_addr)==0) {
        fprintf(stderr, "inet_aton() failed\n");
        exit(1);
    }
    memset((char *) &si_other.sin_zero, 0, sizeof(si_other.sin_zero));

/* ETHERNET SETUP DONE */    
    return ((utfEth_HANDLE)0xdeadface);
}

int32_t utf_EthDestroy(utfEth_HANDLE ethHandle)
{
    ethHandle = ethHandle;
    return (0);
}

int32_t utf_EthSend(utfEth_HANDLE ethhandle, uint8_t *logBuf, uint32_t size, int32_t param)
{
    size_t slen=sizeof(si_other);
    int    errorCode = 0;
    const struct sockaddr* pSockAddr = (struct sockaddr *)&si_other;

    ethhandle = ethhandle;
    printf("Sending packet:%d, of size =%d, buf index: %d\r", count++, size, ((uint8_t *) logBuf - (uint8_t *) param ) / size); 
    fflush(stdout);
    
    errorCode = sendto(skt, logBuf, (size_t)size, (int) 0, pSockAddr, slen);
    if (errorCode == -1){
        printf("ERROR: utf_EthSend: call to sendto returned %d.\n",errorCode);
        return (-1);
    }
    return (0);
}
/* Nothing past this point */
