/**  
 *  (C) Copyright 2012, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include "fw_test.h"
#include "fw_mem_allocator.h"
#include "fw_shm.h"
#include "fw_eth_send.h"
#include "tfutest.h"

/* Semaphore Includes */
#include <semaphore.h>

/* test framework instance */
unitTestFramework_t unitTestFramework;

/* Semaphore instance */
sem_t                       mutex;

/* shared memory handle */
fwShm_HANDLE                fwShmHandle; /* Shared memory handle */

/* This API demonstrates the initialization for contract version 2 */
int initTraceFrameworkContracts_ver2(void)
{
	uint32_t                       coreId;
	int                            i, numV2Contracts, retVal = 0;
    tf_contractConfig_t            tfContractConfig;
    tf_contract_HANDLE             tfContractHandle;
    char*                          name;
    shmLayout_t	                   *shmLayout = (shmLayout_t *)fwShmHandle;

	/* Get the core ID */
	coreId = taskNum;

	/* Setup the contract memory for version 1 contracts
    *  only master core is allowed to create the contract
    */
	if (coreId != 0)
		return (retVal);

	/* Create the unitTest contracts, Trace Framework APIs valid only for Contract Version 1 */
	memset (&tfContractConfig, 0, sizeof (tf_contractConfig_t));
	for (i = 0, numV2Contracts = 0; ; i++ )
	{
		if (tfuTestGlobalContractsV2[i].name == NULL)
			break;
		numV2Contracts++;
	}

	/* Update the information in the Unit Test structure */
    unitTestFramework.tfNumV2Contracts = numV2Contracts;

    /* initialize the first index for the name server */
    unitTestFramework.nameServerIndex = 0;

	for (i = 0; i < numV2Contracts; i ++ )
	{
        if (tfuTestGlobalContractsV2[i].owner != UTF_CONTRACT_OWNER_ARM)
			 continue;

		tfContractConfig.ringbuf_start_addr = &shmLayout->memBufRing[UTF_CONTRACT_VERSION_2][0];
		tfContractConfig.ring_base_type     = TF_CONTRACT_RING_BUF_NOT_PHYSICAL_ADDR;
		tfContractConfig.tf_contract_start_addr = &shmLayout->memContractRam[UTF_CONTRACT_VERSION_2][i * TF_CONTRACT_SIZE_BYTES];		
		tfContractConfig.contract_base_type = TF_CONTRACT_BUF_NOT_PHYSICAL_ADDR;
		tfContractConfig.num_ringbufs       = UTF_NUM_RINGBUFS;
   		tfContractConfig.ringbuf_size       = UTF_SIZE_RINGBUF;					
		name                                = tfuTestGlobalContractsV2[i].name;
		tfContractHandle                    = tf_newContract(&tfContractConfig);

		if (tfContractHandle == NULL)
		{
			System_printf ("Contract Setup Failed for %s", name);
			retVal = -1;
		}

		/* push the contracts created into the name server */
		retVal = testCommonNameServerPush(name, (void*) tfContractHandle);
		if (retVal)
		{
			System_printf("name server push failed : err code = %d\n", retVal);
			break;
		}
	}

	return (retVal);
}

static int clearTraceFrameworkContracts_ver1(void)
{
	uint32_t                       coreId;
    shmLayout_t	                   *shmLayout = (shmLayout_t *)fwShmHandle;

	/* Get the core ID */
	coreId = taskNum;

	/* Setup the contract memory for version 1 contracts
    *  only master core (core 0) of DSP is allowed to create the contract
    */
	if (coreId != 0)
		return (-1);

	/* destroy the contract memory pool that was setup before */
	memset(shmLayout->memContractRam, 0XCC, FW_TF_CONTRACT_MEMORY_SIZE);

	return (0);
}

/* This API demonstrates the initialization for contract version 1 */
int initTraceFrameworkContracts_ver1(void)
{
	uint32_t                       coreId;
	int                            i, numV1Contracts = 0, retVal = 0;
    tf_contractConfig_t            tfContractConfig;
    tf_contract_HANDLE             tfContractHandle;
    char*                          name;
    shmLayout_t	                   *shmLayout = (shmLayout_t *)fwShmHandle;

	/* Get the core ID */
	coreId = taskNum;

	/* Setup the contract memory for version 1 contracts
    *  only master core (core 0) of DSP is allowed to create the contract
    */
	if (coreId != 0)
		return (-1);

	/* Initialize all of the system contract, valid only for Contract Version 1*/
	TF_CONTRACT_INIT(&shmLayout->memContractRam[UTF_CONTRACT_VERSION_1][0], UTF_NUM_CONTRACTS);

	/* Create the unitTest contracts, Trace Framework APIs valid only for Contract Version 1 */
	memset (&tfContractConfig, 0, sizeof (tf_contractConfig_t));

	for (i = 0, numV1Contracts = 0; ; i++ )
	{
		if (tfuTestGlobalContractsV1[i].name == NULL)
			break;
		numV1Contracts++;
	}

	/* Update the information in the Unit Test structure */
    unitTestFramework.tfNumV1Contracts = numV1Contracts;


	for (i = 0; i < numV1Contracts; i ++ )	
	{
        if (tfuTestGlobalContractsV1[i].owner != UTF_CONTRACT_OWNER_ARM)
			 continue;

		tfContractConfig.num_contracts          = UTF_NUM_CONTRACTS;
		tfContractConfig.ringbuf_start_addr     = &shmLayout->memBufRing[UTF_CONTRACT_VERSION_1][0];
		tfContractConfig.ring_base_type         = TF_CONTRACT_RING_BUF_NOT_PHYSICAL_ADDR;
		tfContractConfig.tf_contract_start_addr = &shmLayout->memContractRam[UTF_CONTRACT_VERSION_1][0];
		tfContractConfig.contract_base_type     = TF_CONTRACT_BUF_NOT_PHYSICAL_ADDR;
		name                                    = tfuTestGlobalContractsV1[i].name;
		tfContractConfig.num_ringbufs           = UTF_NUM_RINGBUFS;
   		tfContractConfig.ringbuf_size           = UTF_SIZE_RINGBUF;			
		tfContractHandle                        = TF_CONTRACT_CREATE(&tfContractConfig, name);
		if (tfContractHandle == NULL)
		{
			System_printf ("Contract Setup Failed for %s", name);
			retVal = -1;
			break;
		}
	}

	return (retVal);
}

int initEthSend(void)
{
    FILE *fp;
    utfEthConfig_t ethConfig;
	/* Transport Arguments for ethernet send */
	transportArgs_t  transportArgs;
	extern void utlTestReadValues(FILE* fp, transportArgs_t* tArgs);

	/* Read the information from the transport file */
    fp=fopen("TFUnitTest_input.txt", "r");
    if (fp == NULL)
    {
    	System_printf ("can not open the Unit Test Configuration file: TFUnitTest_input.txt \n");
    	return (-1);
    }

    /* read the values from the file pointer to transportArgs*/
	utlTestReadValues(fp, &transportArgs);

	fclose (fp);

	/* clear the config structure */
	memset (&ethConfig, 0, sizeof (ethConfig));

    strncpy(ethConfig.dstIp, transportArgs.ipDst, 20);
    ethConfig.remoteUdpPortNum = transportArgs.remoteUdpPort;

	utf_GetEthSendHandle(&ethConfig);
	return (0);
}

/* Linux Semaphore Initialization */
int32_t fw_SemInit(void)
{
  /* create, initialize semaphore */
  if( sem_init(&mutex,1,1) < 0) {
    perror("sem_init:");
    return(-1);
  }
  return (0);
}

int32_t fw_SemDestroy(void)
{
  if( sem_destroy(&mutex) < 0) {
    perror("sem_destroy");
    return(-1);
  }
  return (0);
}

/******************************************************************************
* Macro to convert to IP Register Virtual Address from a mapped base Virtual Address
* Input: virtBaseAddr: Virtual base address mapped using mmap for IP
*        phyBaseAddr: Physical base address for the IP
*        phyRegAddr:  Physical register address
******************************************************************************/
static inline void* FW_GET_REG_VADDR (void * virtBaseAddr, uint32_t phyBaseAddr, uint32_t phyRegAddr)
{
    return((void *)((uint8_t *)virtBaseAddr + (phyRegAddr - phyBaseAddr)));
}

void traceFrameworkStartCfg(int isMaster)
{
  tf_StartCfg_t        startCfg;
  /* Set the system configuration for the contract */
  memset(&startCfg, 0, sizeof (tf_StartCfg_t));

  if (isMaster == TRUE) {
  	if (fw_shmCreate(FW_TEST_SHM_KEY, FW_TEST_SHARED_MEMORY_SIZE, &fwShmHandle)) {
      System_printf ("unable to create shared memory \n");
	  return;
  	}
  }
  else {
    if (fw_shmOpen(FW_TEST_SHM_KEY, &fwShmHandle)) {
	  System_printf ("unable to create shared memory \n");
	  return;
	}  	
  }
  /* set to zero for non "ARM ONLY" contracts
   * Note that this step is not mandatory for NON ARM ONLY contracts and is
   * provided below just to indicate the introduction of new API in traceframework
   * library that supports ARM only contracts and needs this function call, and to
   * provide message for customers wondering about what to do with this API with
   * "NON ARM CONTRACTS" that existed earlier
   */
  startCfg.instPoolBaseAddr = fwShmHandle;
  startCfg.instPoolSize     = FW_TEST_SHARED_MEMORY_SIZE;
  startCfg.ctrlBitMap      |= TF_CONTRACT_BUF_NOT_PHYSICAL_ADDR; /* Indicate that contract block base is Virtual address for contract version 1 */
  tf_systemCfg (&startCfg);
}

/* Initialize the test framework */
int setupTestFramework (void)
{
#ifdef __LINUX_USER_SPACE
  uint32_t id = taskNum;
#else
  uint32_t id = CSL_chipReadDNUM();
#endif
	/* Set the startup sync values */
	utlResetSyncVals(FALSE);
	utlWriteSyncValue(SYNC_TOKEN_INIT_VAL, id);

    unitTestFramework.tfGeneralProducerTest = 0; /* Default it is not on */

    testCommonResetTestStatus();

	/* Initialize the semaphore for multiprocess variables */
	if (fw_SemInit ()) {
	  System_printf ("Semaphore initialization failed \n");
	  return -1;
	}

	/* Start the configuration before doing anything on Trace framework per core/per process */
	traceFrameworkStartCfg(TRUE);
	
	/* Setup trace framework contracts for Version 1 */
	if (initTraceFrameworkContracts_ver1()) {
		System_printf ("setupTestFramework: initTraceFrameworkContracts_ver1 returned error, exiting\n");
		return (-1);
	}

	/* Setup trace framework contracts for Version 2 */
	if (initTraceFrameworkContracts_ver2()) {
		System_printf ("setupTestFramework: initTraceFrameworkContracts_ver2 returned error, exiting\n");
		return (-1);
	}	

	/* setup transport per core */
	if (initEthSend()) {
		System_printf ("setupTestFramework: initTraceFrameworkTransport returned error, exiting\n");
		return (-1);
	}	

	utlWriteSyncValue(SYNC_TOKEN_MASTER_SYSINIT_DONE, id);	

	return (0);
}

/* Clear the Test Framework */
int clearTestFramework (void)
{
	/* Destroy all name server elements */
    memset((void*) testCommonGetSysNameServerHandle(), 0xCC, sizeof (tfuTestNameServer_t)*UTF_NAMESERVER_CAPACITY);

	/* Clear the unit test framework elements */
	memset ((void*) &unitTestFramework, 0, sizeof (unitTestFramework_t));

	clearTraceFrameworkContracts_ver1();

    return (0);
}

/* Initialize the test framework */
int setupTestFramework_slave (void)
{
    int i, numContracts;
	/* wait until master set up is done */
	System_printf ("ARM consumers waiting for Master setup completion... ");
	System_flush();
	utlWaitUntilMasterDone(SYNC_TOKEN_MASTER_SYSINIT_DONE);
	System_printf("Done \n");
	System_flush();

	for (i = 0, numContracts = 0; ; i++ )
	{
		if (tfuTestGlobalContractsV2[i].name == NULL)
			break;
		numContracts++;
	}

	/* Update the information in the Unit Test structure */
    unitTestFramework.tfNumV2Contracts = numContracts;	

	for (i = 0, numContracts = 0; ; i++ )
	{
		if (tfuTestGlobalContractsV1[i].name == NULL)
			break;
		numContracts++;
	}
	
	unitTestFramework.tfNumV1Contracts = numContracts;
	unitTestFramework.tfGeneralProducerTest = 0; /* Default it is not on */


	/* Start the configuration before doing anything on Trace framework library functionalities on every core/process */
	if (armOnlyContract)
		traceFrameworkStartCfg(FALSE);	


	return (0);
}

int clearTestFramework_slave (void) {

	/* Clear the unit test framework elements */
	memset ((void*) &unitTestFramework, 0, sizeof (unitTestFramework_t));
	clearTraceFrameworkContracts_ver1();
	return (0);
}


/* Check that all the queues are setup correctly */
int verifyTestFramework (void)
{ 
	return (0);  
}


