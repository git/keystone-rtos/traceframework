#ifndef _TFW_UTEST_SHM
#define _TFW_UTEST_SHM

#ifdef __cplusplus
extern "C" {
#endif

/* ============================================================= */
/**
 *   @file  fw_test_shm.h
 *
 *   @brief  shared memory prototypes for ARM only contracts
 *
 *  ============================================================================
 *  Copyright (c) Texas Instruments Incorporated 2009-2013
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "tfutest.h"

#include <stdint.h>

/* Shared memory inludes */
#include <errno.h>
#include <sys/shm.h>
#include <fcntl.h>
#include <sys/ipc.h>

/* System call references */
extern void    perror();


#define FW_TEST_SHM_KEY  36 /* some random ID generated */
#define FW_TF_CONTRACT_MEMORY_SIZE   ( TF_CONTRACT_SIZE_BYTES * UTF_NUM_CONTRACTS * UTF_NUM_CONTRACT_VERSIONS) /* Total system contracts */
#define FW_TF_RINGBUF_MEMORY_SIZE    ( UTF_NUM_CONTRACT_VERSIONS * UTF_NUM_RINGBUFS * UTF_SIZE_RINGBUF)        /* Same ring buffer is shared for all similar contract versions */
#define FW_TEST_SHARED_MEMORY_SIZE   ( 128 + FW_TF_CONTRACT_MEMORY_SIZE + FW_TF_RINGBUF_MEMORY_SIZE )

typedef void*  fwShm_HANDLE;

typedef struct shmLayout_s {
  uint8_t  dummy[128]; /* 128 bytes of dummy bytes to make sure Contract and Ring buffers are not having zero offsets */
  uint8_t  memContractRam[UTF_NUM_CONTRACT_VERSIONS][UTF_NUM_CONTRACTS * TF_CONTRACT_SIZE_BYTES];  
  uint8_t  memBufRing[UTF_NUM_CONTRACT_VERSIONS][UTF_NUM_RINGBUFS * UTF_SIZE_RINGBUF];  
} shmLayout_t;

/* Function prototypes */
int32_t fw_shmCreate(key_t key, int size, fwShm_HANDLE* handle);
int32_t fw_shmDelete(key_t key);
int32_t fw_shmOpen(key_t key, fwShm_HANDLE* handle);
int32_t fw_shmClose(fwShm_HANDLE* handle);
									  
#ifdef __cplusplus
}
#endif
  

#endif  /* _TFW_UTEST_SHM */
