/*
 *
 * Copyright (C) 2010-2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/



/* TI Resource manager functions for the trace framework unit test */

#include "tfutest.h"

#if  defined(TEST_RM_VER1)
  extern const Rm_Resource rmResourceTable[];
#elif defined(TEST_RM_VER2)
  extern const char rmGlobalResourceList[];
  extern const char rmDspPlusArmPolicy[];
  extern const char rmDspOnlyPolicy[];
#endif


#if   defined(TEST_RM_VER1)
int initRm(void) {
    Rm_Result             rmResult;

    /* Initialize the resource manager with the specified table. */
    rmResult = Rm_init (&rmResourceTable[0]);

    if (rmResult != RM_OK)
    {
        return (-1);
    }

    return (0);

}
#endif

#if   defined(TEST_RM_VER2)
int initRm(void) {
    Rm_InitCfg               rmInitCfg;
    Rm_Handle                rmServerHandle;
    int32_t                  result, coreNum;

    coreNum = CSL_chipReadReg (CSL_CHIP_DNUM);

    rmInitCfg.instName = &rmServerName[0];
    rmInitCfg.instType = Rm_instType_SERVER;

    if (no_bootMode == TRUE) {
      rmInitCfg.instCfg.serverCfg.globalResourceList = (void *)rmGlobalResourceList;
      rmInitCfg.instCfg.serverCfg.globalPolicy = (void *)rmDspOnlyPolicy;
    }
    else {
      rmInitCfg.instCfg.serverCfg.globalResourceList = (void *)rmGlobalResourceList;
      rmInitCfg.instCfg.serverCfg.globalPolicy = (void *)rmDspPlusArmPolicy;
    }

    rmServerHandle = Rm_init(&rmInitCfg, &result);
    System_printf("Core %d: RM Server instance created. Result = %d\n", coreNum, result);

    rmServerServiceHandle = Rm_serviceOpenHandle(rmServerHandle, &result);

    return (0);
}
#endif

/* nothing past this point */
