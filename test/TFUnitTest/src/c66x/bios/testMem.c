/*
 *
 * Copyright (C) 2010-2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/



/* Static memory allocation for test framework */

#include "tfutest.h"

#pragma DATA_ALIGN(unitTestFramework, 16)
#pragma DATA_SECTION(unitTestFramework, ".localL2Sect");
unitTestFramework_t unitTestFramework;

/* Memory used for the linking RAM and descriptor RAM */
#pragma DATA_ALIGN(memLinkRam, 16)
uint64_t memLinkRam[UTF_NUM_DESC];

#pragma DATA_ALIGN(memDescRam, 16)
uint8_t memDescRam[UTF_NUM_DESC * UTF_SIZE_DESC];

#pragma DATA_ALIGN(memSyncRam, 16)
#pragma DATA_SECTION(memSyncRam, ".utestSyncRam");
tfuTestSyncVars_t memSyncRam[1];

#pragma DATA_ALIGN  (memContractRam, TF_CONTRACT_BUFS_ALIGN)
#pragma DATA_SECTION(memContractRam, ".utestContractBufs");
uint8_t memContractRam[UTF_NUM_CONTRACT_VERSIONS][UTF_NUM_CONTRACTS * TF_CONTRACT_SIZE_BYTES];

#pragma DATA_ALIGN  (memBufRing, TF_PRODUCER_LOGBUF_ALGIN)
#pragma DATA_SECTION(memBufRing, ".utestLogBuffers");
uint8_t  memBufRing[UTF_NUM_CONTRACT_VERSIONS][UTF_NUM_RINGBUFS * UTF_SIZE_RINGBUF];

/* PA CPDMA instance and channel handles */
Cppi_Handle tfPaCppiHandle;
Cppi_ChHnd  tfPaTxChHnd[UTF_PA_NUM_TX_CPDMA_CHANNELS];
Cppi_ChHnd  tfPaRxChHnd[UTF_PA_NUM_RX_CPDMA_CHANNELS];

/* Transport Arguments for ethernet send */
transportArgs_t  transportArgs;

/* Flow handles */
Cppi_FlowHnd tfPaFlowHnd;

/* Bare metal Ethernet Transport in master core and local consumers per core, allocate this section in local L2 for DSP */
/* Handle to BMET heap */
IHeap_Handle        uTestLocalHeap;

#pragma DATA_ALIGN  (tfuTestNameServer, TF_CONTRACT_BUFS_ALIGN)
#pragma DATA_SECTION(tfuTestNameServer, ".utestNameServer");
tfuTestNameServer_t    tfuTestNameServer[UTF_NAMESERVER_CAPACITY]; /* can hold up to 200 name, handles */

#pragma DATA_ALIGN  (testStatus, 128)
#pragma DATA_SECTION(testStatus, ".utestStatus");
testStatus_t   testStatus[TEST_STATUS_COUNT];

#ifdef  SIMULATOR_SUPPORT
uint32_t autodetectLogic = FALSE;
#else
uint32_t autodetectLogic = TRUE;
#endif

#ifdef __LINUX_USER_SPACE
uint32_t no_bootMode = FALSE;
#else
uint32_t no_bootMode = TRUE;
#endif

/* resource manager variables */
#if defined (TEST_RM_VER2)
char rmServerName[RM_NAME_MAX_CHARS] = "RM_Server";
Rm_ServiceHandle *rmServerServiceHandle = NULL;
#endif
