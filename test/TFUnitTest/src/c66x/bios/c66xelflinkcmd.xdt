%%{
/* 
 *  Copyright (c) 2013 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * */
%%}
%/*
% * ======== c66xlinkcmd.xdt ========
% *  This is template file is used to generate a linker command file for traceframework 
% *  DSP executables.  It provides a single place where we can embed information
% *  necessary to load/run executables.
% *
% *  This template is expanded after the configuration script runs and the
% *  results placed in a file (with extension .xdl) associated with the
% *  executable.
% *
% *  Linker templates are passed the following arguments:
% *     $out        - an open file stream for the generated linker
% *                   command file
% *     $args[]     - an array of zero or more libraries that should be linked
% *                   with (in the order they appear in the argument list)
% *
% *  In addition to these arguments, there is a global variable named
% *  'this' that is set as follows:
% *     this        - the program object
% */
%var _utils = xdc.loadCapsule("ti/targets/linkUtils.xs");
%var prog = this;    /* meaningful alias for this (== xdc.cfg.Program) */
/*
 * Do not modify this file; it is automatically generated from the template
 * c66xelflinkcmd.xdt in the ti.instrumentation.traceframework.test.TFUnitTest package and will be overwritten.
 */

/*
 * put '"'s around paths because, without this, the linker
 * considers '-' as minus operator, not a file name character.
 */

%for (var i = 0; i < prog.$$asmvec.length; i++) {
"`prog.$$asmvec[i]`"
%}

%for (var i = 0; i < $args.length; i++) {
-l"`$args[i]`"
%}

--retain="*(xdc.meta)"

%if (prog.$$isasm) {
%    return;
%}

--args `utils.toHex(prog.argSize)`
-heap  `utils.toHex(prog.heap)`
-stack `utils.toHex(prog.stack)`

MEMORY
{
%for (var i = 0; i < prog.cpu.memoryMap.length; i++) {
    %var mem = prog.cpu.memoryMap[i];
    %var page = (mem.page != null) ? ("PAGE " + mem.page + ": ") : "";
    %var org = utils.toHex(mem.base);
    %var len = utils.toHex(mem.len);
    %var access = (mem.access != null) ? (" (" + mem.access + ")") : "";
    %var rsvd_size_contract = 36864;
    %var rsvd_size_contract_h = utils.toHex(rsvd_size_contract);    
    %var rsvd_name_contract = ("TF_CONTRACT_RSVD");
    %var rsvd_size_nameserver = 4096;
    %var rsvd_size_nameserver_h = utils.toHex(rsvd_size_nameserver);    
    %var rsvd_name_nameserver = ("TF_NAMESERVER_RSVD");  
    %var rsvd_size_synchvars  = 4096;
    %var rsvd_size_synchvars_h = utils.toHex(rsvd_size_synchvars);    
    %var rsvd_name_synchvars = ("TF_SYNCMEM_RSVD");	
    %var rsvd_size_teststatus = 4096;
    %var rsvd_size_teststatus_h = utils.toHex(rsvd_size_teststatus);    
    %var rsvd_name_teststatus = ("TF_TESTSTATUS_RSVD");	
    %if (mem.name == "MSMCSRAM"){
    `page``rsvd_name_contract``access` : org = `org`, len = `rsvd_size_contract_h`
    %  org1 = utils.toHex(mem.base + rsvd_size_contract);
    `page``rsvd_name_nameserver``access` : org = `org1`, len = `rsvd_size_nameserver_h` 
    %  org2 = utils.toHex(mem.base + rsvd_size_contract + rsvd_size_nameserver);
    `page``rsvd_name_synchvars``access` : org = `org2`, len = `rsvd_size_synchvars_h` 
    %  org3 = utils.toHex(mem.base + rsvd_size_contract + rsvd_size_nameserver + rsvd_size_synchvars);
    `page``rsvd_name_teststatus``access` : org = `org3`, len = `rsvd_size_teststatus_h` 	
    %  org = utils.toHex(mem.base + rsvd_size_contract + rsvd_size_nameserver + rsvd_size_synchvars + rsvd_size_teststatus);
    %  len = utils.toHex(mem.len  - rsvd_size_contract - rsvd_size_nameserver - rsvd_size_synchvars - rsvd_size_teststatus);    
    %} 
    `page``mem.name``access` : org = `org`, len = `len`
%}
}

/*
 * Linker command file contributions from all loaded packages:
 */
`_utils.genContributions($args)`

/*
 * symbolic aliases for static instance objects
 */
%for (var sym in Program.symbol) {
    %var inst = Program.symbol[sym];
    %if (typeof(inst) == 'number') {
`this.build.target.asmName(sym)` = `inst`;
    %}
    %else {
        %var objTab = inst.$module.$name.replace(/\./g, '_') + '_Object__table__V';
        %var off = Program.$capsule.instSize(inst) * inst.$index;
`this.build.target.asmName(sym)` = `this.build.target.asmName(objTab)` + `off`;
    %}
%}

SECTIONS
{
`xdc.loadCapsule("ti/targets/elf/linkUtils.xs").genElfSections(prog)`
}
%%{
/*
 * @(#) ti.targets.elf; 1, 0, 0,367; 6-24-2013 15:21:59; /db/ztree/library/trees/xdctargets/xdctargets-g31x/src/ xlibrary

 */

%%}
