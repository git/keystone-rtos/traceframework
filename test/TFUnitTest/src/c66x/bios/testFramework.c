/*
 *
 * Copyright (C) 2010-2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/



/* Generate and verify the system test framework
 * 
 * The test framework consists of the pa driver instance, a cppi/cdma/qm configuration,
 * memory for packet transmission and reception, and semaphores that are used
 * for every test in the PA unit test.
 * 
 */

#include <stdlib.h>
#include <string.h>
#include "tfutest.h"

#include <ti/csl/cslr_device.h>
#include <ti/csl/csl_semAux.h>
#include <ti/csl/csl_chipAux.h>

#include <ti/sysbios/family/c64p/Hwi.h>
#include <ti/sysbios/family/c64p/EventCombiner.h> 

/* Firmware images */
#include <ti/drv/qmss/qmss_firmware.h>

#if defined (PARTNO_C6678) || defined (PARTNO_C6670) || defined (PARTNO_C6657) || defined (PARTNO_C6614)
/* QMSS device specific configuration */
extern Qmss_GlobalConfigParams  qmssGblCfgParams[];

/* CPPI device specific configuration */
extern Cppi_GlobalConfigParams  cppiGblCfgParams[];
#elif defined (DEVICE_K2L) || defined (DEVICE_K2E) || defined (DEVICE_K2K) || defined (DEVICE_K2H)
extern Qmss_GlobalConfigParams qmssGblCfgParams;
extern Cppi_GlobalConfigParams cppiGblCfgParams;
#endif

extern uint32_t      coreKey [];




/**
 *  @b Description
 *  @n
 *      The function is used to get the handle to the BMET memory heap.
 *      Make sure this heap is allocated in Local L2
 *
 *  @retval
 *      None
 */
static void uTestLocalHeapInit (void)
{
	uTestLocalHeap = HeapMem_Handle_upCast (uTestLocalHeapSect);
}

/**
 *  @b Description
 *  @n
 *      The function is used to enter a critical section.
 *      Function protects against
 *
 *      access from multiple threads on single core
 *      and
 *      access from multiple cores
 *
 *  @param[in]  key
 *      Key used to lock the critical section.
 *
 *  @retval
 *      Not Applicable
 */
void unitTest_MtCsEnter (uint32_t *key)
{

    /* Get the hardware semaphore.
     *
     * Acquire Multi core  synchronization lock
     */
     while ((CSL_semAcquireDirect (UNIT_TEST_HW_SEM)) == 0);
     /* Disable all interrupts and OS scheduler.
      *
      * Acquire Multi threaded / process synchronization lock.
      */
     coreKey [CSL_chipReadReg (CSL_CHIP_DNUM)] = Hwi_disable();
     //*key = 0;
}

/**
 *  @b Description
 *  @n
 *      The function is used to exit a critical section
 *      protected using unitTest_MtCsExit() API.
 *
 *  @param[in]  key
 *      Key used to unlock the critical section.
 *
 *  @retval
 *      Not Applicable
 */
void unitTest_MtCsExit (uint32_t key)
{
    /* Enable all interrupts and enables the OS scheduler back on.
     *
     * Release multi-threaded / multi-process lock on this core.
     */
    Hwi_restore(coreKey [CSL_chipReadReg (CSL_CHIP_DNUM)]);

	/* Release the hardware semaphore */
    CSL_semReleaseSemaphore (UNIT_TEST_HW_SEM);
}

void traceFrameworkStartCfg(void)
{
  tf_StartCfg_t        startCfg;
  /* Set the system configuration for the contract */
  memset(&startCfg, 0, sizeof (tf_StartCfg_t));

  /* set to zero for non "ARM ONLY" contracts
   * Note that this step is not mandatory for NON ARM ONLY contracts and is
   * provided below just to indicate the introduction of new API in traceframework
   * library that supports ARM only contracts and needs this function call, and to
   * provide message for customers wondering about what to do with this API with
   * "NON ARM CONTRACTS" that existed earlier
   */
  tf_systemCfg (&startCfg);
}

/************************** FUNCTIONS *************************/
int setupQmMem (void)
{
  Qmss_InitCfg     qmssInitConfig;
  Qmss_StartCfg    qmssCfg;
  Qmss_MemRegInfo  memInfo;
  Cppi_DescCfg     descCfg;
  int32_t          result;
#if !defined(PARTNO_C6657)
  int              n;
#endif

  memset (&qmssInitConfig, 0, sizeof (Qmss_InitCfg));
  memset (memDescRam,      0, sizeof (memDescRam));

  qmssInitConfig.linkingRAM0Base = utilgAddr((uint32_t)memLinkRam);
  qmssInitConfig.linkingRAM0Size = UTF_NUM_DESC;
  qmssInitConfig.linkingRAM1Base = 0;
  qmssInitConfig.maxDescNum      = UTF_NUM_DESC;

  if (no_bootMode == TRUE)
  {
      qmssInitConfig.pdspFirmware[0].pdspId = Qmss_PdspId_PDSP1;
#ifdef _LITTLE_ENDIAN    
      qmssInitConfig.pdspFirmware[0].firmware = (void *) &acc48_le;
      qmssInitConfig.pdspFirmware[0].size = sizeof (acc48_le);
#else
      qmssInitConfig.pdspFirmware[0].firmware = (void *) &acc48_be;
      qmssInitConfig.pdspFirmware[0].size = sizeof (acc48_be);
#endif
  }
  else  /* Bypass hardware initialization as it is done somewhere else like within linux Kernel */
  {
	  System_printf("WARNING:Due to the boot mode set on the EVM, assuming all Hardware initializations already done (like Linux)\n");
	  System_printf("          Please set the EVM to NO_BOOT_MODE if this is not the case to run this test \n");
      qmssInitConfig.qmssHwStatus     =   QMSS_HW_INIT_COMPLETE;
  }
  
  /* Initialize the configurations */
  memset ((void *)&qmssCfg, 0, sizeof(Qmss_StartCfg));

#if defined (TEST_RM_VER1)
  qmssCfg.rmHandle                   = unitTestFramework.rm_handle = Rm_getHandle();
  qmssGblCfgParams[0].qmRmHandle     = qmssCfg.rmHandle;
  result                             = Qmss_init (&qmssInitConfig, &qmssGblCfgParams[0]);
#elif defined (TEST_RM_VER2)
  qmssCfg.rmServiceHandle            = rmServerServiceHandle;
  qmssGblCfgParams.qmRmServiceHandle = rmServerServiceHandle;
  result                             = Qmss_init (&qmssInitConfig, &qmssGblCfgParams);
#endif

  if (result != QMSS_SOK)  {
      if ((result == QMSS_RESOURCE_LINKING_RAM_INIT_DENIED) || (result == QMSS_RESOURCE_USE_DENIED))
      {
          /* Init Denied error simply implies that the QMSS has been initialized by another
           * entity in the system. We can carry on with the rest of the system initialization. */
      }
      else
      {
    	  System_printf ("setupQmMem: Qmss_Init failed with error code %d\n", result);
    	  return (-1);
      }
  }
#ifdef NO_RESOUCE_MANAGER
  result = Qmss_start();
#else
  result = Qmss_startCfg(&qmssCfg);
#endif
  
  if (result != QMSS_SOK)  {
    System_printf ("setupQmMem: Qmss_start failed with error code %d\n", result);
    return (-1);
  }

  /* Setup a single memory region for descriptors */
  memset (memDescRam, 0, sizeof(memDescRam));
  memInfo.descBase       = (uint32_t *)utilgAddr((uint32_t)memDescRam);
  memInfo.descSize       = UTF_SIZE_DESC;
  memInfo.descNum        = UTF_NUM_DESC;
  memInfo.manageDescFlag = Qmss_ManageDesc_MANAGE_DESCRIPTOR;
  memInfo.memRegion      = Qmss_MemRegion_MEMORY_REGION0;
  memInfo.startIndex     = 0;

  result = Qmss_insertMemoryRegion (&memInfo);
  if (result < QMSS_SOK)  {
    System_printf ("setupQmMem: Qmss_insertMemoryRegion returned error code %d\n", result);
    return (-1);
  }

  
  /* Initialize the descriptors. This function opens a general
   * purpose queue and intializes the memory from region 0, placing
   * the initialized descriptors onto that queue */
  memset (&descCfg, 0, sizeof (descCfg));
  descCfg.memRegion         = Qmss_MemRegion_MEMORY_REGION0;
  descCfg.descNum           = UTF_NUM_DESC;
  descCfg.destQueueNum      = UTF_FREE_GENERAL_QUEUE_NUM;  
  descCfg.queueType         = Qmss_QueueType_GENERAL_PURPOSE_QUEUE;
  descCfg.initDesc          = Cppi_InitDesc_INIT_DESCRIPTOR;
  descCfg.descType          = Cppi_DescType_HOST;
  descCfg.returnQueue.qNum  = QMSS_PARAM_NOT_SPECIFIED;
  descCfg.returnQueue.qMgr  = 0;
  descCfg.epibPresent       = Cppi_EPIB_EPIB_PRESENT;
  
  descCfg.cfg.host.returnPolicy     = Cppi_ReturnPolicy_RETURN_ENTIRE_PACKET;
  descCfg.cfg.host.psLocation       = Cppi_PSLoc_PS_IN_DESC;
  
  descCfg.returnPushPolicy          =   Qmss_Location_TAIL;    


#if defined(PARTNO_C6657)
  /* There is no Free descriptor needed for the ethernet transport */
#else
  /* purge all the descriptors if initially present in the queue */
 // Qmss_queueEmpty (UTF_FREE_GENERAL_QUEUE_NUM);

  unitTestFramework.QfreeDesc = Cppi_initDescriptor (&descCfg, (uint32_t *)&n);

  if (n != descCfg.descNum)  {
    System_printf ("setupQmMem: expected %d descriptors to be initialized, only %d are initialized\n", descCfg.descNum, n);
    return (-1);
  }
#endif

  return (0);

}


int setupCpdma (void)
{
  Cppi_StartCfg     cppiCfg;
  int32_t           result;
#if !defined (PARTNO_C6657)
  Cppi_CpDmaInitCfg cpdmaCfg;
  Cppi_RxChInitCfg  rxChCfg;
  Cppi_TxChInitCfg  txChCfg;
  int   i;
  Uint8 isAlloc;
  uint32_t cppi_pa_tx_ch_disable_list[UTF_PA_NUM_TX_CPDMA_CHANNELS];
  uint32_t cppi_pa_rx_ch_disable_list[UTF_PA_NUM_RX_CPDMA_CHANNELS];
  uint32_t disable_list_flag = 0;

  /* Clear the list by default */
  memset (cppi_pa_tx_ch_disable_list, 0, sizeof (cppi_pa_tx_ch_disable_list));
  memset (cppi_pa_rx_ch_disable_list, 0, sizeof (cppi_pa_rx_ch_disable_list));
#endif

#if defined(TEST_RM_VER1)
  result = Cppi_init(&cppiGblCfgParams[0]);
#elif defined (TEST_RM_VER2)
  result = Cppi_init (&cppiGblCfgParams);
#endif

  if (result != CPPI_SOK)  {
    System_printf ("setupCpdma: cpp_Init returned error %d\n", result);
    return (-1);
  }

#if !defined(PARTNO_C6657)
  memset(&cpdmaCfg, 0, sizeof(Cppi_CpDmaInitCfg));
  cpdmaCfg.dmaNum           = Cppi_CpDma_PASS_CPDMA;

  unitTestFramework.tfPaCppiHandle = Cppi_open (&cpdmaCfg);
  if (unitTestFramework.tfPaCppiHandle == NULL)  {
    System_printf ("setupCpdma: cppi_Open returned NULL cppi handle\n");
    return (-1);
  }
#endif

  memset ((void *)&cppiCfg, 0, sizeof(Cppi_StartCfg));
#if defined (TEST_RM_VER1)
  cppiCfg.rmHandle  = unitTestFramework.rm_handle;
#elif defined (TEST_RM_VER2)
  cppiCfg.rmServiceHandle = rmServerServiceHandle;
#endif

  /* Start the CPPI with the resource manager. */
  Cppi_startCfg(&cppiCfg);

#if !defined (PARTNO_C6657)
  /* Open all 24 rx channels */
  rxChCfg.rxEnable = Cppi_ChState_CHANNEL_DISABLE;

  for (i = 0, disable_list_flag = 0; i < UTF_PA_NUM_RX_CPDMA_CHANNELS; i++)  {
    rxChCfg.channelNum        = i;
    unitTestFramework.tfPaRxChHnd[i] = Cppi_rxChannelOpen (unitTestFramework.tfPaCppiHandle, &rxChCfg, &isAlloc);

    if (unitTestFramework.tfPaRxChHnd[i] == NULL)  {
      if (no_bootMode == TRUE) {
        System_printf ("setupCpdma: cppi_RxChannelOpen returned NULL handle for channel number %d\n", i);
        return (-1);
      }
      else {
        cppi_pa_rx_ch_disable_list[i] = 1;
      	disable_list_flag = 1;
      	continue;
      }
    }
    /* Also enable Rx Channel */
    Cppi_channelEnable (unitTestFramework.tfPaRxChHnd[i]);
  }

  /* Print if there are any CPPI Rx channels that are not enabled by above code, presuming Linux enabled it */
  if (disable_list_flag)
  {
  	System_printf ("Unable to open below cppi Rx channels...presuming Linux has already enabled it \n");
	for (i = 0; i < UTF_PA_NUM_RX_CPDMA_CHANNELS; i++)	{
	  if (cppi_pa_rx_ch_disable_list[i])
	    System_printf ("%d ", i);
	}
	System_printf (" \n ");
  }

  /* Open all 10 tx channels.  */
  txChCfg.priority     = 2;
  txChCfg.txEnable     = Cppi_ChState_CHANNEL_DISABLE;
  txChCfg.filterEPIB   = FALSE;
  txChCfg.filterPS     = FALSE;
  txChCfg.aifMonoMode  = FALSE;
  

  for (i = 0, disable_list_flag = 0; i < UTF_PA_NUM_TX_CPDMA_CHANNELS; i++)  {
    txChCfg.channelNum 	 	  = i;
    unitTestFramework.tfPaTxChHnd[i] = Cppi_txChannelOpen (unitTestFramework.tfPaCppiHandle, &txChCfg, &isAlloc);

    if (unitTestFramework.tfPaTxChHnd[i] == NULL)  {
      if (no_bootMode == TRUE) {
        System_printf ("setupCpdma: cppi_TxChannelOpen returned NULL handle for channel number %d\n", i);
        return (-1);
      }
      else {
        cppi_pa_tx_ch_disable_list[i] = 1;
    	disable_list_flag = 1;
    	continue;
      }
    }
    
    Cppi_channelEnable (unitTestFramework.tfPaTxChHnd[i]);
  }

  /* Print if there are any CPPI Tx channels that are not enabled by above code, presuming Linux enabled it */
  if (disable_list_flag)
  {
  	System_printf ("Unable to open below cppi Tx channels...presuming Linux has already enabled it \n");
	for (i = 0; i < UTF_PA_NUM_TX_CPDMA_CHANNELS; i++)	{
	  if (cppi_pa_tx_ch_disable_list[i])
	    System_printf ("%d ", i);
	}
	System_printf (" \n ");
  }
  /* Clear CPPI Loobpack bit in PASS CDMA Global Emulation Control Register */
  Cppi_setCpdmaLoopback(unitTestFramework.tfPaCppiHandle, 0);
#endif

  return (0);

}

static inline void closeAllOpenedQueues(void) {

#if defined (PARTNO_C6657)
#else
	  unitTestFramework_t *tf=&unitTestFramework;
	  /* Empty the remaining queues */
	  Qmss_queueEmpty (tf->QfreeDesc);
	  /* Close the remaining queues */
	  Qmss_queueClose (tf->QfreeDesc);
#endif
}

/* The QM/CPDMA are cleared */
static int clearQm(void)
{
  int result = QMSS_SOK;
#if defined(PARTNO_C6657)
#else
  int i;
#endif

  /* Close the queues that were setup */
  closeAllOpenedQueues();

#if !defined (PARTNO_C6657)
  if (no_bootMode == TRUE) {
      /* Close the cpDma setup */
     for (i = 0; i < UTF_PA_NUM_RX_CPDMA_CHANNELS; i++)  {
    	 if ((result = Cppi_channelClose (unitTestFramework.tfPaRxChHnd[i])) != CPPI_SOK) {
    	 	return (result);
    	 }	 
     }
     for (i = 0; i < UTF_PA_NUM_TX_CPDMA_CHANNELS; i++)  {
    	 if ((result = Cppi_channelClose (unitTestFramework.tfPaTxChHnd[i])) != CPPI_SOK) {
    	 	return (result);
    	 }	 
     }   
  }
#endif

#if defined(DEVICE_K2K) || defined(DEVICE_K2H)
   /* Free the memory regions */
   if ((result = Qmss_removeMemoryRegion (Qmss_MemRegion_MEMORY_REGION0, 0)) != QMSS_SOK)
   {
       System_printf ("Error Core %d : Remove memory region error code : %d\n", DNUM, result);
   }
#endif
   return (result);

}

/* The QM/CPDMA are setup */
int initQm (void)
{
  if (setupQmMem())  {
  	System_printf ("initQm: setupQmMem failed\n");
    return (-1);
  }

  if (setupCpdma ())  {
  	System_printf ("initQm: setupCpdma failed\n");
    return (-1);
  }
  
  return (0);
  
}

void ipcInterruptHandler(void)
{
   uint32_t   coreId;
   coreId =  CSL_chipReadDNUM();

   /* Post the notification internally */
   unitTestFramework.consumerNotification = UTEST_CONSUMER_NOTIFY_REGMASK_EVENT_POST;

   /* Ack the interrupt */
   CSL_IPC_clearGEMInterruptSource(coreId, UNIT_TEST_TRACE_FRAMEWORK_BUF_IPC_NOTIFY_SRC_ID);

}

#if !defined (DEVICE_K2L) && !defined (DEVICE_K2E) && !defined (DEVICE_K2H) && !defined (DEVICE_K2K)
int initIpc(void) {

	int index;

    /* clear the ipc interrupt */
	for (index = 0; index < MAX_NUM_CORES; index++) {
		CSL_IPC_clearGEMInterruptSource(index, UNIT_TEST_TRACE_FRAMEWORK_BUF_IPC_NOTIFY_SRC_ID);
	}

    EventCombiner_dispatchPlug (CSL_GEM_IPC_LOCAL, (EventCombiner_FuncPtr)ipcInterruptHandler, NULL, TRUE );
	EventCombiner_enableEvent(CSL_GEM_IPC_LOCAL);

    /* Map the event id to hardware interrupt 9 */
    Hwi_eventMap( 9, CSL_GEM_IPC_LOCAL );

    /* Enable interrupt 9 */
    Hwi_enableInterrupt(9);
	
		
    return (0);	
}	

static int clearIpc(void) {
	int index;

    /* clear the ipc interrupt */
	for (index = 0; index < MAX_NUM_CORES; index++) {
		CSL_IPC_clearGEMInterruptSource(index, UNIT_TEST_TRACE_FRAMEWORK_BUF_IPC_NOTIFY_SRC_ID);
	}

	EventCombiner_disableEvent(CSL_GEM_IPC_LOCAL);

    /* Disable interrupt 9 */
    Hwi_disableInterrupt(9);

    return (0);
}
#endif

/* This API demonstrates the initialization for contract version 2 */
int initTraceFrameworkContracts_ver2(void)
{
	uint32_t                       coreId;
	int                            i, numV2Contracts, retVal = 0;
    tf_contractConfig_t            tfContractConfig;
    tf_contract_HANDLE             tfContractHandle;
    char*                          name;

	/* Get the core ID */
	coreId = CSL_chipReadDNUM();

	/* Setup the contract memory for version 1 contracts
    *  only master core (core 0) of DSP is allowed to create the contract
    */
	if (coreId != 0)
		return (retVal);

	/* Create the unitTest contracts, Trace Framework APIs valid only for Contract Version 1 */
	memset (&tfContractConfig, 0, sizeof (tf_contractConfig_t));
	for (i = 0, numV2Contracts = 0; ; i++ )
	{
		if (tfuTestGlobalContractsV2[i].name == NULL)
			break;
		numV2Contracts++;
	}

	/* Update the information in the Unit Test structure */
    unitTestFramework.tfNumV2Contracts = numV2Contracts;

    /* initialize the first index for the name server */
    unitTestFramework.nameServerIndex = 0;

	for (i = 0; i < numV2Contracts; i ++ )
	{
        if (tfuTestGlobalContractsV2[i].owner != UTF_CONTRACT_OWNER_DSP)
			 continue;

		tfContractConfig.ringbuf_start_addr = &memBufRing[UTF_CONTRACT_VERSION_2][0];
		tfContractConfig.ring_base_type     = TF_CONTRACT_RING_BUF_PHYSICAL_ADDR;
		tfContractConfig.tf_contract_start_addr = &memContractRam[UTF_CONTRACT_VERSION_2][i * TF_CONTRACT_SIZE_BYTES];		
		tfContractConfig.contract_base_type = TF_CONTRACT_BUF_PHYSICAL_ADDR;
		
		name                                = tfuTestGlobalContractsV2[i].name;

		/* Create ping pong ring buffers for General Producer */
		if (strcmp(name, uintTestTFDspGenContractV2Name) == 0) {
			tfContractConfig.num_ringbufs       = UTF_NUM_GENPROD_RINGBUFS;
     		tfContractConfig.ringbuf_size       = UTF_GENPROD_BUF_SIZE;			
		}
		else {
			tfContractConfig.num_ringbufs       = UTF_NUM_RINGBUFS;
    		tfContractConfig.ringbuf_size       = UTF_SIZE_RINGBUF;			
		}
		tfContractHandle                    = tf_newContract(&tfContractConfig);
		//tfuTestGlobalContractsV2[i].tf_contract_handle =  tfContractHandle;
		if (tfContractHandle == NULL)
		{
			System_printf ("Contract Setup Failed for %s", name);
			retVal = -1;
		}

		/* push the contracts created into the name server */
		retVal = testCommonNameServerPush(name, (void*) tfContractHandle);
		if (retVal)
		{
			System_printf("name server push failed : err code = %d\n", retVal);
			break;
		}
	}

	return (retVal);
}

static int clearTraceFrameworkContracts_ver1(void)
{
	uint32_t                       coreId;

	/* Get the core ID */
	coreId = CSL_chipReadDNUM();

	/* Setup the contract memory for version 1 contracts
    *  only master core (core 0) of DSP is allowed to create the contract
    */
	if (coreId != 0)
		return (-1);

	/* destroy the contract memory pool that was setup before */
	memset(memContractRam, 0XCC, sizeof (memContractRam));

	return (0);
}

/* This API demonstrates the initialization for contract version 1 */
int initTraceFrameworkContracts_ver1(void)
{
	uint32_t                       coreId;
	int                            i, numV1Contracts = 0, retVal = 0;
    tf_contractConfig_t            tfContractConfig;
    tf_contract_HANDLE             tfContractHandle;
    char*                          name;

	/* Get the core ID */
	coreId = CSL_chipReadDNUM();

	/* Setup the contract memory for version 1 contracts
    *  only master core (core 0) of DSP is allowed to create the contract
    */
	if (coreId != 0)
		return (-1);

	/* Initialize all of the system contract, valid only for Contract Version 1*/
	TF_CONTRACT_INIT(&memContractRam[UTF_CONTRACT_VERSION_1][0], UTF_NUM_CONTRACTS);

	/* Create the unitTest contracts, Trace Framework APIs valid only for Contract Version 1 */
	memset (&tfContractConfig, 0, sizeof (tf_contractConfig_t));

	for (i = 0, numV1Contracts = 0; ; i++ )
	{
		if (tfuTestGlobalContractsV1[i].name == NULL)
			break;
		numV1Contracts++;
	}

	/* Update the information in the Unit Test structure */
    unitTestFramework.tfNumV1Contracts = numV1Contracts;

	for (i = 0; i < numV1Contracts; i ++ )	
	{
        if (tfuTestGlobalContractsV1[i].owner != UTF_CONTRACT_OWNER_DSP)
			 continue;

		tfContractConfig.num_contracts          = UTF_NUM_CONTRACTS;
		tfContractConfig.ringbuf_start_addr     = &memBufRing[UTF_CONTRACT_VERSION_1][0];
		tfContractConfig.ring_base_type         = TF_CONTRACT_RING_BUF_PHYSICAL_ADDR;
		tfContractConfig.tf_contract_start_addr = &memContractRam[UTF_CONTRACT_VERSION_1][0];
		tfContractConfig.contract_base_type     = TF_CONTRACT_BUF_PHYSICAL_ADDR;
		name                                    = tfuTestGlobalContractsV1[i].name;

		/* Create ping pong ring buffers for General Producer */
		if (strcmp(name, uintTestTFDspGenContractV1Name) == 0) {
			tfContractConfig.num_ringbufs       = UTF_NUM_GENPROD_RINGBUFS;
     		tfContractConfig.ringbuf_size       = UTF_GENPROD_BUF_SIZE;			
		}
		else {
			tfContractConfig.num_ringbufs       = UTF_NUM_RINGBUFS;
    		tfContractConfig.ringbuf_size       = UTF_SIZE_RINGBUF;			
		}		

		tfContractHandle                        = TF_CONTRACT_CREATE(&tfContractConfig, name);
		//tfuTestGlobalContractsV1[i].tf_contract_handle =  tfContractHandle;
		if (tfContractHandle == NULL)
		{
			System_printf ("Contract Setup Failed for %s", name);
			retVal = -1;
			break;
		}
	}

	return (retVal);
}

#define STR_VALUE(arg)      #arg
#define FUNCTION_NAME(name) STR_VALUE(name)

//#define PIP              PDK INSTALL LOCATION STRING
#define PDK_INSTALL_PATH_STR   FUNCTION_NAME(PIP)

#define TF_UTEST_FILE_PATH "/ti/instrumentation/traceframework/test/TFUnitTest/TFUnitTest_input.txt"

static void getfname(char *fn)
{
    char bpath[512];
    char tpath[512];
    int l1, l2;

    fn[0] = 0;

    strcpy (tpath, TF_UTEST_FILE_PATH);

    l1 = strlen (PDK_INSTALL_PATH_STR);

    if (l1 < 511)
    	strcpy (bpath, PDK_INSTALL_PATH_STR);
    else
    	bpath[0] = 0;

    l2 = strlen (tpath);

    if ( (l1 + l2) < 511 )
    	strcat (bpath, tpath);

   	strcpy (fn, bpath);

    System_printf ("Input file path discovered in PDK location as = %s \n", fn);
    System_flush();

    return;
}

int initEthSend(void)
{
    FILE *fp;
    utfEthConfig_t ethConfig;
    char fname[512];

    getfname(fname);

	/* Read the information from the transport file */
    fp=fopen(fname, "r");

    if (fp == NULL)
    {
    	System_printf ("can not open the Unit Test Configuration file: TFUnitTest_input.txt \n");
    	return (-1);
    }

    /* read the values from the file pointer to transportArgs*/
	utlTestReadValues(fp, &transportArgs);

	fclose (fp);

	/* clear the config structure */
	memset (&ethConfig, 0, sizeof (ethConfig));

#if defined(PARTNO_C6657)
#else
	ethConfig.QfreeDesc        = unitTestFramework.QfreeDesc;
    ethConfig.desc_size        = UTF_SIZE_DESC;
#endif

    ethConfig.dstIp            = &transportArgs.ipDst;
    ethConfig.dstMac           = &transportArgs.macDst;
    ethConfig.remoteUdpPortNum = transportArgs.remoteUdpPort;
    ethConfig.srcIp            = &transportArgs.ipSrc;
    ethConfig.srcMac           = &transportArgs.macSrc;
#if defined(LOGGERSTREAMER2_SUPPORT) 
	ethConfig.constPayLoadSize        = 1408;
#elif defined(LOGGERSTREAMER2_GENPROD)
    ethConfig.constPayLoadSize		  = UTF_GENPROD_BUF_SIZE;
#else
	ethConfig.constPayLoadSize        = LoggerStreamer_bufSize;
#endif

#if defined(PARTNO_C6657)
	utf_GetEthSendHandle(&ethConfig);
	return (0);
#else
	unitTestFramework.tfEthHandle =  utf_GetEthSendHandle(&ethConfig);

	if (unitTestFramework.tfEthHandle == NULL)
		return (-1);
	else
		return (0);
#endif
}

/* initialize the test framework from slave */
int setupTestFramework_slave(void)
{
   int i;
   
   /* Initialize local heap */
   uTestLocalHeapInit ();

   /* Release any previously owned semaphores */
   if (CSL_semIsFree(UNIT_TEST_HW_SEM) == FALSE)
	   CSL_semReleaseSemaphore(UNIT_TEST_HW_SEM);

   if (CSL_semIsFree(UNIT_TF_HW_SEM) == FALSE)
	   CSL_semReleaseSemaphore(UNIT_TF_HW_SEM);

   /* wait until master set up is done */
   utlWaitUntilMasterDone(SYNC_TOKEN_MASTER_SYSINIT_DONE);

#if defined (DEVICE_K2L) || defined (DEVICE_K2H) || defined (DEVICE_K2K) || defined (DEVICE_K2E)
#else
	if (initIpc())
	{
		System_printf ("setupTesunitTestFramework: initIpc returned error, exiting\n ");
	}
#endif

	for (i = 0; ; i++ )
	{
		if (tfuTestGlobalContractsV1[i].name == NULL)
			break;
	}

	/* Update the information in the Unit Test structure */
    unitTestFramework.tfNumV1Contracts = i;	

	for (i = 0; ; i++ )
	{
		if (tfuTestGlobalContractsV2[i].name == NULL)
			break;
	}	
    unitTestFramework.tfNumV2Contracts = i;
	
	unitTestFramework.tfGeneralProducerTest = 0; /* Default it is not on */

	/* Start the configuration before doing anything on Trace framework library functionalities on every core/process */
	traceFrameworkStartCfg();		
   
   return (0);
}

void myExceptionHook(void)
{
	/* Trap here for the exceptions */
    return;
}

/* Initialize the test framework from master */
int setupTestFramework (void)
{
#ifdef __LINUX_USER_SPACE
    uint32_t id = taskNum;
#else
    uint32_t id = CSL_chipReadDNUM();
#endif

	/* Set up local heap */
	uTestLocalHeapInit ();

	/* Release any previously owned semaphores */
	if (CSL_semIsFree(UNIT_TEST_HW_SEM) == FALSE)
	  CSL_semReleaseSemaphore(UNIT_TEST_HW_SEM);

	if (CSL_semIsFree(UNIT_TF_HW_SEM) == FALSE)
	  CSL_semReleaseSemaphore(UNIT_TF_HW_SEM);

	/* Unlock the chip configuration registers to allow SGMII SERDES registers to	    * be written */
	CSL_BootCfgUnlockKicker();

	/* Set the startup sync values */
	utlResetSyncVals(FALSE);
	utlWriteSyncValue(SYNC_TOKEN_INIT_VAL, id);

    unitTestFramework.tfGeneralProducerTest = 0; /* Default it is not on */

    testCommonResetTestStatus();

#if defined (DEVICE_K2L) || defined (DEVICE_K2H) || defined (DEVICE_K2K) || defined (DEVICE_K2E)
#else
	if (initIpc())
	{
		System_printf ("setupTestFramework: initIpc returned error, exiting\n ");
	}
#endif

	/* setup RM */
	if (initRm()) {
        System_printf ("setupTestFramework: initQm returned error, exiting\n");
        return (-1);
	}
    
	/* Setup the QM with associated buffers and descriptors */
	if (initQm())  {
		System_printf ("setupTesunitTestFramework: initQm returned error, exiting\n");
		return (-1);
	}

    if (no_bootMode == TRUE)
    {
		/* Setup the cpsw */
		if (initCpsw())  {
			System_printf ("setupTestFramework: initCpsw returned error, exiting\n");
			return (-1);
		}
    }

	/* Start the configuration before doing anything on Trace framework per core/per process */
	traceFrameworkStartCfg();
	
	/* Setup trace framework contracts for Version 1 */
	if (initTraceFrameworkContracts_ver1()) {
		System_printf ("setupTestFramework: initTraceFrameworkContracts_ver1 returned error, exiting\n");
		return (-1);
	}

	/* Setup trace framework contracts for Version 2 */
	if (initTraceFrameworkContracts_ver2()) {
		System_printf ("setupTestFramework: initTraceFrameworkContracts_ver2 returned error, exiting\n");
		return (-1);
	}

	/* setup transport per core */
	if (initEthSend()) {
		System_printf ("setupTestFramework: initTraceFrameworkTransport returned error, exiting\n");
		return (-1);
	}

	utlWriteSyncValue(SYNC_TOKEN_MASTER_SYSINIT_DONE, id);
	
	return (0);

}

/* Clear the Test Framework */
int clearTestFramework (void)
{
	/* QM Clean ups */
	if (clearQm()) {
		System_printf ("QMSS clean up failed \n");
		return (-1);
	}

	/* Destroy all previously setup trace framework contracts - version 1 */
	if (clearTraceFrameworkContracts_ver1()){
		System_printf ("Contract Version 1 clean up failed \n");
		return (-1);
	}

#if defined (DEVICE_K2L) || defined (DEVICE_K2H) || defined (DEVICE_K2K) || defined (DEVICE_K2E)
#else
	if (clearIpc()) {
		System_printf ("Clearing IPC setup failed \n");
		return (-1);
	}
#endif
	/* Destroy all name server elements */
    memset((void*) tfuTestNameServer, 0xCC, sizeof (tfuTestNameServer_t)*UTF_NAMESERVER_CAPACITY);

	/* Clear the unit test framework elements */
	memset ((void*) &unitTestFramework, 0, sizeof (unitTestFramework_t));

    return (0);
}

int clearTestFramework_slave (void) {

#if defined (DEVICE_K2L) || defined (DEVICE_K2H) || defined (DEVICE_K2K) || defined (DEVICE_K2E)
#else
	if (clearIpc()) {
		System_printf ("Clearing IPC setup failed \n");
		return (-1);
	}
#endif
	/* Clear the unit test framework elements */
	memset ((void*) &unitTestFramework, 0, sizeof (unitTestFramework_t));

	return (0);
}

/* Check that all the queues are setup correctly */
int verifyTestFramework (Bool flag)
{ 
	int returnVal = 0;
#if defined (PARTNO_C6657)
#else
	int count, refCount;


	if (flag) {
	    /* Destroy the ethernet transport to return the queue descriptors back to the system */
		if (utf_EthDestroy(unitTestFramework.tfEthHandle)) {
		    System_printf ("Destroying the ethernet transport channel failed \n");
			return (-1);
		}
	}

	/* Verify that the number of descriptors in the free descriptor queue is correct */
	count = Qmss_getQueueEntryCount (unitTestFramework.QfreeDesc);

	if (flag)
	    refCount = UTF_NUM_DESC;
	else
		refCount = UTF_NUM_DESC - UTF_NUM_BMET_DESC;

	if (count != refCount )  {
		System_printf ("verifyTestFramework: Expected %d entry count in the free descriptor queue (%d), found %d\n",
				        refCount,
						unitTestFramework.QfreeDesc, count);
		returnVal = 0;
	}
#endif
	return (returnVal);  
}
/* Nothing past this point */
