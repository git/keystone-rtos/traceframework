/*
 *
 * Copyright (C) 2010-2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef _TFUTEST_H
#define _TFUTEST_H

#ifndef __LINUX_USER_SPACE
#include <xdc/runtime/System.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/Error.h>
#include <ti/sysbios/BIOS.h>

#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/cfg/global.h>
#include <ti/sysbios/heaps/HeapBuf.h>
#include <ti/sysbios/heaps/HeapMem.h>
#include "ti/csl/csl_bootcfgAux.h"
#include <ti/csl/csl_cacheAux.h>
#include <ti/csl/csl_xmcAux.h>
#include <ti/csl/csl_chipAux.h>
#include <ti/csl/csl_ipcAux.h>
#else
#include <stdint.h>
#include <stdio.h>
#endif

#include <ti/uia/runtime/UIAPacket.h>
#include <ti/uia/runtime/EventHdr.h>

/* 
 * Shut off: remark #880-D: parameter "descType" was never referenced
*
* This is better than removing the argument since removal would break
* backwards compatibility
*/
#ifdef _TMS320C6X
#pragma diag_suppress 880
#pragma diag_suppress 681
#elif defined(__GNUC__)
/* Same for GCC:
* warning: unused parameter descType [-Wunused-parameter]
*/
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif


#if !defined(__LINUX_USER_SPACE)
/* Unit test Device specific includes */
#if defined(PARTNO_C6678)
  #include <ti/instrumentation/traceframework/test/TFUnitTest/c6678/c66/bios/uTestCpsw.h>
  #include <ti/instrumentation/traceframework/test/TFUnitTest/c6678/c66/bios/uTestEthSend.h>
  #include <ti/drv/pa/pa.h>
#elif defined(PARTNO_C6670)
  #include <ti/instrumentation/traceframework/test/TFUnitTest/c6670/c66/bios/uTestCpsw.h>
  #include <ti/instrumentation/traceframework/test/TFUnitTest/c6670/c66/bios/uTestEthSend.h>
  #include <ti/drv/pa/pa.h>
#elif defined(PARTNO_C6657)
  #include <ti/instrumentation/traceframework/test/TFUnitTest/c6657/c66/bios/uTestCpsw.h>
  #include <ti/instrumentation/traceframework/test/TFUnitTest/c6657/c66/bios/uTestEthSend.h>
#elif defined(PARTNO_C6614)
  #include <ti/instrumentation/traceframework/test/TFUnitTest/c6614/c66/bios/uTestCpsw.h>
  #include <ti/instrumentation/traceframework/test/TFUnitTest/c6614/c66/bios/uTestEthSend.h>
#elif defined(DEVICE_K2L)
  #include <ti/instrumentation/traceframework/test/TFUnitTest/k2l/c66/bios/uTestCpsw.h>
  #include <ti/instrumentation/traceframework/test/TFUnitTest/k2l/c66/bios/uTestEthSend.h>
#elif defined(DEVICE_K2H)
  #include <ti/instrumentation/traceframework/test/TFUnitTest/k2h/c66/bios/uTestCpsw.h>
  #include <ti/instrumentation/traceframework/test/TFUnitTest/k2h/c66/bios/uTestEthSend.h>
#elif defined(DEVICE_K2K)
  #include <ti/instrumentation/traceframework/test/TFUnitTest/k2k/c66/bios/uTestCpsw.h>
  #include <ti/instrumentation/traceframework/test/TFUnitTest/k2k/c66/bios/uTestEthSend.h>
#elif defined(DEVICE_K2E)
  #include <ti/instrumentation/traceframework/test/TFUnitTest/k2e/c66/bios/uTestCpsw.h>
  #include <ti/instrumentation/traceframework/test/TFUnitTest/k2e/c66/bios/uTestEthSend.h>
#endif
#else
  #include <ti/instrumentation/traceframework/test/TFUnitTest/src/armv7/linux/fw_eth_send.h>
#endif /* __LINUX_USER_SPACE */

/* Unit test Device specific includes */
#if defined(PARTNO_C6678)
#define         MAX_NUM_CORES                   8u /* Maximum number of DSP CPU Cores */
#elif defined(PARTNO_C6670)
#define         MAX_NUM_CORES                   4u /* Maximum number of DSP CPU Cores */
#elif defined(PARTNO_C6657)
#define         MAX_NUM_CORES                   2u /* Maximum number of DSP CPU Cores */
#elif defined(PARTNO_C6614)
#define         MAX_NUM_CORES                   4u /* Maximum number of DSP CPU Cores */
#else
#define         MAX_NUM_CORES                   4u /* Maximum number of DSP CPU Cores */
#endif

#define         UTF_ALLOWED_CONSUMERS           4u /* Maximum number of TraceFramework DSP Consumers*/

#include <ti/instrumentation/traceframework/traceframework.h>
#include <string.h>
#include <stdio.h>

#ifdef __LINUX_USER_SPACE
#define System_printf printf
#else
#undef L2_CACHE
#include <ti/drv/qmss/qmss_drv.h>
#include <ti/drv/qmss/qmss_osal.h>
#include <ti/drv/cppi/cppi_drv.h>
#include <ti/drv/cppi/cppi_desc.h>
#include <ti/drv/cppi/cppi_osal.h>
/* Resource manager includes */
#include <ti/drv/rm/rm.h>

#if defined(TEST_RM_VER2)
#include <ti/drv/rm/rm_osal.h>
#include <ti/drv/rm/rm_transport.h>
#include <ti/drv/rm/rm_services.h>
#endif

#endif

typedef enum  {
	UTF_TEST_FAILED  = -1,
	UTF_TEST_NOT_RUN,
	UTF_TEST_PASSED
} tfTestStatus_t;

typedef struct testInfo_s
{
	tfTestStatus_t status;
	uint32_t       consume_count;
	uint32_t       miss_count;
}testInfo_t;

typedef struct testStatus_s
{
	union {
    testInfo_t     info;
	uint8_t        pad[128];
	}u;
}testStatus_t;

#define TEST_STATUS_COUNT (MAX_NUM_CORES + 1)
#define TEST_STATUS_ARRAY_SIZE (TEST_STATUS_COUNT * sizeof (testStatus_t))
extern testStatus_t    testStatus[TEST_STATUS_COUNT];

/* unit test name server */
typedef void* tfTest_nameserver_HANDLE;

/* Define the test interface */
typedef struct tfTest_s
{
#ifdef __LINUX_USER_SPACE
	void* (*testFunction)(void *arg);   /* The code that runs the test */
#else
	void (*testFunction)(UArg, UArg);   /* The code that runs the test */
#endif
	char *name;							/* The test name */
	tfTestStatus_t testStatus;			/* Test status */
	
} tfTest_t;

extern tfTest_t  tfTestList[];

#ifndef __LINUX_USER_SPACE
/* Number of cpdma channels is fixed by device */
#define UTF_PA_NUM_RX_CPDMA_CHANNELS 24
#define UTF_PA_NUM_TX_CPDMA_CHANNELS  9

/* Use SRC27 as the bit to indicate the buffer exchange indicator */
#define     UNIT_TEST_TRACE_FRAMEWORK_BUF_IPC_NOTIFY_SRC_ID  (CSL_IPC_IPCAR_SRCC27_SHIFT - CSL_IPC_IPCAR_SRCC0_SHIFT)

/* Hardware Semaphore to synchronize access from
 * multiple applications for the unit test.
 */
#define     UNIT_TEST_HW_SEM     5

/* Hardware Semaphore to synchronize access from
 * multiple applications for the trace framework.
 */
#define     UNIT_TF_HW_SEM       4

#endif

typedef enum {
  UTF_SYSTEM_ANALYZER_ETHERNET = 0,
  UTF_SYSLOG_ETHERNET
} unitTestEndConsumer_e;

/* Trace Framework needed System Contract Names */
#define UTF_UTEST_CONTRACT_NAME_SIZE        128
extern char unitTestTFDspUiaContractV1Name[UTF_UTEST_CONTRACT_NAME_SIZE];
extern char uintTestTFDspUiaMinstContractV1Name[UTF_UTEST_CONTRACT_NAME_SIZE];
extern char uintTestTFDspGenContractV1Name[UTF_UTEST_CONTRACT_NAME_SIZE];
extern char unitTestTFArmCUiaContractV1Name[UTF_UTEST_CONTRACT_NAME_SIZE];
extern char uintTestTFArmGenContractV1Name[UTF_UTEST_CONTRACT_NAME_SIZE];
extern char unitTestTFDspUiaContractV2Name[UTF_UTEST_CONTRACT_NAME_SIZE];
extern char uintTestTFDspUiaMinstContractV2Name[UTF_UTEST_CONTRACT_NAME_SIZE];
extern char uintTestTFDspGenContractV2Name[UTF_UTEST_CONTRACT_NAME_SIZE];
extern char unitTestTFArmCUiaContractV2Name[UTF_UTEST_CONTRACT_NAME_SIZE];
extern char uintTestTFArmGenContractV2Name[UTF_UTEST_CONTRACT_NAME_SIZE];
extern char unitTestTFDspUiaStreamFreezeContractV2Name[UTF_UTEST_CONTRACT_NAME_SIZE];
/* Contract versions to be tested */
typedef enum
{
   UTF_CONTRACT_VERSION_1  = 0,
   UTF_CONTRACT_VERSION_2,
   UTF_CONTRACT_VERSION_NONE
} uTestContractVers_e;

/* Contract Owners */
typedef enum
{
   UTF_CONTRACT_OWNER_DSP  = 0,
   UTF_CONTRACT_OWNER_ARM,
   UTF_CONTRACT_OWNER_NONE
} uTestContractOwner_e;

/* Consumer types */
typedef enum
{
	UTF_CONSUMER_UIA,
    UTF_CONSUMER_UIA_STREAM_STOP,		
	UTF_CONSUMER_UIAMINST,
	UTF_CONSUMER_GENERALPROD,
	UTF_CONSUMER_CUIA
} uTestConsumers_e;

#define UTF_CACHE_LINESZ    128
#define UTF_ROUND_UP(x, y)   ((x) + ((y)-1))/(y)*(y)

#ifndef __LINUX_USER_SPACE
/* QM memory */
#define UTF_NUM_DESC	  (128 * MAX_NUM_CORES )   /* 128 * num_cores host descriptors managed by the QM, 128 descriptors are needed per BMET instance (per core) */
#define UTF_SIZE_DESC     128		/* Must be multiple of 16 */

/* Memory used for the linking RAM and descriptor RAM */
extern uint64_t memLinkRam[UTF_NUM_DESC];
extern uint8_t  memDescRam[UTF_NUM_DESC * UTF_SIZE_DESC];
#if defined (C6678) || defined (C6670) || defined (PARTNO_C6614) || defined (C6657) || defined (DEVICE_K2H) || defined (DEVICE_K2K)
#define UTF_FREE_GENERAL_QUEUE_NUM   900
#else
#define UTF_FREE_GENERAL_QUEUE_NUM   1024
#endif
#endif

/* Traceframework */
#define UTF_NUM_CONTRACTS         10
#define UTF_NUM_RINGBUFS          32    /* number of ring buffers needed for the unit test */
#define UTF_NUM_GENPROD_RINGBUFS   2    /* General producer uses ping pong buffers, hence the ring size = 2 */
#define UTF_NUM_CONTRACT_VERSIONS UTF_CONTRACT_VERSION_NONE
#ifdef __LINUX_USER_SPACE
#define UTF_SIZE_RINGBUF          UTF_ROUND_UP(1024,  UTF_CACHE_LINESZ)  /* Max Number of Ring Buffer Size */
#else
#define UTF_SIZE_RINGBUF          UTF_ROUND_UP(1408,  UTF_CACHE_LINESZ)  /* Max Number of Ring Buffer Size */
#endif
#define UTF_NUM_GENPROD_ACCUMULATIONS 1000


/* Resource manager variables */
#if defined (TEST_RM_VER2)
extern char rmServerName[RM_NAME_MAX_CHARS];
extern Rm_ServiceHandle *rmServerServiceHandle;
#endif

/* Refer the variables for detecting the boot mode */
extern uint32_t autodetectLogic;
extern uint32_t no_bootMode;

/* Trace Framework Contract memory */
extern uint8_t  memContractRam[UTF_NUM_CONTRACT_VERSIONS][UTF_NUM_CONTRACTS * TF_CONTRACT_SIZE_BYTES];
extern uint8_t  memBufRing[UTF_NUM_CONTRACT_VERSIONS][UTF_NUM_RINGBUFS * UTF_SIZE_RINGBUF];

/* Unit Test Synchronization variables */
typedef enum tfSyncToken
{
     SYNC_TOKEN_INIT_VAL               = 0xdead,
     SYNC_TOKEN_MASTER_SYSINIT_DONE    = 0xface,
     SYNC_TOKEN_MASTER_UNITTEST_DONE   = 0xbabe,
     SYNC_TOKEN_CHECK_CONSUMERS_DONE   = 0xf00d,
     SYNC_TOKEN_POST_CONSUMER_DONE     = 0xea5e,
     SYNC_TOKEN_POST_ACCUMULATION_DONE = 0xdaad,
     SYNC_TOKEN_GET_ACCUMULATION       = SYNC_TOKEN_POST_ACCUMULATION_DONE
}tfSyncToken_e;

typedef struct tfTestSlaveSyncVar_s
{
	uint32_t count;
	uint8_t  pad[124];
}tfTestSlaveSyncVar_t;


typedef struct tfuTestSyncVars_s
{
   union {
   uint32_t isMasterSysInitDone;
   uint32_t masterTestCompleteCount;
   uint8_t  pad1[128];
   } masterState;
   /* One extra slot is for ARM consumer */
   tfTestSlaveSyncVar_t slaveDone[MAX_NUM_CORES + 1];
   /* Number of producer Accumulations */
   uint32_t numAccumulations;
}tfuTestSyncVars_t;

extern tfuTestSyncVars_t memSyncRam[1];

/* Transport arguments */
typedef struct transportArgs_s
{
#ifdef __LINUX_USER_SPACE
    char             ipDst[128];  
    uint32_t		 remoteUdpPort;
#else
	utfEthIpv4Addr_t ipDst;
	utfEthIpv4Addr_t ipSrc;
	utfEthMacAddr_t  macDst;
	utfEthMacAddr_t  macSrc;
	uint32_t         remoteUdpPort;
#endif
} transportArgs_t;

/* Trace Framework test setup */
typedef struct tfuTestSetup_s  {
	uTestContractOwner_e  owner;
    char*                 name;
    uTestContractVers_e   uTestContractVer;
} tfuTestContractSetup_t;

extern tfuTestContractSetup_t tfuTestGlobalContractsV1[];
extern tfuTestContractSetup_t tfuTestGlobalContractsV2[];

/* Simple/primitive Name Server for Contract Version 2 for this test */
typedef void* utfCustom_HANDLE;

typedef enum {
	UTF_NAMESERVER_OK,
	UTF_NAMESERVER_NOTFOUND
}utfNameServer_result;
#define UTF_NAMESERVER_NAME_LEN 120
#define UTF_NAMESERVER_CAPACITY 32
typedef struct tfuTestNameServer_s
{
  char  name[UTF_NAMESERVER_NAME_LEN];
  utfCustom_HANDLE handle;
} tfuTestNameServer_t;

typedef enum {
	UTEST_CONSUMER_NOTIFY_REGMASK_EVENT_PEND = 0,
	UTEST_CONSUMER_NOTIFY_REGMASK_EVENT_POST,
	UTEST_CONSUMER_NOTIFY_NONE,
	UTEST_CONSUMER_NOTIFY_REGMASK_EVENT_ACK = UTEST_CONSUMER_NOTIFY_REGMASK_EVENT_PEND
} uTestConsumerNotifications_e;

extern tfuTestNameServer_t tfuTestNameServer[];

/* Define the unit test framework */
typedef struct unitTestFramework_s  {
  int                  numTests;
	uint32_t             nameServerIndex;
#ifndef __LINUX_USER_SPACE
	Rm_Handle            rm_handle;
#endif
    int                  EnableProdStateChange;
	uint32_t             tfGeneralProducerTest;
	tf_consumer_HANDLE   utfConsumerHandle;
	uint32_t             tfNumBufsConsumed;
    uint32_t             tfPrevValue;
	uint32_t             tfNumBufsMissed;
	uint32_t             tfNumV2Contracts;
	uint32_t             tfNumV1Contracts;
	tfTest_nameserver_HANDLE tfNameServerHandle;
#if !defined (PARTNO_C6657)&& !defined(__LINUX_USER_SPACE)
	Cppi_Handle          tfPaCppiHandle; /* PA CDMA handle */
	utfEth_HANDLE        tfEthHandle;
	Cppi_ChHnd           tfPaTxChHnd[UTF_PA_NUM_TX_CPDMA_CHANNELS];
	Cppi_ChHnd           tfPaRxChHnd[UTF_PA_NUM_RX_CPDMA_CHANNELS];
	/* Queues */
	Qmss_QueueHnd        QfreeDesc;		      				/* Unassociated descriptor queue handle */
	tf_contractType_e    contractType;
#endif
	tf_notifyTypes_e             consumerNotifyType;
	uTestConsumerNotifications_e consumerNotification;

} unitTestFramework_t;

extern unitTestFramework_t unitTestFramework;

#ifdef __LINUX_USER_SPACE
/* Define the arguments to unit test functions */
typedef struct utfTestArgs_s
{
	unitTestFramework_t *tf;
	tfTest_t     *pat;
} utfTestArgs_t;

typedef struct task_args_s {
  void* a0;
  void* a1;
} task_args_t;
#endif

extern int armOnlyContract;

/* UIA packet header */
#define UIA_PKT_HEADER_SIZE      sizeof (UIAPacket_Hdr)
/* 10 words of UIA Event header */
#define UIA_EVENT_HEADER_SIZE    (sizeof (uint32_t) * 10)
/* Total header size needed for UIA */
#define UIA_HEADER_SIZE          (UIA_PKT_HEADER_SIZE + UIA_EVENT_HEADER_SIZE)

/* General producer payload
 * This can be any proprietary information
 * from the application
 * Please make sure the each ring buffer size for
 * the contract is at least equal to the size of
 * (payload + UIA_HEADER_SIZE) to stream to System Analyzer.
 */
#define PAD_SIZE ( UTF_SIZE_RINGBUF - UIA_HEADER_SIZE - 128 )
typedef struct utfGenProdPayload_s
{
    char             name[120];
    uint32_t         accumulator1;
    uint32_t         accumulator2;
	//uint8_t          rsvd[PAD_SIZE];
}utfGenProdPayload_t;

typedef struct utfGenProdBuf_s
{
	uint8_t               uia_header[UIA_HEADER_SIZE];
	utfGenProdPayload_t   payload;
} utfGenProdBuf_t;

#define UTF_GENPROD_BUF_SIZE  UTF_ROUND_UP(sizeof(utfGenProdBuf_t),  UTF_CACHE_LINESZ)

#ifndef __LINUX_USER_SPACE
/* Transport Arguments for ethernet send */
extern transportArgs_t  transportArgs;
void  utlTestReadValues(FILE* fp, transportArgs_t* tArgs);
#endif

/* Synchronization variables */
extern int tf_producer_notification_task_exit_flag;
extern int tf_num_consumers;

/* Prototypes */
int clearTestFramework (void);
int setupTestFramework (void);
int clearTestFramework_slave (void);
int setupTestFramework_slave (void);
int initRm(void);
void utilCycleDelay (int count);
uint32_t utilgAddr(uint32_t x);
uint32_t utilLocalAddr(uint32_t x);
uint32_t l2_global_address (uint32_t addr);
uint32_t utlReadSyncValue(tfSyncToken_e value);
void  utlWriteSyncValue(tfSyncToken_e value, uint32_t id);
void utlWaitUntilMasterDone(tfSyncToken_e token);
void utlResetSyncVals(Bool flag);
void utlBeginMemAccess (void *blockPtr, uint32_t size);
void utlEndMemAccess (void *blockPtr, uint32_t size);
void utlProdWaitUntilConsumersAreDone(void);
void utlWaitUntilMasterTestComplete(uint32_t ref_count);

/* Common test function prototypes */
tf_consAppSendRespStatus_e testCommonEthSendPktFxn(uint32_t param, uint8_t* logBuf, uint32_t logBufSize);
tf_producer_HANDLE testCommonCreateTfProducer(tf_contract_HANDLE contract_handle, uint32_t contract_ver, tf_producerTypes_e pType);
void  testCommonCreateTfConsumer(tf_contract_HANDLE contract_handle, uint32_t prod_coreId, uTestConsumers_e type, uint32_t block_id_start);
void testCommonDeleteTfConsumer(tf_consumer_HANDLE consumer_handle);
void* testCommonGetSysContractVer1BaseAddrPhysical(void);
tfTest_nameserver_HANDLE testCommonGetSysNameServerHandle(void);
tf_contract_HANDLE testCommonGetTfContractHandle(char* contract_name, tf_contractVersion_e contract_version);
int testCommonConsumeBuffers(tf_cons_HANDLE consumer_handle);
void testCommonPrintContractSummary(tf_contract_HANDLE contract_handle, uint32_t coreId);
utfNameServer_result testCommonNameServerPush(char* name, utfCustom_HANDLE handle);
utfNameServer_result testCommonNameServerPop(char* name, utfCustom_HANDLE* handle);
void testCommonPublishSummaryFromConsumer(unitTestFramework_t *tf, tfTest_t* tft, uint32_t index, uint32_t contract_verId);
testStatus_t* testCommonGetTestStatusArrayBase(void);
void testCommonResetTestStatus(void);
tfuTestSyncVars_t* testCommonGetSycMemHandle(void);
void testCommonGenProdAccumulateStatistics(tf_producer_HANDLE producer_handle, uint32_t coreId);
void testCommonInitGenProdRingBuffers(uint8_t* rb_ptr, uint32_t rb_sz, uint32_t num_buf, uint32_t dataSize, char* name, uint32_t id);
tf_consAppSendRespStatus_e testCommonVerifyUiaBuffersFxn(uint32_t param, uint8_t* logBuf, uint32_t logBufSize);
int  testCommonCheckToDeleteConsumer(unitTestFramework_t *tf, tf_contract_HANDLE handle, uint32_t max_buffers);
void testCommonSummarizeAllTestStatus(tfTest_t  *tft, char* testname, uint32_t ref_count, uint32_t exp_num_bufs, uint32_t contract_ver);

/* Tests */
#ifdef __LINUX_USER_SPACE
void* tfTestcUiaProducercUiaConsumers    (void *args);
void* tfTestUiaProducerUiaConsumers      (void *args);
void* tfTestUiaStremFreezeProducerUiaArmConsumers(void *args);
void* tfTestGenProducerGenConsumers      (void *args);
void* tfTestUiaMInstProducerUiaConsumers (void *args);
void  testCommonTrigCUiaBufXchgTask      (int* loopCnt);
void* testCommonTfAccumConsumerTask      (void* args);
void* testCommonNotifyTfConsumersTask    (void* args);
int verifyTestFramework (void);
#else /* DSP side functions */
void tfTestUiaProducerUiaConsumers (UArg a0, UArg a1);
void tfTestUiaStremFreezeProducerUiaArmConsumers(UArg a0, UArg a1);
void tfTestGenProducerGenConsumers (UArg a0, UArg a1);
void tfTestUiaMInstProducerUiaConsumers (UArg a0, UArg a1);
void testCommonTrigUiaBufXchgTask(UArg a0, UArg a1);
void testCommonNotifyTfConsumersTask(UArg a0, UArg a1);
void testCommonGenProdAccumulateStatistics(tf_producer_HANDLE producer_handle, uint32_t coreId);
void testCommonTrigGenProdBufXchgTask(UArg a0, UArg a1);
void testCommonTfAccumConsumerTask(UArg a0, UArg a1);
void unitTest_MtCsExit (uint32_t key);
void unitTest_MtCsEnter (uint32_t *key);
int verifyTestFramework (Bool flag);
extern IHeap_Handle        uTestLocalHeap;
#endif /* __LINUX_USER_SPACE */
#endif /* _TFUTEST_H */
