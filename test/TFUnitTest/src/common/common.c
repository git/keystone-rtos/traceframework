/*
 *
 * Copyright (C) 2010-2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <ti/uia/runtime/LogUC.h>
#include <ti/uia/runtime/LogSnapshot.h>
#include <ti/uia/events/UIABenchmark.h>

#ifdef __LINUX_USER_SPACE
#include "fw_test.h"
#include "fw_task.h"  
#include "fw_shm.h"
#include "fw_mem_allocator.h"
#include <xdc/std.h>
#include <xdc/runtime/Log.h>
#include <ti/uiaplus/loggers/stream/LoggerStreamer.h>
#include <xdc/runtime/Diags.h>    
#else
#include <xdc/runtime/Timestamp.h> 
#if defined(LOGGERSTREAMER2_SUPPORT) || defined(LOGGERSTREAMER2_GENPROD)
#include <ti/uia/sysbios/LoggerStreamer2.h>
#else
#include <ti/uia/sysbios/LoggerStreamer.h>
#endif /* (LOGGERSTREAMER2_SUPPORT) || defined(LOGGERSTREAMER2_GENPROD) */
#endif /* __LINUX_USER_SPACE */

#include "../tfutest.h"

#ifdef __LINUX_USER_SPACE
extern   fwShm_HANDLE fwShmHandle;
#endif

/* Trace Framework needed System Contract Names */
char unitTestTFDspUiaContractV1Name[UTF_UTEST_CONTRACT_NAME_SIZE]     = "TF_UTEST_DSP_UIA_CONTRACT_V1";
char uintTestTFDspUiaMinstContractV1Name[UTF_UTEST_CONTRACT_NAME_SIZE] = "TF_UTEST_DSP_UIA_MINST_CONTRACT_V1";
char uintTestTFDspGenContractV1Name[UTF_UTEST_CONTRACT_NAME_SIZE]      = "TF_UTEST_DSP_GEN_CONTRACT_V1";
char unitTestTFArmCUiaContractV1Name[UTF_UTEST_CONTRACT_NAME_SIZE]    = "TF_UTEST_ARM_CUIA_CONTRACT_V1";
char uintTestTFArmGenContractV1Name[UTF_UTEST_CONTRACT_NAME_SIZE]      = "TF_UTEST_ARM_GEN_CONTRACT_V1";

char unitTestTFDspUiaContractV2Name[UTF_UTEST_CONTRACT_NAME_SIZE]     = "TF_UTEST_DSP_UIA_CONTRACT_V2";
char uintTestTFDspUiaMinstContractV2Name[UTF_UTEST_CONTRACT_NAME_SIZE] = "TF_UTEST_DSP_UIA_MINST_CONTRACT_V2";
char uintTestTFDspGenContractV2Name[UTF_UTEST_CONTRACT_NAME_SIZE]      = "TF_UTEST_DSP_GEN_CONTRACT_V2";
char unitTestTFArmCUiaContractV2Name[UTF_UTEST_CONTRACT_NAME_SIZE]    = "TF_UTEST_ARM_CUIA_CONTRACT_V2";
char uintTestTFArmGenContractV2Name[UTF_UTEST_CONTRACT_NAME_SIZE]      = "TF_UTEST_ARM_GEN_CONTRACT_V2";
char unitTestTFDspUiaStreamFreezeContractV2Name[UTF_UTEST_CONTRACT_NAME_SIZE]    = "TF_UTEST_DSP_UIA_STREAM_FREEZE_CONTRACT_V2";

/* Trace Framework Contracts */
tfuTestContractSetup_t tfuTestGlobalContractsV1[] = {
		{
				UTF_CONTRACT_OWNER_DSP,         /* Contract Owner is DSP */
				unitTestTFDspUiaContractV1Name, /* name of the contract */
				UTF_CONTRACT_VERSION_1           /* Contract Version information */
		},
		{
				UTF_CONTRACT_OWNER_DSP,         /* Contract owner is DSP */
				uintTestTFDspUiaMinstContractV1Name, /* name of the contract */
				UTF_CONTRACT_VERSION_1           /* Contract Version information */
		},
		{
				UTF_CONTRACT_OWNER_DSP,         /* Contract owner is DSP */
				uintTestTFDspGenContractV1Name, /* name of the contract */
				UTF_CONTRACT_VERSION_1           /* Contract Version information */
		},
		{
				UTF_CONTRACT_OWNER_ARM,         /* Contract owner is ARM, resulting ARM only contracts possible */
		        unitTestTFArmCUiaContractV1Name, /* name of the contract */
				UTF_CONTRACT_VERSION_1           /* Contract Version information */		        
		},
		{
				UTF_CONTRACT_OWNER_NONE,         /* Not owned by either DSP or ARM */
				NULL,                            /* name of the contract */
				UTF_CONTRACT_VERSION_NONE        /* Contract Version None  */
		}
};

tfuTestContractSetup_t tfuTestGlobalContractsV2[] = {
		{
				UTF_CONTRACT_OWNER_DSP,         /* Contract Owner is DSP */
				unitTestTFDspUiaContractV2Name, /* name of the contract */
				UTF_CONTRACT_VERSION_2           /* Contract Version information */
		},
		{
				UTF_CONTRACT_OWNER_DSP,         /* Contract Owner is DSP */
				uintTestTFDspUiaMinstContractV2Name, /* name of the contract */
				UTF_CONTRACT_VERSION_2           /* Contract Version information */
		},
		{
				UTF_CONTRACT_OWNER_DSP,         /* Contract Owner is DSP */
				uintTestTFDspGenContractV2Name, /* name of the contract */
				UTF_CONTRACT_VERSION_2           /* Contract Version information */
		},
		{
				UTF_CONTRACT_OWNER_DSP,         /* Contract Owner is DSP */
				unitTestTFDspUiaStreamFreezeContractV2Name, /* name of the contract */
				UTF_CONTRACT_VERSION_2           /* Contract Version information */
		},		
		{
				UTF_CONTRACT_OWNER_ARM,         /* Contract owner is ARM, resulting ARM only contracts possible */
		        unitTestTFArmCUiaContractV2Name, /* name of the contract */
				UTF_CONTRACT_VERSION_2           /* Contract Version information */		        
		},		
		{
				UTF_CONTRACT_OWNER_NONE,         /* Contract Owner is None */
				NULL,                           /* name of the contract */
				UTF_CONTRACT_VERSION_NONE       /* Contract Version None */
		}
};


/* unit test producer core variables */
int tf_producer_notification_task_exit_flag = 0;
int tf_num_consumers = 0;
int tf_nothing_to_consume = 0;
int seqNumProcFirstTime = 0;
int testCommonConsumeBuffers(tf_consumer_HANDLE consumer_handle)
{
	tf_consStatus_e  consStatus = TF_CONSUMER_NOTHING_TO_CONSUME;
	int              retVal = -1;
	int              flag = 0; /* stop the consumer process by default */
 	unitTestFramework_t  *tf  = (unitTestFramework_t *)&unitTestFramework;

	if ((tf->consumerNotifyType == TF_CONSUMER_NOTIFY_VIA_REGMASK)) {
		if (tf->consumerNotification == UTEST_CONSUMER_NOTIFY_REGMASK_EVENT_POST) {
			/* Ack the event */
			tf->consumerNotification = UTEST_CONSUMER_NOTIFY_REGMASK_EVENT_ACK;
			/* wait until IPC ISR posts the notification event */
			flag = 1;
		}
	}
	else {
		flag = 1; /* Enable the consumer process */
	}

	if (flag) {
		do {
			consStatus = tf_consProcess(consumer_handle);
		    if (consStatus == TF_CONSUMER_SUCCESS) {
		       /* Indicate number of buffers consumed */
		        unitTestFramework.tfNumBufsConsumed ++;	
		    	retVal = 0;
		    }
		    else if (consStatus == TF_CONSUMER_NOTHING_TO_CONSUME) {
		    	/* There is nothing to consume, no error */ 
				tf_nothing_to_consume ++;
		    	retVal = 0;
		    }		
		    else {
		    	System_printf (" consumer process is not successful, status received during process is %d \n", consStatus);
		    	System_flush();
		    }
		} while (tf_isthere_to_consume(consumer_handle));
	}
	else {
		retVal = 0;
	}
	return (retVal);
}
#ifndef __LINUX_USER_SPACE
/* Inserts uia header and returns the pointer for the payload */
static void insert_uia_header(void* buf, uint32_t gSequenceCount)
{
	  UIAPacket_Hdr*        ptrUIAPktHeader;
	  uint32_t*             ptrUIAEvtHeader;

      /* Fetch the packet header. */
	  ptrUIAPktHeader = (UIAPacket_Hdr*)(buf);

	  /* Fetch the event header. */
	  ptrUIAEvtHeader = (uint32_t*)((uint32_t)ptrUIAPktHeader + UIA_PKT_HEADER_SIZE);

	  /* Insert sequence number. */
	  UIAPacket_setSequenceCount (ptrUIAPktHeader, gSequenceCount);
	  *(ptrUIAEvtHeader++) = EventHdr_setSeqCount (*ptrUIAEvtHeader, gSequenceCount);

	  /* Update timestamp. */
	  *(ptrUIAEvtHeader++) = TSCL;
	  *(ptrUIAEvtHeader++) = TSCH;

	  return;

}

void testCommonGenProdAccumulateStatistics(tf_producer_HANDLE producer_handle, uint32_t coreId)
{
	  utfGenProdBuf_t*      newBuf;

	  /* Get the current ring buffer which producer would populate */
	  newBuf = (utfGenProdBuf_t *) tf_prodGetCurBuf(producer_handle);

	  newBuf->payload.accumulator1 ++;
	  newBuf->payload.accumulator2 ++;
    utlWriteSyncValue(SYNC_TOKEN_POST_ACCUMULATION_DONE, coreId);
}

/* UIA Log message task that triggers buffer exchange */
void testCommonTrigGenProdBufXchgTask(UArg a0, UArg a1)
{
	  utfGenProdBuf_t       *newBuf, *prevBuf;
	  uint32_t              gSequenceCount = 0, num_consumers;
	  tf_producer_HANDLE    producer_handle = (tf_producer_HANDLE) a0;
	  uint32_t              contract_ver = (uint32_t) a1;
	  char                  ping_string[40], pong_string[40];

	  if (contract_ver == 1)
	  {
		  strcpy(ping_string, "PING  BUFFER CONTRACT VER 1");
		  strcpy(pong_string, "PONG  BUFFER CONTRACT VER 1");
	  }
	  else {
		  strcpy(ping_string, "PING  BUFFER CONTRACT VER 2");
		  strcpy(pong_string, "PONG  BUFFER CONTRACT VER 2");
	  }

	  /* Exit the task if size provided for the ring buffer is not sufficient */
	  if (UTF_GENPROD_BUF_SIZE > UTF_SIZE_RINGBUF) {
		  Task_exit();
	  }


	  /* Get the current ring buffer which producer would populate */
	  newBuf = (utfGenProdBuf_t *) tf_prodGetCurBuf(producer_handle);

	  /* clear the accumulation during startup */
	  memset(&newBuf->payload, 0, sizeof(utfGenProdPayload_t));

	  /* Insert the UIA header information */
	  insert_uia_header((void*)newBuf, gSequenceCount);

	  /* Insert buffer log information */
	  strcpy (newBuf->payload.name, ping_string);

	  do {
	   	  /* 1 tick = 1 ms */
	      Task_sleep(1); /* sleep for a milli second to allow accumulation */

	      /* sample the previous buffer */
	      prevBuf  = newBuf;

	      /* See if we get a new buffer (pongBuf) during exchange */
	      newBuf = (utfGenProdBuf_t*)tf_prodBufExchange (producer_handle, (uint8_t*)newBuf);

	      /* Check if buffer has been exchanged to update the sequence count. */
	      if (newBuf == prevBuf)
	      {
	    	  /* Buffer exchange returned same buffer, which means consumer has not
	    	   * consumed it, so allow it for accumulation
	    	   */
	      }
	      else /* Consumers consumed it, reset the accumulations */
	      {
	    	  gSequenceCount++;

	    	  /* clear the accumulation */
	    	  memset(&newBuf->payload, 0, sizeof(utfGenProdPayload_t));

	    	  /* Insert the UIA header information */
	    	  insert_uia_header((void*)newBuf, gSequenceCount);

	    	  if (gSequenceCount & 0x1) /* Odd counts */
	    	  {
	    		  /* Insert buffer log information */
	    		  strcpy (newBuf->payload.name, pong_string);
	    	  }
	    	  else
	    	  {
	    		  /* Insert buffer log information */
	    		  strcpy (newBuf->payload.name, ping_string);
	    	  }
	      }

		  /* get the information about how many consumers are attached to the contract/producer */
		  num_consumers = tf_prodUpdateConsumers(producer_handle);
		  if ( tf_num_consumers < num_consumers) {
		    	tf_num_consumers = num_consumers; /* Obtain the steady state information about how many consumers got latched during the test */
		  }
 	 } while (num_consumers); /* Run this task until all consumers consume intended number of buffers for the test */

	 Task_exit();
}

/**
 *  @b Description
 *  @n
 *      This function initializes the general-purpose producer buffer with
 *      UIA packet header.
 *
 *
 *  @retval
 *      Not Applicable.
 */
static inline void genProd_initUIAPacketHeader
(
    void*       loggerObj,
    uint8_t*    ptrBuffer,
    uint32_t    bufferSize,
    uint32_t    id
)
{
#ifdef UTEST_TF_GEN
    /* Initialize the UIA packet header. */
    LoggerStreamer2_initBuffer((LoggerStreamer2_Object*)loggerObj, (Ptr)ptrBuffer, id);
#endif
    /* Set the event length in the packet. */
    UIAPacket_setEventLength((UIAPacket_Hdr*)ptrBuffer, bufferSize);
}

static inline void genProd_initUIAEventHeader
(
    char*           formatString,
    uint8_t*        ptrBuffer,
    uint32_t        bufferSize,
    uint32_t        dataSize,
    uint32_t        sequenceCount
)
{
    uint32_t*       writePtr;
    uint8_t*        ptrDataBuffer;
    uint32_t        snapshotId = 0;
    UIAPacket_Hdr*  ptrHeader = (UIAPacket_Hdr*)ptrBuffer;
    uint32_t*       ptrEventHeader;
    uint32_t        dataSizeRound = ((dataSize + sizeof(uint32_t) - 1)/sizeof(uint32_t))*sizeof(uint32_t);
    uint32_t        eventSize = dataSizeRound + UIA_EVENT_HEADER_SIZE;
    uint32_t        numPaddingBytes = bufferSize - UIA_PKT_HEADER_SIZE - UIA_EVENT_HEADER_SIZE - dataSizeRound;

    /* Module's ID (Reference: Text.xs) */
    xdc_runtime_Types_ModuleId mid = Module__MID;

    /* Initialize the event header, including timestamps */
    ptrEventHeader = (uint32_t*)((uint32_t)ptrHeader + UIA_PKT_HEADER_SIZE);
    writePtr = ptrEventHeader;

    /* Store the start of the data buffer. */
    ptrDataBuffer = (uint8_t*)ptrEventHeader + UIA_EVENT_HEADER_SIZE;

    /* Fill in the event header fields. */
    *(writePtr++) = ti_uia_runtime_EventHdr_genEventHdrWord1(eventSize, sequenceCount,
                    ti_uia_runtime_EventHdr_HdrType_EventWithSnapshotIdAndTimestamp);
    *(writePtr++) = TSCL;
    *(writePtr++) = TSCH;
    *(writePtr++) = ((ti_uia_events_UIASnapshot_memoryRange) & 0xffff0000) | mid;
    *(writePtr++) = (IArg)__FILE__;
    *(writePtr++) = (IArg)__LINE__;
    *(writePtr++) = snapshotId;
    *(writePtr++) = (Bits32)ptrDataBuffer;
    *(writePtr++) = (((dataSizeRound & 0x0FFFF)<<16) | (dataSizeRound & 0x0FFFF));

    /* The format string used by System Analyzer to identify what the snapshot
     * data represents. */
    *(writePtr++) = (IArg)formatString;

    /* Increment the write pointer to end of the data section. */
    writePtr = (uint32_t*)(ptrDataBuffer + dataSizeRound);

    /* Set UIA packet length. */
    UIAPacket_setEventLength (ptrHeader, eventSize + UIA_PKT_HEADER_SIZE);

    /* Add an invalid UIA header for the rest of the data in the buffer. */
    UIAPacket_setInvalidHdr (writePtr, numPaddingBytes);
}

void testCommonInitGenProdRingBuffers
(
    uint8_t*            ptrRingBuffer,
    uint32_t            ringBufferSize,
    uint32_t            ringBufferNum,
    uint32_t            dataSize,
    char*               contractname,
    uint32_t            id
)
{
    uint32_t    index;
    void*       logger0 = NULL;

    /* Initialize the UIA headers. */
    for (index = 0; index < ringBufferNum; index++)
    {
        /* Initialize the UIA packet headers. */
    	genProd_initUIAPacketHeader (logger0, ptrRingBuffer + index*ringBufferSize, ringBufferSize, id);

        /* Initialize the UIA event headers. */
    	genProd_initUIAEventHeader (contractname,
                                ptrRingBuffer + index*ringBufferSize,
                                ringBufferSize,
                                dataSize,
                                0);
    }
}

#endif

#ifdef __LINUX_USER_SPACE
void* testCommonTfAccumConsumerTask(void* args)
{
	unitTestFramework_t       *tf = (unitTestFramework_t *) &unitTestFramework;

	args = args; /* to eliminate errors during string compilation */

	/* Wait for 100 milli second accumulation */
	do {
		task_sleep(100);
		if (tf->utfConsumerHandle)
			testCommonConsumeBuffers(tf->utfConsumerHandle);
	} while (tf->utfConsumerHandle);

   return ((void*) 0);
}
#else
void testCommonTfAccumConsumerTask(UArg a0, UArg a1)
{
	unitTestFramework_t       *tf = (unitTestFramework_t *) &unitTestFramework;

	/* Wait for 500 ms accumulation */
	do {
		Task_sleep(500);
		if (tf->utfConsumerHandle)
			testCommonConsumeBuffers(tf->utfConsumerHandle);
	} while (tf->utfConsumerHandle);

	Task_exit();
}
#endif

#ifdef __LINUX_USER_SPACE
void  testCommonTrigCUiaBufXchgTask(int *loopCtr )
{
	//uint32_t i;
	//const struct timespec sleepSetting = {0,100000000};
	//struct timespec remSleepSetting;
    int param1 = 1234;
    int param2 = 0x12345678;       

    char msg1[] = "Main+F2";
   
    /* Process the round robin method for all consumers */
    {
    	/* The Diags_setMask API allows sets of events to be dynamically enabled and disabled */
		if ((*loopCtr & 1) == 0) {
			Diags_setMask((String)msg1);
			Log_warning0((char*)"Enabling Info events(+F) and Analysis events (+Z)");
		} else {
			Diags_setMask((char*) "Main-FZ");
			Log_warning0((char*)"Disabling Info events(-F) and Analysis events (-Z)");
		}
		*loopCtr = (*loopCtr + 1);
     /*  The Log_info APIs (Log_info0..Log_info5) allow you to logging generic
         *  "informational" events that report what line of code logged the event.
         *  They allow you to log a printf-style string along with 0 to 5 values
         *  containing information such as event codes, details about what occurred
         *  application state information, etc.
         */
        Log_info0("Hello World!");

        /* Use Log_warning[0..6] to log an event that reports a warning-level event. */
        Log_warning1("This is a warning with one parameter: %d",param1);
        /* Use Log_error[0..6] to log an event to report that a recoverable error has occurred */
        Log_error2("This is an error with two parameters: %d,0x%x",param1,param2);

    	/* wait for a bit to avoid generating too many UDP packets! */
		//for (i=0; i<2; i++)
		//	nanosleep(&sleepSetting,&remSleepSetting);
        task_sleep(4);
    }
	
	return;
}

static tf_consumer_HANDLE create_consumers_arm(tf_contract_HANDLE contract_handle, uint32_t prod_coreId, uTestConsumers_e type)
{
	tf_consConfig_t       consumer_config;
	tf_consumer_HANDLE    handle = NULL;
 	tf_contractGetInfo_t  contractInfo;

 	/* create the consumers on any DSP cores */
 	memset(&consumer_config, 0, sizeof (tf_consConfig_t));
	
    consumer_config.contract_handle = contract_handle;
	consumer_config.consumer_notify.notify_method = TF_CONSUMER_NOTIFY_NONE;

    /* Send the buffers through ethernet from Core 0 only */
    if (type == UTF_CONSUMER_CUIA) {
       if (taskNum == prod_coreId) /* local notification for the consumers present in the local core */
  	   {
  	     /* local call back notifications in the core where producer is created */
  	     consumer_config.consumer_notify.notify_method = TF_CONSUMER_NOTIFY_VIA_LOCCBK2;
  	     consumer_config.consumer_notify.notifyScheme.notify_callbk2 = &testCommonConsumeBuffers;
       }
	   else  {
         consumer_config.consumer_notify.notify_method = TF_CONSUMER_NOTIFY_NONE;
       }	   
    }

    /* Send the buffers through ethernet from Core 0 only */
    if ( (taskNum == 0)  &&
		 (type  != UTF_CONSUMER_UIA_STREAM_STOP) )
	{
		/* Send the buffers via Ethernet */
        consumer_config.sendFxn         = testCommonEthSendPktFxn;
    }
    else /* rest of the cores just verify the buffers */
    {
 	   /* Just verify the buffers for continuity */
        consumer_config.sendFxn         = testCommonVerifyUiaBuffersFxn;
    }	

 	tf_contractGetInfo(contract_handle, &contractInfo);

	/* Always create one less allowed consumers on DSP side (to have at least one slot for ARM side) */
    if (contractInfo.num_consumers < UTF_ALLOWED_CONSUMERS) {
		 consumer_config.param   = (uint32_t) contractInfo.ringbuffer_base;		
		 handle =  tf_consCreate(&consumer_config);		 
		 if (handle == NULL)
		 {
			 System_printf ("Trap: Error in creating the consumers \n");
			 System_flush();
			 while (1);
		 }
		 System_printf ("Info: Consumer created for task num %d \n", taskNum);
	}
	else {
		 System_printf ("Notification: Maximum test allowed consumers are reached, core (ARM), not creating UIA consumers\n");
		 System_flush();
	}
	/* Record the notification type in test framewor */
	unitTestFramework.consumerNotifyType = (tf_notifyTypes_e)consumer_config.consumer_notify.notify_method;

	return (handle);

}

static tf_consStatus_e delete_consumer_arm(tf_consumer_HANDLE handle)
{
	tf_consStatus_e consStatus;

	System_printf("deleting consumer for Task Num: %d \n", taskNum);
	System_flush();	

	consStatus = tf_consDestroy(handle);

	if (consStatus != TF_CONSUMER_SUCCESS )	{	
		System_printf ("Trapped due to error (code=%d) in tf_consDestroy API for Task Num %d \n", consStatus, taskNum);
	}	
	
	return (consStatus);

}
#else
/* UIA Log message task that triggers buffer exchange */

#if !defined(LOGGERSTREAMER2_GENPROD)
void testCommonTrigUiaBufXchgTask(UArg a0, UArg a1)
{
    int      key, count = 0; 
    int32_t  t0, t1;
#ifdef LOGGERSTREAMER2_SUPPORT
    int32_t  diff1, diff2, t2, t3;
    uint32_t logger_select = (uint32_t) a0;
#endif

	t0 = Timestamp_get32();
    t1 = Timestamp_get32();

    t0 = Timestamp_get32();
    t1 = Timestamp_get32();

    /* Run this task until all consumers have consumed the specified number of buffers */
    while (!tf_producer_notification_task_exit_flag) 	{
	        t0 = Timestamp_get32();
	        key = Hwi_disable();

#ifdef LOGGERSTREAMER2_SUPPORT
	        if (logger_select == 0) {
	            t0 = TSCL;
	            Log_iwriteUC1(logger0, UIABenchmark_startInstanceWithAdrs, diff1);
	            t1 = (int32_t) TSCL;
	        }

			if (logger_select == 1) {
	            t2 = (int32_t) TSCL;
				Log_iwriteUC1(logger1, LoggerStreamer2_L_test, diff2);
	            t3 = (int32_t) TSCL;
			}
	        Hwi_restore(key);

	        diff1 = (int32_t)t1-(int32_t)t0;
	        diff2 = (int32_t)t3-(int32_t)t2;
#else
	        Log_writeUC1(LoggerStreamer_L_test, t1-t0);

	        t1 = TSCL;
	        Hwi_restore(key);
#endif
	        /* yield to other task after few messages are filled up in the buffer */
     		count ++;
            if (count > 1) {
				count = 0;
				Task_sleep(1);
            }
    }
#ifdef LOGGERSTREAMER2_SUPPORT
    if (logger_select == 0)
      LoggerStreamer2_flush(logger0);
    else if (logger_select == 1)
      LoggerStreamer2_flush(logger1);
#else
	LoggerStreamer_flush(); 
#endif
    /* clear it for next test */
	tf_producer_notification_task_exit_flag = 0;
	Task_exit();
}
#endif

static tf_consumer_HANDLE create_consumers_dsp(tf_contract_HANDLE contract_handle, uint32_t prod_coreId, uTestConsumers_e type, uint32_t dsp_core_block_id_start)
{
	tf_consConfig_t       consumer_config;
	tf_consumer_HANDLE    handle = NULL;
 	tf_contractGetInfo_t  contractInfo;
	uint32_t              coreId = CSL_chipReadDNUM();

 	/* create the consumers on any DSP cores */
 	memset(&consumer_config, 0, sizeof (tf_consConfig_t));
    consumer_config.contract_handle = contract_handle;
	seqNumProcFirstTime = 0;

	switch (type) {
		case UTF_CONSUMER_UIA:
        case UTF_CONSUMER_UIA_STREAM_STOP:			
		case UTF_CONSUMER_UIAMINST:
		    if (coreId == prod_coreId) /* local notification for the consumers present in the local core */
		    {
		       /* local call back notifications in the core where producer is created */
		    	consumer_config.consumer_notify.notify_method = TF_CONSUMER_NOTIFY_VIA_LOCCBK2;
		    	consumer_config.consumer_notify.notifyScheme.notify_callbk2 = &testCommonConsumeBuffers;
		    }
		    else 
		    {
               consumer_config.consumer_notify.notify_method = TF_CONSUMER_NOTIFY_NONE;
		    }			
			break;
        case UTF_CONSUMER_GENERALPROD:
		case UTF_CONSUMER_CUIA:			
		    	consumer_config.consumer_notify.notify_method = TF_CONSUMER_NOTIFY_NONE;
			break;
	}
    
    /* Send the buffers through ethernet from Core 0 only */
    if ( (coreId == 0)  &&
		 (type  != UTF_CONSUMER_UIA_STREAM_STOP) )
	{
		/* Send the buffers via Ethernet */
        consumer_config.sendFxn         = testCommonEthSendPktFxn;
    }
    else /* rest of the cores just verify the buffers */
    {
 	   /* Just verify the buffers for continuity */
        consumer_config.sendFxn         = testCommonVerifyUiaBuffersFxn;
    }

 	tf_contractGetInfo(contract_handle, &contractInfo);

    if ( (coreId >= dsp_core_block_id_start) )
    {
    	/* Dont create a consumer on higher DSP cores to allow the slot for ARM side consumer */
        consumer_config.consumer_notify.notify_method = TF_CONSUMER_NOTIFY_NONE;		
    }
    else {
		/* Always create one less allowed consumers on DSP side (to have at least one slot for ARM side) */
		if (contractInfo.num_consumers < UTF_ALLOWED_CONSUMERS) {
			 consumer_config.param   = (uint32_t) contractInfo.ringbuffer_base;
			 System_printf("Creating Consumer on CoreId: %d \n number of consumers before Create: %d\n", coreId, contractInfo.num_consumers);
			 handle =  tf_consCreate(&consumer_config);

			 if (handle == NULL)
			 {
				 System_printf ("Trap: Error in creating the consumers \n");
				 System_flush();
				 while (1);
			 }
   		     tf_contractGetInfo(contract_handle, &contractInfo);
			 System_printf("number of consumers after Create: %d \n", contractInfo.num_consumers);
			 System_printf("Created consumer on coreId %d \n",coreId);
			 System_flush();

		}
		else {
    		 System_printf ("Notification: Maximum test allowed consumers are reached, core (DSP):%d, not creating UIA consumers\n", coreId);			 
			 System_flush();
		}
    }

	/* Record the notification type in test framewor */
	unitTestFramework.consumerNotifyType = (tf_notifyTypes_e)consumer_config.consumer_notify.notify_method;

	return (handle);

}

static tf_consStatus_e delete_consumer_dsp(tf_consumer_HANDLE handle)
{
	tf_consStatus_e consStatus;
	uint32_t         coreId = CSL_chipReadDNUM();

	System_printf("deleting consumer for core : %d \n", coreId);
	System_flush();
	consStatus = tf_consDestroy(handle);

	if (consStatus != TF_CONSUMER_SUCCESS )	{	
		System_printf ("Trapped due to error in tf_consDestroy API \n");
		System_flush();
		while (1);
	}	
	
	return (consStatus);

}
#endif

tf_consAppSendRespStatus_e testCommonVerifyUiaBuffersFxn(uint32_t param, uint8_t* logBuf, uint32_t logBufSize)
{
	UIAPacket_Hdr *uia_pkt_hdr = (UIAPacket_Hdr *) logBuf;
	int16_t        sequence_num;
	int16_t        tmp;
	unitTestFramework_t *tf = &unitTestFramework;

	param = param; logBufSize = logBufSize;

	/* This function sets any missing buffer statistics in the unit test global structure
	 */

	/* Get the sequence Count (uint16_t*/
	sequence_num = UIAPacket_getSequenceCount(uia_pkt_hdr);
	if (sequence_num == -1)
	    /* force success for the trace framework consumer instance after multiple retries */
	    return (TF_CONSUMER_APP_SEND_OK_NO_WAIT_ACK);
	
	tmp          = (sequence_num - tf->tfPrevValue);
    /* if the distance is more than 1, then there is some buffer missing */
	if ((tmp > 1) && (seqNumProcFirstTime))
		tf->tfNumBufsMissed += (tmp -1);
	/* update the previous sequence number */
	tf->tfPrevValue = sequence_num;
	seqNumProcFirstTime = 1;
	
    return (TF_CONSUMER_APP_SEND_OK_NO_WAIT_ACK);
}


void testCommonPrintContractSummary(tf_contract_HANDLE contract_handle, uint32_t coreId)
{
	tf_contractGetInfo_t info;

	tf_contractGetInfo(contract_handle, &info);
#ifdef __LINUX_USER_SPACE
	System_printf ("\n Summary from Producer (task num %d):	\n", coreId );
#else
	System_printf ("\n Summary from Producer (core Id %d):  \n", coreId );
#endif
	System_printf (" -------------------------------------------------------------\n");
	System_printf ("	 Contract Version Information from contract Memory : %d\n", info.contractVersion);	
	System_printf ("	 Number of Consumers in the test : %d\n", tf_num_consumers);
	System_printf ("	 Number of consumers pending deletion from contract : %d\n",  info.num_consumers);
	System_printf ("	 Number of ring buffers in the system: %d  (wrap_flag = %d) \n", info.num_ringbuf, info.rb_wrap_flag);
	System_printf ("	 Number of times producer stalled due to slow consumer = %d \n", info.overrun_count);
	System_flush();
}

#ifdef __LINUX_USER_SPACE
void* testCommonNotifyTfConsumersTask(void* args)
{
    void  *a0 = (void *)((task_args_t *)args)->a0;
#else
void testCommonNotifyTfConsumersTask(UArg a0, UArg a1)
{
#endif
 	tf_producer_HANDLE   producer_handle;
	tf_contract_HANDLE   contract_handle;
	uint32_t             num_consumers;
	uint32_t             producer_state_change = 0;
	tf_producerState_e   prodState = TF_PRODUCER_STATE_STREAMING, prevProdState = TF_PRODUCER_STATE_INVALID;
	uint32_t             statemapIndex = 0;
	tf_contractGetInfo_t contractInfo;
	static char statemap[][50] = {
	 {"TF_PRODUCER_STATE_STREAMING"},
	 {"TF_PRODUCER_STATE_FREE_RUNNING"},
	 {"TF_REQUEST_PRODUCER_STATE_FREEZING"},
	 {"TF_PRODUCER_STATE_FROZEN"},
	 {"TF_PRODUCER_STATE_INVALID"}	 
	};

    producer_handle      = (tf_producer_HANDLE) a0;
 	do {
		/* get the information about how many consumers are attached to the contract/producer */
		num_consumers = tf_prodUpdateConsumers(producer_handle);
		if ( tf_num_consumers < (int) num_consumers) {
			tf_num_consumers =  (int) num_consumers; /* Obtain the steady state information about how many consumers got latched during the test */
		}

		/* notify any consumers if present */
		if (num_consumers) {
			tf_prodGetContractHandle(producer_handle, &contract_handle);
			tf_contractGetInfo(contract_handle, &contractInfo);
			prodState =(tf_producerState_e) contractInfo.producer_current_state;
			producer_state_change = (uint32_t)prodState ^ (uint32_t) prevProdState;
			if (producer_state_change) {
    			if ( (prodState <= TF_PRODUCER_STATE_INVALID) ||
    			     (prodState > TF_PRODUCER_STATE_FROZEN) )
    				statemapIndex = 4;
    			else
    				statemapIndex = (uint32_t) prodState;
				System_printf ("Observed Producer State Change to %s after consuming %d number of buffers \n", statemap[statemapIndex], unitTestFramework.tfNumBufsConsumed);
				System_flush();
				prevProdState = prodState;
			}
			tf_prodNotifyConsumers( producer_handle);
		}

#ifndef __LINUX_USER_SPACE
    	Task_yield();
#endif

 	} while ( (num_consumers) || !tf_num_consumers); /* Wait for at least one consumer for the produer */

	tf_producer_notification_task_exit_flag = 1;

    /* Exit notifications after all the consumers are deleted */
	Task_exit();

#ifdef __LINUX_USER_SPACE
    return ((void*) 0);
#else
	return;
#endif
}

/* trace framework producer create for the contract  
 * Since LoggerStreamer2 (multi instance) supports more than one instance per core, we can use contract ver to create another
 * logger in the same core for the test. Please note that number of Logger2 Instances per core is NOT limitted by number of
 * contract versions supported by trace framework. This is just for this test.
 */
tf_producer_HANDLE testCommonCreateTfProducer(tf_contract_HANDLE contract_handle, uint32_t  contract_ver, tf_producerTypes_e pType )
{
 	tf_prodConfig_t      producer_config;

	contract_ver = contract_ver;
	memset(&producer_config, 0, sizeof (tf_prodConfig_t));

	/* Create the producer */
	producer_config.contract_handle = contract_handle;
	producer_config.prodType        = pType;

	if (pType == TF_PRODUCER_TYPE_GEN) {
		/* Notification is done during the exchange itself */
		producer_config.xchg_cb_fxn   = &tf_prodNotifyConsumers;
	}
#ifdef LOGGERSTREAMER2_SUPPORT	
	if (pType == TF_PRODUCER_TYPE_UIA_MINST) {
		switch (contract_ver) {
			case 1:
				producer_config.obj_handle = (void*) logger0;
				break;
			case 2:
				producer_config.obj_handle = (void*) logger1;
				break;
		}
	}
#endif	
	return (tf_prodCreate (&producer_config));
}

/* consumer create */
void testCommonCreateTfConsumer(tf_contract_HANDLE contract_handle, uint32_t prod_coreId, uTestConsumers_e type, uint32_t dsp_core_block_id_start)
{
    seqNumProcFirstTime = 0;
#ifdef __LINUX_USER_SPACE
    dsp_core_block_id_start = dsp_core_block_id_start;
    unitTestFramework.utfConsumerHandle = create_consumers_arm(contract_handle, prod_coreId, type);
#else
    unitTestFramework.utfConsumerHandle = create_consumers_dsp(contract_handle, prod_coreId, type, dsp_core_block_id_start);
#endif
}

/* Consumer delete */
void testCommonDeleteTfConsumer(tf_consumer_HANDLE consumer_handle)
{
#ifdef __LINUX_USER_SPACE
   delete_consumer_arm(consumer_handle);
#else
   delete_consumer_dsp(consumer_handle);
#endif
}

int testCommonCheckToDeleteConsumer(unitTestFramework_t *tf, tf_contract_HANDLE handle, uint32_t max_buffers)
{
	tf_contractGetInfo_t info;

	tf_contractGetInfo(handle, &info);
	/* Check if specified number of buffers are consumed,
	 * OR producer is deleted making the contract free
	 *  if yes, delete the consumer for the contract
	 */
 	if ( (tf->tfNumBufsConsumed > max_buffers) ||
		 (info.contractState == TF_CONTRACT_AVAILABLE))
	{
	   /* Set the producer to non-freeze state if this is the consumer triggered it */
	   if (tf->EnableProdStateChange) {
         System_printf ("Setting prodcuer State to: TF_PRODCUER_STATE_STREAMING after consuming %d number of buffers \n", tf->tfNumBufsConsumed);
		 System_flush();
		 tf_consumerSetProducerState(tf->utfConsumerHandle,TF_PRODUCER_STATE_STREAMING);
		 tf->EnableProdStateChange = 0;
	   }
       testCommonDeleteTfConsumer(tf->utfConsumerHandle);
	   /* Indicate in the test framework */
	   tf->utfConsumerHandle = 0;
	   /* do not wait for any more notifications in test framework */
	   tf->consumerNotification = UTEST_CONSUMER_NOTIFY_NONE;
	   return 0; /* consumer is deleted, it is inactive */
	}

	return 1; /* consumer is still active */
}

tf_consAppSendRespStatus_e testCommonEthSendPktFxn(uint32_t param, uint8_t* logBuf, uint32_t logBufSize)
{
    uint32_t                   retry_count = 0;
	UIAPacket_Hdr             *uia_pkt_hdr = (UIAPacket_Hdr *) logBuf;
	int16_t                   sequence_num;
	int16_t                   tmp;
	unitTestFramework_t *tf = &unitTestFramework;
    utfEth_HANDLE             handle = NULL;	

	/* This function sets any missing buffer statistics in the unit test global structure
	 */
	/* Get the sequence Count (uint16_t*/
	sequence_num = UIAPacket_getSequenceCount(uia_pkt_hdr);

	/* Below check is a work around to support logger streamer on core 0,
	 * getting to run for contract 0 and contract 1
	 */
	if (sequence_num == -1)
	    /* force success for the trace framework consumer instance after multiple retries */
	    return (TF_CONSUMER_APP_SEND_OK_NO_WAIT_ACK);

	tmp          = (sequence_num - tf->tfPrevValue);
    /* if the distance is more than 1, then there is some buffer missing */
	if ((tmp > 1) && (seqNumProcFirstTime))
		tf->tfNumBufsMissed += (tmp -1);

	/* update the previous sequence number */
	tf->tfPrevValue = sequence_num;
	seqNumProcFirstTime = 1;	

#if !defined(__LINUX_USER_SPACE)	
#if defined (PARTNO_C6657)
    handle = (utfEth_HANDLE) NULL;
#else
    handle = (utfEth_HANDLE) unitTestFramework.tfEthHandle;
#endif
#endif

    do {
    	if (utf_EthSend(handle, logBuf, logBufSize, param)) {
    		/* Ethernet send failed, retry sometime later */
    		retry_count ++;
    	}
    	else /* success, get the terminate conditions */
    		retry_count = 5;
    } while (retry_count < 5);

    /* force success for the trace framework consumer instance after multiple retries */
    return (TF_CONSUMER_APP_SEND_OK_NO_WAIT_ACK);
}

/**************************************************************************************
 * FUNCTION PURPOSE: Get the system contract base address
 **************************************************************************************
 * DESCRIPTION: get the configurtation values
 **************************************************************************************/
void* testCommonGetSysContractVer1BaseAddrPhysical(void)
{
   void* addr;
   /* this is the reserved physical address for the version 1 system contracts
    * The information between DSP and ARM is statically assigned for this unit test
    * Please note that the information exchange can be done via any other mechanisms
    * instead of statically reserving the memory for version 1 trace framework system contracts */
   addr = (void*) 0x0C000000;
   return (addr);
}

/**************************************************************************************
 * FUNCTION PURPOSE: Get the system name server handle
 **************************************************************************************/
tfTest_nameserver_HANDLE testCommonGetSysNameServerHandle(void)
{
	tfTest_nameserver_HANDLE addr;
   /* Very primitive (static) name server is implemented between DSP and ARM */
   addr = (tfTest_nameserver_HANDLE) 0x0C009000;
#ifdef __LINUX_USER_SPACE
   addr = (tfTest_nameserver_HANDLE)fw_PhyToVirt(addr);
#endif
   return (addr);
}

/**************************************************************************************
 * FUNCTION PURPOSE: Get the sync memory pointer
 **************************************************************************************/
tfuTestSyncVars_t* testCommonGetSycMemHandle(void)
{
	tfuTestSyncVars_t* addr;
#ifdef __LINUX_USER_SPACE
   addr = (tfuTestSyncVars_t*) 0x0C00A000;
   addr = (tfuTestSyncVars_t*) fw_PhyToVirt(addr);
#else
   addr = (tfuTestSyncVars_t*) &memSyncRam[0];
#endif
   return (addr);
}

/**************************************************************************************
 * FUNCTION PURPOSE: Get the system contract handle for version 1 contracts in the system
 **************************************************************************************/
static tf_contract_HANDLE get_contract_handle_ver2(char* name)
{
	tf_contract_HANDLE contract_handle;
	utfNameServer_result result;

	result = testCommonNameServerPop(name, (utfCustom_HANDLE *)&contract_handle);

	if (result == UTF_NAMESERVER_OK)
		return (contract_handle);
	else
		return ((tf_contract_HANDLE)NULL);
}

/**************************************************************************************
 * FUNCTION PURPOSE: Get the system contract handle for version 1 contracts in the system
 **************************************************************************************/
static tf_contract_HANDLE get_contract_handle_ver1(char* name)
{
 	void*              system_contracts_base_addr;
	tf_contract_HANDLE contract_handle;
#ifdef __LINUX_USER_SPACE	
    shmLayout_t	       *shmLayout = (shmLayout_t *)fwShmHandle;

    /* get the base address of the contract for version 1 types */
	if (armOnlyContract) {
		system_contracts_base_addr = &shmLayout->memContractRam[UTF_CONTRACT_VERSION_1][0];
	}
	else 
#endif		
	{
	  /* Non ARM only contracts get the physical address */
	  system_contracts_base_addr = testCommonGetSysContractVer1BaseAddrPhysical();
	}
	
    /* Get the contract handle for the test case */
    contract_handle = tf_contractFind (system_contracts_base_addr, unitTestFramework.tfNumV1Contracts, name);

    return (contract_handle);
}
/**************************************************************************************
 * FUNCTION PURPOSE: Set the test status from all cores
 **************************************************************************************/
testStatus_t* testCommonGetTestStatusArrayBase(void)
{
#ifdef __LINUX_USER_SPACE
  return ((testStatus_t *) fw_PhyToVirt((void*)0xc00b000)); /* Provide the memory mapped address (virt address) */
#else
  return ((testStatus_t *) 0xc00b000);
#endif
}

/**************************************************************************************
 * FUNCTION PURPOSE: reset the test status
 **************************************************************************************/
void testCommonResetTestStatus(void)
{
    int          i, size;
	testStatus_t* statusArray = testCommonGetTestStatusArrayBase();
	size = sizeof (testStatus);
	utlBeginMemAccess(statusArray, size);
	
	/* reset values */
	memset(statusArray, 0, size);
	for ( i = 0 ; i < (int)(TEST_STATUS_COUNT); i++)
		statusArray[i].u.info.status = UTF_TEST_NOT_RUN;

	utlEndMemAccess(statusArray, size);
}

/**************************************************************************************
 * FUNCTION PURPOSE: Publish test summary from consumers
 **************************************************************************************/
void testCommonPublishSummaryFromConsumer(unitTestFramework_t *tf, tfTest_t* tft, uint32_t index, uint32_t contract_verId)
{
	testStatus_t* statusArray = testCommonGetTestStatusArrayBase();

	index = index; contract_verId = contract_verId;

	if (tf->tfNumBufsConsumed == 0) {
    	tft->testStatus = UTF_TEST_NOT_RUN;		
	}
	/* Override test status if it is the consumer triggering producer state change test */
	else if (strcmp(tft->name, "DSP UIA Stream/Freeze Producer, Single ARM Consumer Test") == 0)
		tft->testStatus = UTF_TEST_PASSED;
	else if (tf->tfNumBufsMissed) {
    	tft->testStatus = UTF_TEST_FAILED;
	}
	else {
	    tft->testStatus = UTF_TEST_PASSED;		
	}

	utlBeginMemAccess(&statusArray[index], sizeof (testStatus_t));
  	  statusArray[index].u.info.status = tft->testStatus;
	  statusArray[index].u.info.consume_count = tf->tfNumBufsConsumed;
	  statusArray[index].u.info.miss_count    = tf->tfNumBufsMissed;
	utlEndMemAccess(&statusArray[index], sizeof (testStatus_t));	
}

/**************************************************************************************
 * FUNCTION PURPOSE: Summarize all test summary from Master (producer)
 **************************************************************************************/
void testCommonSummarizeAllTestStatus(tfTest_t  *tft, char* testname, uint32_t ref_count, uint32_t exp_num_bufs, uint32_t contract_ver)
{
	testStatus_t* statusArray = testCommonGetTestStatusArrayBase();
	int           i;
	uint32_t      count = 0;
	utlBeginMemAccess(statusArray, TEST_STATUS_ARRAY_SIZE);
	for (i = 0; i < (int) (TEST_STATUS_COUNT); i ++) {
        if (statusArray[i].u.info.status == UTF_TEST_PASSED)
        	count ++;
	}
	/* Update the test status in master core */
	if (count == ref_count)
		tft->testStatus = UTF_TEST_PASSED;
	else
		tft->testStatus = UTF_TEST_FAILED;

	for ( i = 0; i< (int) (TEST_STATUS_COUNT); i++ ) {
		if (statusArray[i].u.info.status == UTF_TEST_NOT_RUN)
			continue;
		System_printf ("	 Consumer (%d) Minimum Number of buffers expected = %d, Actual consumed: %d, buffers missed: %d \n", i, exp_num_bufs, statusArray[i].u.info.consume_count, statusArray[i].u.info.miss_count);
	}

#if 0  
 	if (tft->testStatus == UTF_TEST_PASSED)
		System_printf ("	 %s: PASSED for Contract Version %d\n", testname, contract_ver);
	else
		System_printf ("	 %s: FAILED for Contract Version %d\n", testname, contract_ver);
#endif

	System_flush();

	utlEndMemAccess(statusArray, TEST_STATUS_ARRAY_SIZE);
}


/**************************************************************************************
 * FUNCTION PURPOSE: Get the system contract handle
 **************************************************************************************/
tf_contract_HANDLE testCommonGetTfContractHandle(char* contract_name, tf_contractVersion_e contract_version)
{
	tf_contract_HANDLE contract_handle = NULL;

	if (contract_version == TF_CONTRACT_VERSION_1)
		contract_handle = get_contract_handle_ver1(contract_name);
	else if (contract_version == TF_CONTRACT_VERSION_2)
		contract_handle = get_contract_handle_ver2(contract_name);

	return (contract_handle);
}

/**************************************************************************************
 * FUNCTION PURPOSE: Push the handles into name server
 **************************************************************************************/
utfNameServer_result testCommonNameServerPush(char* name, utfCustom_HANDLE handle)
{
	uint32_t   key;
	int        index;
	tfuTestNameServer_t *name_server;

	name_server = (tfuTestNameServer_t*)testCommonGetSysNameServerHandle();
#ifndef __LINUX_USER_SPACE
	unitTest_MtCsEnter(&key);
#else
   Osal_fwCsEnter(&key);
#endif
	utlBeginMemAccess(name_server, sizeof(tfuTestNameServer_t) * UTF_NAMESERVER_CAPACITY);	
	index = unitTestFramework.nameServerIndex;
	/* copy name and handles to the name server */
	memcpy(name_server[index].name, name, UTF_NAMESERVER_NAME_LEN);
	name_server[index].handle = handle;

	/* Point to next index for future push */
	if (unitTestFramework.nameServerIndex < UTF_NAMESERVER_CAPACITY)
		unitTestFramework.nameServerIndex++;
	else
		unitTestFramework.nameServerIndex = 0;

	utlEndMemAccess(name_server, sizeof(tfuTestNameServer_t) * UTF_NAMESERVER_CAPACITY);

#ifndef __LINUX_USER_SPACE
	unitTest_MtCsExit(key);
#else
	Osal_fwCsExit(key);
#endif
	return(UTF_NAMESERVER_OK);
}

/**************************************************************************************
 * FUNCTION PURPOSE: Pop the handles into name server, returns value in handle
 **************************************************************************************/
utfNameServer_result testCommonNameServerPop(char* name, utfCustom_HANDLE* handle)
{
	tfuTestNameServer_t* name_server;
    int                  i;

	name_server  = (tfuTestNameServer_t*)testCommonGetSysNameServerHandle();

	utlBeginMemAccess(name_server, sizeof(tfuTestNameServer_t) * UTF_NAMESERVER_CAPACITY);

	/* scan the entire name server for the match */
	for (i = 0; i < UTF_NAMESERVER_CAPACITY; i++) {
	    if ( strcmp(name_server[i].name, name) == 0 ) {
	    	/* found the match return the handle */
#ifdef __LINUX_USER_SPACE
            if (armOnlyContract == 0)
				*handle = (utfCustom_HANDLE) fw_PhyToVirt(name_server[i].handle);
			else
#endif
    	    	*handle = name_server[i].handle;
	    	break;
	    }
	}
	
	utlEndMemAccess(name_server, sizeof(tfuTestNameServer_t) * UTF_NAMESERVER_CAPACITY);	

	/* did not find the match */
	if ( i == UTF_NAMESERVER_CAPACITY) {
		*handle = NULL;
		return(UTF_NAMESERVER_NOTFOUND);
	}

	return(UTF_NAMESERVER_OK);
}


/* Nothing past this point */
		
		


