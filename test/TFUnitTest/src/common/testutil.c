/*
 *
 * Copyright (C) 2010-2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "tfutest.h"

#ifndef __LINUX_USER_SPACE
#include "c66x/bios/tfutestCache.h"
extern cregister volatile unsigned int DNUM;
extern cregister volatile unsigned int TSCL;

void utilCycleDelay (int count)
{
  uint32_t TSCLin = TSCL;

  if (count <= (int)0)
    return;

  while ((TSCL - TSCLin) < (uint32_t)count);

}
#else
#include "fw_task.h"

void utilCycleDelay (int32_t count)
{
    int32_t sat; 
    for (sat=0; sat<count; sat++);
}
#endif

#ifndef __LINUX_USER_SPACE
uint32_t utilgAddr(uint32_t x)
{
    if ((x >= (uint32_t)0x800000) && (x < (uint32_t)0x900000))
      x = (1 << 28) | (DNUM << 24) | x;
      
    return (x);
}
uint32_t utilLocalAddr(uint32_t x)
{
    return (x & (uint32_t)0x00FFFFFF);
}
#else
uint32_t utilgAddr(uint32_t x)
{
    return x;
}
uint32_t utilLocalAddr(uint32_t x)
{
    return x;
}
#endif

/**************************************************************************************
 * FUNCTION PURPOSE: Get the values in strings from the file
 **************************************************************************************
 * DESCRIPTION: get the configurtation values
 **************************************************************************************/
#define MAX_LINE_LENGTH 127
#define MAX_NUM_STRING_MATCH 5
/* Input parameters */
#define UTEST_SA_IP_ADDR_MATCH_STRING      "system_analyzer_pc_ip_addr"
#define UTEST_SA_MAC_ADDR_MATCH_STRING     "system_analyzer_pc_mac_addr"
#define UTEST_EVM_IP_ADDR_MATCH_STRING     "evm_ip_addr"
#define UTEST_EVM_MAC_ADDR_MATCH_STRING    "evm_mac_addr"
#define UTEST_SA_UDP_PORT_MATCH_STRING     "system_analyzer_udp_port"

#define UTEST_IP_DST_OFFSET        0
#define UTEST_IP_SRC_OFFSET        1
#define UTEST_MAC_DST_OFFSET       2
#define UTEST_MAC_SRC_OFFSET       3
#define UTEST_SA_UDP_PORT_OFFSET   4

static char match_string[MAX_NUM_STRING_MATCH][MAX_LINE_LENGTH] = {
        UTEST_SA_IP_ADDR_MATCH_STRING,
        UTEST_EVM_IP_ADDR_MATCH_STRING,
        UTEST_SA_MAC_ADDR_MATCH_STRING,     
        UTEST_EVM_MAC_ADDR_MATCH_STRING,
        UTEST_SA_UDP_PORT_MATCH_STRING
};

/* check if the key is in the match list */
static int is_key_in_match_string(char* key, uint32_t* match_string_offset)
{
    int i, found;

    for (i = 0, found = 0; i < MAX_NUM_STRING_MATCH; i ++)  {
        if (strcmp(match_string[i], key) == 0) {
            found =1;
            break;
        }
    }
    
    *match_string_offset = i;
        
    return (found);
}
void utlTestReadValues(FILE* fp, transportArgs_t* tArgs)
{
    char line[MAX_LINE_LENGTH + 1];   
    char tokens[] = " :=;\n";
    char data_tokens[] = ".-:";
    char *key, *data, *ep;    
    uint32_t temp, match_string_offset;  
#ifndef __LINUX_USER_SPACE
    int  i;
#endif
    
    memset(line, 0, MAX_LINE_LENGTH + 1);    
    
    while (fgets(line, MAX_LINE_LENGTH + 1, fp)) {
        key  = (char *)strtok(line, tokens);        
        data = (char *)strtok(NULL, tokens);

        /* Check if Key is in match strings */
        temp = is_key_in_match_string(key, &match_string_offset);

        if(temp == 0) {
            continue;    /* not found, continue to scan next line */
        }

        /* found, process the data string to get the arguments */
        ep = (char *) strtok(data, data_tokens);
#ifdef __LINUX_USER_SPACE
       switch (match_string_offset) {
          case UTEST_IP_DST_OFFSET:
            if (ep != NULL)
            {
                strncpy (tArgs->ipDst, ep, 4);
                strncat (tArgs->ipDst, ".", 1);
                data = (char *)strtok(NULL, data_tokens);
                
                strncat (tArgs->ipDst, data, 4 );
                strncat (tArgs->ipDst, ".", 1);
                data = (char *)strtok(NULL, data_tokens);           
                
                strncat (tArgs->ipDst, data, 4);
                strncat (tArgs->ipDst, ".", 1);
                data = (char *)strtok(NULL, data_tokens);           
                
                strncat (tArgs->ipDst, data, 4);
                strncat (tArgs->ipDst, "", 1);
            }

            printf ("tArgs->ipDst = %s \n", tArgs->ipDst);
            break;
          case UTEST_SA_UDP_PORT_OFFSET:
            tArgs->remoteUdpPort = strtol(ep, NULL, 0);     
            break;
          default:
            break;
        }
#else
        switch (match_string_offset) {
            case UTEST_IP_DST_OFFSET:               
                if (ep != NULL) {
                    tArgs->ipDst[0] = strtol(ep, NULL, 0);
                    for ( i = 1; i < utfEth_IPV4_ADDR_SIZE; i ++ ) {
                        tArgs->ipDst[i] = strtol( (char*) strtok(NULL, data_tokens), NULL, 0);
                    }
                }
                break;
            case UTEST_IP_SRC_OFFSET:
                if (ep != NULL) {
                    tArgs->ipSrc[0] = strtol(ep, NULL, 0);
                    for ( i = 1; i < utfEth_IPV4_ADDR_SIZE; i ++ ) {
                        tArgs->ipSrc[i] = strtol( (char*) strtok(NULL, data_tokens), NULL, 0);
                    }
                }
                break;
            case UTEST_MAC_DST_OFFSET:
                if (ep != NULL) {
                    tArgs->macDst[0] = strtol(ep, NULL, 16);
                    for ( i = 1; i < utfEth_MAC_ADDR_SIZE; i ++ ) {
                        tArgs->macDst[i] = strtol( (char*) strtok(NULL, data_tokens), NULL, 16);
                    }               
                }
                break;              
            case UTEST_MAC_SRC_OFFSET:
                if (ep != NULL) 
                {
                    tArgs->macSrc[0] = strtol(ep, NULL, 16);
                    for ( i = 1; i < utfEth_MAC_ADDR_SIZE; i ++ ) {
                        tArgs->macSrc[i] = strtol( (char*) strtok(NULL, data_tokens), NULL, 16);
                    }               
                }
                break;              
            case UTEST_SA_UDP_PORT_OFFSET:
                if (ep != NULL) {
                    tArgs->remoteUdpPort = strtol(ep, NULL, 0);
                }
            default:
                break;
        }       
#endif      
    }
}

void utlEndMemAccess (void *blockPtr, uint32_t size)
{
    extern void Osal_writeBackCache (void *blockPtr, uint32_t size);
#ifdef __LINUX_USER_SPACE
    Osal_writeBackCache(blockPtr, size);
#else
    uint32_t    key;
    /* Disable Interrupts */
    key = Hwi_disable();

    SYS_CACHE_WB (blockPtr, size, CACHE_FENCE_WAIT);

    asm   (" nop      4");
    asm   (" nop      4");
    asm   (" nop      4");
    asm   (" nop      4");

    /* Reenable Interrupts. */
    Hwi_restore(key);
#endif
}

void utlBeginMemAccess (void *blockPtr, uint32_t size)
{
       extern void Osal_invalidateCache (void *blockPtr, uint32_t size);
#ifdef __LINUX_USER_SPACE
       Osal_invalidateCache(blockPtr, size);
#else
       uint32_t    key;

       /* Disable Interrupts */
        key = Hwi_disable();

        /* Cleanup the prefetch buffer also. */
        CSL_XMC_invalidatePrefetchBuffer();

        SYS_CACHE_INV (blockPtr, size, CACHE_FENCE_WAIT);

        asm   (" nop      4");
        asm   (" nop      4");
        asm   (" nop      4");
        asm   (" nop      4");

        /* Reenable Interrupts. */
        Hwi_restore(key);
#endif
}

void utlWaitUntilMasterDone(tfSyncToken_e token)
{
    tfSyncToken_e sync_token;

  switch (token)    {
    case SYNC_TOKEN_MASTER_SYSINIT_DONE:
        do {
             sync_token = (tfSyncToken_e)utlReadSyncValue(SYNC_TOKEN_MASTER_SYSINIT_DONE);
#ifdef __LINUX_USER_SPACE
      task_sleep(10);
#endif           
         } while (sync_token != token);
          break;
    case SYNC_TOKEN_MASTER_UNITTEST_DONE:
        do {
             sync_token = (tfSyncToken_e)utlReadSyncValue(SYNC_TOKEN_MASTER_UNITTEST_DONE);
#ifdef __LINUX_USER_SPACE
      task_sleep(10);
#endif           
         } while (sync_token != token);
        break;
    default: 
            break;
  }
}

void utlResetSyncVals(Bool flag)
{
    tfuTestSyncVars_t *memSync = testCommonGetSycMemHandle();
    uint32_t temp;
    
    utlBeginMemAccess(memSync, sizeof(tfuTestSyncVars_t));

    temp = memSync->masterState.masterTestCompleteCount;
    memset(memSync, 0 , sizeof (tfuTestSyncVars_t));
    if (flag)
        memSync->masterState.masterTestCompleteCount = temp;

    utlEndMemAccess(memSync, sizeof(tfuTestSyncVars_t));
}

uint32_t utlReadSyncValue(tfSyncToken_e token)
{
    uint32_t      retVal = SYNC_TOKEN_INIT_VAL;
    uint32_t      i;
    tfuTestSyncVars_t *memSync = testCommonGetSycMemHandle();
    
    utlBeginMemAccess(memSync, sizeof(tfuTestSyncVars_t));

      switch (token)    {
        case SYNC_TOKEN_MASTER_SYSINIT_DONE:
             retVal = memSync->masterState.isMasterSysInitDone;
          break;
        case SYNC_TOKEN_MASTER_UNITTEST_DONE:
             retVal = memSync->masterState.masterTestCompleteCount;
             break;
        case SYNC_TOKEN_CHECK_CONSUMERS_DONE:
            retVal = 0;
            for (i = 0; i < (MAX_NUM_CORES + 1); i++)
                retVal += memSync->slaveDone[i].count;
            break;
        case SYNC_TOKEN_POST_ACCUMULATION_DONE:
            retVal = memSync->numAccumulations;
            break;
        default: 
            break;          
      }
      return (retVal);
}

void  utlWriteSyncValue(tfSyncToken_e token, uint32_t id)
{
    tfuTestSyncVars_t *memSync = testCommonGetSycMemHandle();

      switch (token)    {
        case SYNC_TOKEN_INIT_VAL:
             memSync->masterState.isMasterSysInitDone = SYNC_TOKEN_INIT_VAL;
             break;
        case SYNC_TOKEN_MASTER_SYSINIT_DONE:
             memSync->masterState.isMasterSysInitDone = SYNC_TOKEN_MASTER_SYSINIT_DONE;
          break;
        case SYNC_TOKEN_MASTER_UNITTEST_DONE:
             memSync->masterState.masterTestCompleteCount += 1; /* Increment the test complete count */
             break;
        case SYNC_TOKEN_POST_CONSUMER_DONE:
             memSync->slaveDone[id].count++;
             break;
        case SYNC_TOKEN_POST_ACCUMULATION_DONE:
             memSync->numAccumulations++;
             break;
        default:
                break;
      }

      utlEndMemAccess(memSync, sizeof(tfuTestSyncVars_t));
}

void utlProdWaitUntilConsumersAreDone(void)
{
    uint32_t num_consumers_done;
    
    /* wait till all consumers are done */
    do {
          num_consumers_done = utlReadSyncValue(SYNC_TOKEN_CHECK_CONSUMERS_DONE);
#ifdef __LINUX_USER_SPACE
      task_sleep(10);
#endif        
    } while(num_consumers_done < (uint32_t) tf_num_consumers);

}

void utlWaitUntilMasterTestComplete(uint32_t ref_count)
{
    uint32_t test_complete_count;
    
    /* Wait until master is complete on the test */
    do {
        test_complete_count = (uint32_t)utlReadSyncValue(SYNC_TOKEN_MASTER_UNITTEST_DONE);  

#ifdef __LINUX_USER_SPACE
      task_sleep(10);
#endif
    }while(test_complete_count != ref_count);

}

/* Nothing past this point */
        
        


