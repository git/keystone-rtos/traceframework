/*
 *
 * Copyright (C) 2010-2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/* UIA Producer Consumer Test (local to local).
 * This test creates a single trace framework producer and multiple consumers on a single core
 * for UIA applications
 * a0 is a pointer to the test framework structure
 * a1 is a pointer to the paTest_t structure for this test, as initialized in testMain.
 * 
 * General purpose Queue assignment 
 * 0 - Recycle queue for the data packets
 * 1 - Recycle queue for the request stats request
 * 2 - Destination queue for the request stats reply
 * 
 */
 
#include "tfutest.h"
#ifndef __LINUX_USER_SPACE
#define PRODUCER_FG_PRIORITY        2 /* for the test, set to same priority as the task spawning it */
#define PRODUCER_BG_PRIORITY        2 /* for the test, set to same priority as the task spawning it */
#define PRODUCER_INACTIVE_PRIORITY -1
#if defined (DEVICE_K2E)
#define UTF_CONSUMERS_BLOCKED_DSP_CORE_START 1 /* consumers would not be created starting from this core ID */
#else
#define UTF_CONSUMERS_BLOCKED_DSP_CORE_START 2 /* consumers would not be created starting from this core ID */
#endif /* DEVICE_K2E */

#else
#include "fw_test.h"
#include "fw_mem_allocator.h"
#define UTF_CONSUMERS_BLOCKED_DSP_CORE_START -1 /* Not applicable  */
#endif

#define PRODUCER_CORE_CONTRACT_VER1 0
#define PRODUCER_CORE_CONTRACT_VER2 0
#define UTF_NUM_BUFS_CONSUMED       128
#define UTF_MASTER_COMPLETE_REF_COUNT (SYNC_TOKEN_MASTER_SYSINIT_DONE + 1)

/* Indicate this test is not for ARM only contracts */
int armOnlyContract = 0;

/* NULL terminated The list of tests */
tfTest_t  tfTestList[] = {
	{ tfTestUiaMInstProducerUiaConsumers, 	"DSP UIA Multi Instance Producer DSP/ARM Consumer Test",        UTF_TEST_NOT_RUN },
	{ NULL,                                  NULL,                                           UTF_TEST_NOT_RUN }
};

#define TFU_NUM_TESTS       ((sizeof(tfTestList)/sizeof(tfTest_t)) - 1)

typedef struct test_contracts_s
{
    uint32_t             prodCoreId;
 	tf_contract_HANDLE   contract_handle;
}test_contracts_t;

#ifndef __LINUX_USER_SPACE
static void produce_consume_buffers (unitTestFramework_t* tf, tfTest_t *tft, char* testName, tf_contract_HANDLE contract_handle, uint32_t coreId, uint32_t contract_verId)
 {
    Task_Params          prod_fg_task_params;
	Task_Params          prod_bg_task_params;
    Task_Handle          task_fg_handle;
	Task_Handle          task_bg_handle;
    Task_Stat            tstat;
    uint32_t             active, count = 0;
	int                  consumer_active = 1; /* Assume that consumers are active to start with, since this test creates the consumers first  */
 	tf_producer_HANDLE   producer_handle;

	/* Create the producer */
	producer_handle = testCommonCreateTfProducer(contract_handle, contract_verId, TF_PRODUCER_TYPE_UIA_MINST);

	if (producer_handle == NULL)
	{
		System_printf ("tfTestUiaProducerDspUiaConsumersCVer1:Trap here, error in creating the prodcuer in Master Core \n");
		while (1);
	}

    /* Reset to initial values before the producer test */
	utlResetSyncVals(TRUE);
	testCommonResetTestStatus();
	tf_num_consumers = 0;

	/* Create Producer buffer exchange trigger task */
 	Task_Params_init(&prod_fg_task_params);
 	prod_fg_task_params.arg0 = (UArg) (contract_verId - 1);
 	prod_fg_task_params.arg1 = NULL;
 	prod_fg_task_params.priority = PRODUCER_INACTIVE_PRIORITY;  /* set inactive, wait till other tasks are created */
 	task_fg_handle = Task_create((Task_FuncPtr)&testCommonTrigUiaBufXchgTask, &prod_fg_task_params, NULL);

 	/* Create Producer notify task */
 	Task_Params_init(&prod_bg_task_params);
 	prod_bg_task_params.arg0 = (UArg)producer_handle;
 	prod_bg_task_params.arg1 = NULL;
 	prod_bg_task_params.priority = PRODUCER_INACTIVE_PRIORITY;  	    /* set inactive, yield to consumer on the same core */
 	task_bg_handle = Task_create((Task_FuncPtr)&testCommonNotifyTfConsumersTask, &prod_bg_task_params, NULL);

    /* set the task priorities to active */
    Task_setPri(task_bg_handle, PRODUCER_BG_PRIORITY);
    Task_setPri(task_fg_handle, PRODUCER_FG_PRIORITY);

    /* Wait until all consumers are done consuming buffers */
    active = count = 0;
    do {
    	Task_stat (task_bg_handle, &tstat);
    	if (tstat.mode != Task_Mode_TERMINATED)
    		active |= 1;
    	else
    		active &= ~1;
    	Task_stat (task_fg_handle, &tstat);
        if (tstat.mode != Task_Mode_TERMINATED)
    		active |= 2;
    	else
    		active &= ~2;
		/* Check to see if we consumed everything in the unit test */
    	if (consumer_active) {
			consumer_active = testCommonCheckToDeleteConsumer(tf, contract_handle, UTF_NUM_BUFS_CONSUMED);
    	}
    	count ++;
    	Task_sleep(1);
    } while (active);

    /* post message that consumer is done on the master core */
    utlWriteSyncValue(SYNC_TOKEN_POST_CONSUMER_DONE, coreId);

  	/* delete the all of the local producer tasks */
	/* stop notifying consumers */
   	Task_delete (&task_bg_handle);
	/* Stop buffer exchange triggering */
   	Task_delete (&task_fg_handle);

    /* Delete the local producer */
	tf_prodDestroy(producer_handle);

    /* Print the summary from producer core */
   	testCommonPrintContractSummary(contract_handle, coreId);

	testCommonPublishSummaryFromConsumer(tf, tft, coreId, contract_verId);

	System_printf ("Waiting for %d number of consumers to finish the test \n", tf_num_consumers);
    System_flush();
    utlProdWaitUntilConsumersAreDone();

   	testCommonSummarizeAllTestStatus (tft, testName, tf_num_consumers, UTF_NUM_BUFS_CONSUMED, contract_verId);

    /* Publish prodcuer core (master) is complete */
   	utlWriteSyncValue(SYNC_TOKEN_MASTER_UNITTEST_DONE, coreId);

	return;
 }
#endif

static void consume_buffers(unitTestFramework_t* tf, tfTest_t *tft, tf_contract_HANDLE contract_handle, uint32_t coreId, uint32_t contract_verId, uint32_t test_ref_count )
{
	int        retVal, consumer_active = 1;
	tf_contractGetInfo_t info;

	tf_contractGetInfo(contract_handle, &info);	

	/* Wait on Slave process/cores for the specified number of buffers consumption */
	while (tf->utfConsumerHandle) {

		/* do the consumer process after the intended notification is processed */
		if (tf->utfConsumerHandle)
			retVal = testCommonConsumeBuffers(tf->utfConsumerHandle);
    	/* Trap here if the consumer process did not go through */
		if (retVal)
		{
			System_printf ("Trapped in consume_buffers() service routine due to core id(%d) consumer process API \n", coreId);
			System_flush();
			while (1);
		}

		/* Check to see if we consumed everything in the unit test */
		if (consumer_active)
			consumer_active = testCommonCheckToDeleteConsumer(tf, contract_handle, UTF_NUM_BUFS_CONSUMED);
	} 
#ifdef __LINUX_USER_SPACE
	System_printf ("\n");
#endif
	testCommonPublishSummaryFromConsumer(tf, tft, coreId, contract_verId);

	utlWriteSyncValue(SYNC_TOKEN_POST_CONSUMER_DONE, coreId);

    utlWaitUntilMasterTestComplete(test_ref_count);

}


/* This test case creates one UIA producer with contract version 1 on Core 0 with at least one UIA consumer with 
 * contract version 1 on Core 0 and extended contract version 1 consumers on other cores. This test also performs
 * optional contract version 2 verification when the program is loaded on core 0 and core 1 (DSP).
 */
#ifdef __LINUX_USER_SPACE
void* tfTestUiaMInstProducerUiaConsumers (void* args)
{
    void  *a0 = (void *)((utfTestArgs_t *)args)->tf;
    void  *a1 = (void *)((utfTestArgs_t *)args)->pat;
#else
void tfTestUiaMInstProducerUiaConsumers (UArg a0, UArg a1)
{
#endif
 	unitTestFramework_t  *tf  = (unitTestFramework_t *)a0;
 	tfTest_t      *tft = (tfTest_t *)a1;
 	test_contracts_t     tfContracts[2];
    uint32_t             coreId, contract_verId;
	int                  i;
	
	tft->testStatus = UTF_TEST_PASSED;
  tf->numTests    = TFU_NUM_TESTS;

    /* get the core ID */
#ifdef __LINUX_USER_SPACE
	coreId = MAX_NUM_CORES; /* assuming zero base index is used and lower indexes map to DSP cores */
#else
	coreId = CSL_chipReadDNUM();
#endif

	/* Get a free contract handle for the test case for both versions of contract 
	 * Please go through the system initialization code for trace framework on how to craete
	 * the contracts for both the versions 
	 */
 	tfContracts[0].contract_handle  = testCommonGetTfContractHandle (uintTestTFDspUiaMinstContractV1Name, TF_CONTRACT_VERSION_1);
	tfContracts[0].prodCoreId       = PRODUCER_CORE_CONTRACT_VER1;
 	tfContracts[1].contract_handle  = testCommonGetTfContractHandle (uintTestTFDspUiaMinstContractV2Name, TF_CONTRACT_VERSION_2);
	tfContracts[1].prodCoreId       = PRODUCER_CORE_CONTRACT_VER2;

	
    /* create at least one consumer for the contract (from producer core) before the notification task
     * This is NOT a mandatory requirement for trace framework, it is done due to the unit test
     * notifications demanding no consumers to exit the task */
    for ( i = 0; i < UTF_NUM_CONTRACT_VERSIONS ; i++ ) {
		testCommonCreateTfConsumer(tfContracts[i].contract_handle, tfContracts[i].prodCoreId, UTF_CONSUMER_UIAMINST, UTF_CONSUMERS_BLOCKED_DSP_CORE_START);
		contract_verId = i+1;
		
#ifndef __LINUX_USER_SPACE	
    	if (coreId == tfContracts[i].prodCoreId) 
		{

			System_printf ("\nTesting for Contract Version %d\n", contract_verId);
		    /* produce and consume buffers in producer core */
			produce_consume_buffers(tf, tft, tfTestList[0].name, tfContracts[i].contract_handle, coreId, contract_verId);
		}
		else 
#endif			
		{
			/* Consume the buffers from producer core in other cores */
			consume_buffers(tf, tft, tfContracts[i].contract_handle, coreId, contract_verId,  UTF_MASTER_COMPLETE_REF_COUNT + i);
		}
        /* Reset intermediate variables to run the next test */	
		tf_num_consumers = 0;
		tf->tfNumBufsConsumed = tf->tfNumBufsMissed = 0;
    }

    /* Return when all consumers and procuders are done */
	Task_exit();
	
#ifdef __LINUX_USER_SPACE
	return (void *)0;
#endif

}
/* Nothing past this point */
