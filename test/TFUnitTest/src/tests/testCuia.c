/*
 *
 * Copyright (C) 2010-2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/* UIA Producer Consumer Test (local to local).
 * This test creates a single trace framework producer and multiple consumers on a single core
 * for UIA applications
 * a0 is a pointer to the test framework structure
 * a1 is a pointer to the paTest_t structure for this test, as initialized in testMain.
 * 
 * General purpose Queue assignment 
 * 0 - Recycle queue for the data packets
 * 1 - Recycle queue for the request stats request
 * 2 - Destination queue for the request stats reply
 * 
 */
 
#include "tfutest.h"
#include "fw_test.h"
#include "fw_task.h"
#include "fw_mem_allocator.h"

#define PRODUCER_TASK_CONTRACT_VER2 0
#define UTF_NUM_BUFS_CONSUMED       128
#define UTF_MASTER_COMPLETE_REF_COUNT (SYNC_TOKEN_MASTER_SYSINIT_DONE + 1)

/* Indicate this test is for ARM only contracts */
int armOnlyContract = 1;

/* NULL terminated The list of tests */
tfTest_t  tfTestList[] = {
	{ tfTestcUiaProducercUiaConsumers, 	     "ARM cUIA Producer, ARM Consumer Test",        UTF_TEST_NOT_RUN },
	{ NULL,                                  NULL,                                          UTF_TEST_NOT_RUN }
};
#define TFU_NUM_TESTS       ((sizeof(tfTestList)/sizeof(tfTest_t)) - 1)

typedef struct test_contracts_s
{
    uint32_t             prodTaskId;
 	tf_contract_HANDLE   contract_handle;
}test_contracts_t;

static void produce_consume_buffers (unitTestFramework_t* tf, tfTest_t *tft, char* testName, tf_contract_HANDLE contract_handle, uint32_t coreId, uint32_t contract_verId)
{
 	tf_producer_HANDLE   producer_handle;
	uint32_t             num_consumers;
	int                  loopCnt = 0, consumer_active = 1;
#if 0	
	static char statemap[][50] = {
	 {"TF_PRODUCER_STATE_STREAMING"},
	 {"TF_PRODUCER_STATE_FREE_RUNNING"},
	 {"TF_REQUEST_PRODUCER_STATE_FREEZING"},
	 {"TF_PRODUCER_STATE_FROZEN"},
	 {"TF_PRODUCER_STATE_INVALID"}	 
	};
	uint32_t             statemapIndex = 0;
#endif	
	tf_contractGetInfo_t contractInfo;
	uint32_t             producer_state_change = 0;	
	
	tf_producerState_e   prodState = TF_PRODUCER_STATE_STREAMING, prevProdState = TF_PRODUCER_STATE_INVALID;	
  tf_prodStatus_e      tf_pStatus;

	/* Create the producer */
	producer_handle = testCommonCreateTfProducer(contract_handle, contract_verId, TF_PRODUCER_TYPE_CUIA);

	if (producer_handle == NULL)
	{
		System_printf ("tfTestUiaProducerDspUiaConsumersCVer1:Trap here, error in creating the prodcuer in Master Core \n");
		while (1);
	}

	/* get the information about how many consumers are attached to the contract/producer */
	tf_prodUpdateConsumers(producer_handle);

    /* Reset to initial values before the producer test */
	utlResetSyncVals(TRUE);
	testCommonResetTestStatus();
	tf_num_consumers = 0;

    do {
        /* Run the cuia Logger */
		testCommonTrigCUiaBufXchgTask(&loopCnt);

		/* get the information about how many consumers are attached to the contract/producer */
		num_consumers = tf_prodUpdateConsumers(producer_handle);
		if ( tf_num_consumers < (int)num_consumers) {
			tf_num_consumers = (int) num_consumers; /* Obtain the steady state information about how many consumers got latched during the test */
		}

		/* notify any consumers if present */
		if (num_consumers) {
			tf_prodGetContractHandle(producer_handle, &contract_handle);
			tf_contractGetInfo(contract_handle, &contractInfo);
			//System_printf ("Producer Current index foot print in contract is :%d \n", contractInfo.producer_index);
			prodState =(tf_producerState_e) contractInfo.producer_current_state;
			producer_state_change = (uint32_t)prodState ^ (uint32_t) prevProdState;
			if (producer_state_change) {
#if 0
				if ( (prodState <= TF_PRODUCER_STATE_INVALID) ||
					 (prodState > TF_PRODUCER_STATE_FROZEN) )
					statemapIndex = 4;
				else
					statemapIndex = (uint32_t) prodState;
				System_printf ("Observed Producer State Change to %s after consuming %d number of buffers \n", statemap[statemapIndex], unitTestFramework.tfNumBufsConsumed);
#endif				
				System_flush();
				prevProdState = prodState;
			}
		}
		/* Check to see if we consumed everything in the unit test */
		if (consumer_active) {  
			consumer_active = testCommonCheckToDeleteConsumer(tf, contract_handle, UTF_NUM_BUFS_CONSUMED);		
		}

	} while ( (num_consumers) || !tf_num_consumers); /* Wait for at least one consumer for the produer */

    System_printf ("Local task (%d) consumption is complete ..\n", taskNum);
	System_flush();
    /* post message that consumer is done on the master core */
    utlWriteSyncValue(SYNC_TOKEN_POST_CONSUMER_DONE, coreId);

    /* Delete the local producer */
	  tf_pStatus = tf_prodDestroy(producer_handle);

    if (tf_pStatus != TF_PRODUCER_PROD_SUCCESS)
      System_printf (" Producer Destroy failed \n");

    /* Print the summary from producer core */
   	testCommonPrintContractSummary(contract_handle, coreId);

	  testCommonPublishSummaryFromConsumer(tf, tft, coreId, contract_verId);

    utlProdWaitUntilConsumersAreDone();

   	testCommonSummarizeAllTestStatus (tft, testName, tf_num_consumers, UTF_NUM_BUFS_CONSUMED, contract_verId);

    /* Publish prodcuer core (master) is complete */
   	utlWriteSyncValue(SYNC_TOKEN_MASTER_UNITTEST_DONE, coreId);

	return;
 }

static void consume_buffers(unitTestFramework_t* tf, tfTest_t *tft, tf_contract_HANDLE contract_handle, uint32_t coreId, uint32_t contract_verId, uint32_t test_ref_count )
{
	int        retVal, consumer_active = 1;
	tf_contractGetInfo_t info;

	tf_contractGetInfo(contract_handle, &info);	

	/* Wait on Slave process/cores for the specified number of buffers consumption */
	while (tf->utfConsumerHandle) {

		/* do the consumer process after the intended notification is processed */
		if (tf->utfConsumerHandle)
			retVal = testCommonConsumeBuffers(tf->utfConsumerHandle);
    	/* Trap here if the consumer process did not go through */
		if (retVal)
		{
			System_printf ("Trapped in consume_buffers() service routine due to core id(%d) consumer process API \n", coreId);
			System_flush();
			while (1);
		}

		/* Check to see if we consumed everything in the unit test */
		if (consumer_active)
			consumer_active = testCommonCheckToDeleteConsumer(tf, contract_handle, UTF_NUM_BUFS_CONSUMED);

        task_sleep(1);
	} 
	System_printf ("Consume Buffers Task completed (Task num: %d ) \n", taskNum);
	testCommonPublishSummaryFromConsumer(tf, tft, coreId, contract_verId);
	utlWriteSyncValue(SYNC_TOKEN_POST_CONSUMER_DONE, coreId);
    utlWaitUntilMasterTestComplete(test_ref_count);
}

/* This test case creates one UIA producer with contract version 1 on Core 0 with at least one UIA consumer with 
 * contract version 1 on Core 0 and extended contract version 1 consumers on other cores. This test also performs
 * optional contract version 2 verification when the program is loaded on core 0 and core 1 (DSP).
 */

void* tfTestcUiaProducercUiaConsumers (void* args)
{
    void  *a0 = (void *)((utfTestArgs_t *)args)->tf;
    void  *a1 = (void *)((utfTestArgs_t *)args)->pat;
 	unitTestFramework_t  *tf  = (unitTestFramework_t *)a0;
 	tfTest_t      *tft = (tfTest_t *)a1;
 	test_contracts_t     tfContracts;
#ifdef DEBUG_MULTIPLE_ITERATIONS  
	int                  i;
#endif

	tft->testStatus = UTF_TEST_PASSED;
  tf->numTests    = TFU_NUM_TESTS;

    /* Get a free contract handle for the test case for both versions of contract 
	 * Please go through the system initialization code for trace framework on how to craete
	 * the contracts for both the versions 
	 */
 	tfContracts.contract_handle  = testCommonGetTfContractHandle (unitTestTFArmCUiaContractV2Name, TF_CONTRACT_VERSION_2);
	tfContracts.prodTaskId       = PRODUCER_TASK_CONTRACT_VER2;

  /* create at least one consumer for the contract (from producer core) before the notification task
   * This is NOT a mandatory requirement for trace framework, it is done due to the unit test
   * notifications demanding no consumers to exit the task */
	testCommonCreateTfConsumer(tfContracts.contract_handle, tfContracts.prodTaskId, UTF_CONSUMER_CUIA, -1);
		
  if (taskNum == tfContracts.prodTaskId)	{
	  /* produce and consume buffers in producer core */
		produce_consume_buffers(tf, tft, tfTestList[0].name, tfContracts.contract_handle, taskNum, 1);
  }
	else 	{
	  /* Consume the buffers from producer core in other cores */
	  consume_buffers(tf, tft, tfContracts.contract_handle, taskNum, 1, UTF_MASTER_COMPLETE_REF_COUNT);
  }

  /* Reset intermediate variables to run the next test */	
	tf_num_consumers = 0;
	tf->tfNumBufsConsumed = tf->tfNumBufsMissed = 0;
  return (void *)0;
}

/* Nothing past this point */
