/*
 *
 * Copyright (C) 2010-2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/* UIA Producer Consumer Test (local to local).
 * This test creates a single trace framework producer and multiple consumers on a single core
 * for UIA applications
 * a0 is a pointer to the test framework structure
 * a1 is a pointer to the paTest_t structure for this test, as initialized in testMain.
 * 
 * General purpose Queue assignment 
 * 0 - Recycle queue for the data packets
 * 1 - Recycle queue for the request stats request
 * 2 - Destination queue for the request stats reply
 * 
 */
 
#include "tfutest.h"
#ifndef __LINUX_USER_SPACE
#define PRODUCER_TASK_PRIORITY      2 /* for the test, set to next high priority as the task spawning it */
#define CONSUMER_TASK_PRIORITY      3
#define PRODUCER_INACTIVE_PRIORITY -1
#if defined (DEVICE_K2E)
#define UTF_CONSUMERS_BLOCKED_DSP_CORE_START 1 /* consumers would not be created starting from this core ID */
#else
#define UTF_CONSUMERS_BLOCKED_DSP_CORE_START 2 /* consumers would not be created starting from this core ID */
#endif /* DEVICE_K2E */

#else
#include "fw_test.h"
#include "fw_task.h"
#include "fw_mem_allocator.h"
#define UTF_CONSUMERS_BLOCKED_DSP_CORE_START -1 /* Not applicable  */
#endif
#define PRODUCER_CORE_CONTRACT_VER1 0
#define PRODUCER_CORE_CONTRACT_VER2 0
#define UTF_NUM_BUFS_CONSUMED       20 /* 20 buffers implies at least 20 seconds for the test since each buffer needs 1 second accumulation*/
#define UTF_MASTER_COMPLETE_REF_COUNT (SYNC_TOKEN_MASTER_SYSINIT_DONE + 1)

/* Indicate this test is not for ARM only contracts */
int armOnlyContract = 0;

/* NULL terminated The list of tests */
tfTest_t  tfTestList[] = {
	{ tfTestGenProducerGenConsumers, 	    "DSP General Producer DSP/ARM Consumer Test",        UTF_TEST_NOT_RUN },
	{ NULL,                                  NULL,                                               UTF_TEST_NOT_RUN }
};

#define TFU_NUM_TESTS       ((sizeof(tfTestList)/sizeof(tfTest_t)) - 1)

typedef struct test_contracts_s
{
    uint32_t             prodCoreId;
 	tf_contract_HANDLE   contract_handle;
}test_contracts_t;

#ifndef __LINUX_USER_SPACE
static void produce_consume_buffers (unitTestFramework_t* tf, tfTest_t *tft, char* testName, tf_contract_HANDLE contract_handle, uint32_t coreId, uint32_t contract_verId)
 {
    Task_Params          prod_fg_task_params;
    Task_Handle          task_fg_handle;
    Task_Params          task_params;
    Task_Handle          task_handle;	
    Task_Stat            tstat;
    uint32_t             active, count = 0;
	int                  consumer_active = 1; /* Assume that consumers are active to start with, since this test creates the consumers first  */
 	tf_producer_HANDLE   producer_handle;

	/* Create the producer */
	producer_handle = testCommonCreateTfProducer(contract_handle, contract_verId, TF_PRODUCER_TYPE_GEN);

	if (producer_handle == NULL)
	{
		System_printf ("tfTestUiaProducerDspUiaConsumersCVer1:Trap here, error in creating the prodcuer in Master Core \n");
		while (1);
	}

    /* Reset to initial values before the producer test */
	utlResetSyncVals(TRUE);
	testCommonResetTestStatus();
	tf_num_consumers = 0;

	/* Create Producer buffer exchange trigger task */
 	Task_Params_init(&prod_fg_task_params);
 	prod_fg_task_params.arg0 = (UArg) producer_handle;
 	prod_fg_task_params.arg1 = (UArg) contract_verId;
 	prod_fg_task_params.priority = PRODUCER_TASK_PRIORITY;  
 	task_fg_handle = Task_create((Task_FuncPtr)&testCommonTrigGenProdBufXchgTask, &prod_fg_task_params, NULL);

    /* Create the polling task for consumers for General Producer consumers */
   	 Task_Params_init(&task_params);
   	 task_params.arg0 = NULL;
   	 task_params.arg1 = NULL;
   	 task_params.priority = CONSUMER_TASK_PRIORITY;  /* set to inactive during create  */
   	 task_handle = Task_create((Task_FuncPtr)&testCommonTfAccumConsumerTask, &task_params, NULL);	


    /* Wait until all consumers are done consuming buffers */
    active = count = 0;
    do {
    	Task_stat (task_fg_handle, &tstat);
        if (tstat.mode != Task_Mode_TERMINATED)
    		active |= (uint32_t)0x00000001;
    	else
    		active &= (uint32_t)0xFFFFFFFE;

    	Task_stat (task_handle, &tstat);
        if (tstat.mode != Task_Mode_TERMINATED)
    		active |= (uint32_t)0x00000002;
    	else
    		active &= (uint32_t)0xFFFFFFFD;
		
		/* Check to see if we consumed everything in the unit test */
    	if (consumer_active) {
			consumer_active = testCommonCheckToDeleteConsumer(tf, contract_handle, UTF_NUM_BUFS_CONSUMED);
    	}

		testCommonGenProdAccumulateStatistics(producer_handle, coreId);
        count ++;
		Task_yield();

    } while (active);

    /* post message that consumer is done on the master core */
    utlWriteSyncValue(SYNC_TOKEN_POST_CONSUMER_DONE, coreId);

  	/* delete the all of the local producer tasks */
	/* Stop buffer exchange triggering */
   	Task_delete (&task_fg_handle);

    /* Delete the local producer */
	tf_prodDestroy(producer_handle);

    /* Print the summary from producer core */
   	testCommonPrintContractSummary(contract_handle, coreId);

	testCommonPublishSummaryFromConsumer(tf, tft, coreId, contract_verId);

    utlProdWaitUntilConsumersAreDone();

   	testCommonSummarizeAllTestStatus (tft, testName, tf_num_consumers, UTF_NUM_BUFS_CONSUMED, contract_verId);

    /* Publish prodcuer core (master) is complete */
   	utlWriteSyncValue(SYNC_TOKEN_MASTER_UNITTEST_DONE, coreId);

	return;
 }
#endif

static void consume_buffers(unitTestFramework_t* tf, tfTest_t *tft, tf_contract_HANDLE contract_handle, uint32_t coreId, uint32_t contract_verId, uint32_t test_ref_count )
{
#ifdef __LINUX_USER_SPACE
    task_handle          thandle;
    int                  status;
#else
    Task_Params          tparams;
    Task_Handle          thandle;
    Task_Stat            tstat;
#endif	
	int                  consumer_active = 1; /* Assume that consumers are active to start with, since this test creates the consumers first  */

#ifdef __LINUX_USER_SPACE
	if (status = task_create(testCommonTfAccumConsumerTask, NULL, &thandle)) {
		  printf("ERROR: \"Consume buffers \" task-create failed (%d)\n", status);
		  return;
	}
	/* Wait on Slave process/cores for the specified number of buffers consumption */
	do {
		/* Check to see if we consumed everything in the unit test */
    	if (consumer_active) {
			consumer_active = testCommonCheckToDeleteConsumer(tf, contract_handle, UTF_NUM_BUFS_CONSUMED);
    	}
	} while (tf->utfConsumerHandle);
	
    task_wait(&thandle);	
#else
    /* Create the polling task for consumers for General Producer consumers */
   	 Task_Params_init(&tparams);
   	 tparams.arg0 = NULL;
   	 tparams.arg1 = NULL;
   	 tparams.priority = CONSUMER_TASK_PRIORITY;  /* set to inactive during create  */
   	 thandle = Task_create((Task_FuncPtr)&testCommonTfAccumConsumerTask, &tparams, NULL);

	/* Wait on Slave process/cores for the specified number of buffers consumption */
	do {
		/* Delete the consumer task once done */
    	Task_stat (thandle, &tstat);
        if (tstat.mode == Task_Mode_TERMINATED)
		   	Task_delete (&thandle);
		
		/* Check to see if we consumed everything in the unit test */
    	if (consumer_active) {
			consumer_active = testCommonCheckToDeleteConsumer(tf, contract_handle, UTF_NUM_BUFS_CONSUMED);
    	}

   		/* Yield to other task until they are complete */
		Task_yield();
	} while (tf->utfConsumerHandle);

	/* Delete the consumer task once done */
	Task_stat (thandle, &tstat);
    if (tstat.mode == Task_Mode_TERMINATED)
	   	Task_delete (&thandle);
#endif
	testCommonPublishSummaryFromConsumer(tf, tft, coreId, contract_verId);

	utlWriteSyncValue(SYNC_TOKEN_POST_CONSUMER_DONE, coreId);

    utlWaitUntilMasterTestComplete(test_ref_count);

}


/* This test case creates one UIA producer with contract version 1 on Core 0 with at least one UIA consumer with 
 * contract version 1 on Core 0 and extended contract version 1 consumers on other cores. This test also performs
 * optional contract version 2 verification when the program is loaded on core 0 and core 1 (DSP).
 */

#ifdef __LINUX_USER_SPACE
void* tfTestGenProducerGenConsumers      (void *args)
{
    void  *a0 = (void *)((utfTestArgs_t *)args)->tf;
    void  *a1 = (void *)((utfTestArgs_t *)args)->pat;
#else
void tfTestGenProducerGenConsumers (UArg a0, UArg a1)
{
#endif
 	unitTestFramework_t  *tf  = (unitTestFramework_t *)a0;
 	tfTest_t      *tft = (tfTest_t *)a1;	
 	test_contracts_t     tfContracts[UTF_NUM_CONTRACT_VERSIONS];
    uint32_t             coreId, contract_verId;
	int                  i;
	
	tft->testStatus = UTF_TEST_PASSED;
  tf->numTests    = TFU_NUM_TESTS;
	tf->tfGeneralProducerTest = 1;

    /* get the core ID */
#ifdef __LINUX_USER_SPACE
	coreId = MAX_NUM_CORES; /* assuming zero base index is used and lower indexes map to DSP cores */
#else
	coreId = CSL_chipReadDNUM();

	/* Set up the ring buffers used in the contracts for the general producer to have the UIA header information, so that
	 * when streamed over ethernet System Analyzer can view it */
	testCommonInitGenProdRingBuffers(&memBufRing[UTF_CONTRACT_VERSION_1][0],
			                          UTF_GENPROD_BUF_SIZE,
			                          UTF_NUM_GENPROD_RINGBUFS,
			                          sizeof(utfGenProdPayload_t),
			                          uintTestTFDspGenContractV1Name,
			                          UTF_CONTRACT_VERSION_1);

	testCommonInitGenProdRingBuffers(&memBufRing[UTF_CONTRACT_VERSION_2][0],
			                          UTF_GENPROD_BUF_SIZE,
			                          UTF_NUM_GENPROD_RINGBUFS,
			                          sizeof(utfGenProdPayload_t),
			                          uintTestTFDspGenContractV2Name,
			                          UTF_CONTRACT_VERSION_2);

#endif

	/* Get a free contract handle for the test case for both versions of contract 
	 * Please go through the system initialization code for trace framework on how to craete
	 * the contracts for both the versions 
	 */
 	tfContracts[0].contract_handle  = testCommonGetTfContractHandle (uintTestTFDspGenContractV2Name, TF_CONTRACT_VERSION_2);
	tfContracts[0].prodCoreId       = PRODUCER_CORE_CONTRACT_VER2;
#if 0	
 	tfContracts[1].contract_handle  = testCommonGetTfContractHandle (uintTestTFDspGenContractV2Name, TF_CONTRACT_VERSION_2);
	tfContracts[1].prodCoreId       = PRODUCER_CORE_CONTRACT_VER2;
#endif
    /* create at least one consumer for the contract (from producer core) before the notification task
     * This is NOT a mandatory requirement for trace framework, it is done due to the unit test
     * notifications demanding no consumers to exit the task */
    for ( i = 0; i < 1 ; i++ ) {
		testCommonCreateTfConsumer(tfContracts[i].contract_handle, tfContracts[i].prodCoreId, UTF_CONSUMER_GENERALPROD, UTF_CONSUMERS_BLOCKED_DSP_CORE_START);
		contract_verId = i+1;
		
#ifndef __LINUX_USER_SPACE	
    	if (coreId == tfContracts[i].prodCoreId) 
		{

			System_printf ("\nTesting for Contract Version %d\n", contract_verId);
		    /* produce and consume buffers in producer core */
			produce_consume_buffers(tf, tft, tfTestList[0].name, tfContracts[i].contract_handle, coreId, contract_verId);
		}
		else 
#endif			
		{
			/* Consume the buffers from producer core in other cores */
			consume_buffers(tf, tft, tfContracts[i].contract_handle, coreId, contract_verId,  UTF_MASTER_COMPLETE_REF_COUNT + i);
		}
        /* Reset intermediate variables to run the next test */	
		tf_num_consumers = 0;
		tf->tfNumBufsConsumed = tf->tfNumBufsMissed = 0;
    }
	
	tf->tfGeneralProducerTest = 0; /* reset it back after completion */
    /* Return when all consumers and procuders are done */
	Task_exit();
	
#ifdef __LINUX_USER_SPACE
		return (void *)0;
#endif
}
/* Nothing past this point */
