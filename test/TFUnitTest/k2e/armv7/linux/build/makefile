#
# Macro definitions referenced below
#

empty =
space =$(empty) $(empty)

# Dependent package install location
CUIA_INSTALL_DIR ?= $HOME/cuia_1_00_00_13

ARMV7OBJDIR ?= ./obj/$(DEVICE)
ARMV7BINDIR ?= ./bin/$(DEVICE)
CUIA_LIB_DIR ?= $(CUIA_INSTALL_DIR)/lib
TFW_LIB_DIR ?= $(ARMV7LIBDIR)

ARMV7OBJDIR := $(ARMV7OBJDIR)/tfw/test
ARMV7BINDIR := $(ARMV7BINDIR)/tfw/test

#by default do not echo
REPORT?=@

#Cross tools
ifdef CROSS_TOOL_INSTALL_PATH
# Support backwards compatibility with KeyStone1 approach
 CC = $(CROSS_TOOL_INSTALL_PATH)/$(CROSS_TOOL_PRFX)gcc
 AC = $(CROSS_TOOL_INSTALL_PATH)/$(CROSS_TOOL_PRFX)as
 AR = $(CROSS_TOOL_INSTALL_PATH)/$(CROSS_TOOL_PRFX)ar
 LD = $(CROSS_TOOL_INSTALL_PATH)/$(CROSS_TOOL_PRFX)gcc
endif

# INCLUDE/Source Directories
TFW_INC_DIR ?= $(PDK_INSTALL_PATH)/ti/instrumentation/traceframework
TFW_SRC_DIR ?= $(PDK_INSTALL_PATH)/ti/instrumentation/traceframework
TFW_TEST_DIR = $(TFW_SRC_DIR)/test/TFUnitTest
TFW_ARM_LIN_TEST_DIR = $(TFW_TEST_DIR)/src/armv7/linux

INCDIR := $(TFW_INC_DIR);$(PDK_INSTALL_PATH);$(CUIA_INSTALL_DIR)/packages;$(CUIA_INSTALL_DIR)/sources;$(TFW_TEST_DIR)/src;$(TFW_ARM_LIN_TEST_DIR)

# Libraries
TFW_LIB = -ltraceframework
CUIA_LIBS = -lLoggerStreamer -lLogSnapshot

ifeq ($(USEDYNAMIC_LIB), yes)
#presuming ARM executable would depend on dynamic library dependency
EXE_EXTN = _so
LIBS     = $(TFW_LIB) $(CUIA_LIBS)
else
#forcing ARM executable to depend on static LLD libraries
EXE_EXTN =
LIBS     = -Wl,-Bstatic $(TFW_LIB) $(CUIA_LIBS) -Wl,-Bdynamic
endif

# Compiler options
INTERNALDEFS = $(DEBUG_FLAG) -D__ARMv7 -DDEVICE_K2E -D_VIRTUAL_ADDR_SUPPORT -D__LINUX_USER_SPACE -D_LITTLE_ENDIAN=1 -DMAKEFILE_BUILD
INTERNALDEFS +=-D_GNU_SOURCE -DARCH_$(ARCH) -Dxdc_target_name__=GCArmv5T -Dxdc_target_types__=gnu/targets/arm/std.h

# Linker options
INTERNALLINKDEFS = -Wl,--start-group -L $(CUIA_LIB_DIR) -L $(TFW_LIB_DIR) $(LIBS) -lrt -Wl,--end-group -pthread $(LDFLAGS)

TFW_UIACONSUMER_TEST_EXE=tfwUiaArmConsumer$(EXE_EXTN).out
TFW_UIAMINSTCONSUMER_TEST_EXE=tfwUiaMinstArmConsumer$(EXE_EXTN).out
TFW_GENCONSUMER_TEST_EXE=tfwGenArmConsumer$(EXE_EXTN).out
TFW_CUIAPRODCONSUMER_TEST_EXE=tfwcUiaProdConsumers$(EXE_EXTN).out

OBJEXT = o 

SRCDIR = $(TFW_TEST_DIR)/src:$(TFW_ARM_LIN_TEST_DIR):$(TFW_TEST_DIR)/src/common:$(TFW_TEST_DIR)/src/tests:$(CUIA_INSTALL_DIR)/packages:$(CUIA_INSTALL_DIR)/sources

INCS = -I. -I$(strip $(subst ;, -I,$(INCDIR)))

VPATH=$(SRCDIR)

#List the Source Files
FW_TEST_SRC = \
    fw_main.c \
    fw_task.c \
    fw_init.c \
    fw_osal.c \
    fw_eth_send.c \
    fw_shm.c \
    fw_mem_allocator.c \
    common.c \
    testutil.c 	

TFW_UIACONSUMER_TEST_SRC = \
    testUia.c

TFW_UIAMINSTCONSUMER_TEST_SRC = \
    testUiaMInst.c

TFW_GENCONSUMER_TEST_SRC = \
    testGen.c

TFW_CUIAPRODCONSUMER_TEST_SRC = \
	testCuia.c

# FLAGS for the SourceFiles
SRC_CFLAGS = -I. $(CFLAGS)

# Make Rule for the SRC Files
FW_TEST_SRC_OBJS = $(patsubst %.c, $(ARMV7OBJDIR)/%.$(OBJEXT), $(FW_TEST_SRC))
TFW_UIACONSUMER_TEST_SRC_OBJS = $(patsubst %.c, $(ARMV7OBJDIR)/%.$(OBJEXT), $(TFW_UIACONSUMER_TEST_SRC))
TFW_UIAMINSTCONSUMER_TEST_SRC_OBJS = $(patsubst %.c, $(ARMV7OBJDIR)/%.$(OBJEXT), $(TFW_UIAMINSTCONSUMER_TEST_SRC))
TFW_GENCONSUMER_TEST_SRC_OBJS = $(patsubst %.c, $(ARMV7OBJDIR)/%.$(OBJEXT), $(TFW_GENCONSUMER_TEST_SRC))
TFW_CUIAPRODCONSUMER_TEST_SRC_OBJS = $(patsubst %.c, $(ARMV7OBJDIR)/%.$(OBJEXT), $(TFW_CUIAPRODCONSUMER_TEST_SRC))

all:$(ARMV7BINDIR)/$(TFW_UIACONSUMER_TEST_EXE) $(ARMV7BINDIR)/$(TFW_UIAMINSTCONSUMER_TEST_EXE) $(ARMV7BINDIR)/$(TFW_CUIAPRODCONSUMER_TEST_EXE) 

$(ARMV7BINDIR)/$(TFW_UIACONSUMER_TEST_EXE): $(TFW_UIACONSUMER_TEST_SRC_OBJS) $(FW_TEST_SRC_OBJS) $(ARMV7BINDIR)/.created
	@echo linking $(TFW_UIACONSUMER_TEST_SRC_OBJS) $(FW_TEST_SRC_OBJS) into $@ ...
	$(REPORT)$(CC) $(TFW_UIACONSUMER_TEST_SRC_OBJS) $(FW_TEST_SRC_OBJS) $(INTERNALLINKDEFS) -o $@

$(ARMV7BINDIR)/$(TFW_UIAMINSTCONSUMER_TEST_EXE): $(TFW_UIAMINSTCONSUMER_TEST_SRC_OBJS) $(FW_TEST_SRC_OBJS) $(ARMV7BINDIR)/.created
	@echo linking $(TFW_UIAMINSTCONSUMER_TEST_SRC_OBJS) $(FW_TEST_SRC_OBJS) into $@ ...
	$(REPORT)$(CC) $(TFW_UIAMINSTCONSUMER_TEST_SRC_OBJS) $(FW_TEST_SRC_OBJS) $(INTERNALLINKDEFS) -o $@

$(ARMV7BINDIR)/$(TFW_GENCONSUMER_TEST_EXE): $(TFW_GENCONSUMER_TEST_SRC_OBJS) $(FW_TEST_SRC_OBJS) $(ARMV7BINDIR)/.created
	@echo linking $(TFW_GENCONSUMER_TEST_SRC_OBJS) $(FW_TEST_SRC_OBJS) into $@ ...
	$(REPORT)$(CC) $(TFW_GENCONSUMER_TEST_SRC_OBJS) $(FW_TEST_SRC_OBJS) $(INTERNALLINKDEFS) -o $@

$(ARMV7BINDIR)/$(TFW_CUIAPRODCONSUMER_TEST_EXE): $(TFW_CUIAPRODCONSUMER_TEST_SRC_OBJS) $(FW_TEST_SRC_OBJS) $(ARMV7BINDIR)/.created
	@echo linking $(TFW_CUIAPRODCONSUMER_TEST_SRC_OBJS) $(FW_TEST_SRC_OBJS) into $@ ...
	$(REPORT)$(CC) $(TFW_CUIAPRODCONSUMER_TEST_SRC_OBJS) $(FW_TEST_SRC_OBJS) $(INTERNALLINKDEFS) -o $@
	
$(ARMV7OBJDIR)/%.$(OBJEXT): %.c $(ARMV7OBJDIR)/.created
	@echo compiling $< ...
	$(REPORT)$(CC) -c $(SRC_CFLAGS) $(INTERNALDEFS) $(INCS)  $< -o $@

$(ARMV7OBJDIR)/.created:
	@mkdir -p $(ARMV7OBJDIR)
	@touch $(ARMV7OBJDIR)/.created

$(ARMV7BINDIR)/.created:
	@mkdir -p $(ARMV7BINDIR)
	@touch $(ARMV7BINDIR)/.created

clean:
	@rm -fr $(ARMV7OBJDIR)
	@rm -fr $(ARMV7BINDIR)

