/*
 *
 * Copyright (C) 2010-2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/



/* Generate and verify the system test framework
 * 
 * The test framework consists of the pa driver instance, a cppi/cdma/qm configuration,
 * memory for packet transmission and reception, and semaphores that are used
 * for every test in the PA unit test.
 * 
 */

#include "uTestCpsw.h"
/*
 * Default test configuration for the silicon
 *
 * To run test at the CCS simulator
 *    cpswSimTest = 1
 *    cpswLpbkMode = CPSW_LOOPBACK_INTERNAL
 */
#ifdef  SIMULATOR_SUPPORT
int cpswSimTest = 1;
int cpswLpbkMode = CPSW_LOOPBACK_EXTERNAL;
#else
int cpswSimTest = 0;
int cpswLpbkMode = CPSW_LOOPBACK_NONE;
#endif
int cpswEvm6678 = 0;

/** ============================================================================
 *   @n@b initSGMIISerdes
 *
 *   @b Description
 *   @n This API sets up the configuration for the SGMII SERDES. Assumes a 125 MHz
 *       reference clock.
 *
 *   @param[in]  
 *   @n None
 * 
 *   @return
 *   @n None
 * =============================================================================
 */
static int32_t initSGMIISerdes(void)
{
    if (cpswSimTest)
    {
    
	    /* Unlock the chip configuration registers to allow SGMII SERDES registers to
	    * be written */
	    CSL_BootCfgUnlockKicker();
    
        CSL_BootCfgSetSGMIIConfigPLL (0x00000041);
        CSL_BootCfgSetSGMIIRxConfig (0, 0x00700621);
        CSL_BootCfgSetSGMIITxConfig (0, 0x000108A1);
        CSL_BootCfgSetSGMIIRxConfig (1, 0x00700621);
        CSL_BootCfgSetSGMIITxConfig (1, 0x000108A1);        
       
    }

    /* SGMII SERDES Configuration complete. Return. */
    return 0;
}

/** ============================================================================
 *   @n@b initSGMII
 *
 *   @b Description
 *   @n SGMII peripheral initialization code.
 *
 *   @param[in]
 *   @n macPortNum      MAC port number for which the SGMII port setup must
 *                      be performed.
 *
 *   @return
 *   @n None
 * =============================================================================
 */
static int32_t initSGMII (uint32_t macPortNum)
{
  
    CSL_SGMII_ADVABILITY    sgmiiCfg;
	CSL_SGMII_STATUS        sgmiiStatus;
    
    if ((macPortNum == 0) && (cpswEvm6678))
    {
        /* EVM6678 back end: MAC-to-MAC force link */

        /* Reset the port before configuring it */
        CSL_SGMII_doSoftReset (macPortNum);        
        while (CSL_SGMII_getSoftResetStatus (macPortNum) != 0);   

        /* Hold the port in soft reset and set up
        * the SGMII control register:
        *      (1) Enable Master Mode (default)
        */
        CSL_SGMII_startRxTxSoftReset (macPortNum);  
        CSL_SGMII_enableMasterMode (macPortNum);
        if (cpswLpbkMode != CPSW_LOOPBACK_NONE)
        {
	        CSL_SGMII_enableLoopback (macPortNum);
        }    
        
	    /* Setup the Advertised Ability register for this port:
        *      (1) Enable Full duplex mode
        *      (2) Speed = 1000M
        *      (3) Force the Link
        */
        sgmiiCfg.bLinkUp        =   1;
        sgmiiCfg.linkSpeed      =   CSL_SGMII_1000_MBPS;
        sgmiiCfg.duplexMode     =   CSL_SGMII_FULL_DUPLEX;
        CSL_SGMII_setAdvAbility (macPortNum, &sgmiiCfg);
        
        CSL_SGMII_endRxTxSoftReset (macPortNum);   
    
	    /* Wait for SGMII Link */
	    do
	    {
	        CSL_SGMII_getStatus(macPortNum, &sgmiiStatus);
	    } while (sgmiiStatus.bIsLinkUp != 1);
    }
    else 
    {
        /* Reset the port before configuring it */
        CSL_SGMII_doSoftReset (macPortNum);        
        while (CSL_SGMII_getSoftResetStatus (macPortNum) != 0);   

        /* Hold the port in soft reset and set up
        * the SGMII control register:
        *      (1) Enable Master Mode (default)
        *      (2) Enable Auto-negotiation
        */
        CSL_SGMII_startRxTxSoftReset (macPortNum);  
        if (cpswLpbkMode == CPSW_LOOPBACK_NONE)
        {      
            CSL_SGMII_disableMasterMode (macPortNum);
        }
        else
        {
            CSL_SGMII_enableMasterMode (macPortNum);
        
            if (cpswLpbkMode == CPSW_LOOPBACK_INTERNAL)
            {
	            CSL_SGMII_enableLoopback (macPortNum);
            }    
        }
    
	    /* Setup the Advertised Ability register for this port:
        *      (1) Enable Full duplex mode
        *      (2) Enable Auto Negotiation
        */
        sgmiiCfg.linkSpeed      =   CSL_SGMII_1000_MBPS;
        sgmiiCfg.duplexMode     =   CSL_SGMII_FULL_DUPLEX;
        CSL_SGMII_setAdvAbility (macPortNum, &sgmiiCfg);
    
        CSL_SGMII_enableAutoNegotiation (macPortNum);
        CSL_SGMII_endRxTxSoftReset (macPortNum);   
    
	    /* Wait for SGMII Link */
        if (!cpswSimTest && ((cpswLpbkMode == CPSW_LOOPBACK_EXTERNAL) || (cpswLpbkMode == CPSW_LOOPBACK_NONE))) 
        {
	        do
	        {
	            CSL_SGMII_getStatus(macPortNum, &sgmiiStatus);
	        } while (sgmiiStatus.bIsLinkUp != 1);
	
	        /* Wait for SGMII Autonegotiation to complete without error */
	        do
	        {
	            CSL_SGMII_getStatus(macPortNum, &sgmiiStatus);
	            if (sgmiiStatus.bIsAutoNegError != 0)
	                return -1;
	        } while (sgmiiStatus.bIsAutoNegComplete != 1);
        }
    }

    /* All done with configuration. Return Now. */
    return 0;
}

/** ============================================================================
 *   @n@b initMAC
 *
 *   @b Description
 *   @n This API initializes the CPGMAC Sliver (MAC Port) port.
 *
 *   @param[in]
 *   @n macPortNum      MAC port number for which the initialization must be done.
 *
 *   @param[in]
 *   @n macAddress      MAC address to configure on this port.
 *
 *   @param[in]
 *   @n mtu             Maximum Frame length to configure on this port.
 *
 *   @return
 *   @n None
 * =============================================================================
 */
static int initMAC (uint32_t macPortNum, uint8_t macAddress[6], uint32_t mtu)
{

    /* Reset MAC Sliver 0 */            
    CSL_CPGMAC_SL_resetMac (macPortNum);
    while (CSL_CPGMAC_SL_isMACResetDone (macPortNum) != TRUE);

    /* Setup the MAC Control Register for this port:
     *      (1) Enable Full duplex
     *      (2) Enable GMII
     *      (3) Enable Gigabit 
     *      (4) Enable External Configuration. This enables 
     *          the "Full duplex" and "Gigabit" settings to be
     *          controlled externally from SGMII
     *      (5) Don't enable any control/error/short frames
     */
    CSL_CPGMAC_SL_enableFullDuplex (macPortNum);
    CSL_CPGMAC_SL_enableGMII (macPortNum);
    CSL_CPGMAC_SL_enableGigabit (macPortNum);
    CSL_CPGMAC_SL_enableExtControl (macPortNum);

    /* Configure the MAC address for this port */
    CSL_CPSW_3GF_setPortMACAddress (macPortNum, macAddress);

    /* Configure VLAN ID/CFI/Priority.
     *
     * For now, we are not using VLANs so just configure them
     * to all zeros.
     */
    CSL_CPSW_3GF_setPortVlanReg (macPortNum, 0, 0, 0);

    /* Configure the Receive Maximum length on this port,
     * i.e., the maximum size the port can receive without
     * any errors.
     *
     * Set the Rx Max length to the MTU configured for the
     * interface.
     */
    CSL_CPGMAC_SL_setRxMaxLen (macPortNum, mtu);  

    /* Done setting up the MAC port */
    return 0;
}

/** ============================================================================
 *   @n@b initMDIO
 *
 *   @b Description
 *   @n Not supported at moment. MDIO is not simulated yet.
 *
 *   @param[in]
 *   @n None
 *
 *   @return
 *   @n None
 * =============================================================================
 */
static void initMDIO (void)
{
    /* Return success. */
    return;
}
/** ============================================================================
 *   @n@b switch_update_addr
 *
 *   @b Description
 *   @n This API add/delete entries in the Address Lookup Engine (ALE) in "Switch" mode.
 *
 *   @param[in]
 *   @n portNum         Switch port number.

 *   @param[in]
 *   @n macAddress      MAC address to configure on the switch.
 *
 *   @param[in]
 *   @n add             0:add; 1:delete.
 *
 *   @return
 *   @n None
 *
 *   @Note  It supports "add" operation only now.
 * =============================================================================
 */
static int switch_update_addr (uint32_t portNum, uint8_t macAddress[6], uint16_t add)
{
    int                                 i;
    CSL_CPSW_3GF_ALE_PORTCONTROL        alePortControlCfg;
    CSL_CPSW_3GF_ALE_UNICASTADDR_ENTRY  ucastAddrCfg;


    /* Configure the address in "Learning"/"Forward" state */
    alePortControlCfg.portState             =   ALE_PORTSTATE_FORWARD;
    alePortControlCfg.dropUntaggedEnable    =   0;
    alePortControlCfg.vidIngressCheckEnable =   0;
    alePortControlCfg.noLearnModeEnable     =   (cpswLpbkMode != CPSW_LOOPBACK_NONE)?1:0;
    alePortControlCfg.mcastLimit            =   0;
    alePortControlCfg.bcastLimit            =   0;

    CSL_CPSW_3GF_setAlePortControlReg (portNum, &alePortControlCfg);

    /*
     * The following code is required for device simulator only.
     * It is also served as an example of adding MAC address to the ALE table manually
     */

    if (cpswSimTest)
    {
        /* Program the ALE with the MAC address.
        *
        * The ALE entries determine the switch port to which any
        * matching received packet must be forwarded to.
        */
        /* Get the next free ALE entry to program */
        for (i = 0; i < CSL_CPSW_3GF_NUMALE_ENTRIES; i++)
        {
            if (CSL_CPSW_3GF_getALEEntryType (i) == ALE_ENTRYTYPE_FREE)
            {
                /* Found a free entry */
                break;
            }
        }
        if (i == CSL_CPSW_3GF_NUMALE_ENTRIES)
        {
            /* No free ALE entry found. return error. */
            return -1;
        }
        else
        {
            /* Found a free ALE entry to program our MAC address */
            memcpy (ucastAddrCfg.macAddress, macAddress, 6);    // Set the MAC address
            ucastAddrCfg.ucastType      =      ALE_UCASTTYPE_UCAST_NOAGE;   // Add a permanent unicast address entryALE_UCASTTYPE_UCAST_NOAGE.
            ucastAddrCfg.secureEnable   =      FALSE;
            ucastAddrCfg.blockEnable    =      FALSE;
            ucastAddrCfg.portNumber     =      portNum;   // Add the ALE entry for this port

            /* Setup the ALE entry for this port's MAC address */
            CSL_CPSW_3GF_setAleUnicastAddrEntry (i, &ucastAddrCfg);
        }
    }

    /* Done with upading address */
    return 0;
}

/** ============================================================================
 *   @n@b initSwitch
 *
 *   @b Description
 *   @n This API sets up the ethernet switch subsystem and its Address Lookup
 *      Engine (ALE) in "Switch" mode.
 *
 *   @param[in]
 *   @n mtu             Maximum Frame length to configure on the switch.
 *
 *   @return
 *   @n None
 * =============================================================================
 */
static void initSwitch (uint32_t mtu)
{
    CSL_CPSW_3GF_PORTSTAT               portStatCfg;

    /* Enable the CPPI port, i.e., port 0 that does all
     * the data streaming in/out of EMAC.
     */
    CSL_CPSW_3GF_enablePort0 ();
    CSL_CPSW_3GF_disableVlanAware ();
    CSL_CPSW_3GF_setPort0VlanReg (0, 0, 0);
    CSL_CPSW_3GF_setPort0RxMaxLen (mtu);

    /* Enable statistics on both the port groups:
     *
     * MAC Sliver ports -   Port 1, Port 2
     * CPPI Port        -   Port 0
     */
    portStatCfg.p0AStatEnable   =   1;
    portStatCfg.p0BStatEnable   =   1;
    portStatCfg.p1StatEnable    =   1;
    portStatCfg.p2StatEnable    =   1;
    CSL_CPSW_3GF_setPortStatsEnableReg (&portStatCfg);

    /* Setup the Address Lookup Engine (ALE) Configuration:
     *      (1) Enable ALE.
     *      (2) Clear stale ALE entries.
     *      (3) Disable VLAN Aware lookups in ALE since
     *          we are not using VLANs by default.
     *      (4) No Flow control
     *      (5) Configure the Unknown VLAN processing
     *          properties for the switch, i.e., which
     *          ports to send the packets to.
     */
    CSL_CPSW_3GF_enableAle ();
    CSL_CPSW_3GF_clearAleTable ();

    CSL_CPSW_3GF_disableAleVlanAware ();
    CSL_CPSW_3GF_disableAleTxRateLimit ();
    CSL_CPSW_3GF_setAlePrescaleReg (125000000u/1000u);
    CSL_CPSW_3GF_setAleUnkownVlanReg (7, 3, 3, 7);

    if(cpswLpbkMode != CPSW_LOOPBACK_NONE)
        CSL_CPSW_3GF_enableAleBypass();

    /* Done with switch configuration */
    return;
}

/* Setup the cpsw used in the example */
int initCpsw (void)
{
  uint32_t  macPortNum, mtuSize = 1518, portNum;
  uint8_t   macSrcAddress [2][6] =  {{0x10, 0x11, 0x12, 0x13, 0x14, 0x15},
                                     {0x20, 0x21, 0x22, 0x23, 0x24, 0x25}};
  uint8_t   macAddress [] [6] = {{0x00, 0x01, 0x02, 0x03, 0x04, 0x05},  /* MAC address for (CPPI) Port 0 */
                                {0x10, 0x11, 0x12, 0x13, 0x14, 0x15},   /* MAC address for (EMAC1) Port 1 */
                                {0x20, 0x21, 0x22, 0x23, 0x24, 0x25}};  /* MAC address for (EMAC2) Port 2 */

  initSGMIISerdes();

  /* Initialize the SGMII/Sliver submodules for the
   * two corresponding MAC ports.
   */
  for (macPortNum = 1; macPortNum < UTF_NUM_MAC_PORTS; macPortNum++)
  {
	  if (initSGMII (macPortNum))
		return -1;
	  initMAC (macPortNum, &macSrcAddress[macPortNum][0], mtuSize);
  }

  /* Setup the Phys by initializing the MDIO */
  initMDIO(); 

  /* Setup the Ethernet switch finally. */
  initSwitch (mtuSize);
  
  if(cpswLpbkMode == CPSW_LOOPBACK_NONE)
	  switch_update_addr(0, macAddress[0], 0);
  else
	  switch_update_addr(0, macAddress[1], 0);
	  
  for (portNum = 1; portNum < UTF_NUM_PORTS; portNum++)
	  switch_update_addr(portNum, macAddress[portNum], 0);

  return (0);
  
}
/* Nothing past this point */
