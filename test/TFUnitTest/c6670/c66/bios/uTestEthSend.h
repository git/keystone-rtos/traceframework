#ifndef _TFW_UTEST_C6670_ETHSEND
#define _TFW_UTEST_C6670_ETHSEND

#ifdef __cplusplus
extern "C" {
#endif

/* ============================================================= */
/**
 *   @file  uTestEthSend.h
 *
 *   @brief  Ethernet Send function definitions
 *
 *  ============================================================================
 *  Copyright (c) Texas Instruments Incorporated 2009-2013
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include <ti/transport/bmet_eth/bmet_transport.h>
#include <ti/sysbios/family/c64p/Hwi.h>

#include <ti/csl/csl_cacheAux.h>
#include <ti/csl/csl_xmcAux.h>
#include "uTestCpsw.h"

/* allocated descriptors for BMET transport */
#define UTF_NUM_BMET_DESC         128

/* Ethernet send port */
#define        UTF_ETH_SEND_PORT                1u

/* ethernet send handle */
typedef void* utfEth_HANDLE;

#define utfEth_MAC_ADDR_SIZE       6 
typedef unsigned char utfEthMacAddr_t[utfEth_MAC_ADDR_SIZE];

#define utfEth_IPV4_ADDR_SIZE      4
typedef unsigned char utfEthIpv4Addr_t[utfEth_IPV4_ADDR_SIZE];

typedef struct utfEthConfig_s
{
	utfEthMacAddr_t *dstMac;
	utfEthMacAddr_t *srcMac;
	utfEthIpv4Addr_t *dstIp;
	utfEthIpv4Addr_t *srcIp;
	uint32_t remoteUdpPortNum;
	uint32_t constPayLoadSize;
	Qmss_QueueHnd QfreeDesc;		      				/* System Descriptor */
	uint32_t      desc_size;                           /* system descriptor size */
} utfEthConfig_t;

/* Ethernet Send Initialization */
utfEth_HANDLE utf_GetEthSendHandle(utfEthConfig_t *utfEthConfig);
int32_t       utf_EthSend(utfEth_HANDLE handle, uint8_t *buffer, uint32_t size, int32_t param);
int32_t       utf_EthDestroy(utfEth_HANDLE ethHandle);

#ifdef __cplusplus
}
#endif
  

#endif  /* _TFW_UTEST_C6670_ETHSEND */
