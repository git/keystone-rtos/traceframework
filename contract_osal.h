/**
 *   @file  contract_osal.h
 *
 *   @brief   
 *      This is a very slim consumer framework library implementation.
 *
 *  ============================================================================
 *  @n   (C) Copyright 2012, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
 
#ifndef __CONTRACTOSAL_H__
#define __CONTRACTOSAL_H__
#include <ti/instrumentation/traceframework/tf_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/** @addtogroup TRACEFRAMEWORK_OSAL
@{ 
*/

/**********************************************************************
 ************************* Extern Declarations ************************
 **********************************************************************/
extern  void Osal_contract_EndMemAccess (void* addr, uint32_t sizeWords);
extern  void Osal_contract_BeginMemAccess (void* addr, uint32_t sizeWords);
extern  void Osal_contract_Enter (void);
extern  void Osal_contract_Exit (void);
extern void  Osal_contract_MemFree(void* ptr, uint32_t size);
extern void* Osal_contract_MemAlloc(uint32_t num_bytes, uint32_t alignment);
extern void* Osal_contract_mmap(void* phy_addr, uint32_t size);
extern void  Osal_contract_unmap(void* virt_addr, uint32_t size);


/**
 * @brief   The macro is used by the trace framework Library to allocate the memory
 *
 * <b> Prototype: </b>
 *  The following is the C prototype for the expected OSAL API.
 *
 *  @verbatim
 *       void* Osal_contract_MemAlloc (uint32_t num_bytes, uint32_t alignment)
 *  @endverbatim
 *
 *  <b> Parameter </b>
 *  @n num_bytes      - number of bytes to be allocated
 *  @n alignment      - memory alignment
 *
 *  <b> Return Value </b>
 *  @n  memory address allocated
 */

#define  TF_CONTRACT_osalMemAlloc   Osal_contract_MemAlloc


/**
 * @brief   The macro is used by the trace framework Library to free the memory
 *
 * <b> Prototype: </b>
 *  The following is the C prototype for the expected OSAL API.
 *
 *  @verbatim
 *       void  Osal_contract_MemFree (void* ptr, uint32_t size)
 *  @endverbatim
 *
 *  <b> Parameter </b>
 *  @n ptr      - memory base address to be freed
 *  @n size     - size of the mem block
 *
 *  <b> Return Value </b>
 *  @n  None
 */

#define  TF_CONTRACT_osalMemFree   Osal_contract_MemFree


/**
 * @brief   The macro is used by the producer/consumer Library to indicate that memory
 * access has been accessed & updated . If the values are in cached memory the 
 * implementation should invalidate the contents of the cache
 *
 * <b> Prototype: </b>
 *  The following is the C prototype for the expected OSAL API.
 *
 *  @verbatim
 *       void Osal_contract_BeginMemAccess (void* addr, uint32_t sizeWords)
 *  @endverbatim
 *
 *  <b> Parameter </b>
 *  @n  addr        - Pointer to the memory address being accessed.
 *  @n  size        - Size of the packet.
 *
 *  <b> Return Value </b>
 *  @n  None
 */

#define TF_CONTRACT_osalBeginMemAccess   Osal_contract_BeginMemAccess


/**
 * @brief   The macro is used by the producer/consumer Library to indicate that memory
 * access has been completed. If the values are in cached memory the 
 * implementation should writeback the contents of the contents
 *
 * <b> Prototype: </b>
 *  The following is the C prototype for the expected OSAL API.
 *
 *  @verbatim
 *       void Osal_contract_EndMemAccess (void* addr, uint32_t sizeWords)
 *  @endverbatim
 *
 *  <b> Parameter </b>
 *  @n  addr        - Pointer to the memory address being accessed.
 *  @n  size        - Size of the packet.
 *
 *  <b> Return Value </b>
 *  @n  None
 */

#define TF_CONTRACT_osalEndMemAccess   Osal_contract_EndMemAccess


/**
 * @brief   The macro is used by the contract to indicate that memory
 * access needs to be protected
 *
 * <b> Prototype: </b>
 *  The following is the C prototype for the expected OSAL API.
 *
 *  @verbatim
 *       void Osal_contract_Enter (void)
 *  @endverbatim
 *
 *  <b> Parameter </b>
 *  @n  None
 *
 *  <b> Return Value </b>
 *  @n  None
 */

#define TF_CONTRACT_osalEnter   Osal_contract_Enter

/**
 * @brief   The macro is used by the contract to indicate that memory
 * access needs to be protected
 *
 * <b> Prototype: </b>
 *  The following is the C prototype for the expected OSAL API.
 *
 *  @verbatim
 *       void Osal_contract_Exit (void)
 *  @endverbatim
 *
 *  <b> Parameter </b>
 *  @n  None
 *
 *  <b> Return Value </b>
 *  @n  None
 */

#define TF_CONTRACT_osalExit   Osal_contract_Exit

/**
 * @brief   The macro is used by the trace framework Library to get the virtual address
 *          from Physical address (Applicable for ARM user mode library only)
 *
 * <b> Prototype: </b>
 *  The following is the C prototype for the expected OSAL API.
 *
 *  @verbatim
 *       void* Osal_contract_mmap (void* phy_addr, uint32_t size)
 *  @endverbatim
 *
 *  <b> Parameter </b>
 *  @n  addr        - Pointer to the physical memory address.
 *
 *  <b> Return Value </b>
 *  @n  virtual address space
 */
#define TF_CONTRACT_osalMmap   Osal_contract_mmap

/**
 * @brief   The macro is used by the trace framework Library to get the physical address
 *          from virtual address (Applicable for ARM user mode library only)
 *
 * <b> Prototype: </b>
 *  The following is the C prototype for the expected OSAL API.
 *
 *  @verbatim
 *       void Osal_contract_unmap (void* virt_addr, uint32_t size)
 *
 *  @endverbatim
 *
 *  <b> Parameter </b>
 *  @n  addr        - Pointer to the virtual memory address.
 *  @n  size        - Size to be un mapped
 *
 *  <b> Return Value </b>
 *  @n  none
 */
#define TF_CONTRACT_osalUnmap   Osal_contract_unmap

/**
@}
*/

#ifdef __cplusplus
}
#endif

#endif /* __CONTRACTOSAL_H__ */
