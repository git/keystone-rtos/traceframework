/**
 *   @file  producer.h
 *
 *   @brief   
 *      This is a very slim ring producer framework library implementation.
 *
 *  ============================================================================
 *  @n   (C) Copyright 2012, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef __GEN_PRODUCER_H__
#define __GEN_PRODUCER_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "ti/instrumentation/traceframework/trace_contract.h"

/** @addtogroup TRACEFRAMEWORK_ENUM
@{
*/

/**
 * @brief Producer Exception enumerations 
*/
typedef enum prodException
{
  TF_PRODUCER_EXCEPTION_NOTIFY_FAIL = 0,
  /**< Producer failed to write to the notify register address with the register mask  */   

  TF_PRODUCER_EXCEPTION_INVALID_NOTIFY  
  /**< Invalid notify option is passed  */     
} tf_prodException_e;

/**
 * @brief Producer Status enumerations 
*/
typedef enum prodStatus 
{
  TF_PRODUCER_NO_AVAILABLE_CONTRACT = 0,
  /**< There is no contract available for the producer  */ 
  
  TF_PRODUCER_INVALID_CONTRACT_HANDLE,
  /**< Invalid Contract handle is noticed in the producer */   

  TF_PRODUCER_INVALID_PRODID,
  /**< Invalid producer Id is noticed  */   

  TF_PRODUCER_PROD_SUCCESS,
  /**< Producer ended up with a successful operation */   

  TF_PRODUCER_MAX_SUPPORTED_CONSUMERS_REACHED,
  /**< Producer reached maximum number of consumers */   

  TF_PRODUCER_PROD_FAIL,
  /**< Producer ended up with an unsuccessful operation */   

  TF_PRODUCER_INVALID_HANDLE
  /**< Invalid producer handle */   
  
} tf_prodStatus_e;

/**
 * @brief   Producer types supported in trace framework
 */
typedef enum {
  TF_PRODUCER_TYPE_UIA     = 0x1<<0,
  /**< used for creating UIA producer ID */ 
  TF_PRODUCER_TYPE_GEN     = 0x1<<1,
  /**< used for creating General producer ID */  
  TF_PRODUCER_TYPE_UIA_MINST = 0x1<<2,
  /**< used for creating Multi-instance UIA producer ID */
  TF_PRODUCER_TYPE_CUIA     = 0x1<<3
  /**< used for creating CUIA producer ID */   
} tf_producerTypes_e;

/**
@}
*/

/** @addtogroup TRACEFRAMEWORK_DATASTRUCT
@{
*/


/**
 * @brief   Producer handle
 */
typedef void*  tf_producer_HANDLE;

/**
 * @brief   Producer State in Contract
 */
typedef enum {
  TF_PRODUCER_STATE_INVALID	= -1,
  /**< Producer state is invalid  */ 

  TF_PRODUCER_STATE_STREAMING     = 0,
  /**< Producer is streaming the ring buffer information to the consumer */ 
  
  TF_PRODUCER_STATE_FREE_RUNNING,
  /**< Producer is in free run state */ 

  TF_REQUEST_PRODUCER_STATE_FREEZING,  
  /**< Producer got the request from consumer to freeze */	

  TF_PRODUCER_STATE_FROZEN
  /**< Producer is in freeze state */    
  
} tf_producerState_e;

/**
 * @brief   Producer Response States
 */
typedef enum {

  TF_PRODUCER_RESPONSE_STATE_ACK_FREEZE_REQ = TF_PRODUCER_STATE_FROZEN
  /**< Producer acknowledged consumers that it froze for the freeze request */  
} tf_producerResponseState_e;

/**
 * @brief   Producer Request States
 */
typedef enum {
  TF_PRODUCER_STATE_PROCESS_FREEZE_REQ = TF_REQUEST_PRODUCER_STATE_FREEZING
  /**< Producer is processing freeze request */  	
} tf_producerRequestState_e;


/**
 * @brief Structure with producer config 
*/
typedef struct  prodConfig
{
  char          name[128];
  /**< name of the contract involving the producer */

  tf_contract_HANDLE   contract_handle;  
  /**< contract handle to be associated with this producer ring */ 

  uint8_t       oob_info[128];
  /**< Out of Band information from the System */

  tf_producerTypes_e  prodType;
  /**< producer type to be created */

  uint32_t  moduleId;
  /**< producer unique moduleId from system */  

  void*     obj_handle;
  /** object handle */
#ifdef TF_LINUX_USERSPACE  
   uint32_t  crcApp16;
   /**< CRC16 of the application, should be unique per application, supported only for ARM side producers  */  
#else
   void		   (*xchg_cb_fxn)(tf_producer_HANDLE phandle);
   /**< buffer exchange call back function for application to trigger
		   the notifications to the registered consumers by calling tf_prodNotifyConsumers() function (supported only for DSP side producers) */
#endif

} tf_prodConfig_t;

/**
@}
*/

/** @addtogroup TRACEFRAMEWORK_FUNCTIONS
@{
*/
/**
 * ============================================================================
 *  @n@b tf_prodNotifyConsumers
 *
 *  @b  brief
 *  @n  Each time a buffer exchange happens for that producer instance, the notifications needs to be 
 *        posted to all registered consumers for that producer.
 *        This function identifies if there are any buffers in the ring buffer to be notified to the registered
 *        consumers of that producer. 
 *        Few cases where this function can be called is in a background task  
 *        or can be hooked to the buffer exchange done function for the producer where performance of the
 *        buffer exchange is not so critical 
 *
 *  @param[in]  pHandle
 *      Pointer to the producer handle
 *
 *  @return
 *      void
 *
 * =============================================================================
 */
extern void tf_prodNotifyConsumers(tf_producer_HANDLE pHandle);

/**
 * ============================================================================
 *  @n@b tf_prodUpdateConsumers
 *
 *  @b  brief
 *  @n  The local producer instance would need to be synced to the footprints from the contract
 *        This function can be called to syncrhonize the local producer instance to contrat.
 *        @n Typical example of use: can be called periodically from a task/isr
 *
 *  @param[in]  p_handle
 *      Pointer to the producer handle
 *
 *  @return
 *      number of consumers in the producer contract
 *
 * =============================================================================
 */
extern int32_t          tf_prodUpdateConsumers (tf_producer_HANDLE p_handle);

/**
 * ============================================================================
 *  @n@b tf_prodDestroy
 *
 *  @b  brief
 *  @n  frees up the producer instance created 
 *
 *  @param[in]  pHandle
 *      Pointer to the producer instance
 *
 *  @return
 *      Status of the operation
 *
 * =============================================================================
 */
extern tf_prodStatus_e  tf_prodDestroy(tf_producer_HANDLE pHandle);

/**
 * ============================================================================
 *  @n@b tf_prodCreate
 *
 *  @b  brief
 *  @n  Creates the local producer instance for a given contract
 *
 *  @param[in]  config
 *      pointer to producer configuration information structure
 *
 *  @return
 *      Handle to the producer 
 *
 * =============================================================================
 */

extern tf_producer_HANDLE  tf_prodCreate(  tf_prodConfig_t   *config);

/**
 * ============================================================================
 *  @n@b tf_prodGetCurBuf
 *
 *  @b  brief
 *  @n  current ring buffer which producer is intended to write to.
 *
 *  @param[in]  pHandle
 *      Producer Handle 
 *
 *
 *  @return
 *      current buffer in use
 *
 * =============================================================================
 */

void* tf_prodGetCurBuf(tf_producer_HANDLE pHandle);

/**
 * ============================================================================
 *  @n@b tf_prodBufExchange
 *
 *  @b  brief
 *  @n  Implements the buffer exchange function interface for the LoggerStreamer  
 *
 *  @param[in]  pHandle
 *      Producer Handle 
 *
 *  @param[in]  full
 *      filled log buffer 
 *
 *  @return
 *      next free log buffer 
 *
 * =============================================================================
 */
extern Ptr tf_prodBufExchange(tf_producer_HANDLE pHandle, uint8_t *full);

/**
 * ============================================================================
 *  @n@b tf_prodSetOOBData
 *
 *  @b  brief
 *  @n  sets the out of band (OOB) data from the application in the contract memory 
 *
 *  @param[in]  cHandle
 *      contract handle  
 *
 *  @param[in]  data
 *      out of band (OOB) data  to be filled in the contrat memory
 *
 *  @return
 *      void 
 *
 * =============================================================================
 */

extern void tf_prodSetOOBData(tf_contract_HANDLE cHandle, uint8_t* data);

/**
 * ============================================================================
 *  @n@b tf_prodGetOOBData
 *
 *  @b  brief
 *  @n  Gets the out of band (OOB) data in the contract memory to the application
 *
 *  @param[in]  cHandle
 *      contract handle  
 *
 *  @param[in]  data
 *      out of band (OOB) data  read from the contrat memory
 *
 *  @return
 *      void 
 *
 * =============================================================================
 */

extern void tf_prodGetOOBData(tf_contract_HANDLE cHandle, uint8_t* data);

/**
 * ============================================================================
 *  @n@b tf_prodGetContractHandle
 *
 *  @b  brief
 *  @n  API to get the contract handle that is associated with the producer
 *
 *  @param[in]  pHandle
 *      producer handle
 *
 *  @param[in]  contract_handle
 *      Address of the contract handle to store
 *  @return
 *      Status of the operation
 *
 * =============================================================================
 */
extern tf_prodStatus_e tf_prodGetContractHandle(tf_producer_HANDLE pHandle, tf_contract_HANDLE *contract_handle);

/**
@}
*/

/* Below definitions are added for backwards compatibility */
#define	GEN_NO_AVAILABLE_CONTRACT            TF_PRODUCER_NO_AVAILABLE_CONTRACT	
#define	GEN_INVALID_CONTRACTHANDLE           TF_PRODUCER_INVALID_CONTRACT_HANDLE
#define	GEN_INVALID_PRODID                   TF_PRODUCER_INVALID_PRODID
#define	GEN_PROD_SUCCESS                     TF_PRODUCER_PROD_SUCCESS
#define	GEN_MAX_SUPPORTED_CONSUMERS_REACHED  TF_PRODUCER_MAX_SUPPORTED_CONSUMERS_REACHED
#define	GEN_PROD_FAIL                        TF_PRODUCER_PROD_FAIL
#define	UIA_PROD_MAGIC_ID                    TF_PRODUCER_TYPE_UIA	
#define	GEN_PROD_MAGIC_ID                    TF_PRODUCER_TYPE_GEN
#define	UIA_MINST_PROD_MAGIC_ID              TF_PRODUCER_TYPE_UIA_MINST	
#define	CUIA_PROD_MAGIC_ID                   TF_PRODUCER_TYPE_CUIA

#ifdef __cplusplus
}
#endif

#endif /* __GEN_PRODUCER_H__ */
/* Nothing past this point */

